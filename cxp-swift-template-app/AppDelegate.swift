//
//  AppDelegate.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 15/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.makeKeyAndVisible()
        
        setup()
        
        
        return true
    }
    
    
    
    func setup()
    {
        do{
            //FIXME: In release, set forceDecryption to true.
            try CXP.initialize(Config.configFilePath, forceDecryption:false)
            
            createLoadingViewController()
            
            PluginsManager.registerAvailablePlugins()
            
            ScreensHelper.registerNativeRenderers()
            
            loadCXPModel()
            
            NavigationEventsHandler.instance.setup()
            
            blockTheAppIfDeviceIsJailbroken()
            
        }catch {
            BBUtils.displayAlert(withTitle: NSLocalizedString("Something wrong!", comment: "Configurion file can not be loaded"), andMessage: NSLocalizedString("Something is wrong with the project configuration file", comment: ""))
        }
    }
    
    //MARK: Model Loading
    func loadCXPModel()
    {
        ModelManager.instance.setup()
        ModelManager.instance.loadModel { (error) in
            if error != nil{
                UIAlertController.displayAlert(title: NSLocalizedString("Something wrong!", comment: "Title of the error message shown when portal model loading fails"), message: NSLocalizedString("Could not load the application details, please retry again!", comment: "The error message shown when portal model loading fails"), confirmTitle: NSLocalizedString("Retry", comment: ""), confirmAction: {
                    self.loadCXPModel()
                })
            }
        }
    }
    
    
    
   
    
    func createLoadingViewController()
    {
        let loadingViewController = LoadingViewController(nibName: NSStringFromClass(LoadingViewController.self), bundle: Bundle.main)
        window?.rootViewController = loadingViewController
    }
    
    
    
    //MARK: Security
    func blockTheAppIfDeviceIsJailbroken()
    {
        // Check if the device is jailbroken, deny app usage if this is the case
        if CXP.isDeviceJailbroken(){
            BBUtils.displayAlert(withTitle: NSLocalizedString("DeviceJailbroken", comment: "Device is jailbroken"), andMessage: NSLocalizedString("JailbrokenDeviceBlocked", comment: "A nice message that app cannot run on jailbroken devices"))
        }
        
    }
    
   
}

