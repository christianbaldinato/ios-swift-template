//
//  NSBundle+I18nAdditions.h
//  Bawag Mobile
//
//  Created by Bernardo Oliveira on 29/06/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (I18nAdditions)
+ (void)setLanguage:(NSString*)language;
@end
