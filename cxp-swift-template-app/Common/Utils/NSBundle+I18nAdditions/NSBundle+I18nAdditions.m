//
//  NSBundle+I18nAdditions.m
//  Bawag Mobile
//
//  Created by Bernardo Oliveira on 29/06/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "NSBundle+I18nAdditions.h"
#import <objc/runtime.h>

static const char _bundle = 0;

@interface BundleExtension : NSBundle
@end

@implementation BundleExtension
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName {
    NSBundle *bundle = objc_getAssociatedObject(self, &_bundle);
    return bundle ? [bundle localizedStringForKey:key value:value table:tableName]
                  : [super localizedStringForKey:key value:value table:tableName];
}
@end

@implementation NSBundle (I18nAdditions)
+ (void)setLanguage:(NSString *)language {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object_setClass([NSBundle mainBundle], [BundleExtension class]);
    });

    objc_setAssociatedObject(
        [NSBundle mainBundle], &_bundle,
        language ? [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]] : nil,
        OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
