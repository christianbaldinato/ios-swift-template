//
//  UIAlertController_Extention.swift
//  cxp-swift-template-app
//
//  Created by Ahmed Ali on 04/10/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation

extension UIAlertController
{
    
    
    class func displayAlert(
        title: String,
        message: String,
        confirmTitle: String?,
        confirmAction : (() -> Void)?
        )
    {
        displayAlert(title: title, message: message, confirmTitle: confirmTitle, cancelTitle: nil, confirmAction: confirmAction, cancelAction: nil)
    }
    
    class func displayAlert(
        title: String,
        message: String,
        confirmTitle: String?,
        cancelTitle : String?,
        confirmAction : (() -> Void)?,
        cancelAction: (() -> Void)? )
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if confirmTitle != nil{
            let action = UIAlertAction(title: confirmTitle, style: .default, handler: { (alertAction) in
                confirmAction?()
            })
            
            alert.addAction(action)
        }
        
        if cancelTitle != nil{
            let action = UIAlertAction(title: cancelTitle, style: .cancel, handler: { (alertAction) in
                cancelAction?()
            })
            
            alert.addAction(action)
        }
        
        alert.show(true)
    }
}
