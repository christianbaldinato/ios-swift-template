//
//  TPKeyboardAvoidingScrollView.m
//  TPKeyboardAvoiding
//
//  Created by Michael Tyson on 30/09/2013.
//  Copyright 2015 A Tasty Pixel. All rights reserved.
//

#import "TPKeyboardAvoidingScrollView.h"
#import <QuartzCore/QuartzCore.h>

@interface TPKeyboardAvoidingScrollView () <UITextFieldDelegate, UITextViewDelegate>
@end

// Uncomment if using fading
// static float const fadePercentage = .1;

@implementation TPKeyboardAvoidingScrollView

#pragma mark - Setup/Teardown

- (void)setup {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(TPKeyboardAvoiding_keyboardWillShow:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(TPKeyboardAvoiding_keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollToActiveTextField)
                                                 name:UITextViewTextDidBeginEditingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollToActiveTextField)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:nil];
}

- (id)initWithFrame:(CGRect)frame {
    if (!(self = [super initWithFrame:frame]))
        return nil;
    [self setup];
    return self;
}

- (void)awakeFromNib {
	[super awakeFromNib];
    [self setup];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self TPKeyboardAvoiding_updateContentInset];
}

- (void)setContentSize:(CGSize)contentSize {
    [super setContentSize:contentSize];
    [self TPKeyboardAvoiding_updateFromContentSizeChange];
}

- (void)contentSizeToFit {
    self.contentSize = [self TPKeyboardAvoiding_calculatedContentSizeFromSubviewFrames];
}

- (BOOL)focusNextTextField {
    return [self TPKeyboardAvoiding_focusNextTextField];
}
- (void)scrollToActiveTextField {
    return [self TPKeyboardAvoiding_scrollToActiveTextField];
}

#pragma mark - Responders, events

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    if (!newSuperview) {
        [NSObject
            cancelPreviousPerformRequestsWithTarget:self
                                           selector:@selector(TPKeyboardAvoiding_assignTextDelegateForViewsBeneathView:)
                                             object:self];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self TPKeyboardAvoiding_findFirstResponderBeneathView:self] resignFirstResponder];
    [super touchesEnded:touches withEvent:event];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (![self focusNextTextField]) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [NSObject
        cancelPreviousPerformRequestsWithTarget:self
                                       selector:@selector(TPKeyboardAvoiding_assignTextDelegateForViewsBeneathView:)
                                         object:self];
    [self performSelector:@selector(TPKeyboardAvoiding_assignTextDelegateForViewsBeneathView:)
               withObject:self
               afterDelay:0.1];

	/* To add fading, uncomment this code
    NSObject *transparent = (NSObject *)[[UIColor colorWithWhite:0 alpha:0] CGColor];
    NSObject *opaque = (NSObject *)[[UIColor colorWithWhite:0 alpha:1] CGColor];

    CALayer *maskLayer = [CALayer layer];
    maskLayer.frame = self.bounds;

    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(self.bounds.origin.x, 60, self.bounds.size.width, self.bounds.size.height - 80);

    gradientLayer.colors = [NSArray arrayWithObjects:transparent, opaque, opaque, transparent, nil];

    // Set percentage of scrollview that fades at top & bottom
    gradientLayer.locations =
        [NSArray arrayWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:fadePercentage],
                                  [NSNumber numberWithFloat:1.0 - fadePercentage], [NSNumber numberWithFloat:1], nil];

    [maskLayer addSublayer:gradientLayer];
    self.layer.mask = maskLayer;
	 */
}

@end
