//
//  BBUtils.h
//  app-template
//
//  Created by Bernardo Oliveira on 24/02/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "BBUtils.h"

#import "UIAlertController+Window.h"
#import "SVProgressHUD.h"

static UIAlertController *alert;

@implementation BBUtils

+ (void)displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    // Displays the alert in the UI thread
    dispatch_async(dispatch_get_main_queue(), ^{

        alert = [UIAlertController alertControllerWithTitle:title
                                                    message:message
                                             preferredStyle:UIAlertControllerStyleAlert];

        [alert show];
    });
}

+ (void)displayAlertWithTitle:(NSString *)title
                      message:(NSString *)message
                      buttons:(NSArray *)buttons
                  cancelIndex:(NSNumber *)cancelIndex {
    // Displays the alert in the UI thread
    dispatch_async(dispatch_get_main_queue(), ^{

        alert = [UIAlertController alertControllerWithTitle:title
                                                    message:message
                                             preferredStyle:UIAlertControllerStyleAlert];
        NSUInteger index = 0;
        for (NSString *button in buttons) {
            UIAlertAction *defaultAction = [UIAlertAction
                actionWithTitle:button
                          style:(!cancelIndex || index != [cancelIndex integerValue] ? UIAlertActionStyleDefault
                                                                                     : UIAlertActionStyleCancel)
                        handler:nil];

            [alert addAction:defaultAction];
            index++;
        }

        [alert show];
    });
}

+ (void)dismissAlert {
    // Displays the alert in the UI thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [alert dismiss];
    });
}

+ (void)showNetworkActivityIndicator {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

+ (void)hideNetworkActivityIndicator {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

+ (void)hideLoadingIndicator {
	[SVProgressHUD dismiss];
}

+ (void)showLoadingIndicator {
	[SVProgressHUD show];
}

+ (void)presentStatusViewWithMessage:(NSString *)message {
    // Show status view
	//UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
	//[[[BBStatusView alloc] initWithMessage:message] presentInView:window];
	
	[self showLoadingIndicator];
}

+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);

    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    // Draw your image
    [image drawInRect:rect];

    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();

    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();

    return image;
}

+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color {
    // begin a new image context, to draw our colored image onto with the right scale
    UIGraphicsBeginImageContextWithOptions(source.size, NO, [UIScreen mainScreen].scale);

    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();

    // set the fill color
    [color setFill];

    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, source.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);

    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, source.size.width, source.size.height);
    CGContextDrawImage(context, rect, source.CGImage);

    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context, kCGPathFill);

    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // return the color-burned image
    return coloredImg;
}

+ (UIViewController *)topViewController {
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

// Finds the currently visible view controller
+ (UIViewController *)topViewController:(UIViewController *)rootViewController {
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController;
        return [self topViewController:[navigationController.viewControllers lastObject]];
    }
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabController = (UITabBarController *)rootViewController;
        return [self topViewController:tabController.selectedViewController];
    }
    if (rootViewController.presentedViewController) {
        return [self topViewController:rootViewController.presentedViewController];
    }
    return rootViewController;
}

+ (NSString *)formattedCurrencyFromString:(NSString *)string {
    NSDecimalNumber *price =
        [NSDecimalNumber decimalNumberWithString:[string substringWithRange:NSMakeRange(0, string.length)]];
    NSLocale *priceLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_UK"];

    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setLocale:priceLocale];
    [currencyFormatter setCurrencySymbol:@""];
    //    NSString *currencyString = [currencyFormatter internationalCurrencySymbol]; // EUR, GBP, USD...
    NSString *format = [currencyFormatter positiveFormat];
    //    format = [format stringByReplacingOccurrencesOfString:@"¤" withString:nil];
    // ¤ is a placeholder for the currency symbol
    [currencyFormatter setPositiveFormat:format];

    NSString *formattedCurrency = [NSString stringWithFormat:@"€ %@", [currencyFormatter stringFromNumber:price]];
    return formattedCurrency;
}
@end
