//
//  StyleHelper.swift
//  cxp-swift-template-app
//
//  Created by Ahmed Ali on 03/10/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import UIKit

class StyleHelper: NSObject {
    static func applyIcons(_ icons: [IconPack], forTabBarItem tabBarItem: UITabBarItem)
    {
        let defaultImage = UIImage(named:"other_icon")?.withRenderingMode(.alwaysTemplate)
        if icons.count > 0{
            let icon = icons.first!
            tabBarItem.image = icon.normal() != nil ? icon.normal() : defaultImage
            tabBarItem.selectedImage = icon.active() != nil ? icon.active() : defaultImage
        }else{
            tabBarItem.image = defaultImage
        }
        
        
        // Set icon (if available) using the icon pack (icon preference)
      
    }
    
    static func applyStyle(forNavigationController navController: UINavigationController)
    {
        navController.navigationBar.tintColor = Config.navigationBarTintColor
        navController.navigationBar.barTintColor = Config.navigationbarColor
        
        navController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Config.navigationBarTextColor]
        
        navController.navigationBar.barStyle = .black;
        navController.navigationBar.isTranslucent = false;
    }
}
