//
//  NavigationFactory.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 19/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation
enum ScreenType {
    case singleScreen, multiScreens
}

class ScreensHelper
{
    //MARK: View Controllers
    static func viewController(screenType: ScreenType, sitemape: [SiteMapItemChild]) -> UIViewController?
    {
        
        switch screenType {
        case .singleScreen:
            return singleScreenViewController(forSitemap: sitemape)
        default:
            return multiScreenViewController(forSitemap: sitemape)
        }
    }
      
    static func singleScreenViewController(forSitemap sitemap: [SiteMapItemChild]) -> UIViewController?
    {
        let sitemapObject = sitemap.first!
        let renderable = ModelManager.instance.model!.item(byId: sitemapObject.itemRef())!
        if sitemapObject.children().count > 0{
            let navController =  navigationController(forRenderable:renderable)
            navController.tabBarItem.title = renderable.preference(forKey: "title")
            let icons = renderable.itemIcons() as! [IconPack]
            
            StyleHelper.applyIcons(icons, forTabBarItem:navController.tabBarItem)
            return navController
        }
        
        return viewController(forRenderable: renderable)
    }
    
    
    
    static func multiScreenViewController(forSitemap sitemap: [SiteMapItemChild]) -> UIViewController?
    {
        let tabBar = UITabBarController()
        var viewControllers = [UIViewController]()
        for sitemapObject in sitemap{
            let renderable = ModelManager.instance.model!.item(byId: sitemapObject.itemRef())!
            let vc = navigationController(forRenderable: renderable)
            viewControllers.append(vc)
            vc.tabBarItem.title = renderable.preference(forKey: "title")
            
            let icons = renderable.itemIcons() as! [IconPack]
            
            StyleHelper.applyIcons(icons, forTabBarItem:vc.tabBarItem)
        }
        
        tabBar.viewControllers = viewControllers
        return tabBar
    }
    
    
    
    static func navigationController(forRenderable renderable: Renderable) -> UINavigationController
    {
        let navigationController = UINavigationController()
        navigationController.viewControllers = [viewController(forRenderable: renderable)]
        StyleHelper.applyStyle(forNavigationController: navigationController)
        return navigationController
    }
    
    
    static func viewController(forRenderable renderable: Renderable) -> UIViewController
    {
        var viewController: UIViewController!
        if (renderable.preference(forKey: "title") as String) == "Settings"{
            viewController = SettingsViewController(page: renderable)
        }else{
            viewController = WidgetViewController(page: renderable)
        }
        
        return viewController
    }
    
    //MARK: Renderers
    static func registerNativeRenderers()
    {
        do{
            try CXPRendererFactory.registerRenderer(LoginWidget.self)
        } catch{
            //FIXME: Think how to handle such error
            CXP.logError(self, message: "Unable to register renderer \(LoginWidget.self) due to error \(error.localizedDescription)")
            
        }
        
    }
    
}
