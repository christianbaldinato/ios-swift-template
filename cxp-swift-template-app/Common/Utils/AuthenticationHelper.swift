//
//  CustomSDK.swift
//  RB
//
//  Created by Armin Mehinovic on 25/07/16.
//  Copyright © 2016 Backbase. All rights reserved.
//

import Foundation
import BackbaseCXP

class AuthenticationHelper {
    
    static var sessionId: String?
    
    class func startSession(login: String, password: String, delegate: BackbaseCXP.LoginDelegate) {
        let hasSessionId = sessionId != nil
        var serverRequest : String!
        
        if hasSessionId{
            serverRequest = "/j_spring_security_check?rd=" + String(format: "%d", NSDate().timeIntervalSince1970)
        }else{
            serverRequest = "/services/rest/v1/authentication/session/initiate"
        }
      
        guard let url = NSURL(string: CXP.configuration().portal.serverURL + serverRequest) as NSURL? else {
            
            handleError(error: NSError(domain: "Could not create request URL", code: -1, userInfo: nil), delegate: delegate)
            return
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let paramPrefix = hasSessionId ? "j_" : ""
        let queryString = String(format: "%@username=%@&%@password=%@", paramPrefix, login, paramPrefix, (hasSessionId ? sessionId! : password))

        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("JWT_COOKIE", forHTTPHeaderField: "Req-X-Auth-Token")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.httpBody = queryString.data(using:String.Encoding.utf8)

        let session = URLSession.shared
        

        let completionHandler = {(_ data : Data?, _ response: URLResponse?, _ connectionError: Error?) -> Void in
        
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if connectionError != nil {
                self.handleError(error: connectionError! as NSError, delegate: delegate)
            } else if (response as! HTTPURLResponse).statusCode >= 400 {
                handleError(error: NSError(domain: "HTTP error", code: (response as! HTTPURLResponse).statusCode, userInfo: nil), delegate: delegate)
            } else {
                let httpResponse = response as! HTTPURLResponse
                let cookies: [HTTPCookie]?
                if let responseHeaders = httpResponse.allHeaderFields as? [String:String] {
                    cookies = HTTPCookie.cookies(withResponseHeaderFields: responseHeaders, for:url as URL)
                    for cookie in cookies! {
                        addSessionCookie(JWTCookie: cookie)
                    }
                }
                
                // If there is no session Id, save it and do the call again
                if hasSessionId {
                    sessionId = nil
                    handleSuccess(statusCode: httpResponse.statusCode, delegate: delegate)
                } else {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
                        if let sId = (json.object(forKey: "session") as! NSDictionary).object(forKey: "id") as! String? {
                            sessionId = sId
                            startSession(login: login, password: password, delegate: delegate)
                            return
                        }
                    } catch let error as NSError {
                        handleError(error: error, delegate: delegate)
                    }
                }
            }
        
        }
        
        
        let task = session.dataTask(with: (request as URLRequest), completionHandler: completionHandler)
        
        task.resume()
    }
    
    class func addSessionCookie(JWTCookie: HTTPCookie) {
        let cookieJar = HTTPCookieStorage.shared
        cookieJar.setCookie(JWTCookie)
    }
    
    class func handleSuccess(statusCode: NSInteger, delegate: BackbaseCXP.LoginDelegate) {
      
        DispatchQueue.main.async {
            delegate.loginDidSucceed(withCode: statusCode)
        }
    }
    
    class func handleError(error: NSError, delegate: BackbaseCXP.LoginDelegate) {
        DispatchQueue.main.async {
            delegate.loginDidFailWithError(error)
        }
    }
    
    class func logout(completionHandler: () -> Void)
    {
        CXP.endSession()
        CXP.clearSession()
        CXP.invalidateModel()
        completionHandler()
    }
}
