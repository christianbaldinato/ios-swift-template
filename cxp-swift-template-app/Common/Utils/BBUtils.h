//
//  BBUtils.h
//  app-template
//
//  Created by Bernardo Oliveira on 24/02/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BBUtils : NSObject
// Shows an alert view with title and message
+ (void)displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
// Shows an alert view with title, message and buttons
+ (void)displayAlertWithTitle:(NSString *)title
                      message:(NSString *)message
                      buttons:(NSArray *)buttons
                  cancelIndex:(NSNumber *)cancelIndex;
// Dismisses the alert view
+ (void)dismissAlert;
// Shows the network activity indicator
+ (void)showNetworkActivityIndicator;
// Hides the network activity indicator
+ (void)hideNetworkActivityIndicator;
// Shows the activity indicator
+ (void)showLoadingIndicator;
// Hides the activity indicator
+ (void)hideLoadingIndicator;
// Returns the UIViewContoller currently being shown on the screen
+ (UIViewController *)topViewController;
+ (void)presentStatusViewWithMessage:(NSString *)message;
// Returns an UIImage with a flat color
+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius;
// Returns the source UIImage with a color mask
+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color;
// Returns a currency formatted string
+ (NSString *)formattedCurrencyFromString:(NSString *)string;
@end
