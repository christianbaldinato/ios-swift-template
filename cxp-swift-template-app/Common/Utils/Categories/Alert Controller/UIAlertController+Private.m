//
//  UIAlertController+Private.m
//  CXPMobileTemplate
//
//  Created by Bernardo Oliveira on 24/02/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "UIAlertController+Private.h"
#import "UIAlertController+Window.h"

@implementation UIAlertController (Private)

@dynamic alertWindow;

#pragma mark - Custom Accessors

- (void)setAlertWindow:(UIWindow *)alertWindow {
    objc_setAssociatedObject(self, @selector(alertWindow), alertWindow, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIWindow *)alertWindow {
    return objc_getAssociatedObject(self, @selector(alertWindow));
}

@end