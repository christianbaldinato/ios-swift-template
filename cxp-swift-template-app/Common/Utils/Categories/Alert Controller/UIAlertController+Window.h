//
//  UIAlertController+Window.h
//  CXPMobileTemplate
//
//  Created by Bernardo Oliveira on 24/02/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "UIAlertController+Private.h"
#import <objc/runtime.h>

@interface UIAlertController (Window)

- (void)show;
- (void)show:(BOOL)animated;
- (void)dismiss;

@end