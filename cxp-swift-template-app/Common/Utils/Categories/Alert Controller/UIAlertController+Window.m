//
//  UIAlertController+Window.m
//  CXPMobileTemplate
//
//  Created by Bernardo Oliveira on 24/02/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "UIAlertController+Window.h"

@implementation UIAlertController (Window)

#pragma mark - Lifecycle Methods

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    // precaution to insure window gets destroyed
    self.alertWindow.hidden = YES;
    self.alertWindow = nil;
}

#pragma mark - Public Methods

- (void)show {
    [self show:YES];
}

- (void)show:(BOOL)animated {
    self.alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.alertWindow.rootViewController = [[UIViewController alloc] init];
    self.alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [self.alertWindow makeKeyAndVisible];
    [self.alertWindow.rootViewController presentViewController:self animated:animated completion:nil];
}

- (void)dismiss {
    [self.alertWindow.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

@end