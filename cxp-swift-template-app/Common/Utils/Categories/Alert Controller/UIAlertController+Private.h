//
//  UIAlertController+Private.h
//  CXPMobileTemplate
//
//  Created by Bernardo Oliveira on 24/02/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Private)

@property (nonatomic, strong) UIWindow *alertWindow;

@end
