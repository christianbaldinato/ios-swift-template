//
//  LoginView.swift
//  cxp-swift-template-app
//
//  Created by Ahmed Ali on 03/10/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import UIKit

protocol LoginViewDelegate : class {
    func loginView(_ loginView: LoginView, didFireLoginActionWithUsername username: String, andPassword password: String)
}

@objc(LoginView)
class LoginView: UIView, NativeView, SwipeViewDelegate, SwipeViewDataSource {

    
    @IBOutlet weak var swipeView: SwipeView!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    private let images = [UIImage(named: "Scan Code App")!, UIImage(named: "Register App")!, UIImage(named: "Sign In")!]
    
    var delegate : LoginViewDelegate!
    
    //MARK: Actions
    @IBAction func loginAction(_ sender: UIButton)
    {
        delegate.loginView(self, didFireLoginActionWithUsername: usernameField.text!, andPassword: passwordField.text!)
    }
    
    //MAKR: NativeView
    static func initialize(with contractImpl: NativeContract!, container: UIView!) -> UIView
    {
        
        let view = LoginView()
        
        view.delegate =  contractImpl as! LoginViewDelegate
        var frame = container.bounds
        //Start just below the status bar
        frame.origin.y += UIApplication.shared.statusBarFrame.size.height
        view.frame = frame
        view.setupFromNib()
        container.addSubview(view)
        
        return view
    }

    
    
    //MARK: Private
    private func setupFromNib()
    {
        let nibFileName = NSStringFromClass(LoginView.self)
        let view = Bundle.main.loadNibNamed(nibFileName, owner: self, options: nil)?.first! as! LoginView
        view.frame = bounds
        addSubview(view)
        
    }
    
    //MARK: SwipeViewDataSource
    func numberOfItems(in swipeView: SwipeView!) -> Int {
        return 3
    }
    
    func swipeView(_ swipeView: SwipeView!, viewForItemAt index: Int, reusing view: UIView!) -> UIView! {
        var imageView = view as? UIImageView
        
        if imageView == nil{
            imageView = UIImageView(frame: swipeView.bounds)
            imageView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
        
        imageView?.image = images[index]
        
        return imageView!
    }
    //MARK: SwipeViewDelegate
    func swipeViewItemSize(_ swipeView: SwipeView!) -> CGSize {
        return swipeView.bounds.size
    }

    

}
