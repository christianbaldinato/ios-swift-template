//
//  LoginWidget.swift
//  cxp-swift-template-app
//
//  Created by Ahmed Ali on 03/10/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import UIKit
@objc(LoginWidget)
class LoginWidget: NativeRenderer, LoginViewDelegate, LoginDelegate {
   
    
    override func start(_ container: UIView!) throws {
        
        super.initializeView(at: container, nativeContract: self)
    }
    
    override func size() -> CGSize {
        return view.bounds.size
    }
    
    
    //MARK: LoginViewDelegate
    func loginView(_ loginView: LoginView, didFireLoginActionWithUsername username: String, andPassword password: String)
    {
        if username.characters.count == 0 || password.characters.count == 0{
            
            BBUtils.displayAlert(withTitle: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Please fill in your username and password", comment: ""), buttons: [NSLocalizedString("Retry", comment: "")], cancelIndex: nil)
            return
        }
        BBUtils.showLoadingIndicator()
        AuthenticationHelper.startSession(login: username, password: password, delegate: self)
    }
    
    
    //MARK: LoginDelegate
   
    public func loginDidSucceed(withCode responseCode: Int)
    {
        
        ModelManager.instance.loadModel { (error) in
            
            if error != nil{
                self.loginDidFailWithError(error!)
            }else{
                BBUtils.hideLoadingIndicator()
            }
        }
    }
    

    public func loginDidFailWithError(_ error: Error!)
    {
        BBUtils.hideLoadingIndicator()
        BBUtils.displayAlert(withTitle: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, buttons: [NSLocalizedString("Retry", comment: "")], cancelIndex: nil)
    }
}
