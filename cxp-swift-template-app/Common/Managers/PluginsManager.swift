//
//  PluginsManager.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 15/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation

/**
#import "AlertDialog.h"
#import "DatePicker.h"
#import "FrequencyPicker.h"
#import "l18n.h"
#import "SecureStoragePlugin.h"
#import "TouchIDPlugin.h"

*/
class PluginsManager {
    private static let secureStorageSecretKey = "secret"
    
    class func registerAvailablePlugins()
    {
        let plugins : Array<Plugin> = [
            ActivityIndicator(),
            AlertDialog(),
            DatePicker(),
            FrequencyPicker(),
            I18n(),
            SecureStoragePlugin(),
            TouchIDPlugin(),
            Snackbar()
        ]
        
        for plugin in plugins{
            do{
                try CXP.register(plugin)
            }catch{
                let errorMessage = (error.localizedDescription.characters.count > 0) ? error.localizedDescription : "Unknown error"
                let pluginName = NSStringFromClass(type(of: plugin))
                CXP.logError(self, message: "Could not register the \(pluginName) plugin due to \(errorMessage)")
            }
        }
        
        // Initialize Secure Storage Plugin
        CXP.registeredPlugin(SecureStoragePlugin.self).initialize([secureStorageSecretKey : Config.secureStorageSecret])
    }
}
