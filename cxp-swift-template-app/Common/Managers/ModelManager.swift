//
//  CXPModelManager.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 19/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation

@objc(CXPModelManager)
class ModelManager : NSObject, ModelDelegate
{
    
    static let instance = ModelManager()
    
    private var modelLoadCompletionHandler : ((Error?) -> Void)?
    var model : Model?
    
    //MARK: Public methods
    func setup()
    {
        CXP.registerPreloadObserver(self, selector: #selector(preloadDidComplete(_:)))
    }
    
    func cleanup()
    {
        CXP.unregisterPreloadObserver(self)
    }
    
    func loadModel(completionHandler: ((Error?) -> Void)?)
    {
        self.modelLoadCompletionHandler = completionHandler
        CXP.model(self, order: [kModelSourceServer])
        
    }
    
    func invalidateModel()
    {
        CXP.invalidateModel()
    }
    
    func findRenderableByObjectId(objectId: String) -> Renderable?
    {
        return model?.siteMapItemChildren(for: objectId) as! Renderable?
    }
    
    
    func preloadDidComplete(_ notification: NSNotification)
    {
        
        let appDelegate = UIApplication.shared.delegate!
        
        let sitemap = model!.siteMapItemChildren(for: Config.modelMainNavigationSitemapName)! as! [SiteMapItemChild]
        
        var rootController : UIViewController?
        if sitemap.count <= 1{
            rootController = ScreensHelper.viewController(screenType: .singleScreen, sitemape: sitemap)
        }else{
            rootController = ScreensHelper.viewController(screenType: .multiScreens, sitemape: sitemap)
        }
        
        appDelegate.window??.rootViewController = rootController
        modelLoadCompletionHandler?(nil)
        
    }
    //MARK: Private methods
    private func applyI18l()
    {
        
        let currentLang = I18n.currentLanguage()
        CXP.addTargetingParameter(currentLang!, forKey: "Local")
        Bundle.setLanguage(currentLang!)
    }
    
    
    //MARK: ModelDelegate
    
    public func modelDidLoad(_ model: Model!)
    {
        self.model = model
        applyI18l()
    }
    

    public func modelDidFailLoadWithError(_ error: Error!)
    {
        modelLoadCompletionHandler?(error)
    }
}


