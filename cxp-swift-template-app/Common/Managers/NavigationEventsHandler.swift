//
//  NavagationManager.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 19/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation


@objc(NavigationEventsHandler)

class NavigationEventsHandler : NSObject
{
    
    private var actions : [String : Selector] = [
        kCXPNavigationFlowRelationshipSelf : #selector(NavigationEventsHandler.handleSelfNavigation(_ :)),
        kCXPNavigationFlowRelationshipChild : #selector(NavigationEventsHandler.handleChildNavigation(_ :)),
        kCXPNavigationFlowRelationshipRootAncestor : #selector(NavigationEventsHandler.handleRootNavigation(_ :)),
        kCXPNavigationFlowRelationshipRoot : #selector(NavigationEventsHandler.handleRootNavigation(_ :)),
        kCXPNavigationFlowRelationshipOther : #selector(NavigationEventsHandler.handleOtherNavigation(_ :)),
        kCXPNavigationFlowRelationshipExternal : #selector(NavigationEventsHandler.handleExternalNavigation(_ :)),
    ]
    
    private override init() {}
    
    /**
     Lazely load and return the singleton instance of the NavigationManager
     */
    struct Static {
        static var onceToken : Int = 0
        static var instance : NavigationEventsHandler? = nil
    }
    
    class var instance : NavigationEventsHandler {
        
        _ = NavigationEventsHandler.__once
        return Static.instance!
    }
    
    private static var __once: () = {
        Static.instance = NavigationEventsHandler()
    }()

    
    
    func setup()
    {
        CXP.registerNavigationEventListener(self, selector: #selector(handleNavigationNotification(_:)))
    }
    
    func cleanup()
    {
        CXP.unregisterNavigationEventListener(self)
    }

    
    
    func handleNavigationNotification(_ notification: NSNotification)
    {
        let relationship = notification.userInfo!["relationship"] as! String
        if let selector = actions[relationship]{
            perform(selector, with: notification)
        }
    }
    
    @objc private func handleSelfNavigation(_ notification: NSNotification)
    {
        
    }
    
    @objc private func handleChildNavigation(_ notification: NSNotification)
    {
        let target = notification.userInfo!["target"] as! String
        let appDelegate = UIApplication.shared.delegate!
        var navigatoinController : UINavigationController?
        if let tabBar = appDelegate.window??.rootViewController as? UITabBarController{
            navigatoinController = tabBar.selectedViewController! as? UINavigationController
            if tabBar.selectedIndex >= 5{
                navigatoinController = tabBar.moreNavigationController
            }
        }else{
            navigatoinController = appDelegate.window??.rootViewController as! UINavigationController?
        }
        
        let renderable = ModelManager.instance.model?.item(byId: target)
        let vc = WidgetViewController(page: renderable!)
        navigatoinController?.pushViewController(vc, animated: true)
    }
    
    
    @objc private func handleRootNavigation(_ notification: NSNotification)
    {
        let appDelegate = UIApplication.shared.delegate
        let tabBarController = appDelegate?.window??.rootViewController as? UITabBarController
        let target = notification.userInfo!["target"] as! String
        if let viewControllers = tabBarController?.viewControllers{
            for vc in viewControllers{
                if let nav = vc as? UINavigationController{
                    let widgetVC = nav.viewControllers.first! as! WidgetViewController
                    if widgetVC.page.itemId() == target{
                        tabBarController?.selectedViewController = nav
                        return
                    }
                }
            }
        }
    }
    
    
    @objc private func handleOtherNavigation(_ notification: NSNotification)
    {
        let origin = notification.userInfo!["origin"]! as! String
        let target = notification.userInfo!["target"]! as! String
        let appDelegate = UIApplication.shared.delegate!
        var currentNavController : UINavigationController!
        if let tabBarController = appDelegate.window!!.rootViewController! as? UITabBarController{
            if tabBarController.selectedIndex >= 5{
                currentNavController = tabBarController.moreNavigationController
            }else{
                currentNavController = tabBarController.selectedViewController! as! UINavigationController
            }
            
        }else{
            currentNavController = appDelegate.window!!.rootViewController! as! UINavigationController
        }
        
        if !isPageInMainNavigation(origin) && isPageInMainNavigation(target){
            currentNavController.dismiss(animated: true, completion: nil)
        }else{
            let renderable = ModelManager.instance.model!.item(byId: target)
            let vc = WidgetViewController(page: renderable!)
            let nav = UINavigationController(rootViewController: vc)
            
            let cancelButton = UIBarButtonItem(image: UIImage(named: "cancel_icon"), style: .plain, target: vc, action: #selector(WidgetViewController.dismissViewController))
            vc.navigationItem.rightBarButtonItem = cancelButton
            nav.navigationItem.title = renderable!.preference(forKey: "title")
            nav.modalPresentationStyle = .fullScreen
            currentNavController.present(nav, animated: true, completion: nil)
            
            
        }
        
    }
    
    @objc private func handleExternalNavigation(_ notification: NSNotification)
    {
        let target = notification.userInfo!["target"]! as! String
        let url = URL(string: target)!
        UIApplication.shared.openURL(url)
    }
    
    
    //MARK: Private
    func isPageInMainNavigation(_ pageId: String) -> Bool
    {
        let sitemapChildren = ModelManager.instance.model!.siteMapItemChildren(for: Config.modelMainNavigationSitemapName)! as! [SiteMapItemChild]
            
        for child in sitemapChildren{
            if child.itemRef() == pageId{
                return true
            }
        }
        return false
    }
}
