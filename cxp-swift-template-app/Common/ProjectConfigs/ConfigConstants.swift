//
//  ConfigConstants.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 15/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation

enum MultiPagesLayout {
    case tabbedLayout
    case hamburgerMenu
}


@objc(Config)
class Config : NSObject{
    
    static let configFilePath = "assets/backbase/conf/configs-ios.json"
    
   
    static let pagesLayout = MultiPagesLayout.tabbedLayout
    
    static let settingsOptionLanguageKey : String = "com.backbase.app.language.code";
    
    static let modelMainNavigationSitemapName = "navroot_mainmenu"
    
    //MARK: Appearance
    static let navigationBarTintColor = UIColor.white
    static let navigationbarColor = UIColor.red
    static let navigationBarTextColor = UIColor.white
    static let defaultBackgroundColor = UIColor.white
    
    //MARK: Nagivation
    static let openNewContactPageEvent = "navigation:addressbook-contact"
    
    //MARK: Model preferences
    static let offlineWidgetPreference = "offline"
    static let modelPreferenceNotifications = "notifications"
    static let modelPreferencePlusButtonTargetHref = "plusButtonTargetHref"
    static let modelPreferenceSaveButtonEventName = "saveButtonEventName"
    static let modelPreferenceCancelButtonEventName = "cancelButtonEventName"
    static let modelPreferenceDoneButtonEventName = "doneButtonEventName"
    static let modelPreferenceEditButtonEventName = "editButtonEventName"
    static let modelLoadFailedEvent = "backbase.shc.modelLoadFailed";
    
    
    //MARK: Secure storage
    // TODO: Salt/Hash the SecureStorageSecret
    static let secureStorageSecret = "plugin.backbase.secureStorage";
    
    //

    
}

