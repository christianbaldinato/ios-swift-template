//
//  TouchIDFeature.m
//  Triodos
//
//  Created by Bernardo Oliveira on 04/01/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "TouchIDPlugin.h"
#import <LocalAuthentication/LocalAuthentication.h>

@implementation TouchIDPlugin

- (void)authenticate:(NSString *)callbackId {
    LAContext *context = [[LAContext alloc] init];

    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context
             evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
            localizedReason:@"Are you the device owner?"
                      reply:^(BOOL success, NSError *error) {

                          dispatch_async(dispatch_get_main_queue(), ^{
                              if (error) {
                                  [self error:@{
                                      @"error" : @"There was an error authorizing with TouchID."
                                  }
                                      callbackId:callbackId];
                                  return;
                              }

                              if (success) {
                                  [self success:@{} callbackId:callbackId];
                                  return;
                              } else {
                                  [self error:@{ @"error" : @"You are not the device owner." } callbackId:callbackId];
                                  return;
                              }
                          });
                      }];

    } else {
        [self error:@{ @"error" : @"Your device cannot authenticate using TouchID." } callbackId:callbackId];
    }
}

@end
