//
//  TouchIDFeature.h
//  Triodos
//
//  Created by Bernardo Oliveira on 04/01/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <BackbaseCXP/Plugin.h>

@protocol TouchIDPluginSpec <Plugin>
- (void)authenticate:(NSString*)callbackId;
@end

@interface TouchIDPlugin : Plugin <TouchIDPluginSpec>
@end
