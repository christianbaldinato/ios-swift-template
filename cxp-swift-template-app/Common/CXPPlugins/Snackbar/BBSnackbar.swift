//
//  Snackbar.swift
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit

@objc enum SnackbarType: UInt {
    case Info
    case Warning
    case Error
    case Connectivity
    case Success
}

class BBSnackbar: UIView {
    
    /// Default margin of the view.
    static var margin: CGFloat = 16
    /// Font size of the message shown. If 0 then UIFont.systemFontSize()
    static var messageFontSize: CGFloat = 0
    /// Font size of the icon label shown. If 0 then UIFont.systemFontSize()
    static var iconFontSize: CGFloat = 0
    /// Shadow radious around the snackbar.
    static var shadowRadius: CGFloat = 3
    /// Corner radious around the snackbar.
    static var cornerRadius: CGFloat = 3
    /// ✗ ✘ ✖︎ ✕ ✓ ✔︎
    static var iconTextInfo: String = "✖︎"
    static var iconTextWarning: String = "✖︎"
    static var iconTextError: String = "✖︎"
    static var iconTextConnectivity: String = "✖︎"
    static var iconTextSuccess: String = "✔︎"
    /// Background colors
    static var infoBackgroundColor: UIColor = UIColor(red: 0x16, green: 0x6D, blue: 0x9E)
    static var warningBackgroundColor: UIColor = UIColor(red: 0xEA, green: 0xA4, blue: 0)
    static var errorBackgroundColor: UIColor = UIColor(red: 0xD7, green: 0x32, blue: 0x17)
    static var connectivityBackgroundColor: UIColor = UIColor(red: 0x46, green: 0x46, blue: 0x46)
    static var successBackgroundColor: UIColor = UIColor(red: 0x22, green: 0x98, blue: 0x6b)
    
    /// Auto dismiss default value
    static var autoDismissInSeconds: TimeInterval = 0
    
    let type: SnackbarType
    let contentView: UIView
    lazy var messageLabel: UILabel = {
        let messageLabel = UILabel()
        messageLabel.font = UIFont.systemFont(ofSize: BBSnackbar.messageFontSize > CGFloat(0) ? BBSnackbar.messageFontSize : UIFont.systemFontSize)
        messageLabel.textColor = UIColor.white
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .left
        return messageLabel
    }()
    lazy var iconLabel: UILabel = {
        let iconLabel = UILabel()
        iconLabel.font = UIFont.boldSystemFont(ofSize: BBSnackbar.iconFontSize > CGFloat(0) ? BBSnackbar.iconFontSize : UIFont.systemFontSize)
        iconLabel.textColor = UIColor.white
        iconLabel.textAlignment = .right
        return iconLabel
    }()
	var lifeTime = TimeInterval(5)
	
    // MARK: Initialization
    
    init(withMessage message: String, type: SnackbarType) {
        self.type = type
        self.contentView = UIView()
        
        super.init(frame: CGRect())
        
        messageLabel.text = message
        styleSnackbarForType(type)
        layoutSnackbar()
        addShadow()
        addRoundedCorners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Layout
    
    private func addShadow() {
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowOffset = CGSize(width:0, height:0)
        contentView.layer.shadowOpacity = 0.5
        contentView.layer.shadowRadius = BBSnackbar.shadowRadius
    }
    
    private func addRoundedCorners() {
        contentView.layer.cornerRadius = BBSnackbar.cornerRadius
    }
    
    override var intrinsicContentSize : CGSize {
        if let iconLabelText = iconLabel.text as NSString?,
            let messageLabelText = messageLabel.text as NSString?,
            let superview = self.superview {
            let iconLabelRect = iconLabelText.boundingRect(with: CGSize(width:superview.frame.width, height:CGFloat.greatestFiniteMagnitude),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName : iconLabel.font],
                    context: nil)
                let ignoreWidth = 3 * BBSnackbar.margin + iconLabelRect.width
            let messageLabelRect = messageLabelText.boundingRect(with: CGSize(width:superview.frame.width - ignoreWidth, height:CGFloat.greatestFiniteMagnitude),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName : messageLabel.font],
                    context: nil)
            return CGSize(width:UIViewNoIntrinsicMetric, height: messageLabelRect.size.height)
        }
        return CGSize(width: UIViewNoIntrinsicMetric, height: UIViewNoIntrinsicMetric)
    }
    
    private func layoutSnackbar() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        iconLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(contentView)
        contentView.addSubview(messageLabel)
        contentView.addSubview(iconLabel)
        
        layoutMargins = UIEdgeInsetsMake(BBSnackbar.margin, BBSnackbar.margin, BBSnackbar.margin, BBSnackbar.margin)
        contentView.layoutMargins = UIEdgeInsetsMake(BBSnackbar.margin, BBSnackbar.margin, BBSnackbar.margin, BBSnackbar.margin)
        
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .left,
            relatedBy: .equal, toItem: self, attribute: .leftMargin, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .top,
            relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .bottom,
            relatedBy: .equal, toItem: self, attribute: .bottomMargin, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .right,
            relatedBy: .equal, toItem: self, attribute: .rightMargin, multiplier: 1, constant: 0))
        
        contentView.addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .left,
            relatedBy: .equal, toItem: contentView, attribute: .leftMargin, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .top,
            relatedBy: .equal, toItem: contentView, attribute: .topMargin, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .bottom,
            relatedBy: .equal, toItem: contentView, attribute: .bottomMargin, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .right,
            relatedBy: .equal, toItem: iconLabel, attribute: .left, multiplier: 1, constant: -BBSnackbar.margin))
        contentView.addConstraint(NSLayoutConstraint(item: iconLabel, attribute: .right,
            relatedBy: .equal, toItem: contentView, attribute: .rightMargin, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: iconLabel, attribute: .centerY,
            relatedBy: .equal, toItem: contentView, attribute: .centerYWithinMargins, multiplier: 1, constant: 0))
        iconLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, for: .horizontal)
    }
    
    // MARK: Utility methods
    
    private func styleSnackbarForType(_ type: SnackbarType) {
        switch type {
        case .Info:
            contentView.backgroundColor = BBSnackbar.infoBackgroundColor
            iconLabel.text = BBSnackbar.iconTextInfo
        case .Warning:
            contentView.backgroundColor = BBSnackbar.warningBackgroundColor
            iconLabel.text = BBSnackbar.iconTextWarning
        case .Error:
            contentView.backgroundColor = BBSnackbar.errorBackgroundColor
            iconLabel.text = BBSnackbar.iconTextError
        case .Connectivity:
            contentView.backgroundColor = BBSnackbar.connectivityBackgroundColor
            iconLabel.text = BBSnackbar.iconTextConnectivity
        case .Success:
            contentView.backgroundColor = BBSnackbar.successBackgroundColor
            iconLabel.text = BBSnackbar.iconTextSuccess
        }
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}
