//
//  Snackbar.swift
//
//  The MIT License (MIT)
//  Copyright (c) 2015 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import BackbaseCXP

@objc (SnackbarSpec)
protocol SnackbarSpec: PluginProtocol {
    func error(callbackId: String, _ message: String)
    func success(callbackId: String, _ message: String)
    func warning(callbackId: String, _ message: String)
    func info(callbackId: String, _ message: String)
    func noInternet(callbackId: String, _ message: String)
    func hide(callbackId: String)
}

@objc(Snackbar)
class Snackbar: Plugin, SnackbarSpec {
    
    static let kSnackbarContentViewKey: String = "contentView"
    weak var contentView: UIView?
    static var position: Position = .Bottom
    
    override func initialize(_ parameters: [AnyHashable : Any]!) {
        if let contentValue = parameters[Snackbar.kSnackbarContentViewKey] as? UIView {
            contentView = contentValue
        }
    }
    
    func error(callbackId: String, _ message: String) {
        presentSnackbarWithMessage(message, type: .Error)
        success(["result":true], callbackId: callbackId)
    }
    
    func success(callbackId: String, _ message: String) {
        presentSnackbarWithMessage(message, type: .Success)
        success(["result":true], callbackId: callbackId)
    }
    
    func warning(callbackId: String, _ message: String) {
        presentSnackbarWithMessage(message, type: .Warning)
        success(["result":true], callbackId: callbackId)
    }
    
    func info(callbackId: String, _ message: String) {
        presentSnackbarWithMessage(message, type: .Info)
        success(["result":true], callbackId: callbackId)
    }
    
    func noInternet(callbackId: String, _ message: String) {
        presentSnackbarWithMessage(message, type: .Connectivity)
        success(["result":true], callbackId: callbackId)
    }
    
    func hide(callbackId: String) {
        BBSnackbarPresentor.shared.dismissViews(atPosition: Snackbar.position)
        success(["result":true], callbackId: callbackId)
    }
    
    private func presentSnackbarWithMessage(_ message: String, type: SnackbarType) {
        guard let _ = contentView else {
            NSException(name:NSExceptionName(rawValue: "Content view has not been initialized"),
                        reason:"Call initialize method with the key kSnackbarContentViewKey and the content view",
                        userInfo:nil).raise()
            return
        }
        BBSnackbarPresentor.shared.presentView(BBSnackbar(withMessage: message, type: type), inView: contentView!, from: Snackbar.position)
    }
}
