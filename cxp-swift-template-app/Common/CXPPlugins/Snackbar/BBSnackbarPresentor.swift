//
//  BBSnackbarPresentor.swift
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit

@objc enum Position: UInt {
    case Top
    case Bottom
}

enum ItemStatus: UInt {
    case None
    case Queued
    case Dismissing
    case Dismissed
    case Showing
    case Shown
}

// MARK: Item class definition

class Item: NSObject {
    let view: UIView
    weak var parentView: UIView?
    let position: Position
    let autoDismiss: TimeInterval
    weak var constraint: NSLayoutConstraint?
    var status: ItemStatus
    
    init(view: UIView, parentView: UIView, position: Position, autoDismiss: TimeInterval = 0,
         constraint: NSLayoutConstraint? = nil, status: ItemStatus = .None) {
        self.view = view
        self.parentView = parentView
        self.position = position
        self.autoDismiss = autoDismiss
        self.constraint = constraint
        self.status = status
    }
}

class BBSnackbarPresentor: NSObject {
    
    static let shared = BBSnackbarPresentor() // Singleton
    private override init() {} // This prevents others from using the default '()' initializer for this class.
    
    // MARK: Variables
    
    static let animationShowDuration = TimeInterval(0.5)
    static let animationHideDuration = TimeInterval(0.5)
    static let animationDelay = TimeInterval(0.0)
    static let springWithDamping = CGFloat(0.7)
    static let initialSpringVelocity = CGFloat(0.7)
    static let animationShowOptions: UIViewAnimationOptions = .allowUserInteraction
    static let animationHideOptions: UIViewAnimationOptions = .beginFromCurrentState
    
    var topQueue: [Item] = []
    var bottomQueue: [Item] = []
    
    // Public methods
    
    func presentView(_ snackbar: BBSnackbar, inView view: UIView, from position: Position) {
        addTapGestureRecognizer(toView: snackbar)
        let item = Item(view: snackbar, parentView: view, position: position, autoDismiss: snackbar.lifeTime)
        appendItem(item: item)
    }
    
    func dismissView(_ view: UIView) {
        if let item = getItemWithView(view) {
            dismissAnimatedItem(item)
        }
    }
    
    func dismissViews(atPosition position: Position) {
        for item in position == .Top ? topQueue : bottomQueue {
            dismissAnimatedItem(item)
        }
    }
    
    // MARK: Queue
    
    func getItemWithView(_ view: UIView) -> Item? {
        if let topItemIndex = topQueue.index(where: {$0.view == view}) {
            return topQueue[topItemIndex]
        }
        if let bottomItemIndex = bottomQueue.index(where: {$0.view == view}) {
            return bottomQueue[bottomItemIndex]
        }
        return nil
    }
    
    func appendItem(item: Item) {
        item.status = .Queued
        if item.position == .Top {
            topQueue.append(item)
            presentTopQueueItemIfNeeded()
        } else {
			let newMessage = (item.view as! BBSnackbar).messageLabel.text!
			
			for(_, queuedItem) in bottomQueue.enumerated() {
				let queuedMessage = (queuedItem.view as! BBSnackbar).messageLabel.text!
				
				if(queuedMessage == newMessage) {
					return
				}
			}
			
            bottomQueue.append(item)
			
            presentBottomQueueItemIfNeeded()
        }
    }
    
    func removeItem(_ item: Item) {
        item.status = .Dismissed
        item.view.removeFromSuperview()
        if item.position == .Top {
            topQueue = topQueue.filter({$0 != item})
            presentTopQueueItemIfNeeded()
        } else {
            bottomQueue = bottomQueue.filter({$0 != item})
            presentBottomQueueItemIfNeeded()
        }
    }
    
    func presentTopQueueItemIfNeeded() {
        if let item: Item = topQueue.first
            , item.status == .Queued {
            addAnimatedItem(item: item)
        }
    }
    
    func presentBottomQueueItemIfNeeded() {
        if let item: Item = bottomQueue.first
            , item.status == .Queued {
            addAnimatedItem(item: item)
        }
    }
    
    // MARK: Animations
    
    private func addAnimatedItem(item: Item) {
        guard let parentViewValue = item.parentView else {
            return
        }
        
        item.status = .Showing
        item.view.translatesAutoresizingMaskIntoConstraints = false
        parentViewValue.addSubview(item.view)
        parentViewValue.addConstraint(NSLayoutConstraint(item: item.view, attribute: .left,
            relatedBy: .equal, toItem: parentViewValue, attribute: .left, multiplier: 1, constant: 0))
        parentViewValue.addConstraint(NSLayoutConstraint(item: item.view, attribute: .right,
            relatedBy: .equal, toItem: parentViewValue, attribute: .right, multiplier: 1, constant: 0))
        let subviewAttribute: NSLayoutAttribute = (item.position == .Top ? .bottom : .top)
        let parentViewAttribute: NSLayoutAttribute = (item.position == .Top ? .top : .bottom)
        item.constraint = NSLayoutConstraint(item: item.view, attribute: subviewAttribute,
                                             relatedBy: .equal, toItem: parentViewValue, attribute: parentViewAttribute, multiplier: 1, constant: 0)
        
        parentViewValue.addConstraint(item.constraint!)
        parentViewValue.setNeedsLayout()
        parentViewValue.layoutIfNeeded()
        
        item.constraint?.constant = (item.view.frame.height + 44) * (item.position == .Top ? 1 : -1)
        UIView.animate(withDuration: BBSnackbarPresentor.animationShowDuration,
            delay: BBSnackbarPresentor.animationDelay,
            usingSpringWithDamping: BBSnackbarPresentor.springWithDamping,
            initialSpringVelocity: BBSnackbarPresentor.initialSpringVelocity,
            options: BBSnackbarPresentor.animationShowOptions,
            animations: { () -> Void in
                parentViewValue.layoutIfNeeded()
            },
            completion: { (finished) -> Void in
                item.status = .Shown
                if (item.autoDismiss > 0) {
                    let triggerTime = DispatchTime.now() + Double(Int64(item.autoDismiss * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: triggerTime, execute: { 
                        self.dismissAnimatedItem(item)
                    })
                    
                }
        })
    }
    
    private func dismissAnimatedItem(_ item: Item) {
        guard let parentViewValue = item.parentView,
            let constraintValue = item.constraint
            , item.status == .Shown || item.status == .Showing else {
                removeItem(item)
                return
        }
        
        item.status = .Dismissing
        
        parentViewValue.setNeedsLayout()
        parentViewValue.layoutIfNeeded()
        
        constraintValue.constant = 0
        UIView.animate(withDuration: BBSnackbarPresentor.animationHideDuration,
            delay: BBSnackbarPresentor.animationDelay,
            usingSpringWithDamping: BBSnackbarPresentor.springWithDamping,
            initialSpringVelocity: BBSnackbarPresentor.initialSpringVelocity,
            options: BBSnackbarPresentor.animationHideOptions,
            animations: { () -> Void in
                parentViewValue.layoutIfNeeded()
            },
            completion: { (finished) -> Void in
                self.removeItem(item)
        })
    }
    
    // MARK: Gestures
    
    private func addTapGestureRecognizer(toView view: UIView) {
        view.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BBSnackbarPresentor.didTapWithGestureRecognizer(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    func didTapWithGestureRecognizer(_ gesture: UITapGestureRecognizer) {
        if let view = gesture.view {
            dismissView(view)
        }
    }
}
