//
//  FrequencyPicker.m
//  FrequencyPicker
//

#import "FrequencyPicker.h"

NSString *const kFrequencyPickerContentViewKey = @"contentView";
static CGFloat kToolBarHeight = 44;

@interface FrequencyPicker ()

@property (weak, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) UIView *inputView;
@property (strong, nonatomic) UIToolbar *toolBar;
@property (strong, nonatomic) NSArray *interval;
@property (strong, nonatomic) NSArray *frequency;
@property int selectedFrequency;
@property int selectedInterval;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;

@property (strong, nonatomic) NSLayoutConstraint *inputViewConstraint;

@end

@implementation FrequencyPicker

- (void)initialize:(NSDictionary *)parameters {
    self.contentView = parameters[kFrequencyPickerContentViewKey];
}

- (void)show:(NSString *)callbackId
    /*interval*/:(NSArray *)interval
    /*frequency*/:(NSArray *)frequency
    /*selectedInterval*/:(NSString *)selectedInterval
    /*selectedFrequency*/:(NSString *)selectedFrequency {
    if (!self.contentView) {
        @throw [NSException
            exceptionWithName:@"FrequencyPicker content view has not been initialized"
                       reason:@"Call initialize method with the key kFrequencyPickerContentViewKey and the "
                              @"content view"
                     userInfo:nil];
    }

    if ([interval count] == 0) {
        [self error:@{ @"error" : @"interval must be specified" } callbackId:callbackId];
        return;
    }

    if ([frequency count] == 0) {
        [self error:@{ @"error" : @"frequency must be specified" } callbackId:callbackId];
        return;
    }

    self.interval = interval;
    self.frequency = frequency;
    self.selectedInterval = 0;
    self.selectedFrequency = 0;

    [self createInputFrequency:frequency
                      interval:interval
              selectedInterval:selectedInterval
             selectedFrequency:selectedFrequency
                    callbackId:callbackId];
    [self presentInputView];
}

#pragma mark - Layout

- (void)createInputFrequency:(NSArray *)frequency
                    interval:(NSArray *)interval
            selectedInterval:(NSString *)selectedInterval
           selectedFrequency:(NSString *)selectedFrequency
                  callbackId:(NSString *)callbackId {
    [self createToolBarWithCallbackId:callbackId];
    [self createSelectFrequencyPicker:frequency
                             interval:interval
                     selectedInterval:selectedInterval
                    selectedFrequency:selectedFrequency];
    [self createInputViewInOverlay];
    [self addConstraints];
}

- (void)createToolBarWithCallbackId:(NSString *)callbackId {
    self.toolBar = [UIToolbar new];
    self.toolBar.exclusiveTouch = YES;
    self.toolBar.translucent = YES;
    self.toolBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(doneButtonPressed:)];
    doneButton.accessibilityLabel = callbackId;
    UIBarButtonItem *flexItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                      target:self
                                                      action:@selector(cancelButtonPressed:)];
    cancelButton.accessibilityLabel = callbackId;
    self.toolBar.items = @[ cancelButton, flexItem, doneButton ];
}

- (void)createSelectFrequencyPicker:(NSArray *)frequency
                           interval:(NSArray *)interval
                   selectedInterval:(NSString *)selectedInterval
                  selectedFrequency:(NSString *)selectedFrequency {
    self.picker = [UIPickerView new];
    self.picker.delegate = self;
    self.picker.dataSource = self;

    // Pre-select values
    self.selectedFrequency = (int)[selectedFrequency integerValue];
    self.selectedInterval = (int)[selectedInterval integerValue];
    [self.picker selectRow:self.selectedFrequency inComponent:1 animated:NO];
    [self.picker reloadComponent:0];
    [self.picker selectRow:self.selectedInterval inComponent:0 animated:NO];
}

- (void)createInputViewInOverlay {
    self.backgroundView = [UIView new];
    self.backgroundView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.backgroundView];
    self.inputView = [UIView new];
    self.inputView.backgroundColor = [UIColor whiteColor];
    [self.inputView addSubview:self.toolBar];
    //[self.inputView addSubview:self.datePicker];
    [self.inputView addSubview:self.picker];
    [self.backgroundView addSubview:self.inputView];
}

- (void)addConstraints {
    self.backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    self.inputView.translatesAutoresizingMaskIntoConstraints = NO;
    self.toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    self.picker.translatesAutoresizingMaskIntoConstraints = NO;

    NSDictionary *viewsDictionary = @{
        @"backgroundView" : self.backgroundView,
        @"inputView" : self.inputView,
        @"toolBar" : self.toolBar,
        @"picker" : self.picker
    };

    [self.inputView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[toolBar]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:viewsDictionary]];
    [self.inputView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[picker]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:viewsDictionary]];
    [self.inputView
        addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[toolBar(toolBarHeight)][picker]|"
                                                               options:0
                                                               metrics:@{
                                                                   @"toolBarHeight" : @(kToolBarHeight)
                                                               }
                                                                 views:viewsDictionary]];

    [self.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[inputView]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary]];

    self.inputViewConstraint = [NSLayoutConstraint constraintWithItem:self.inputView
                                                            attribute:NSLayoutAttributeBottom
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self.backgroundView
                                                            attribute:NSLayoutAttributeBottom
                                                           multiplier:1
                                                             constant:[self inputViewHeight]];
    [self.backgroundView addConstraint:self.inputViewConstraint];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundView]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDictionary]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDictionary]];
}

- (CGFloat)inputViewHeight {
    return 160 + kToolBarHeight;
}

- (void)presentInputView {
    [self.backgroundView layoutIfNeeded];
    self.inputViewConstraint.constant = 0;
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.backgroundView layoutIfNeeded];
                     }];
}

- (void)dismissInputView {
    [self.backgroundView layoutIfNeeded];
    self.inputViewConstraint.constant = [self inputViewHeight];
    [UIView animateWithDuration:0.3
        animations:^{
            [self.backgroundView layoutIfNeeded];
        }
        completion:^(BOOL finished) {
            [self.backgroundView removeFromSuperview];
        }];
}

#pragma mark - UI Actions

- (void)doneButtonPressed:(id)sender {
    UIBarButtonItem *item = sender;
    [self success:@{
        @"interval" : [NSNumber numberWithInt:self.selectedInterval],
        @"frequency" : [NSNumber numberWithInt:self.selectedFrequency]
    }
        callbackId:item.accessibilityLabel];
    [self dismissInputView];
}

- (void)cancelButtonPressed:(id)sender {
    [self dismissInputView];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        self.selectedInterval = (int)row;
    } else {
        self.selectedFrequency = (int)row;
        self.selectedInterval = 0;
        [self.picker reloadComponent:0];
        [self.picker selectRow:0 inComponent:0 animated:YES];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    return screenWidth / 2;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return [self.interval objectAtIndex:row];
    } else {
        return [self.frequency objectAtIndex:row];
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        if (self.selectedFrequency == 0) {
            self.selectedInterval = 0;
            // don't display numbers for 'only once'
            return 0;
        } else {
            return [self.interval count];
        }
    } else {
        return [self.frequency count];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}
@end
