//
//  SecureStoragePlugin.m
//  CXPMobile
//
//  Created by Bernardo Oliveira on 23/03/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "NSUserDefaults+SecureAdditions.h"
#import "SecureStoragePlugin.h"
#import <CommonCrypto/CommonDigest.h>

@implementation SecureStoragePlugin

- (void)initialize:(NSDictionary *)parameters {
    // Configure Secure UserDefaults
    [[NSUserDefaults standardUserDefaults] setSecret:parameters[kSecureStorageSecretKey]];
}

- (void)setStringForKey:(NSString *)callbackId /*string*/:(NSString *)string /*key*/:(NSString *)key {
    // Store NSString securely
    [[NSUserDefaults standardUserDefaults] setSecretObject:string forKey:key];

    [self success:@{} callbackId:callbackId];
}

- (void)stringForKey:(NSString *)callbackId /*key*/:(NSString *)key {
    NSString *string = [[NSUserDefaults standardUserDefaults] secretStringForKey:key];

    if (string)
        [self success:@{ @"result" : string } callbackId:callbackId];
    else
        [self error:@{ @"error" : @"String not found." } callbackId:callbackId];
}

@end
