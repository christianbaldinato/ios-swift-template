//
//  SecureStoragePlugin.h
//  CXPMobile
//
//  Created by Bernardo Oliveira on 23/03/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <BackbaseCXP/BackbaseCXP.h>

// Secure Storage Secret - Change this before using the plugin
static NSString *const kSecureStorageSecretKey = @"secret";

@protocol SecureStoragePluginSpec <Plugin>
- (void)setStringForKey:(NSString *)callbackId /*string*/:(NSString *)string /*key*/:(NSString *)key;
- (void)stringForKey:(NSString *)callbackId /*key*/:(NSString *)key;
@end

@interface SecureStoragePlugin : Plugin <SecureStoragePluginSpec>
@end
