//
//  DatePicker.m
//  DatePicker
//
//  The MIT License (MIT)
//  Copyright (c) 2015 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "DatePicker.h"

NSString *const kDatePickerContentViewKey = @"contentView";
static CGFloat kToolBarHeight = 44;

@interface DatePicker ()

@property (weak, nonatomic) UIView *contentView;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) UIView *inputView;
@property (strong, nonatomic) UIToolbar *toolBar;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) NSLayoutConstraint *inputViewConstraint;

@end

@implementation DatePicker

- (void)initialize:(NSDictionary *)parameters {
    self.contentView = parameters[kDatePickerContentViewKey];
}

- (void)show:(NSString *)callbackId
    /*dateFormat*/:(NSString *)dateFormat
    /*minDate*/:(NSString *)minDate
    /*maxDate*/:(NSString *)maxDate
    /*selectedDate*/:(NSString *)selectedDate {
    if (!self.contentView) {
        @throw [NSException exceptionWithName:@"DatePicker content view has not been initialized"
                                       reason:@"Call initialize method with the key kDatePickerContentViewKey and the "
                                              @"content view"
                                     userInfo:nil];
    }
    if (!dateFormat.length) {
        [self error:@{ @"error" : @"dateFormat must be specified" } callbackId:callbackId];
        return;
    }
    self.dateFormatter = [NSDateFormatter new];
    self.dateFormatter.dateFormat = dateFormat;
    NSDate *maximumDate;
    NSDate *minimumDate;
    if (minDate.length) {
        minimumDate = [self.dateFormatter dateFromString:minDate];
    }
    if (maxDate.length) {
        maximumDate = [self.dateFormatter dateFromString:maxDate];
    }
    if (minimumDate && maximumDate && [self isDate:minimumDate higherThanDate:maximumDate]) {
        [self error:@{ @"error" : @"minDate must be earlier than maxDate" } callbackId:callbackId];
        return;
    }
    NSDate *currentDate = [self.dateFormatter dateFromString:selectedDate];
    [self createInputWithMinimunDate:minimumDate
                         maximumDate:maximumDate
                        selectedDate:currentDate
                          callbackId:callbackId];
    [self presentInputView];
}

#pragma mark - Layout

- (void)createInputWithMinimunDate:(NSDate *)minimumDate
                       maximumDate:(NSDate *)maximumDate
                      selectedDate:(NSDate *)selectedDate
                        callbackId:(NSString *)callbackId {
    [self createToolBarWithCallbackId:callbackId];
    [self createDatePickerWithMinimunDate:minimumDate maximumDate:maximumDate selectedDate:selectedDate];
    [self createInputViewInOverlay];
    [self addConstraints];
}

- (void)createToolBarWithCallbackId:(NSString *)callbackId {
    self.toolBar = [UIToolbar new];
    self.toolBar.exclusiveTouch = YES;
    self.toolBar.translucent = YES;
    self.toolBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(doneButtonPressed:)];
    doneButton.accessibilityLabel = callbackId;
    UIBarButtonItem *flexItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                      target:self
                                                      action:@selector(cancelButtonPressed:)];
    cancelButton.accessibilityLabel = callbackId;
    self.toolBar.items = @[ cancelButton, flexItem, doneButton ];
}

- (void)createDatePickerWithMinimunDate:(NSDate *)minimumDate
                            maximumDate:(NSDate *)maximumDate
                           selectedDate:(NSDate *)selectedDate {
    self.datePicker = [UIDatePicker new];
    self.datePicker.opaque = YES;
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.date = [NSDate date];
    if (selectedDate && (!minimumDate || [selectedDate compare:minimumDate] != NSOrderedAscending) &&
        (!maximumDate || [selectedDate compare:maximumDate] != NSOrderedDescending)) {
        self.datePicker.date = selectedDate;
    }
    self.datePicker.locale = [NSLocale currentLocale];
    self.datePicker.calendar = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
    self.datePicker.minimumDate = minimumDate;
    self.datePicker.maximumDate = maximumDate;
}

- (void)createInputViewInOverlay {
    self.backgroundView = [UIView new];
    self.backgroundView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.backgroundView];
    self.inputView = [UIView new];
    self.inputView.backgroundColor = [UIColor whiteColor];
    [self.inputView addSubview:self.toolBar];
    [self.inputView addSubview:self.datePicker];
    [self.backgroundView addSubview:self.inputView];
}

- (void)addConstraints {
    self.backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    self.inputView.translatesAutoresizingMaskIntoConstraints = NO;
    self.toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    self.datePicker.translatesAutoresizingMaskIntoConstraints = NO;

    NSDictionary *viewsDictionary = @{
        @"backgroundView" : self.backgroundView,
        @"inputView" : self.inputView,
        @"toolBar" : self.toolBar,
        @"picker" : self.datePicker
    };

    [self.inputView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[toolBar]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:viewsDictionary]];
    [self.inputView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[picker]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:viewsDictionary]];
    [self.inputView
        addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[toolBar(toolBarHeight)][picker]|"
                                                               options:0
                                                               metrics:@{
                                                                   @"toolBarHeight" : @(kToolBarHeight)
                                                               }
                                                                 views:viewsDictionary]];

    [self.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[inputView]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary]];

    self.inputViewConstraint = [NSLayoutConstraint constraintWithItem:self.inputView
                                                            attribute:NSLayoutAttributeBottom
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self.backgroundView
                                                            attribute:NSLayoutAttributeBottom
                                                           multiplier:1
                                                             constant:[self inputViewHeight]];
    [self.backgroundView addConstraint:self.inputViewConstraint];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundView]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDictionary]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDictionary]];
}

- (CGFloat)inputViewHeight {
    return self.datePicker.frame.size.height + kToolBarHeight;
}

- (void)presentInputView {
    [self.backgroundView layoutIfNeeded];
    self.inputViewConstraint.constant = 0;
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.backgroundView layoutIfNeeded];
                     }];
}

- (void)dismissInputView {
    [self.backgroundView layoutIfNeeded];
    self.inputViewConstraint.constant = [self inputViewHeight];
    [UIView animateWithDuration:0.3
        animations:^{
            [self.backgroundView layoutIfNeeded];
        }
        completion:^(BOOL finished) {
            [self.backgroundView removeFromSuperview];
        }];
}

#pragma mark - UI Actions

- (void)doneButtonPressed:(id)sender {
    UIBarButtonItem *item = sender;
    NSString *dateString = [self.dateFormatter stringFromDate:self.datePicker.date];
    if (dateString.length) {
        [self success:@{ @"date" : dateString } callbackId:item.accessibilityLabel];
    } else {
        [self error:@{ @"error" : @"could not read date with current dateFormat" } callbackId:item.accessibilityLabel];
    }
    [self dismissInputView];
}

- (void)cancelButtonPressed:(id)sender {
    UIBarButtonItem *item = sender;
    [self success:@{ @"date" : [NSNull null] } callbackId:item.accessibilityLabel];
    [self dismissInputView];
}

#pragma mark - Utility methods

- (BOOL)isDate:(NSDate *)date1 higherThanDate:(NSDate *)date2 {
    NSComparisonResult comparison = [date1 compare:date2];
    return comparison == NSOrderedDescending;
}

@end
