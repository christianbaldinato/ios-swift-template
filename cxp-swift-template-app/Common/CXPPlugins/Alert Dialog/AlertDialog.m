//
//  CxpDialog.m
//  CxpDialog
//
//  The MIT License (MIT)
//  Copyright (c) 2015 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "AlertDialog.h"

NSString *const kAlertDialogControllerKey = @"controller";

@interface AlertDialog () <UIAlertViewDelegate>
@property (weak, nonatomic) UIViewController *controller;
@end

@implementation AlertDialog

- (void)initialize:(NSDictionary *)parameters {
    self.controller = parameters[kAlertDialogControllerKey];
}

- (void)show:(NSString *)callbackId
    /*title*/:(NSString *)title
    /*message*/:(NSString *)message
    /*buttonsJSON*/:(NSArray *)buttonsJSON {
    if (!self.controller) {
        @throw [NSException exceptionWithName:@"AlertDialog controller has not been initialized"
                                       reason:@"Call initialize method with the key kAlertDialogControllerKey and the "
                                       @"current view controller"
                                     userInfo:nil];
    }

    NSError *error = nil;
    NSArray *buttons = buttonsJSON;

    if (error) {
        [self error:@{
            @"error" : @"Unable to create AlertDialog. Invalid button descriptor, you have to follow the schema: { type"
            @": 'POSITIVE|NEGATIVE|NEUTRAL', text: '<your text here>', callbackFn: 'yourJsCallbackFunctionName'}"
        } callbackId:callbackId];
        return;
    }

     UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
     message:message
     preferredStyle:UIAlertControllerStyleAlert];

     __block AlertDialog *dlg = self;

     for (NSDictionary *btn in buttons) {
        __block NSString *btnCallbackFn = btn[@"callbackFn"];
        NSString *label = btn[@"text"];
        NSString *type = btn[@"type"];

        void (^handler)() = ^(UIAlertAction *action) {
            [dlg success:@{ @"callback" : btnCallbackFn } callbackId:callbackId];
        };

        UIAlertAction *defaultAction =
            [UIAlertAction actionWithTitle:label style:[self convert:type] handler:handler];
        [alert addAction:defaultAction];
     }

     [self.controller presentViewController:alert animated:YES completion:nil];
}

- (UIAlertActionStyle)convert:(NSString *)type {
    if ([type isEqualToString:@"NEGATIVE"])
        return UIAlertActionStyleCancel;
    return UIAlertActionStyleDefault;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSDictionary *data =
        [NSJSONSerialization JSONObjectWithData:[alertView.accessibilityLabel dataUsingEncoding:NSUTF8StringEncoding]
                                        options:NSJSONReadingMutableContainers
                                          error:nil];

    NSArray *buttons = data[@"buttons"];
    NSString *callbackId = data[@"callbackId"];

    for (NSDictionary *btn in buttons) {
        __block NSString *btnCallbackFn = btn[@"callbackFn"];
        NSInteger index = [btn[@"index"] intValue];
        if (index == buttonIndex) {
            [self success:@{ @"callback" : btnCallbackFn } callbackId:callbackId];
            break;
        }
    }
}
@end
