//
//  I18n.m
//  Backbase Retail Mobile
//
//  Created by Bernardo Oliveira on 13/06/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "CXP-Swift-Headers.h"
#import "I18n.h"
#import "NSUserDefaults+SecureAdditions.h"

@implementation I18n

- (void)getLocale:(NSString *)callbackId {
    [self success:@{ @"locale" : [I18n currentLanguage] } callbackId:callbackId];
}

+ (NSString *)currentLanguage {
    NSString *currentLanguage = [[NSUserDefaults standardUserDefaults] secretStringForKey:Config.settingsOptionLanguageKey];
    if (!currentLanguage) {
        // we don't have a language setting inside app, so we will use one from the model
        // if we don't have locale inside model either, fallback to English
        currentLanguage = @"en";
        NSDictionary *preferences = [[[CXP currentModel] app] allPreferences];
        if (preferences) {
            NSString *locale = preferences[@"locale"];
            if (locale) {
                currentLanguage = locale;
            }
        }
    }

    return currentLanguage;
}

+ (NSDictionary *)availableLanguages:(NSError **)error {
    NSDictionary *languages = @{};
    NSMutableDictionary *errorMessage = [NSMutableDictionary dictionary];
    NSDictionary *preferences = [[[CXP currentModel] app] allPreferences];
    if (!preferences) {
        [errorMessage
            setValue:NSLocalizedString(@"PortalPreferenceLoadError", @"Cannot get preferences from the portal")
              forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"I18i" code:100 userInfo:errorMessage];
        return languages;
    }

    NSString *locales = preferences[@"locales"];
    if (!locales) {
        [errorMessage
            setValue:NSLocalizedString(@"PortalLocaleReadError", @"Cannot read locales property from the portal")
              forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"I18i" code:110 userInfo:errorMessage];
        return languages;
    }

    NSData *data = [locales dataUsingEncoding:NSUTF8StringEncoding];
    languages = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:error];

    if (!languages)
        return @{};

    return languages;
}

+ (NSString *)localizedTitleForRenderable:(NSObject<Renderable> *)renderable {
    NSString *localizedTitleKey = [NSString stringWithFormat:@"%@%@", @"title-", [I18n currentLanguage]];
    NSLog(@"Localized title key: %@ | For renderable: %@", localizedTitleKey, renderable.itemName);
    if ([renderable preferenceForKey:localizedTitleKey]) {
        return [renderable preferenceForKey:localizedTitleKey];
    } else {
        return [renderable preferenceForKey:@"title"];
    }
}

@end
