//
//  I18n.h
//  Backbase Retail Mobile
//
//  Created by Bernardo Oliveira on 14/06/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <BackbaseCXP/BackbaseCXP.h>
@protocol I18nSpec <Plugin>
- (void)getLocale:(NSString *)callbackId;
@end

@interface I18n : Plugin <I18nSpec>

+ (NSString *)currentLanguage;
+ (NSDictionary *)availableLanguages:(NSError **)error;
+ (NSString *)localizedTitleForRenderable:(NSObject<Renderable> *)renderable;

@end
