//
//  ActivityIndicator.swift
//
//  The MIT License (MIT)
//  Copyright (c) 2015 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import BackbaseCXP

@objc(ActivityIndicatorSpec)
protocol ActivityIndicatorSpec: PluginProtocol {
    func show(callbackId: String, _ title: String)
    func hide(callbackId: String)
}

@objc(ActivityIndicator)
class ActivityIndicator: Plugin, ActivityIndicatorSpec {
    
    static let kActivityIndicatorContentViewKey: String = "contentView"
    var contentView: UIView?
    var statusView: BBStatusView?
    
    override func initialize(_ parameters: [AnyHashable : Any]!) {
        if let contentValue = parameters[ActivityIndicator.kActivityIndicatorContentViewKey] as? UIView {
            contentView = contentValue
        }
    }
    
    func show(callbackId: String, _ title: String) {
        guard let _ = contentView else {
            NSException(name:NSExceptionName(rawValue: "Controller has not been initialized"),
                reason:"Call initialize method with the key kAcitvityIndicatorControllerKey and the current view controller",
                userInfo:nil).raise()
            return
        }
        statusView = BBStatusView(message: title).presentInView(inView: contentView)
        success(["result":true], callbackId: callbackId)
    }
    
    func hide(callbackId: String) {
        statusView?.dismiss()
        success(["result":false], callbackId: callbackId)
    }
}
