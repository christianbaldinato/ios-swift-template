//
//  BBStatusView.swift
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import Foundation

/// Custom status view with an activity indicator and a dynamic message.
class BBStatusView: UIView {

    /// Default margin of the view.
    static var margin: CGFloat = 16
    /// Font size of the message shown.
    static var messageFontSize: CGFloat = 16.0
    /// File image name of the asset to create the custom activity indicator
    static var imageName: String? = nil
    
    static private let contentCornerRadius: CGFloat = 5
    
    lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return view
    }()
    lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.95, alpha: 0.9)
        view.layer.cornerRadius = BBStatusView.contentCornerRadius
        return view
    }()
    lazy var activityIndicator: UIView = {
        if (BBStatusView.imageName != nil) {
            do {
                let customIndicator = try BBActivityIndicator(imageName: BBStatusView.imageName!)
                return customIndicator
            } catch { }
        }
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.color = UIColor.gray
        activityIndicator.startAnimating()
        return activityIndicator
    }()
    lazy var messageLabel: UILabel = {
        let messageLabel = UILabel()
        messageLabel.font = UIFont.systemFont(ofSize: BBStatusView.messageFontSize)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        return messageLabel
    }()
    
    // MARK: Initialization
    
    init(message: String) {
        super.init(frame: CGRect())
        messageLabel.text = message
        customizeStatusView()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Public methods
    
    func presentInView(inView: UIView?) -> BBStatusView? {
        if let viewValue = inView {
            self.translatesAutoresizingMaskIntoConstraints = false
            viewValue.addSubview(self)
            viewValue.addConstraint(NSLayoutConstraint(item: self, attribute: .left,
                relatedBy: .equal, toItem: viewValue, attribute: .left, multiplier: 1, constant: 0))
            viewValue.addConstraint(NSLayoutConstraint(item: self, attribute: .right,
                relatedBy: .equal, toItem: viewValue, attribute: .right, multiplier: 1, constant: 0))
            viewValue.addConstraint(NSLayoutConstraint(item: self, attribute: .top,
                relatedBy: .equal, toItem: viewValue, attribute: .top, multiplier: 1, constant: 0))
            viewValue.addConstraint(NSLayoutConstraint(item: self, attribute: .bottom,
                relatedBy: .equal, toItem: viewValue, attribute: .bottom, multiplier: 1, constant: 0))
            return self
        }
        return nil
    }
    
    func setMessage(label: String) {
        messageLabel.text = label
    }

    func dismiss() {
        self.removeFromSuperview()
    }
    
    // MARK: Layout
    
    private func customizeStatusView() {
        addSubview(overlayView)
        addSubview(contentView)
        contentView.addSubview(activityIndicator)
        contentView.addSubview(messageLabel)
        
        addStatusViewConstraints()
    }
    
    private func addStatusViewConstraints() {
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let viewsDictionary = ["overlay" : overlayView, "spinner" : activityIndicator, "message" : messageLabel]
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[overlay]|",
            options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[overlay]|",
            options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary))
        
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .centerX,
            relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .centerY,
            relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .width,
            relatedBy: .lessThanOrEqual, toItem: self, attribute: .width, multiplier: 1, constant: -BBStatusView.margin*12))
        addConstraint(NSLayoutConstraint(item: contentView, attribute: .height,
            relatedBy: .lessThanOrEqual, toItem: self, attribute: .height, multiplier: 1, constant: -BBStatusView.margin*4))

        contentView.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .top,
            relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: BBStatusView.margin))
        contentView.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .
            centerX,
            relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: .bottom,
            relatedBy: .equal, toItem: messageLabel, attribute: .top, multiplier: 1, constant: -BBStatusView.margin))
        contentView.addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .centerX,
            relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .bottom,
            relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -BBStatusView.margin))
        contentView.addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .width,
            relatedBy: .lessThanOrEqual, toItem: contentView, attribute: .width, multiplier: 1, constant: -BBStatusView.margin*2))
    }
}
