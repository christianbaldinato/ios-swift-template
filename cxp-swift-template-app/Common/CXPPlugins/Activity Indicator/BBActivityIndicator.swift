//
//  BBActivityIndicator.swift
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Backbase
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit

class BBActivityIndicator: UIView {

    private let kRotationAnimationKey = "my.rotation.animation.key"
    static private let animationDuration : CFTimeInterval = 1.0
    var imageView: UIImageView?

    // MARK: Initialization
    
    /**
    Initializes a custom activity indicator with a given image name.
    The activity indicator provided has a infinite rotation animation.
    - note: The activity indicator will start animating
    - important: The image name assets must exist in the project, otherwise a exception will be thrown.
    - parameter imageName: the name of the file to show.
    */
    init(imageName : String) throws {
        super.init(frame: CGRect())
        
        if let image = getImageWithName(imageName) {
            imageView = UIImageView(image: image)
            if let imageViewValue = imageView {
                imageViewValue.translatesAutoresizingMaskIntoConstraints = false
                addSubview(imageViewValue)
                addConstraint(NSLayoutConstraint(item: imageViewValue, attribute: .left,
                    relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0))
                addConstraint(NSLayoutConstraint(item: imageViewValue, attribute: .right,
                    relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0))
                addConstraint(NSLayoutConstraint(item: imageViewValue, attribute: .top,
                    relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
                addConstraint(NSLayoutConstraint(item: imageViewValue, attribute: .bottom,
                    relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
                startAnimating()
            }
        } else {
            throw NSError(domain: "com.backbase.activity.indicator", code: 404,
                userInfo: ["BBActivityIndicator error" : "Could not find image in assets: \(imageName)"])
        }
    }
    
    /** User Interface not supported! */
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getImageWithName(_ name: String) -> UIImage? {
        return UIImage(named: name)
    }
    
    /**
     Starts a rotation animation
     */
    func startAnimating() {
        if (imageView?.layer.animation(forKey: kRotationAnimationKey) != nil) {
            return
        }
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Float(M_PI * 2.0)
        rotationAnimation.duration = BBActivityIndicator.animationDuration
        rotationAnimation.repeatCount = Float.infinity
        imageView?.layer.add(rotationAnimation, forKey: kRotationAnimationKey)
    }
    
    /**
     Stops animating the activity indicator
     */
    func stopAnimating() {
        if imageView?.layer.animation(forKey: kRotationAnimationKey) != nil {
            imageView?.layer.removeAnimation(forKey: kRotationAnimationKey)
        }
    }
}
