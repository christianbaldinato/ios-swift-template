//
//  SettingsViewController.swift
//  cxp-swift-template-app
//
//  Created by Ahmed Ali on 05/10/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import UIKit
@objc(SettingsViewController)
class SettingsViewController: UIViewController {

    private var page : Renderable!
    
    init(page renderable: Renderable)
    {
        super.init(nibName: nil, bundle: nil)
        self.page = renderable
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = I18n.localizedTitle(for: page)
        createNavigationBarButtons()
    }
    
    
    //MARK: private
    func createNavigationBarButtons()
    {
        let logoutButton = UIBarButtonItem(image: #imageLiteral(resourceName: "logout_icon"), style: .plain, target: self, action: #selector(SettingsViewController.logout))
        
        navigationItem.rightBarButtonItem = logoutButton
        
    }
    
    func logout()
    {
        BBUtils.showLoadingIndicator()
        AuthenticationHelper.logout {
            ModelManager.instance.loadModel(completionHandler: { (error) in
                
                BBUtils.hideLoadingIndicator()
                if error != nil{
                    BBUtils.displayAlert(withTitle: NSLocalizedString("Error", comment: ""), andMessage: error?.localizedDescription)
                }
                
            })
        }
    }


}
