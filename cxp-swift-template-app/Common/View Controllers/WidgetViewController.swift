//
//  WidgetViewController.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 19/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation
class WidgetViewController : BaseViewController{
    
    
    private let supportedButtons : [String : Selector] = [
        Config.modelPreferenceNotifications : #selector(WidgetViewController.createNotificationsButton),
        Config.modelPreferencePlusButtonTargetHref : #selector(WidgetViewController.createPlusButton),
        Config.modelPreferenceSaveButtonEventName : #selector(WidgetViewController.createSaveButton),
        Config.modelPreferenceCancelButtonEventName :#selector(WidgetViewController.createCancelButton),
        Config.modelPreferenceDoneButtonEventName :#selector(WidgetViewController.createDoneButton),
        Config.modelPreferenceEditButtonEventName :#selector(WidgetViewController.createEditButton)
    ]
    
    var page : Renderable!
    
    init(page renderable: Renderable)
    {
        super.init(nibName: nil, bundle: nil)
        self.page = renderable
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllerTitle()
        initilizeBackground()
        createNavigationButtonFromModelPreferences()
        if page.preference(forKey: "title") == "Login"{
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
        renderPage()
    }
    
    
    
    func setViewControllerTitle()
    {
        navigationItem.title = I18n.localizedTitle(for: page)

    }
    
    func initilizeBackground()
    {
        view.backgroundColor = Config.defaultBackgroundColor
        if let backgroundImageName = page.preference(forKey: "background"){
            if let backgroundImage = UIImage(named: backgroundImageName){
                let imageView = UIImageView(image: backgroundImage)
                imageView.contentMode = .scaleAspectFill
                imageView.frame = view.bounds
                imageView.autoresizingMask = [.flexibleWidth]
                imageView.clipsToBounds = true
                view.addSubview(imageView)
            }
        }
    }
    
    
    
    @discardableResult func renderPage() -> Bool
    {
        do {
            let renderer = try CXPRendererFactory.renderer(forItem: page!)
            
            if let renderer = renderer as? WebRenderer{
                renderer.enableText(toLink: false)
            }
            do {
                try applyReferences(from: page!, to: renderer)
                try renderer.start(self.view)
                return true
                
            } catch {
                CXP.logError(self, message: "Error while loading page: \(error.localizedDescription)")
                return false
            }
        } catch {
            CXP.logError(self, message: "Error while creating renderer: \(error.localizedDescription)")
            return false
        }
        
    }
    
    
    func applyReferences(from renderable: Renderable, to renderer: Renderer) throws
    {
        
        if let bouncing = renderable.preference(forKey: "bouncing"){
            renderer.enableBouncing(bouncing == "true")
        }
       
        if let scrolling = renderable.preference(forKey: "scrolling"){
            renderer.enableBouncing(scrolling == "true")
        }
        
    }
    
    //MARK: Navigation buttons
    func createNavigationButtonFromModelPreferences()
    {
        for (preferenceKey, selector) in supportedButtons{
            if page.preference(forKey: preferenceKey) != nil{
                perform(selector)
            }
        }
        
    }
    
    
    func createNotificationsButton()
    {
        let barButtonItem = UIBarButtonItem(image: UIImage(named:"notifications_normal"), style: .plain, target: self, action: #selector(notificationButtonAction))
        navigationItem.leftBarButtonItem = barButtonItem
    }
    
    func createPlusButton()
    {
        let barItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(plusButtonAction))
        navigationItem.rightBarButtonItem = barItem
    }
    
    func createSaveButton()
    {
        createButton(title: "Save", rightButton: true, action: #selector(saveButtonAction))
    }
    
    func createCancelButton()
    {
        createButton(title: "Cancel", rightButton: false, action: #selector(cancelButtonAction))
    }
    
    func createDoneButton()
    {
        createButton(title: "Done", rightButton: true, action: #selector(doneButtonAction))
    }
    func createEditButton()
    {
        createButton(title: "Edit", rightButton: true, action: #selector(editButtonAction))
    }
    
    func createButton(title: String, rightButton: Bool, action: Selector)
    {
        let buttonTitle = NSLocalizedString(title, comment: "")
        let button = UIBarButtonItem(title: buttonTitle, style: .plain, target: self, action: action)
        if rightButton{
            navigationItem.rightBarButtonItem = button
        }else{
            navigationItem.leftBarButtonItem = button
        }
        
    }
    
    
    //MARK: Navigation buttons' actions
    func doneButtonAction()
    {
        CXP.publishEvent(page.preference(forKey: Config.modelPreferenceDoneButtonEventName), payload: nil)
    }
    func editButtonAction()
    {
        CXP.publishEvent(page.preference(forKey: Config.modelPreferenceEditButtonEventName), payload: nil)
    }
    func saveButtonAction()
    {
        CXP.publishEvent(page.preference(forKey: Config.modelPreferenceSaveButtonEventName), payload: nil)
    }
    
    func plusButtonAction()
    {
        CXP.postNavigationRequest(page.itemId(), to: page.preference(forKey: Config.modelPreferencePlusButtonTargetHref))
        
        CXP.publishEvent(Config.openNewContactPageEvent, payload: nil)
    }
    
    func cancelButtonAction()
    {
        CXP.publishEvent(page.preference(forKey: Config.modelPreferenceCancelButtonEventName), payload: nil)
    }
    
    func notificationButtonAction()
    {
        CXP.postNavigationRequest(page.itemId(), to: "/bb-mobile-app/notifications")
    }
    
}
