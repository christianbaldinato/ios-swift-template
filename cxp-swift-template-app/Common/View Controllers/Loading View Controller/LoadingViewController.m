//
//  LoadingViewController.m
//  app-template
//
//  Created by Ignacio Calderon on 01/03/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import "CXP-Swift-Headers.h"
#import <BackbaseCXP/CXP.h>
#import "LoadingViewController.h"

@implementation LoadingViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [CXP registerObserver:self selector:@selector(hideActivityIndicator) forEvent:Config.modelLoadFailedEvent];
}

- (void)hideActivityIndicator {
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
}
@end
