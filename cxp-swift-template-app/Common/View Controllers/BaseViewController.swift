//
//  BaseViewController.swift
//  xcp-swift-template-app
//
//  Created by Ahmed Ali on 19/09/16.
//  Copyright © 2016 Ahmed Ali. All rights reserved.
//

import Foundation

class BaseViewController : UIViewController
{
    private var activityIndicatorPlaceholder : UIView?
    
    
    
    var background : UIImageView?
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigation = navigationController, let background = background{
            let top =  -(navigation.navigationBar.frame.size.height +
                navigation.navigationBar.frame.origin.y);
            var frame = background.frame
            frame.origin.y = top
            background.frame = frame
        }
        
        registerPlugins()
        
    }
    
    //MARK: Public methods
    func removeActivityIndicator()
    {
     
        for view in view.subviews{
            if view is BBStatusView{
                view.removeFromSuperview()
                break
            }
        }
        
    }
    
    @objc public func dismissViewController()
    {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Private
    private func setupNavigationBar()
    {
        if let navController = navigationController{
            StyleHelper.applyStyle(forNavigationController: navController)
        }
        // navigation bar transparency/insets
        automaticallyAdjustsScrollViewInsets = false;
        
        self.edgesForExtendedLayout = [];
    }
    
    private func registerPlugins()
    {
        
        let alertDialog = CXP.registeredPlugin(AlertDialog.self)
        alertDialog?.initialize([kAlertDialogControllerKey : self])
        
       
        let activityIndicator = CXP.registeredPlugin(ActivityIndicator.self)
        activityIndicator?.initialize([ActivityIndicator.kActivityIndicatorContentViewKey : view])
        
        let frequencyPicker = CXP.registeredPlugin(FrequencyPicker.self)
        frequencyPicker?.initialize([kFrequencyPickerContentViewKey : view])

        let datePicker = CXP.registeredPlugin(DatePicker.self)
        datePicker?.initialize([kDatePickerContentViewKey : view])

        
        let snackbar = CXP.registeredPlugin(Snackbar.self)
        snackbar?.initialize([Snackbar.kSnackbarContentViewKey : UIApplication.shared.delegate!.window!!])
        
        activityIndicatorPlaceholder = UIView(frame: view.bounds)
    }
}
