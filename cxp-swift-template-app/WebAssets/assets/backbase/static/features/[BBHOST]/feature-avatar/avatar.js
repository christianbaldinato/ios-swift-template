/**
 * ------------------------------------------------------------------------
 * avatar entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'avatar';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
    'use strict';

    function avatar() {
        return {
            restrict: 'EA',
            scope: {
                photoUrl: '@',
                name: '@'
            },
            template: '<div class="avatar-img">' +
                          '<img ng-if="hasPhotoUrl" ng-src="{{photo}}" class="img img-circle" width="35" height="35"/>' +
                          '<span ng-if="!hasPhotoUrl" class="avatar-initials" data-initials="{{ getInitials(name) }}"></span>' +
                      '</div>',
            link: function (scope, element, attrs) {
                var baseUrl = window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver';
                var regExp = {
                    base64img: /^data.+/
                };

                scope.hasPhotoUrl = false;

                if (/^http.*/.test(baseUrl)) {
                    baseUrl = baseUrl.replace(/^((\w+:)?\/\/[^\/]+\/?).*$/,'$1');
                    if (baseUrl.charAt(baseUrl.length - 1) === '/') {
                        baseUrl = baseUrl.slice(0, -1);
                    } 
                } else {
                    baseUrl = '';
                }

                if (!!scope.photoUrl) {
                    scope.hasPhotoUrl = true;

                    // For base64 images don't prepend the base url
                    scope.photo = regExp.base64img.test(scope.photoUrl) ? scope.photoUrl : baseUrl + scope.photoUrl;
                }

                /**
                 * Get initials for the contact list
                 *
                 * @param name
                 * @return {string}
                 */
                scope.getInitials = function getInitials(name) {
                    var names = name.split(' ');
                    return names[0].charAt(0) + names[names.length - 1].charAt(0);
                };

            }
        }
    }

    return avatar;
}));

