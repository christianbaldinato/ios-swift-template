/**
 * ------------------------------------------------------------------------
 * focusElement entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'focusElement';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name, $timeout, $parse) {
    'use strict';

    function focusElement($timeout, $parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.focusElement);
                console.log('Focus Element loaded:', model);
                scope.$watch(model, function(value) {
                    console.log('Value: ', value)
                    if(value === true) {
                        $timeout(function() {
                            element[0].focus();
                        });
                    } else {
                        $timeout(function() {
                            element[0].blur();
                        });
                    }
                });
                // remove focus on blur
                // element.bind('blur', function() {
                //     console.log('blur');
                //     scope.$apply(model.assign(scope, false));
                // });
            }
        };
    };

    return focusElement;
}));
