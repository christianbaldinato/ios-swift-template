 (function (window, factory) {
  'use strict';
  var name = 'mobilePlugins';
  if (typeof define === 'function' && define.amd) {
  define(['angular'], function (angular) {
		 return factory(name, angular);
		 });
  } else if (typeof module === 'object' && module.exports) {
  module.exports = factory(name, require('angular'));
  }
  window[name] = factory(name, window.angular);
  }(this, function (name, angular) {
	
	'use strict';
	
	var pluginsSupported = !!(window.cxp && window.cxp.mobile && window.cxp.mobile.plugins);
	
	function pluginSupported(pluginName) {
	return !!(pluginsSupported && window.cxp.mobile.plugins[pluginName]);
	}
	
	// Abstract factory (factory for creating factories).
	function PluginCreator(pluginName, pluginMethods) {
	// Return factory for given plugin.
	return function() {
	return getMobilePlugin(pluginName) || mockMobilePlugin(pluginName, pluginMethods)
	}
	}
	
	function getMobilePlugin(pluginName) {
	return pluginSupported(pluginName) ? window.cxp.mobile.plugins[pluginName] : null;
	}
	
	function mockMobilePlugin(pluginName, methods) {
	var mock = { unavailable: true };
	methods.forEach(function(method) {
					mock[method] = debug(pluginName, method); // todo: change debug to noop
					});
	return mock;
	}
	
	// Create a debug function to debug the plugin call.
	function debug (pluginName, methodName) {
	return function() {
	console.log('plugin call: ' + pluginName + '.' + methodName, arguments);
	};
	}
	
	// Create plugin wrappers.
	var Snackbar = PluginCreator('Snackbar',
								 [ 'success', 'error', 'warning', 'info', 'noInternet', 'hide' ]);
	var ActivityIndicator = PluginCreator('ActivityIndicator', [ 'show', 'hide' ]);
	var DatePicker = PluginCreator('DatePicker', [ 'show', 'hide' ]);
	var FrequencyPicker = PluginCreator('FrequencyPicker', [ 'show', 'hide' ]);
	var AlertDialog = PluginCreator('AlertDialog', [ 'show' ]);
	var SecureStorage = PluginCreator('SecureStoragePlugin', [ 'setStringForKey', 'stringForKey' ]);
	var TouchId = PluginCreator('TouchIDPlugin', [ 'authenticate' ]);
	
	function I18nProvider() {
	var locale = null;
	var labels = { };
	
	var i18nPlugin = getMobilePlugin('I18n');
	var DEFAULT_LOCALE = 'en';
	
	this.setDefaultLabels = function(defaultLabels) {
	labels = defaultLabels;
	};
	
	this.$get = ['$q', function($q) {
				 function getLocale() {
				 return $q(function(resolve, reject) {
						   if (!i18nPlugin) {
						   return resolve(DEFAULT_LOCALE);
						   }
						   
						   i18nPlugin.getLocale(function(result) {
												resolve(result.locale)
												}, reject);
						   });
				 }
				 
				 /**
				  *
				  * @type {*|Function|any}
				  */
				 var localeLoaded = getLocale()
				 .then(function(localeSetting) {
					   return locale = localeSetting || DEFAULT_LOCALE;
					   })
				 .catch(function(err) {
						return locale = DEFAULT_LOCALE;
						});
				 
				 /**
				  *
				  */
				 var labelsLoaded = localeLoaded.then(function() {
													  return getLabels();
													  });
				 
				 /**
				  *
				  * @return {*}
				  */
				 function getLabels() {
				 if (locale) {
				 return labels[locale] || labels[DEFAULT_LOCALE];
				 }
				 else {
				 return undefined;
				 }
				 }
				 
				 return {
				 localeLoaded: localeLoaded,
				 labelsLoaded: labelsLoaded,
				 get labels() {
				 return getLabels();
				 }
				 }
				 }];
	}
	
	return angular.module(name, [])
	.factory('bbSnackbar', Snackbar)
	.factory('bbActivityIndicator', ActivityIndicator)
	.factory('bbAlertDialog', AlertDialog)
	.factory('bbSecureStorage', SecureStorage)
	.factory('bbTouchId', TouchId)
	.factory('bbDatePicker', DatePicker)
	.factory('bbFrequencyPicker', FrequencyPicker)
	.provider('bbI18n', I18nProvider);
	}));
