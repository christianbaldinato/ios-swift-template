
/**
 * ------------------------------------------------------------------------
 * Mobile account panel
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'bbmAccountPanel';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {

    function templateFn() {
        return [
    '<div class="account-details panel-body">',
        '<i class="icon icon-angle-right detail-icon"></i>',
        '<h4 class="account-title" ng-bind="$ctrl.account.alias"></h4>',
        '<small class="text-muted iban">{{$ctrl.account.accountIdentification[0].id | iban}}</small>',
        '<div class="account-balance-label text-right">',
            '<small class="text-muted">Available balance</small>',
        '</div>',
        '<div class="account-balance text-right" format-amount="{{$ctrl.account.availableBalance}}"',
            'format-amount-currency="{{$ctrl.account.currency}}">',
        '</div>',
    '</div>'
        ].join('');
    }

    return {
        bindings: {
            account: '<'
        },
        template: templateFn
    }
}));
