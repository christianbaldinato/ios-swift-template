/**
 * ------------------------------------------------------------------------
 * ibanModule entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'ibanModule';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {

    'use strict';


    /*=============================================
     =            IBAN Service                   =
     =============================================*/
    // @ngInject
    /**
     * IBAN Service
     *
     * @param $http
     * @constructor
     */
    function IbanService($http) {
        this.regExp = {
            'ALL_COUNTRIES': /^AL\d{10}[0-9A-Z]{16}$|^AD\d{10}[0-9A-Z]{12}$|^AT\d{18}$|^BH\d{2}[A-Z]{4}[0-9A-Z]{14}$|^BE\d{14}$|^BA\d{18}$|^BG\d{2}[A-Z]{4}\d{6}[0-9A-Z]{8}$|^HR\d{19}$|^CY\d{10}[0-9A-Z]{16}$|^CZ\d{22}$|^DK\d{16}$|^FO\d{16}$|^GL\d{16}$|^DO\d{2}[0-9A-Z]{4}\d{20}$|^EE\d{18}$|^FI\d{16}$|^FR\d{12}[0-9A-Z]{11}\d{2}$|^GE\d{2}[A-Z]{2}\d{16}$|^DE\d{20}$|^GI\d{2}[A-Z]{4}[0-9A-Z]{15}$|^GR\d{9}[0-9A-Z]{16}$|^HU\d{26}$|^IS\d{24}$|^IE\d{2}[A-Z]{4}\d{14}$|^IL\d{21}$|^IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^[A-Z]{2}\d{5}[0-9A-Z]{13}$|^KW\d{2}[A-Z]{4}22!$|^LV\d{2}[A-Z]{4}[0-9A-Z]{13}$|^LB\d{6}[0-9A-Z]{20}$|^LI\d{7}[0-9A-Z]{12}$|^LT\d{18}$|^LU\d{5}[0-9A-Z]{13}$|^MK\d{5}[0-9A-Z]{10}\d{2}$|^MT\d{2}[A-Z]{4}\d{5}[0-9A-Z]{18}$|^MR13\d{23}$|^MU\d{2}[A-Z]{4}\d{19}[A-Z]{3}$|^MC\d{12}[0-9A-Z]{11}\d{2}$|^ME\d{20}$|^NL\d{2}[A-Z]{4}\d{10}$|^NO\d{13}$|^PL\d{10}[0-9A-Z]{,16}n$|^PT\d{23}$|^RO\d{2}[A-Z]{4}[0-9A-Z]{16}$|^SM\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^SA\d{4}[0-9A-Z]{18}$|^RS\d{20}$|^SK\d{22}$|^SI\d{17}$|^ES\d{22}$|^SE\d{22}$|^CH\d{7}[0-9A-Z]{12}$|^TN59\d{20}$|^TR\d{7}[0-9A-Z]{17}$|^AE\d{21}$|^GB\d{2}[A-Z]{4}\d{14}$/,
            // Albania
            'AL': /^AL\d{10}[0-9A-Z]{16}$/,
            // Andorra
            'AD': /^AD\d{10}[0-9A-Z]{12}$/,
            //Austria
            'AT': /^AT\d{18}$/,
            //Kingdom of Bahrain
            'BH': /^BH\d{2}[A-Z]{4}[0-9A-Z]{14}$/,
            //Belgium
            'BE': /^BE\d{14}$/,
            //Bosnia and Herzegovina
            'BA': /^BA\d{18}$/,
            //Bulgaria
            'BG': /^BG\d{2}[A-Z]{4}\d{6}[0-9A-Z]{8}$/,
            //Croatia
            'HR': /^HR\d{19}$/,
            //Cyprus
            'CY': /^CY\d{10}[0-9A-Z]{16}$/,
            //Czech Republic
            'CZ': /^CZ\d{22}$/,
            //Denmark
            'DK': /^DK\d{16}$|^FO\d{16}$|^GL\d{16}$/,
            //Dominican Republic
            'DO': /^DO\d{2}[0-9A-Z]{4}\d{20}$/,
            //Estonia
            'EE': /^EE\d{18}$/,
            //Finland
            'FI': /^FI\d{16}$/,
            //France
            'FR': /^FR\d{12}[0-9A-Z]{11}\d{2}$/,
            //Georgia
            'GE': /^GE\d{2}[A-Z]{2}\d{16}$/,
            //Germany
            'DE': /^DE\d{20}$/,
            //Gibraltar
            'GI': /^GI\d{2}[A-Z]{4}[0-9A-Z]{15}$/,
            //Greece
            'GR': /^GR\d{9}[0-9A-Z]{16}$/,
            //Hungary
            'HU': /^HU\d{26}$/,
            //Iceland
            'IS': /^IS\d{24}$/,
            //Ireland
            'IE': /^IE\d{2}[A-Z]{4}\d{14}$/,
            //Israel
            'IL': /^IL\d{21}$/,
            //Italy
            'IT': /^IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$/,
            //Kazakhstan
            'KZ': /^[A-Z]{2}\d{5}[0-9A-Z]{13}$/,
            //KUWAIT
            'KW': /^KW\d{2}[A-Z]{4}22!$/,
            //Latvia
            'LV': /^LV\d{2}[A-Z]{4}[0-9A-Z]{13}$/,
            //LEBANON
            'LB': /^LB\d{6}[0-9A-Z]{20}$/,
            //Liechtenstein (Principality of)
            'LI': /^LI\d{7}[0-9A-Z]{12}$/,
            //Lithuania
            'LT': /^LT\d{18}$/,
            //Luxembourg
            'LU': /^LU\d{5}[0-9A-Z]{13}$/,
            //Macedonia
            'MK': /^MK\d{5}[0-9A-Z]{10}\d{2}$/,
            //Malta
            'MT': /^MT\d{2}[A-Z]{4}\d{5}[0-9A-Z]{18}$/,
            //Mauritania
            'MR': /^MR13\d{23}$/,
            //Mauritius
            'MU': /^MU\d{2}[A-Z]{4}\d{19}[A-Z]{3}$/,
            //Monaco
            'MC': /^MC\d{12}[0-9A-Z]{11}\d{2}$/,
            //Montenegro
            'ME': /^ME\d{20}$/,
            //The Netherlands
            'NL': /^NL\d{2}[A-Z]{4}\d{10}$/,
            //Norway
            'NO': /^NO\d{13}$/,
            //Poland
            'PL': /^PL\d{10}[0-9A-Z]{,16}n$/,
            //Portugal
            'PT': /^PT\d{23}$/,
            //Romania
            'RO': /^RO\d{2}[A-Z]{4}[0-9A-Z]{16}$/,
            //San Marino
            'SM': /^SM\d{2}[A-Z]\d{10}[0-9A-Z]{12}$/,
            //Saudi Arabia
            'SA': /^SA\d{4}[0-9A-Z]{18}$/,
            //Serbia
            'RS': /^RS\d{20}$/,
            //Slovak Republic
            'SK': /^SK\d{22}$/,
            //Slovenia
            'SI': /^SI\d{17}$/,
            //Spain
            'ES': /^ES\d{22}$/,
            //Sweden
            'SE': /^SE\d{22}$/,
            //Switzerland
            'CH': /^CH\d{7}[0-9A-Z]{12}$/,
            //Tunisia
            'TN': /^TN59\d{20}$/,
            //Turkey
            'TR': /^TR\d{7}[0-9A-Z]{17}$/,
            //United Arab Emirates
            'AE': /^AE\d{21}$/,
            //United Kingdom
            'GB': /^GB\d{2}[A-Z]{4}\d{14}$/
        };
    }

    IbanService.prototype = {
        constructor: IbanService,

        /**
         * Normalize the IBAN number
         * change it to uppercase an remove unnecessarry characters
         *
         * @param iban
         * @return {String}
         */
        normalize: function(iban) {

            if (typeof iban !== 'string') {
                return '';
            }

            return iban.toUpperCase().replace(/[\-\. ]/g, '');
        },

        /**
         * Validate the IBAN code
         *
         * @param iban
         * @param countryCode
         * @return {Boolean}
         */
        validate: function(iban, countryCode) {

            var regExp = typeof countryCode === 'string' && this.regExp.hasOwnProperty(countryCode) ?
                         this.regExp[countryCode] : this.regExp['ALL_COUNTRIES'];

            iban = this.normalize(iban);

            return regExp.test(iban);
        },

        /**
         * Format the input as IBAN
         * 
         * @param {String} value
         * @return String
         */
        format: function(input) {
            input = typeof input === 'string' ? input.toUpperCase().replace(/[\-\. ]/g, '') : '';
            return input && input.match(/(\d|\D){1,4}/g).join(' ');
        }
    };

    /*=============================================
     =            IBAN validator                   =
     =============================================*/
    function IbanValidator(ibanService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.validIban = function(modelValue, viewValue) {
                    // Check if there is value
                    // If there is not a value, leave it for the require directive to validate it
                    if (typeof modelValue === 'string' && !!modelValue.length) {
                        return ibanService.validate(modelValue);
                    } else {
                        return true;
                    }
                };

            }
        };
    }


    /*=============================================
     =            IBAN Input directive            =
     =============================================*/
    function IbanInput(ibanService, $timeout) {
        return {
            restrict: 'A',
            scope: {
                'value': '=',
                'suggestedBanks': '@',
                'keyup': '&keyupEvent',
            },
            template:   '<div class="bb-iban-input">' +
                            '<input placeholder="NL39 RABO 0300 0652 64" ng-keyup="handleKeyUp($event)" ' + 
                                'ng-focus="handleFocusIn()" ng-blur="handleFocusOut()" ng-model="value" required=""/>' +
                            '<ul ng-if="banks.length">' +
                            '<li ng-repeat="bank in banks" ng-click="setBank(bank)">{{bank}}</li>' +
                            '</ul>' +
                        '</div>',
            replace: true,
            link: function(scope, element, attrs) {
                
                var suggestedBanks = parseSuggestedBanks();
                var $input = element.find('input')
                scope.banks = [];

                /**
                 * Bank for IBAN
                 *
                 * @param {String} bank
                 */
                scope.setBank = function(bank) {
                    var iban = scope.value;
                    iban = ibanService.normalize(iban);
                    iban = iban.substr(0, 4) + bank + iban.substr(8);

                    // Add space at the end
                    scope.value = ibanService.format(iban);
                    
                    scope.banks = [];
                    $input[0].focus();
                };

                /**
                 * Handle key up
                 */
                scope.handleKeyUp = function handleKeyUp(event) {
                    normalizeAndSuggest(event);
                    scope.keyup();
                };

                /**
                 * Handle focus in
                 */
                scope.handleFocusIn = function handleFocusIn() {
                    normalizeAndSuggest();
                };

                /**
                 * Clear the bank suggestion
                 */
                scope.handleFocusOut = function handleFocusOut() {
                    $timeout(function() {
                        scope.banks = [];
                    }, 100);
                };

                /**
                 * Parse the suggested banks string to object
                 * e.g. { 'NL' : ['RABO', 'INGB'] }
                 */
                function parseSuggestedBanks() {
                    var banks = trim(String(scope.suggestedBanks));
                    var suggestedBanks = {};

                    if (banks !== '') {
                        banks = banks.split('|');
                        banks.forEach(function(item) {
                            var countryBanks = item.split(':');
                            suggestedBanks[countryBanks[0]] = countryBanks[1].split(',').map(function(code) {
                                return trim(code);
                            });
                        });
                    }

                    return suggestedBanks;
                }

                /**
                 * Format the iban and give bank suggestion
                 * 
                 * @param {Object} event that is passed as a rezult of key up
                 */
                function normalizeAndSuggest(event) {

                    var iban, countryCode;
                    iban = ibanService.normalize(scope.value);
                    var value = ibanService.format(iban);

                    // Add empty space at the end of every 4 characters when typed char different than delete
                    if (iban.length % 4 === 0 && (!event || [46, 8].indexOf(event.which) === -1)) {
                        value += ' ';  
                    }

                    scope.value = value;

                    if (iban.length >= 4 && iban.length <= 7) {
                        countryCode = iban.substr(0, 2);                            
                        if (suggestedBanks.hasOwnProperty(countryCode)) {
                            scope.banks = suggestedBanks[countryCode];
                        } else {
                            scope.banks = [];
                        }
                    } else {
                        scope.banks = [];
                    }
                }

                /**
                 * Trim string from empty spaces
                 */
                function trim(value) {
                    return value.replace(/^\s+|\s+$/g, ''); 
                }

            }
        };
    }

    /*=============================================
     =            IBAN Filter                     =
     =============================================*/
    /**
     * Format the input as IBAN
     * @return {Function}
     * @constructor
     */
     function IbanFilter() {
        return function (input) {
            input = typeof input === 'string' ? input.toUpperCase().replace(/[\-\. ]/g, '') : '';
            return input && input.match(/(\d|\D){1,4}/g).join(' ');
        };
     }

    /**
     * Public API
     */
    return {
        service: IbanService,
        filter: IbanFilter,
        input: IbanInput,
        validator: IbanValidator
    };
}));