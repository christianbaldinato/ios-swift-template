/**
 * ------------------------------------------------------------------------
 * expandableList entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'serviceAccounts';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {

    'use strict';

    function unwrap(response) {
        return response.data;
    }

    function handleError(requestDetails) {
        // Return error handler.
        return function(err) {
            // Normalise error.
            throw {
                message: err.message,
                requestDetails: requestDetails,
                __httpError: err
            };
        }
    }

    // @ngInject
    var serviceAccounts = function($http) {
        var fetch = $http;
        var config = {
            listEndpoint: '/portalserver/payment-orders'
        };

        var api = {
            setConfig: setConfig,
            get: get,
            getAll: getAll
        };

        function setConfig(options) {
            if (options.listEndpoint) {
                config.listEndpoint = options.listEndpoint;
            }
        }

        function getAll() {
            return fetch.get(config.listEndpoint)
                .then(unwrap)
                .catch(handleError({}));
        }

        function get(id) {
            return getAll()
                .then(function(accounts) {
                    var found = null;
                    var i;
                    for (i = 0; found ===null && i < accounts.length; i++) {
                        if (accounts[i].id == id) {
                            found = accounts[i];
                        }
                    }

                    if (!found) {
                        throw 'Account not found';
                    }

                    return found;
                })
                .catch(handleError({id: id}));
        }

        return api;
    }

    return serviceAccounts;
}));
