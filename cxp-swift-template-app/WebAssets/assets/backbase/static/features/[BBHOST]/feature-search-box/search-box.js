
/**
 * ------------------------------------------------------------------------
 * swipeableList entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'searchBox';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
        'use strict';

        function searchBox($document, $compile, $timeout) {
            return {
                    restrict: 'EA',
                    scope: {
                        model: '=searchBox',
                        hasFocus: '=',
                        placeholder: '@'
                    },
                template: '<div class="search">' +
                            '<div class="search-input-group">' +
                              '<input class="search-field input-lg" placeholder="{{placeholder}}" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" ng-model="model" ng-click="hasFocus = true" ng-class="{focused: hasFocus}" />' +
                              '<i class="icon icon-times-circle clear-icon icon-lg" ng-if="hasFocus" ng-click="clearModel()"></i>' +
                            '</div>' +
                            '<button class="btn btn-link btn-lg cancel-btn pull-right" ng-if="hasFocus" ng-click="removeFocus();">Cancel</button>' +
                          '</div>',
                    link: function (scope, element, attrs) {
                        element.addClass('search-element');
                        var input = element.find('input');
                        scope.removeFocus = function () {
                            scope.hasFocus = false;
                            scope.model = '';
                            input[0].blur();
                        };

                        scope.clearModel = function () {
                            scope.model = '';
                        };
                    }
                }
        }

        return searchBox;
    }
));
