'use strict';

/**
 * @name uniq
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#uniq}
 */
exports.uniq = require('lodash/uniq');

/**
 * @name findIndex
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#findIndex}
 */
exports.findIndex = require('lodash/findIndex');

/**
 * @name findLastIndex
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#findLastIndex}
 */
exports.findLastIndex = require('lodash/findLastIndex');
