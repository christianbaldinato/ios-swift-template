'use strict';

var trim = require('./string').trim;

/**
 * @name  isNull
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isNull}
 */
exports.isNull = require('lodash/isNull');

/**
 * @name  isUndefined
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isUndefined}
 */
exports.isUndefined = require('lodash/isUndefined');

/**
 * @name  isEmpty
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isEmpty}
 */
exports.isEmpty = require('lodash/isEmpty');

/**
 * @name  isString
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isString}
 */
exports.isString = require('lodash/isString');

/**
 * @name  isFunction
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isFunction}
 */
exports.isFunction = require('lodash/isFunction');

/**
 * @name  isArray
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isArray}
 */
exports.isArray = require('lodash/isArray');

/**
 * @name isObject
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isObject}
 */
exports.isObject = require('lodash/isObject');

/**
 * @name isElement
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#isElement}
 */
exports.isElement = require('lodash/isElement');

/**
 * @name clone
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#clone}
 */
exports.clone = require('lodash/clone');

/**
 * @name cloneDeep
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#cloneDeep}
 */
exports.cloneDeep = require('lodash/cloneDeep');

/**
 * Check is value is a string and not empty.
 * @name  isEmptyString
 * @memberof module:utils
 * @param {String} value The value to check.
 * @returns {Boolean} Returns true if value is an empty string.
 *
 */
exports.isEmptyString = function isEmptyString(val) {
    return exports.isString(val) && exports.isEmpty(trim(val));
};
