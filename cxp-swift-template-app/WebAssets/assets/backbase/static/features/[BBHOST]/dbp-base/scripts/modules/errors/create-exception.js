'use strict';

var ExceptionTypes = require('./error-exception-types');

/**
 * Creates a custom Error Exception constructor
 * @memberof module:errors
 * @example
 * Basic usage:
 *
 * ```javascript
 * var AuthErrorException = errors.createException('AuthErrorException');
 * if( response.status === '401' ) {
 *     throw new AuthErrorException('Unauthorized access!')
 * }
 * ```
 *
 * @param   {String} name  Exception name
 * @returns {Object}       ErrorException constructor
 */

function createException(name) {
    function ErrorException(message) {
        this.name = name || ExceptionTypes.DEFAULT;
        this.message = message || 'Default Message';
        this.stack = (new Error()).stack;
    }
    ErrorException.prototype = Object.create(Error.prototype);
    ErrorException.prototype.constructor = ErrorException;

    return ErrorException;
}

module.exports = createException;
