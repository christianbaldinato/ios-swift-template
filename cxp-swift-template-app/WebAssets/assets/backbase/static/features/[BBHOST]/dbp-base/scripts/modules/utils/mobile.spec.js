'use strict';

var mobile = require('./mobile');
var namespace = require('./object').namespace;
var noop = require('./utility').noop;

describe('Utils::Mobile', function() {
    beforeEach(function(){
        window.cxp = undefined;
        spyOn(console, 'warn');
    });

    it('expect mobile plugin to be defined', function() {
        var plugin = namespace('cxp.mobile.plugins.AlertDialog');
        plugin.show = noop;

        var result = mobile.getMobilePlugin('AlertDialog');

        expect(result).toEqual(plugin);
        expect(result.show).toBeDefined();
        expect(console.warn).not.toHaveBeenCalled();
    });

    it('expect mobile plugin not to be defined', function() {
        expect(mobile.getMobilePlugin('AlertDialog')).not.toBeDefined();
        expect(console.warn).toHaveBeenCalled();
    });
});
