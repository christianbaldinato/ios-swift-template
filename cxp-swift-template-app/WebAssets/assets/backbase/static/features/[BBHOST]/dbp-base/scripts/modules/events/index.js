'use strict';

var EE = require('./event-emitter');
var pubsub = require('./pubsub');

/**
 * @module events
 * @type {Object}
 */
module.exports = {
    EventEmitter: EE,
    pubsub: pubsub
};
