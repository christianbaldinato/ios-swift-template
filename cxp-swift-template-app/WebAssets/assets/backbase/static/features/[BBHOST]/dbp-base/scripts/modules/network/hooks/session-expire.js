'use strict';

var pubsub = require('../../events/pubsub');
var StatusCode = require('../status-code.js');

/**
 * HTTP Hook to fire 'cxp.session.expire' event when recieved response with 401 status code.
 * Event will not be fired if special 'skip' setting recieved.
 * @param {Response} [response] HTTP Response object
 * @param {Error} [error] Error object
 * @param {Object} options Hook options
 * @param {Boolean} [options.skipExpireEvent] Event will not be fired if this option is set to true
 * @private
 */
function SessionExpireHttpHook(response, error, options) {
    if(error && error.response && error.response.status === StatusCode.UNAUTHORIZED && !options.skipExpireEvent) {
        pubsub.publish('cxp.session.expire', { text: error.message });
    }
};

module.exports = SessionExpireHttpHook;
