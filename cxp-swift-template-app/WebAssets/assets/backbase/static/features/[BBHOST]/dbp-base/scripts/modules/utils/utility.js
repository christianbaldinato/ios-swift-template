'use strict';

/**
 * @name mixin
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#mixin}
 */
exports.mixin = require('lodash/mixin');

/**
 * @name noop
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#noop}
 */
exports.noop = require('lodash/noop');

/**
 * @name flow
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#flow}
 */
exports.flow = require('lodash/flow');
