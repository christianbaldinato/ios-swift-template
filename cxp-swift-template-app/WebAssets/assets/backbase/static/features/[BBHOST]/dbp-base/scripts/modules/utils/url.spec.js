'use strict';

var url = require('./url');

describe('Utils::url', function() {
    it('should build query string from parameters object', function(){
        var fn = url.buildQueryString;

        expect(fn({})).toBe('');
        expect(fn({ a: 1 })).toBe('a=1');
        expect(fn({ a: 'abe' })).toBe('a=abe');
        expect(fn({ a: { b: 45 }})).toBe('a=%5Bobject%20Object%5D');
        expect(fn({ a: [45, 74] })).toBe('a=45&a=74');
        expect(fn({ a: [45, 74] })).toBe('a=45&a=74');
        expect(fn({ a: [45, 'abc'], b: ['rty', 56] })).toBe('a=45&a=abc&b=rty&b=56');
        expect(fn({ a: '!@#$%^&*(' })).toBe('a=!%40%23%24%25%5E%26*(');
    });

    it('should return empty string for non-object', function() {
        var fn = url.buildQueryString;

        expect(fn()).toBe('');
        expect(fn(234)).toBe('');
    });

    describe('buildUrl', function () {
        it('should return url according to given base url and query parameters', function () {
            var buildUrl = url.buildUrl;

            expect(buildUrl('http://foo.bar')).toBe('http://foo.bar');
            expect(buildUrl('http://foo.bar', {foo: 'bar'})).toBe('http://foo.bar?foo=bar');
            expect(buildUrl('http://foo.bar', {foo: {foo: 'bar'}})).toBe('http://foo.bar?foo=%5Bobject%20Object%5D');
        });
    });
});
