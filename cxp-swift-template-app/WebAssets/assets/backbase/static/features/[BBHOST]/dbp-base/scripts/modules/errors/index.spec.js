'use strict';

var errors = require('./index');

describe('Error:Type', function() {
    it('should be defined as object', function() {
        expect(errors.Type).toBeNonEmptyObject();
    });

    it('should have default property', function() {
        expect(errors.Type).toHaveNonEmptyString('DEFAULT');
    });
});

describe('Errors::createException', function() {
    describe('with custom Error type', function() {
        beforeEach(function() {
            this.CustomErrorException = errors.createException('SomeException');
        });

        afterEach(function() {
            this.CustomErrorException = null;
        });

        it('should be defined as a function', function() {
            expect(this.CustomErrorException).toBeFunction();
        });

        it('should throw a custom ErrorException', function() {
            var err = new this.CustomErrorException('Some message');
            expect(function() {
                throw err;
            }).toThrowErrorOfType('SomeException');

            expect(err.message).toEqual('Some message');
        });
    });

    describe('with default Error type', function() {
        beforeEach(function() {
            this.DefaultErrorException = errors.createException();
        });

        afterEach(function() {
            this.DefaultErrorException = null;
        });

        it('should be defined as a function', function() {
            expect(this.DefaultErrorException).toBeFunction();
        });

        it('should throw a custom ErrorException', function() {
            var err = new this.DefaultErrorException();
            expect(function () {
                throw err;
            }).toThrowErrorOfType('Error');

            expect(err.message).toEqual('Default Message');
        });
    });
});
