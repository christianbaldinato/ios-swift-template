'use strict';

var utils = require('../utils');
var errors = require('../errors');
var StatusCode = require('./status-code.js');
var HttpError = errors.createException(errors.Type.HTTP);

/**
 * Type of the hook
 * @enum {String}
 * @name HookType
 */
var HookType = {
    REQUEST: 'request',
    RESPONSE: 'response'
};

/**
 * Contains collection of the registered hooks, grouped by type
 * @type {Object}
 * @private
 */
var hooks = {};

/**
 * Default configuration for HTTP service requests
 * @type {Object}
 * @private
 */
var defaultConfig = {
    // Set credentials to 'include' by default to send cookies with request
    credentials: 'include',
    timeout: 60 * 1000 // 1 minute
};

/**
 * Skip parsing to JSON for listed response status codes, because response will have empty body
 * @type {Array}
 * @private
 */
var parseWhiteListStatusCodes = [
    StatusCode.NO_CONTENT,
    StatusCode.RESET_CONTENT
];

/**
 * Try to parse response for supported status code and content type
 * @param {Object} response Http response object
 * @return {Object|String} Valid JSON object or string
 * @private
 */
function tryParseJSON(response) {
    var contentType;

    return response.text().then(function(data) {
        if(parseWhiteListStatusCodes.indexOf(response.status) === -1 && data) {
            try {
                contentType = response.headers.get('Content-Type');
            } catch (err) {
                console.error(err);
            }

            if(contentType && contentType.indexOf('application/json') !== -1) {
                data = JSON.parse(data);
            }
        }
        return data;
    });
}

/**
 * Method to check the http status
 * @param {object} response - Http response object
 * @returns {object} if status `ok` Http response object or reject promise
 * @private
 */
function checkHttpStatus(response) {
    if (response.ok) {
        return response;
    } else {
        var error = new HttpError(response.statusText);
        error.response = response;
        throw error;
    }
}

/**
 * Performs actual request with provided parameters.
 * This method will also call registered Before and After hooks
 * @param {String} url Request url
 * @param {Object} config Request configuration object
 * @param {Object} [options] Options object, will be used mostly by hooks
 * @returns {object} Promise
 * @private
 */
function makeRequest(url, config, options) {
    try{
        config = utils.defaultsDeep({}, config, defaultConfig);

        var request = new Request(url, config);
        request = processSuccessHooks(HookType.REQUEST, request, options);
    } catch(error) {
        // No need to handle request error hooks here
        return Promise.reject(error);
    }

    const fetch = window.fetch(request);
    const requestTimeout = new Promise((resolve, reject) => {
        if(config.timeout) {
            setTimeout(() => reject(new HttpError('Request timeout')), config.timeout);
        }
    });

    return Promise.race([fetch, requestTimeout])
        .then(checkHttpStatus)
        .then(function(response) {
            return processSuccessHooks(HookType.RESPONSE, response, options);
        })
        .then(tryParseJSON)
        .catch(function(error) {
            return processErrorHooks(HookType.RESPONSE, error, options);
        });
}

/**
 * Executes registered hooks for requests and successful responses
 * @param  {HookType} type Type of the hooks to execute
 * @param  {Request|Response} networkObject Request or response object
 * @param  {Object} [options] Hooks options
 * @return {Request|Response} Original or new Request or Response object
 * @private
 */
function processSuccessHooks(type, networkObject, options) {
    var list = hooks[type] || [];
    list.forEach(function(hook) {
        networkObject = hook(networkObject, null, utils.defaultsDeep({}, options)) || networkObject;
    });
    return networkObject;
}

/**
 * Executes registered hooks for failed responses
 * @param  {HookType} type Type of the hooks to execute
 * @param  {Error} errorObject Error object
 * @param  {Object} [options] Hooks options
 * @return {Error} Original or new Error object
 * @private
 */
function processErrorHooks(type, errorObject, options) {
    var list = hooks[type] || [];
    list.forEach(function(hook) {
        errorObject = hook(null, errorObject, utils.defaultsDeep({}, options)) || errorObject;
    });
    return Promise.reject(errorObject);
}

/**
 * HTTP service based on window.fetch API
 * @see native {@link https://developer.mozilla.org/en/docs/Web/API/Fetch_API}
 * @see polyfill {@link https://github.com/github/fetch}
 * @memberof module:network
 * @example
 * Basic usage:
 *
 * ```javascript
 * var config = {
 *     method: 'post',
 *         headers: {
 *             'Accept': 'application/json',
 *             'Content-Type': 'application/json'
 *       }
 *   };
 * var options = {
 *     customHookOption: 123
 * };
 * var req = http('/some/url/', config, options);
 * ```
 */
function http(url, config, options) {
    return makeRequest(url, config, options);
}

http.HookType = HookType;
http.StatusCode = StatusCode;

/**
 * Registers hook for http requests
 * @param {HookType} type Type of the hook
 * @param {Function} hook Hook function
 */
http.addHook = function addHook(type, hook) {
    if(utils.isString(type) && utils.isFunction(hook)) {
        var list = hooks[type] = hooks[type] || [];
        list.push(hook);
    }
};

var methods = [
    'DELETE',
    'GET',
    'POST',
    'PUT'
];

methods.forEach(function(method) {
    http[method.toLowerCase()] = function(url, config, options) {
        config = config || {};
        config.method = method;
        return makeRequest(url, config, options);
    };
});

module.exports = http;
