'use strict';

/**
 * HTTP status codes
 * @name  StatusCode
 * @readOnly
 * @enum {Number}
 */
var StatusCode = {
    // Successful
    OK: 200,
    ACCEPTED: 202,
    NO_CONTENT: 204,
    RESET_CONTENT: 205,

    // Client Error
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    NOT_ALLOWED: 405,
    NOT_ACCEPTABLE: 406,
    REQUEST_TIMEOUT: 408,
    CONFLICT: 409,

    // Server Error
    INTERNAL_SERVER_ERROR: 500,
    NOT_IMPLEMENTED: 501,
    BAD_GATEWAY: 502,
    SERVICE_UNAVAILABLE: 503,
    GATEWAY_TIMEOUT: 504
};

module.exports = StatusCode;
