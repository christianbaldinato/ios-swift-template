'use strict';

var namespace = require('./object').namespace;
var errors = require('../errors');

describe('Utils::Object', function() {
    var namespaceObj;
    var namespaceError;
    var rootObj = {};

    beforeAll(function() {
        var NamespaceError = errors.createException('NamespaceError');
        namespaceError = new NamespaceError('utils:namespace - invalid namespace string');
    });

    function callNamespaceFn(namespaceStr, root) {
        return root === window ? namespace(namespaceStr) : namespace(namespaceStr, root);
    }

    function testNamespaceMethod(description, root) {
        describe(description, function() {
            beforeEach(function(){
                namespaceObj = undefined;
            });

            it('expect namespace object `cxp.mobile.plugins` to be defined', function() {
                expect(root.cxp).not.toBeDefined();

                namespaceObj = callNamespaceFn('cxp.mobile.plugins', root);

                expect(root.cxp.mobile.plugins).toBeDefined();
                expect(root.cxp.mobile.plugins).toEqual(namespaceObj);
            });

            it('expect namespace object `some.specific.namespace` to be defined', function() {
                expect(root.some).not.toBeDefined();
                root.some = {
                    foo: 123
                };

                namespaceObj = callNamespaceFn('some.specific.namespace', root);

                expect(root.some.specific.namespace).toBeDefined();
                expect(root.some.specific.namespace).toEqual(namespaceObj);
                expect(root.some.foo).toEqual(123);
            });

            it('expect namespace object `namespace-with-dash` to be defined', function() {
                expect(root['namespace-with-dash']).not.toBeDefined();

                namespaceObj = callNamespaceFn('namespace-with-dash', root);

                expect(root['namespace-with-dash']).toBeDefined();
                expect(root['namespace-with-dash']).toEqual(namespaceObj);
            });

            it('expect to throw error for empty namespace string', function() {
                var fn = function() {
                    namespaceObj = callNamespaceFn('', root);
                };

                expect(fn).toThrow(namespaceError);
            });

            it('expect to throw error for empty parameters', function() {
                var fn = function() {
                    namespaceObj = callNamespaceFn();
                };

                expect(fn).toThrow(namespaceError);
            });

            it('expect to throw error for invalid namespace string', function() {
                var fn = function(str) {
                    return function() {
                        namespaceObj = callNamespaceFn(str, root);
                    }
                };

                expect(fn('invalid.namespace.')).toThrow(namespaceError);
                expect(fn('two.dots..namespace')).toThrow(namespaceError);
            });
        });
    }

    testNamespaceMethod('should create namespace object in window object', window);
    testNamespaceMethod('should create namespace object in the root object', rootObj);
});
