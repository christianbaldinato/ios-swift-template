'use strict';

var utils = require('../utils');
var shims = require('./shims-cxp-6');

/**
 *  @memberof module:widget
 */

function BaseWidget() {}
/**
 * Factory function to create a new Widget Application
 * @param  {object} widget          Widget instance
 * @param  {function} factoryApp    Factory application
 * @param  {object} [fromPrototype] mixin prototype
 * @static
 * @return {object}                 Public Widget Application instance
 * @example
 * Basic usage:
 *
 * ```javascript
 * function App(widget) {
 *     // Widget Application
 * }
 * var MyWidgetApp = BaseWidget.create(widget, App, {
 *     start: function() {
 *         angular.bootstrap(widget.body, [widget.name])
 *     }
 * });
 * ```
 */
BaseWidget.create = function create(widget, factoryApp, fromPrototype) {

    var proto = utils.assign(fromPrototype, BaseWidget.prototype);
    var obj = Object.create(proto);
    obj.widget = shims(widget);
    obj.app = factoryApp.call(this, widget);
    return obj;
};

/**
 * Mixin functionality
 * @param  {object} Params object to mixin
 * @return {object} Public Widget Application instance
 * @public
 * @example
 * Basic usage:
 *
 * ```javascript
 * var api = {
 *     customMethod = function() {
            console.log(this)
 *     }
 * };
 * MyWidgetApp.mixin(api)
 * ```
 */
BaseWidget.prototype.mixin = function mixin(params) {
    utils.mixin(this, params);
    return this;
};

module.exports = BaseWidget;
