'use strict';

/**
 * @name filter
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#filter}
 */
exports.filter = require('lodash/filter');

/**
 * @name map
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#map}
 */
exports.map = require('lodash/map');
