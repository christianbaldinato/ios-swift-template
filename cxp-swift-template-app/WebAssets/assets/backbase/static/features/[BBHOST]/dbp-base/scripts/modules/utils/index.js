'use strict';
/**
 * @module utils
 * @type {Object}
 */

var utility = require('./utility');
var mixin = utility.mixin;
var utils = {};

mixin(utils, require('./array'));
mixin(utils, require('./collection'));
mixin(utils, require('./lang'));
mixin(utils, require('./object'));
mixin(utils, require('./string'));
mixin(utils, require('./url'));
mixin(utils, require('./mobile'));
// mixin(utils, require('./date'));
mixin(utils, utility);

module.exports = utils;
