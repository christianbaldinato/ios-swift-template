'use strict';
var utils = require('./utils');

describe('i18n:utils', function() {

    describe('::getCurrencySymbol', function() {
        it('should return correct symbol', function() {
            expect(utils.getCurrencySymbol('USD')).toEqual('$');
            expect(utils.getCurrencySymbol('CRC')).toEqual('₡');
            expect(utils.getCurrencySymbol('KGS')).toEqual('лв');
        });

        it('should return empty string for unknown symbol code', function() {
            expect(utils.getCurrencySymbol('USD123')).toEqual('');
            expect(utils.getCurrencySymbol('')).toEqual('');
            expect(utils.getCurrencySymbol(null)).toEqual('');
        });
    });
});
