'use strict';

var utils = require('./lang');

describe('Utils::Lang', function() {
    var fn = utils.isEmptyString;
    it('should test for empty strings', function() {
        expect(fn(0)).toBe(false);
        expect(fn(345)).toBe(false);
        expect(fn({})).toBe(false);
        expect(fn('234')).toBe(false);
        expect(fn(false)).toBe(false);
        expect(fn(function(){})).toBe(false);
        expect(fn(/123/)).toBe(false);
        expect(fn('')).toBe(true);
        expect(fn(String())).toBe(true);
    });
});
