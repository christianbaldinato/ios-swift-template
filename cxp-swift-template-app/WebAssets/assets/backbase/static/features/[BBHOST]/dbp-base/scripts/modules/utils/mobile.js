'use strict';

/**
 * Utility to get the mobile plugin
 * @name getMobilePlugin
 * @memberof module:utils
 * @param {string} name - Name of the plugin
 * @returns {object|undefined} Returns plugin object if available.
 * @example
 * Basic Usage:
 *
 * ```javascript
 * var plugin = utils.getMobilePlugin('AlertDialog');
 * plugin.someMethod();
 * ```
 */
exports.getMobilePlugin = function getMobilePlugin(name) {
    var plugin;
    try {
        plugin = window.cxp.mobile.plugins[name];
    } catch (e) {
        console.warn('Unable to get ' + name + ' plugin.');
    }

    return plugin;
};
