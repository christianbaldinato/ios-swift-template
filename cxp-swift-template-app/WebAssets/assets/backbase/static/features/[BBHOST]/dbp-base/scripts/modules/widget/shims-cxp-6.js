'use strict';

var utils = require('../utils');
var pubsub = require('../events/pubsub');

function ShimCXP6(widget) {

    /**
     * Widget name
     */
    if(utils.isUndefined(widget.name)) {

        // old portal client doesn't have a widget.name property
        // but it has a widget.model.name and widget.id which is basically the id
        // e.g. 'widget-sample-name-123456'// we will extract based on this
        widget.name = widget.id && widget.id.split('-').slice(0, -1).join('-');
    }
    
    /**
     * Widget features
     */
    widget.features = widget.features || {};
    if(utils.isUndefined(widget.features.pubsub)) {
        utils.assign(widget.features, { pubsub: pubsub });
    }

    return widget;
}

module.exports = ShimCXP6;
