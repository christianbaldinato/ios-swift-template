'use strict';

/**
 * @module i18n
 * @type {Object}
 */
var i18n = {
    utils: require('./utils')
};

module.exports = i18n;
