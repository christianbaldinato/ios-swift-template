'use strict';
/**
 * @submodule utils
 * @memberof module:i18n
 * @name utils
 * @type {Object}
 */

var CURRENCY_MAP = {
    'ALL': 'Lek',
    'AFN': '؋',
    'ARS': '$',
    'AWG': 'ƒ',
    'AUD': '$',
    'AZN': 'ман',
    'BSD': '$',
    'BBD': '$',
    'BYR': 'p.',
    'BZD': 'BZ$',
    'BMD': '$',
    'BOB': '$b',
    'BAM': 'KM',
    'BWP': 'P',
    'BGN': 'лв',
    'BRL': 'R$',
    'BND': '$',
    'KHR': '៛',
    'CAD': '$',
    'KYD': '$',
    'CLP': '$',
    'CNY': '¥',
    'COP': '$',
    'CRC': '₡',
    'HRK': 'kn',
    'CUP': '₱',
    'CZK': 'Kč',
    'DKK': 'kr',
    'DOP': 'RD$',
    'XCD': '$',
    'EGP': '£',
    'SVC': '$',
    'EEK': 'kr',
    'EUR': '€',
    'FKP': '£',
    'FJD': '$',
    'GHC': '¢',
    'GIP': '£',
    'GTQ': 'Q',
    'GGP': '£',
    'GYD': '',
    'HNL': 'L',
    'HKD': '$',
    'HUF': 'Ft',
    'ISK': 'kr',
    'IDR': 'Rp',
    'IRR': '﷼',
    'IMP': '£',
    'ILS': '₪',
    'JMD': 'J$',
    'JPY': '¥',
    'JEP': '£',
    'KZT': 'лв',
    'KGS': 'лв',
    'LAK': '₭',
    'LVL': 'Ls',
    'LBP': '£',
    'LRD': '$',
    'LTL': 'Lt',
    'MKD': 'ден',
    'MYR': 'RM',
    'MUR': '₨',
    'MXN': '$',
    'MNT': '₮',
    'MZN': 'MT',
    'NAD': '$',
    'NPR': '₨',
    'ANG': 'ƒ',
    'NZD': '$',
    'NIO': 'C$',
    'NGN': '₦',
    'KPW': '₩',
    'NOK': 'kr',
    'OMR': '﷼',
    'PKR': '₨',
    'PAB': 'B/.',
    'PYG': 'Gs',
    'PEN': 'S/.',
    'PHP': '₱',
    'PLN': 'zł',
    'QAR': '﷼',
    'RON': 'lei',
    'RUB': 'руб',
    'SHP': '£',
    'SAR': '﷼',
    'RSD': 'Дин.',
    'SCR': '₨',
    'SGD': '$',
    'SBD': '$',
    'SOS': 'S',
    'ZAR': 'R',
    'KRW': '₩',
    'LKR': '₨',
    'SEK': 'kr',
    'CHF': 'CHF',
    'SRD': '$',
    'SYP': '£',
    'TWD': 'NT$',
    'THB': '฿',
    'TTD': 'TT$',
    'TRL': '₤',
    'TVD': '$',
    'UAH': '₴',
    'GBP': '£',
    'USD': '$',
    'UYU': '$U',
    'UZS': 'лв',
    'VEF': 'Bs',
    'VND': '₫',
    'YER': '﷼',
    'ZWD': 'Z$'
};
/*
 * Coverts currency symbol code to currency symbol.
 * @name utils.getCurrencySymbol
 * @memberof submodule:utils
 * @param {String} currencyCode Currency symbol code.
 * @returns {String} Currency symbol if found, if not - empty string.
 * @public
 * @example
 * Basic usage:
 *
 * ```javascript
 * var euroSymbol = i18n.utils.getCurrencySymbol('EUR');
 * ```
 */
exports.getCurrencySymbol = function getCurrencySymbol(currencyCode) {
    return CURRENCY_MAP.hasOwnProperty(currencyCode) ? CURRENCY_MAP[currencyCode] : '';
};
