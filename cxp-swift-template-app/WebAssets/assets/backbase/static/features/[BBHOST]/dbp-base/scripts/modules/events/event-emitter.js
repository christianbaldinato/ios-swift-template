'use strict';

/**
 * ------------------------------------------------------------------------
 * Events::EventEmitter
 * API compatible with the EventEmitter that ships by default with Node.js
 * ------------------------------------------------------------------------
 *
 */

// bblp-cli doesn't include the absolute files eg. require('eventemitter3')
// that's why we use relative paths. Also it helps to pack the external lib
// which by default is not minified

/**
 * EventEmitter constructor
 * @memberof module:events
 * @example
 * Basic usage:
 *
 * ```javascript
 * var ee = base.events.EventEmitter.prototype;
 * utils.assign(Object.getPrototypeOf(widget), ee);
 * ```
 */
var EventEmitter = require('../../../node_modules/eventemitter3');

module.exports = EventEmitter;
