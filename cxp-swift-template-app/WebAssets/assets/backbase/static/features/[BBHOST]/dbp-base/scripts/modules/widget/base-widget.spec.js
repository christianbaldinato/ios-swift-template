'use strict';
var widgetInstanceMock = {
    id: 'widget-id'
};

var BaseWidget = require('./base-widget');

describe('Widget::BaseWidget', function() {
    it('should be defined as a function', function() {
        expect(BaseWidget.create).toBeFunction();
    });
});

describe('Widget::BaseWidget::App', function() {
    beforeEach(function() {
        this.App = BaseWidget.create(widgetInstanceMock, function() {}, { start: function(){} } );
    });
    it('should contain all the properties', function() {
        expect(this.App.start).toBeFunction();
    });
    it('should support mixin functionality', function() {
        this.App.mixin({customMethod: function() {
            return 42;
        }});
        expect(this.App.customMethod()).toEqual(42);
    });
});

describe('Widget::BaseWidget::Instance', function() {
    var App;
    beforeEach(function() {
        App = jasmine.createSpy('App');
        this.app = BaseWidget.create(widgetInstanceMock, App);
    });
    it('should contain widget instance extended properties', function() {
        expect(this.app.widget).toBeObject();
        expect(this.app.widget.features).toBeObject();
    });
});
