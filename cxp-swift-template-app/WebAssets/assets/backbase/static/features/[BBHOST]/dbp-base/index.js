/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - DBP
 *  Filename : index.js
 *  Description: DBP mobile base feature package
 *  ----------------------------------------------------------------
 */

module.exports = {
    data: require('./scripts/modules/data'),
    events: require('./scripts/modules/events'),
    errors: require('./scripts/modules/errors'),
    i18n: require('./scripts/modules/i18n'),
    network: require('./scripts/modules/network'),
    widget: require('./scripts/modules/widget'),
    utils: require('./scripts/modules/utils')
};
