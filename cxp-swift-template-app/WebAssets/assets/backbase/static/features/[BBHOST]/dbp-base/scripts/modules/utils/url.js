'use strict';

var isObject = require('./lang').isObject;
var isArray = require('./lang').isArray;
var map = require('./collection').map;
var keys = require('./object').keys;
/**
 * Given an `object` returns its query string equivalent.
 * @name buidlQueryString
 * @memberof module:utils
 * @param {Object} obj A json compliant object.
 * @returns {String} Returns the query string.
 * @example
 * Basic usage:
 *
 * ```javascript
 * utils.buildQueryString({
 *   search: 'backbase',
 *   page: 3
 * }); // "search=backbase&page=3"
 * ```
 */
exports.buildQueryString = function buildQueryString(obj) {

    if (!isObject(obj)) {
        return '';
    }

    return map(keys(obj), function(key) {
        var val = obj[key];

        if (isArray(val)) {
            return map(val, function(val2) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(val2);
            }).join('&');
        }

        return encodeURIComponent(key) + '=' + encodeURIComponent(val);
    }).join('&');
};

/**
 * Builds URL according to given url and query parameters.
 * @name  buildUrl
 * @memberof module:utils
 * @param {String} url
 * @param {Object} [params]
 * @returns {String}
 * @example
 * Basic usage:
 *
 * ```javascript
 * // NAN
 * ```
 */
exports.buildUrl = function(uri, params) {
    if (params) {
        // @todo This is a naive implementation. Should be improved.
        return uri + '?' + exports.buildQueryString(params);
    } else {
        return uri;
    }
};
