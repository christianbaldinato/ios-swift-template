'use strict';

var lang = require('./lang');
var errors = require('../errors');

/**
 * @memberof module:utils
 * @name assign
 * @see {@link https://lodash.com/docs#assign}
 */
exports.assign = require('lodash/assign');

/**
 * @memberof module:utils
 * @name keys
 * @see {@link https://lodash.com/docs#keys}
 */
exports.keys = require('lodash/keys');

/**
 * @memberof module:utils
 * @name values
 * @see {@link https://lodash.com/docs#values}
 */
exports.values = require('lodash/values');

/**
 * @memberof module:utils
 * @name mapValues
 * @see {@link https://lodash.com/docs#mapValues}
 */
exports.mapValues = require('lodash/mapValues');

/**
 * @memberof module:utils
 * @name defaults
 * @see {@link https://lodash.com/docs#defaults}
 */
exports.defaults = require('lodash/defaults');

/**
 * @memberof module:utils
 * @name defaultsDeep
 * @see {@link https://lodash.com/docs#defaultsDeep}
 */
exports.defaultsDeep = require('lodash/defaultsDeep');

/**
 * Utility to create a namespace
 * @name namespace
 * @memberof module:utils
 * @param {string} namespaceStr Namespace string. Namespace partials should be separated with a dot (.)
 * @param {object} [root] Namespace root object. Window object by default.
 * @return {object} Namespace object
 * @example
 * Basic Usage:
 *
 * ```javascript
 * var pluginsObj = utils.namespace('cxp.mobile.plugins');
 * console.log(pluginsObj === window.cxp.mobile.plugins); // will print true
 *
 * var obj = {};
 * var pluginsObj = utils.namespace('cxp.mobile.plugins', obj);
 * console.log(pluginsObj === obj.cxp.mobile.plugins); // will print true
 * ```
 */
exports.namespace = function namespace(namespaceStr, root) {
    function throwError() {
        var NamespaceError = errors.createException('NamespaceError');
        throw new NamespaceError('utils:namespace - invalid namespace string');
    }

    if(!lang.isString(namespaceStr)) {
        throwError();
    }

    var parts = namespaceStr.split('.');
    root = root || window;
    parts.forEach(function(part) {
        if (part) {
            if (!root[part]) {
                root[part] = {};
            }
            root = root[part];
        } else {
            throwError();
        }
    });
    return root;
};
