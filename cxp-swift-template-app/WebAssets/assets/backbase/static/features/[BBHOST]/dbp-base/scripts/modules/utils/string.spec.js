'use strict';
/**
 * @memberof module:utils
 * @type {Object}
 */

var utils = require('./string');

describe('Utils::string', function() {
    var fn = utils.trim;
    it('should trim for spaces', function() {
        expect(fn(' sds ')).toBe('sds');
    });
});
