'use strict';

/**
 * @module errors
 * @type {Object}
 */
var errors = {
    createException: require('./create-exception'),
    Type: require('./error-exception-types')
};

module.exports = errors;
