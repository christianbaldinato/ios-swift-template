'use strict';

require('es6-promise');
require('whatwg-fetch');

var http;
var utils = require('../utils');
var pubsub = require('../events/pubsub');
var errors = require('../errors');
var HttpError = errors.createException(errors.Type.HTTP);

describe('Network::http', function() {
    // private helpers
    var origWindowFetch;
    var testUrl = 'http://test.com?foo=bar';

    function mockWindowFetch(mockConfig = {delay: 0}) {
        var returnValue;

        if(mockConfig.error) {
            returnValue = new TypeError(mockConfig.error);
            spyOn(window, 'fetch').and.returnValue(Promise.reject(returnValue));
        } else {
            returnValue = {
                headers: {
                    get: function(header) {
                        if(mockConfig.throwHeadersError) {
                            throw new Error('Headers error');
                        }

                        var headers = mockConfig.headers || {};
                        return headers[header] || null;
                    }
                },
                ok: !mockConfig.fail,
                status: mockConfig.status || 200,
                statusText: mockConfig.statusText,
                json: function() {
                    return new Promise(function(resolve, reject) {
                        var json = JSON.parse(mockConfig.response || '{}');
                        resolve(json);
                    });
                },
                text: function() {
                    return new Promise(function(resolve) {
                        resolve(mockConfig.response);
                    });
                }
            };

            spyOn(window, 'fetch').and.returnValue(new Promise((resolve, reject) => {
                setTimeout(() => resolve(returnValue), mockConfig.delay);
            }));
        }

        return returnValue;
    }

    function getRequest(url, config, method) {
        config = utils.defaultsDeep({}, config, { credentials: 'include', method: method.toUpperCase() });
        return new Request(url, config);
    }

    // hooks
    beforeEach(function() {
        origWindowFetch = window.fetch;
        http = require('./http');
    });
    afterEach(function() {
        window.fetch = origWindowFetch;
        delete require.cache[require.resolve('./http')];
    });


    describe('Default http object', function() {
        it('should define HookType enum', function() {
            expect(http.HookType).toBeObject();
        });

        it('should define StatusCode enum', function() {
            expect(http.StatusCode).toBeObject();
        });

        it('should perform a get HTTP request as default', function() {
            mockWindowFetch();

            http(testUrl);

            expect(window.fetch.calls.count()).toBe(1);
            expect(window.fetch.calls.argsFor(0)).toEqual([new Request(testUrl, {
                credentials: 'include'
            })]);
        });
    });


    function testMethod(method) {
        describe(method + '::alias', function() {
            it('should perform ' + method.toUpperCase() + ' HTTP request', function() {
                mockWindowFetch();

                http[method](testUrl);

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    new Request(testUrl, {
                        method: method.toUpperCase(),
                        credentials: 'include'
                    })
                ]);
            });

            it('should use given configuration', function() {
                mockWindowFetch();

                http[method](testUrl, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, { headers: { 'Content-Type': 'application/json' } }, method)
                ]);
            });

            it('should use override credentials configuration if specified', function() {
                mockWindowFetch();

                http[method](testUrl, {
                    credentials: 'same-origin'
                });

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, { credentials: 'same-origin' }, method)
                ]);
            });

            it('should parse response as JSON if content type is `application/json`', function(done) {
                mockWindowFetch({
                    response: '{"foo": "bar"}',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                });

                http[method](testUrl)
                    .then(function(data) {
                        expect(data).toEqual({
                            foo: 'bar'
                        });
                        done();
                    })
                    .catch(function(err) {
                        done.fail('Error should not be thrown here! ' + err);
                    });

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, null, method)
                ]);
            });

            it('should treat response as text if content type is different from `application/json`', function(done) {
                mockWindowFetch({
                    response: '{"foo": "bar"}',
                    headers: {
                        'Content-Type': 'text/html'
                    }
                });

                spyOn(console, 'error').and.callFake(function() {});

                http[method](testUrl)
                    .then(function(data) {
                        expect(data).toBe('{"foo": "bar"}');
                        done();
                    })
                    .catch(function(err) {
                        done.fail('Error should not be thrown here! ' + err);
                    });

                expect(console.error.calls.count()).toBe(0);
                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, null, method)
                ]);
            });

            it('should treat response as text if unable to get Content-Type header', function(done) {
                mockWindowFetch({
                    response: '{"foo": "bar"}',
                    throwHeadersError: true
                });

                spyOn(console, 'error').and.callFake(function() {});

                http[method](testUrl)
                    .then(function(data) {
                        expect(data).toBe('{"foo": "bar"}');
                        done();
                    })
                    .catch(function(err) {
                        done.fail('Error should not be thrown here! ' + err);
                    });

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, null, method)
                ]);
            });

            it('should treat response as text if status code is 204', function(done) {
                mockWindowFetch({
                    response: '',
                    status: 204
                });

                spyOn(console, 'error').and.callFake(function() {});

                http[method](testUrl)
                    .then(function(data) {
                        expect(data).toBe('');
                        done();
                    })
                    .catch(function(err) {
                        done.fail('Error should not be thrown here! ' + err);
                    });

                expect(console.error.calls.count()).toBe(0);
                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, null, method)
                ]);
            });

            it('should correct handle empty body for response with 202 status code and `application/json` content-type', function(done) {
                mockWindowFetch({
                    response: '',
                    status: 202,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                });

                spyOn(console, 'error').and.callFake(function() {});

                http[method](testUrl)
                    .then(function(data) {
                        expect(data).toBe('');
                        done();
                    })
                    .catch(function(err) {
                        done.fail('Error should not be thrown here! ' + err);
                    });

                expect(window.fetch.calls.count()).toBe(1);
            });

            it('should fail if server returns error status', function(done) {
                var errorMsg = 'Some strange error';
                mockWindowFetch({
                    fail: true,
                    status: 400,
                    statusText: errorMsg
                });

                http[method](testUrl)
                    .then(done.fail)
                    .catch(function(err) {
                        expect(err.name).toBe(errors.Type.HTTP);
                        expect(err.response.status).toBe(400);
                        expect(err.message).toBe(errorMsg);
                        done();
                    });

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, null, method)
                ]);
            });

            it('should fail in case of network error', function(done) {
                var errorMsg = 'Oops, network error!';
                mockWindowFetch({
                    error: errorMsg
                });

                http[method](testUrl)
                    .then(done.fail)
                    .catch(function(err) {
                        expect(err.name).toBe('TypeError');
                        expect(err.response).not.toBeDefined();
                        expect(err.message).toBe(errorMsg);
                        done();
                    });

                expect(window.fetch.calls.count()).toBe(1);
            });

            it('should publish if server returns 401 error status', function(done) {
                spyOn(pubsub, 'publish');

                mockWindowFetch({
                    fail: true,
                    status: 401
                });

                var sessionHook = require('./hooks/session-expire');
                http.addHook(http.HookType.RESPONSE, sessionHook);

                http[method](testUrl)
                    .then(done.fail)
                    .catch(function(err) {
                        expect(err.name).toBe(errors.Type.HTTP);
                        expect(err.response.status).toBe(401);
                        expect(pubsub.publish).toHaveBeenCalled();
                        done();
                    });

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, null, method)
                ]);
            });

            it('should fail if server returns invalid json', function(done) {
                mockWindowFetch({
                    response: 'text',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                });

                http[method](testUrl)
                    .then(done.fail)
                    .catch(done);

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, null, method)
                ]);
            });

            it('should client timeout if server not responding', function(done) {
                mockWindowFetch({
                    response: '{"foo": "bar"}',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    delay: 50
                });

                http[method](testUrl, {timeout: 30})
                    .then(done.fail)
                    .catch((error) => {
                        expect(error.name).toEqual('HttpError');
                        expect(error.message).toEqual('Request timeout');
                        done();
                    });

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, {timeout: 30}, method)
                ]);
            });

            it('expect client not to timeout if server responds', function(done) {
                mockWindowFetch({
                    response: '{"foo": "bar"}',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    delay: 20
                });

                http[method](testUrl, {timeout: 100})
                    .then(done)
                    .catch(done.fail);

                expect(window.fetch.calls.count()).toBe(1);
                expect(window.fetch.calls.argsFor(0)).toEqual([
                    getRequest(testUrl, {timeout: 100}, method)
                ]);
            });
        });

        describe(method + '::alias hooks', function() {
            it('should execute registered hooks on successful requests', function(done) {
                var response = mockWindowFetch({
                    response: 'Some text in response'
                });

                var options = {
                    hookOption: 123
                };

                var beforeHook = jasmine.createSpy('bhook')
                var afterHook = jasmine.createSpy('ahook')

                http.addHook(http.HookType.REQUEST, beforeHook);
                http.addHook(http.HookType.RESPONSE, afterHook);

                http[method](testUrl, null, options)
                    .then(function() {
                        expect(beforeHook.calls.count()).toBe(1);
                        expect(beforeHook.calls.mostRecent().args).toEqual([
                            getRequest(testUrl, null, method),
                            null,
                            options
                        ]);

                        expect(afterHook.calls.count()).toBe(1);
                        expect(afterHook.calls.mostRecent().args).toEqual([
                            response,
                            null,
                            options
                        ]);

                        done();
                    })
                    .catch(done.fail);
            });

            it('should execute registered hooks on failed requests', function(done) {
                var errorText = 'Error text';
                mockWindowFetch({
                    fail: true,
                    statusText: errorText
                });

                var options = {
                    hookOption: 123
                };

                var beforeHook = jasmine.createSpy('bhook')
                var afterHook = jasmine.createSpy('ahook')

                http.addHook(http.HookType.REQUEST, beforeHook);
                http.addHook(http.HookType.RESPONSE, afterHook);

                http[method](testUrl, null, options)
                    .then(done.fail)
                    .catch(function() {
                        expect(beforeHook.calls.count()).toBe(1);
                        expect(beforeHook.calls.mostRecent().args).toEqual([
                            getRequest(testUrl, null, method),
                            null,
                            options
                        ]);

                        expect(afterHook.calls.count()).toBe(1);
                        expect(afterHook.calls.mostRecent().args).toEqual([
                            null,
                            new HttpError(errorText),
                            options
                        ]);

                        done();
                    });
            });

            it('should skip hooks when request hooks failed', function(done) {
                var response = mockWindowFetch({
                    response: 'Some text in response'
                });

                var options = {
                    hookOption: 123
                };

                var beforeHook = jasmine.createSpy('bhook').and.throwError('hook-error');

                http.addHook(http.HookType.REQUEST, beforeHook);

                http[method](testUrl, null, options)
                    .then(done.fail)
                    .catch(err => {
                        expect(err.message).toEqual('hook-error');
                    })
                    .then(done);
            });
        });
    }

    ['get', 'delete', 'post', 'put'].forEach(testMethod);
});
