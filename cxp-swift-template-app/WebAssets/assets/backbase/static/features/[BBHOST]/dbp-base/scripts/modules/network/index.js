'use strict';

var http = require('./http');
var sessionHook = require('./hooks/session-expire');

http.addHook(http.HookType.RESPONSE, sessionHook);

/**
 * @module network
 * @type {Object}
 */
module.exports = {
    http: http
};
