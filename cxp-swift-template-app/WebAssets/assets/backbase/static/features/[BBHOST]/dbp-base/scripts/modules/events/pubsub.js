'use strict';

var utils = require('../utils');
/**
 * ------------------------------------------------------------------------
 * This file returns the available Global CXP pubsub implementation
 * current available implementation is window.gadgets.pubsub.
 * https://my.backbase.com/docs/product-documentation/documentation/mobile-sdk/latest/mobileapp_publish_and_subscribe.html#/list
 * In CXP 6.0 gadgets.pubsub  will be deprecated in favor of Postal.js API.
 * this file is responsible for maintaining the compatibility between them.
 * ------------------------------------------------------------------------
 * PubSub Event System
 * @returns {Object}
 * @memberof module:events
 * @example
 * Basic usage:
 *
 * ```javascript
 * var pubsub = base.events.pubsub;
 * pubsub.subscribe('topic', callbackFunction);
 * pubsub.publish('topic', {some: 'data'});
 * pubsub.unsubscribe('topic');
 * ```
 */

var pubsub = (window.gadgets && window.gadgets.pubsub) || {
    publish: utils.noop,
    subscribe: utils.noop,
    unsubscribe: utils.noop,
    flush: utils.noop,
    channel: utils.noop
};

module.exports = pubsub;
