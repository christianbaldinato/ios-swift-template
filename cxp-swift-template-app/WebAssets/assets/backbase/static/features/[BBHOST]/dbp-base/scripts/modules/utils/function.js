'use strict';

/**
 * @name  bindAll
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#bindAll}
 */
exports.bindAll = require('lodash/bindAll');

