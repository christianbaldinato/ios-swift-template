'use strict';

/**
 * Custom ErrorException types.
 * @memberof module:errors
 * @name Type
 * @readOnly
 */
var ErrorExceptionTypes = {
    DEFAULT: 'Error',
    HTTP: 'HttpError'
};

module.exports = ErrorExceptionTypes;
