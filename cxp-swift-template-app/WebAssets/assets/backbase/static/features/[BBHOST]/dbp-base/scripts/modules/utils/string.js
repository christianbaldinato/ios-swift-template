'use strict';
/**
 * @name capitalize
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#capitalize}
 */
exports.capitalize = require('lodash/capitalize');
/**
 * @name parseInt
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#parseInt}
 */
exports.parseInt = require('lodash/parseInt');
/**
 * @name trim
 * @memberof module:utils
 * @see {@link https://lodash.com/docs#trim}
 */
exports.trim = require('lodash/trim');
