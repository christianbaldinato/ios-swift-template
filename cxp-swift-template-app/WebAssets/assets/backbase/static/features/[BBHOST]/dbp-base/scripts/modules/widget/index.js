'use strict';

/**
 * @module widget
 * @type {Object}
 */

var Widget = {
    BaseWidget: require('./base-widget')
};

module.exports = Widget;
