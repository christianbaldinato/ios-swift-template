/**
 * ------------------------------------------------------------------------
 * swipeableList entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'swipeableList';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
        'use strict';
        function swipeableList() {
            return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {

                        var interactionMap = {
                            isDragging: false,
                            firstChild: element[0],
                            menuIsOpen: false
                        };

                        element.on('touchstart touchmove touchend', dragDropHandler);

                        function dragDropHandler(e) {
                            e.stopImmediatePropagation();
                            var el = e.currentTarget,
                                map = interactionMap;

                            map.touchPositionX = e.originalEvent ? e.originalEvent.changedTouches[0].clientX : e.changedTouches[0].clientX;
                            map.containerSize = el.clientWidth;

                            switch (e.type) {
                                case 'touchstart':
                                    map.startTouchPosition = map.touchPositionX;
                                    break;

                                case 'touchend':
                                    drop();
                                    break;

                                default:
                                    if (interactionMap.startTouchPosition - interactionMap.touchPositionX >= attrs.threshold) {
                                        drag(e);
                                    }
                                    break;
                            }

                        }

                        function drag(e) {
                            interactionMap.isDragging = true;
                            interactionMap.draggedEl = interactionMap.startTouchPosition - interactionMap.touchPositionX;
                            interactionMap.firstChild.style["-webkit-transform"] = 'translateX(-' + interactionMap.draggedEl + 'px)';
                            interactionMap.firstChild.style["-webkit-transition"] = 'none';
                            e.preventDefault();
                        }

                        function drop() {
                            interactionMap.isDragging = false;
                            if (!interactionMap.menuIsOpen) {
                                if (interactionMap.draggedEl >= attrs.threshold * 2) {
                                    openMenu();
                                    interactionMap.draggedEl = 0;
                                } else {
                                    closeMenu();
                                }
                            }
                            return true;
                        }

                        function openMenu() {
                            interactionMap.menuIsOpen = true;
                            interactionMap.firstChild.style["-webkit-transform"] = 'translateX(' + -100 + 'px)';
                            interactionMap.firstChild.style["-webkit-transition"] = '-webkit-transform .5s';
                            removeEvents();
                            initSubMenu();

                            scope.mainCtrl.swipeableCellOpen();
                        }

                        function removeEvents() {
                            element.off('touchstart touchmove touchend');
                        }

                        function initSubMenu() {
                            // close Menu
                            interactionMap.firstChild.addEventListener('touchend', function () {
                                closeMenu();
                                element.on('touchstart touchmove touchend', dragDropHandler);
                            });
                        }

                        function initActions() {
                            if (typeof attrs.editFn === 'function') {
                                console.log('edit f.n is there... call it');
                            }
                        }

                        function closeMenu() {
                            interactionMap.menuIsOpen = false;
                            interactionMap.firstChild.style["-webkit-transform"] = 'translateX(' + 0 + ')';
                            interactionMap.firstChild.style["-webkit-transition"] = '-webkit-transform .5s';
                            scope.mainCtrl.swipeableCellClosed();
                        }
                    }
                }
        }

        return swipeableList;
    }
));
