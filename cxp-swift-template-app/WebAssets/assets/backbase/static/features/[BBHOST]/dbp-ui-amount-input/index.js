/**
 * ------------------------------------------------------------------------
 * dbpUiAmountInput entry point
 * ------------------------------------------------------------------------
 */
'use strict';

var angular = require('angular');
var dbpUiBase = require('dbp-ui-base');

var Component = require('./scripts/component');

module.exports = angular.module('dbp-ui-amount-input', [
        dbpUiBase.name
    ])
    .component('dbpUiAmountInput', Component);
