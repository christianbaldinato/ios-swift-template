'use strict';

/**
 * ------------------------------------------------------------------------
 * Angular dbpUiAmountInput component
 * ------------------------------------------------------------------------
 */
var template = require('../templates/index.ng.html');
var MainCtrl = require('./controllers/main');

module.exports = {
    bindings: {
        amount: '=',
        currency: '<',
        empty: '=?',
        maxIntegerLength: '<?'
    },
    controller: MainCtrl,
    template: template
};
