// var base = require('dbp-base');
// var i18n = base.i18n;

var i18n = {
    utils: {
        getCurrencySymbol: function(value) {
            return '€';
        }
    }
}

var INTEGER_MAX_LENGTH = 6;
var DECIMAL_MAX_LENGTH = 2;
var CLASS_NAME = 'dbp-ui-amount-input';
var EMPTY_INTEGER_CLASS_NAME = 'empty-integer';
var EMPTY_DECIMAL_CLASS_NAME = 'empty-decimal';
var EMPTY_SYMBOL_CLASS_NAME = 'empty-symbol';

/**
 * Returns ngModel controller for element
 * @param {Object} element Directive element
 * @return {Object} ngModel controller
 * @private
 */
function getNgModelCtrl(element) {
    return element.controller('ngModel');
}

/**
 * Parses and returns the decimal part of the given amount as an integer.
 * @param {Number} amount
 * @return {Number}
 */
function parseDecimal(amount) {
    return parseInt((amount % 1).toFixed(2).substr(2), 10);
}

/**
 * Sets initial integer and decimal values from amount
 * @param {Object} ctrl Controller instance
 * @private
 */
function setIntegerAndDecimal(ctrl) {
    var value = parseFloat(ctrl.amount);
    if (isNaN(value)) {
        ctrl.integer = ctrl.decimal = null;
    } else {
        value = Math.abs(value);

        var integer = Math.floor(value);
        if (integer >= Math.pow(10, ctrl.maxIntegerLength)) {
            integer = 0;
        }

        var decimal = parseDecimal(value);

        ctrl.integer = integer === 0 ? null : integer;
        ctrl.decimal = decimal === 0 ? null : decimal;
    }
}

/**
 * Handles external changes of the amount.
 * @param {Number} val  Current value
 * @param {Number} prev Previous value
 * @private
 */
function onAmountChange(val) {
    if (!this.hasFocus()) {
        setIntegerAndDecimal(this);
        this.updateEmptyState();
    }
}

/**
 * Process input in the Integer input element
 * @param  {Object} e Input event
 * @private
 */
function onIntegerInput(e) {
    var elem = this.integerElement;
    var ctrl = getNgModelCtrl(elem);
    var newValue = parseInt(elem.val());

    if (!elem.prop('validity').valid && isNaN(newValue)) {
        // If value is invalid - set to previuosly valid value
        ctrl.$rollbackViewValue();

        // Additional step to avoid minuses in the input
        if (ctrl.$viewValue === '') {
            elem.val('');
        }
    } else {
        newValue += '';

        if (newValue.length > this.maxIntegerLength) {
            if (ctrl.$viewValue && ctrl.$viewValue.length > this.maxIntegerLength) {
                newValue = newValue.substr(0, this.maxIntegerLength);
            } else {
                newValue = ctrl.$viewValue;
            }
        }

        ctrl.$setViewValue(newValue, e.type);
        ctrl.$render();
    }
    this.updateEmptyState();
    this.updateAmount();
}

/**
 * Handles keypress event on the Integer input field
 * @param  {Object} e Keypress event
 * @private
 */
function onIntegerKeyPress(e) {
    // Select decimals field when period key pressed
    if (String.fromCharCode(e.charCode) === this.separator) {
        this.decimalElement[0].focus();
    }
}

/**
 * Process input in the Decimal input element
 * @param  {Object} e Input event
 * @private
 */
function onDecimalInput(e) {
    var elem = this.decimalElement;
    var ctrl = getNgModelCtrl(elem);
    var newValue = elem.val();

    if (!elem.prop('validity').valid) {
        ctrl.$rollbackViewValue();

        // Additional step to avoid minuses in the input
        if (ctrl.$viewValue === '') {
            elem.val('');
        }
    } else {
        newValue += '';

        if (newValue.length > this.maxDecimalLength) {
            newValue = newValue.substr(0, this.maxDecimalLength);
        }

        ctrl.$setViewValue(newValue, e.type);
        ctrl.$render();

        if (newValue.length === 1) {
            ctrl.$modelValue = ctrl.$modelValue * 10;
            this.updateScope();
        }
    }
    this.updateEmptyState();
    this.updateAmount();
}

/**
 * Adds trailing zeros on blur to always have two digits in the input field
 * @param  {Object} e Blur event
 * @private
 */
function onDecimalBlur(e) {
    var ctrl = getNgModelCtrl(this.decimalElement);
    if (ctrl.$viewValue && ctrl.$viewValue.length === 1) {
        ctrl.$setViewValue(ctrl.$modelValue + '0', e.type);
        ctrl.$render();
        this.updateAmount();
    }
}

/**
 * Sets the correct symbol based on the currency code
 * USD => $
 * @param  {String} value currency code value e.g. USD
 * @private
 */
function setCurrencySymbol(value) {
    this.currencySymbol = i18n.utils.getCurrencySymbol(value);
    this.element.toggleClass(EMPTY_SYMBOL_CLASS_NAME, !this.currencySymbol);
}

/**
 * Directive MainCtrl implementation
 * @ngInject
 */
function MainCtrl($scope, $element, $locale, dbpUiUtils) {
    var ctrl = this;
    ctrl.element = $element;
    ctrl.maxIntegerLength = ctrl.maxIntegerLength || INTEGER_MAX_LENGTH;
    ctrl.maxDecimalLength = DECIMAL_MAX_LENGTH;
    ctrl.separator = $locale.NUMBER_FORMATS.DECIMAL_SEP;

    var children = $element.children();
    ctrl.integerElement = children.eq(1);
    ctrl.decimalElement = children.eq(3);

    ctrl.integerElement.on('input', onIntegerInput.bind(ctrl));
    ctrl.integerElement.on('keypress', onIntegerKeyPress.bind(ctrl));
    ctrl.decimalElement.on('input', onDecimalInput.bind(ctrl));
    ctrl.decimalElement.on('blur', onDecimalBlur.bind(ctrl));

    ctrl.updateScope = function() {
        dbpUiUtils.safeApply($scope);
    };

    $scope.$watch(function() {
        return this.amount;
    }.bind(this), function(currentValue, previousValue) {
        if (currentValue !== 'undefined' && currentValue !== previousValue) {
            onAmountChange.call(this, currentValue);
        }
    }.bind(this));
};
/**
 * Called on each controller after all the controllers on an element have been constructed
 * and had their bindings initialized.
 * This is a good place to put initialization code for your controller.
 */
MainCtrl.prototype.$onInit = function() {
    var ctrl = this;
    setCurrencySymbol.call(this, this.currency);
    setIntegerAndDecimal(ctrl);
    ctrl.updateEmptyState();

};

/**
 * Called after this controller’s element and its children have been linked.
 * This hook can be considered analogous to the `ngAfterViewInit` and `ngAfterContentInit` hooks in Angular 2.
 */
MainCtrl.prototype.$postLink = function() {
    this.element.addClass(CLASS_NAME);
};

/**
 * Called whenever one-way bindings are updated.
 * The `changesObj` is a hash whose keys are the names of the bound properties that have changed,
 * and the values are an object of the form
 */
MainCtrl.prototype.$onChanges = function(changesObj) {

    if (changesObj.currency && changesObj.currency.currentValue !== changesObj.currency.previousValue) {
        setCurrencySymbol.call(this, changesObj.currency.currentValue);
    }
};

/**
 * Checks whether the component has focus.
 * Returns true, if either integer input or decimal input is focused.
 * Otherwise returns false.
 * @return {Boolean}
 * @public
 */
MainCtrl.prototype.hasFocus = function hasFocus() {
    var focusedElem = document.activeElement;
    return focusedElem && (focusedElem === this.decimalElement[0] ||
        focusedElem === this.integerElement[0]);
};

/**
 * Updates amount property based on the integer and decimal values
 */
MainCtrl.prototype.updateAmount = function() {
    var ctrl = getNgModelCtrl(this.decimalElement);
    var decimalFactor = ctrl.$viewValue && ctrl.$viewValue.length === 1 ? 10 : 100;

    this.amount = (this.integer || 0) + (this.decimal || 0) / decimalFactor;
    this.updateScope();
};

/**
 * Updates class names of directive element based on the integer and decimal values.
 * Adds "empty-integer" or "empty-decimal" class names if one of the values is empty.
 */
MainCtrl.prototype.updateEmptyState = function() {
    var isIntegerEmpty = this.integer === null || this.integer === undefined;
    var isDecimalEmpty = this.decimal === null || this.decimal === undefined;
    this.element.toggleClass(EMPTY_INTEGER_CLASS_NAME, isIntegerEmpty);
    this.element.toggleClass(EMPTY_DECIMAL_CLASS_NAME, isDecimalEmpty);

    this.empty = isIntegerEmpty && isDecimalEmpty;
};

module.exports = MainCtrl;
