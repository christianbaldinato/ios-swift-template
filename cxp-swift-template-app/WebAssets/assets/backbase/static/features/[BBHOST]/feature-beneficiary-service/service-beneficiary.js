/**
 * ------------------------------------------------------------------------
 * Beneficiary Service entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'serviceBeneficiary';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {

    'use strict';

    // @ngInject
    var serviceBeneficiary = function($http, $httpParamSerializer) {
        var config = {
            counterparties: '/portalserver/services/rest/v1/counterparties',
            partyDataManagementEndpoint: '/portalserver/services/rest/v1/party-data-management/counter-party/{id}',
            partyDataManagementDetailsEndpoint: '/portalserver/services/rest/v1/party-data-management/counter-party/{id}/details'
        };

        return {

            /**
             * UUID generator
             *
             * @returns {string}
             */
            generateUUID: function generateUUID(){
                var d = new Date().getTime();
                if(window.performance && typeof window.performance.now === "function"){
                    d += performance.now(); //use high-precision timer if available
                }
                var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = (d + Math.random()*16)%16 | 0;
                    d = Math.floor(d/16);
                    return (c=='x' ? r : (r&0x3|0x8)).toString(16);
                });
                return uuid;
            },

            /**
             * Set the configuration of the service
             *
             * @param {Object} options
             */
            setConfig: function setConfig(options) {
                config = angular.extend({}, config, options);
            },

            /**
             * Group contact by initial
             *
             * @param {Array} contacts
             * @return {Object}
             */
            groupContactsByInitial: function groupContactsByInitial(contacts) {
                var groups = { };
                contacts.forEach(function(contact) {
                    var letter = contact.name[0].toUpperCase();
                    if (!groups[letter]) {
                        groups[letter] = [];
                    }
                    groups[letter].push(contact);
                });

                return groups;
            },

            /**
             * Retrieve contact details
             *
             * @param id
             * @returns {*}
             */
            getContactDetails: function getContactDetails(id) {
                var endpoint = config.partyDataManagementDetailsEndpoint.replace('{id}', id);
                return $http({
                    method: 'GET',
                    url: endpoint,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },

            /**
             * Retrieve contact
             *
             * @param id
             * @returns {*}
             */
            getContact: function getContact(id) {
                var endpoint = config.partyDataManagementEndpoint.replace('{id}', id);
                return $http({
                    method: 'GET',
                    url: endpoint,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },

            /**
             * Retrieve contact
             *
             * @param id
             * @returns {*}
             */
            getContacts: function getContacts(id) {
                var endpoint = config.counterparties;
                return $http({
                    method: 'GET',
                    url: endpoint,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },

            /**
             * Create new contact
             *
             * @param data
             * @returns {*}
             */
            postCreateContact: function postCreateContact(data) {
                var endpoint = config.partyDataManagementEndpoint.replace('{id}', data.id);
                return $http({
                    method: 'POST',
                    url: endpoint,
                    data: $httpParamSerializer(data),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },

            /**
             * Update contact
             *
             * @param data
             * @returns {*}
             */
            putUpdateContact: function putUpdateContact(data) {
                var endpoint = config.partyDataManagementEndpoint.replace('{id}', data.id);
                return $http({
                    method: 'PUT',
                    url: endpoint,
                    data: $httpParamSerializer(data),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            },

            /**
             * Delete contact
             *
             * @param id
             * @returns {*}
             */
            deleteContact: function(id) {
                var endpoint = config.partyDataManagementEndpoint.replace('{id}', id);
                return $http({
                    method: 'DELETE',
                    url: endpoint,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            }
        };
    };

    return serviceBeneficiary;
}));
