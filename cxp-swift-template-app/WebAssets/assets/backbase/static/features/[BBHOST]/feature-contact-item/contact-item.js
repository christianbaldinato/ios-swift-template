
/**
 * ------------------------------------------------------------------------
 * swipeableList entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'contactItem';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
    'use strict';

    function contactItem() {
        return {
            restrict: 'A',
            scope: {
                click: '&clickEvent',
                identifier: '@',
                title: '@',
                subtitle: '@',
                image: '@'
            },
            template: '<div class="panel-body contact-item">' +
                          '<div class="row clearfix">' +
                              '<div class="col-xs-2 ">' +
                                  '<avatar photo-url="{{image}}" name="{{title}}"></avatar>' +
                              '</div>' +
                              '<div class="col-xs-10">' +
                                  '<h4 class="account-list-item-title" ng-bind="title"></h4>' +
                                  '<small class="text-muted account-list-item-iban" ng-bind="subtitle"></small>' +
                              '</div>' +
                          '</div>' +
                      '</div>',
            link: function (scope, element) {

                var myElem = angular.element(element);

                element.bind('click', clickContactHandler);

                function clickContactHandler(e) {
                    // Only calls the method passed to scope.click() if element is not dragged
                    scope.click({id: scope.identifier});
                }
            }
        }
    }

    return contactItem;
}));
