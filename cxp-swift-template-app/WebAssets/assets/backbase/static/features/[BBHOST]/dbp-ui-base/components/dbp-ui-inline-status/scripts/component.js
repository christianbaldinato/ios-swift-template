'use strict';

var template = require('../templates/index.ng.html');
var MainCtrl = require('./controllers/main');

module.exports = {
    bindings: {
        text: '<',
        showSpinner: '='
    },
    controller: MainCtrl,
    template: template
};
