'use strict';

var utils = require('dbp-base').utils;

/**
 * Snackbar Plugin
 * Uses snackbar mobile plugin
 * @see @link https://stash.backbase.com/projects/SDK-EXT/repos/snackbar/browse
 * @return {Object}
 * @public
 */
function Snackbar() {
    var PLUGIN_NAME = 'Snackbar';

    return {
        /**
         * Shows success snackbar with given message
         * @param {String} message - Message to be shown
         * @returns {Promise}
         * @public
         */
        success: function success(message) {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).success(resolve, reject, message || '');
            });
        },

        /**
         * Shows error snackbar with given message
         * @param {String} message - Message to be shown
         * @returns {Promise}
         * @public
         */
        error: function error(message) {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).error(resolve, reject, message || '');
            });
        },

        /**
         * Shows warning snackbar with given message
         * @param {String} message - Message to be shown
         * @returns {Promise}
         * @public
         */
        warning: function warning(message) {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).warning(resolve, reject, message || '');
            });
        },

        /**
         * Shows info snackbar with given message
         * @param {String} message - Message to be shown
         * @returns {Promise}
         * @public
         */
        info: function info(message) {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).info(resolve, reject, message || '');
            });
        },

        /**
         * Shows noInternet snackbar with given message
         * @param {String} message - Message to be shown
         * @returns {Promise}
         * @public
         */
        noInternet: function noInternet(message) {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).noInternet(resolve, reject, message || '');
            });
        },

        /**
         * Method to hide the snackbar
         * @returns {Promise}
         * @public
         */
        hide: function hide() {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).hide(resolve, reject);
            });
        }
    };
}

module.exports = Snackbar;
