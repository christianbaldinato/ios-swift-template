'use strict';

var _ = require('lodash');
var DatePicker = require('./date-picker');
var utils = require('dbp-base').utils;

describe('DatePicker', function() {
    var datePickerMock = {
        show: _.noop,
        hide: _.noop
    };

    beforeEach(function() {
        this.datePicker = DatePicker();
    });

    describe('show', function() {
        it('should show date picker with given date format, and min and max dates', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'show');

            this.datePicker.show({
                dateFormat: 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ',
                minDate: '2015-10-05T14:00:00+01:00',
                maxDate: '2016-10-05T14:00:00+01:00'
            });

            expect(datePickerMock.show).toHaveBeenCalled();

            expect(datePickerMock.show.calls.argsFor(0)[2]).toBe('yyyy-MM-dd\'T\'HH:mm:ssZZZZZ');
            expect(datePickerMock.show.calls.argsFor(0)[3]).toBe('2015-10-05T14:00:00+01:00');
            expect(datePickerMock.show.calls.argsFor(0)[4]).toBe('2016-10-05T14:00:00+01:00');
        });

        it('should use empty string by default for minDate and maxDate', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'show');

            this.datePicker.show({
                dateFormat: 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ'
            });

            expect(datePickerMock.show.calls.argsFor(0)[2]).toBe('yyyy-MM-dd\'T\'HH:mm:ssZZZZZ');
            expect(datePickerMock.show.calls.argsFor(0)[3]).toBe('');
            expect(datePickerMock.show.calls.argsFor(0)[4]).toBe('');
        });

        it('should use default format by default', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'show');

            this.datePicker.show();

            expect(datePickerMock.show.calls.argsFor(0)[2]).toBe('yyyy-MM-dd\'T\'HH:mm:ssZZZZZ');
        });

        it('should return a promise will be resolved in case of success', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'show').and.callFake(function(onSuccess) {
                onSuccess();
            });

            this.datePicker.show()
                .then(done)
                .catch(done.fail);
        });

        it('should return a promise that will be rejected in case of error', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'show').and.callFake(function(onSuccess, onError) {
                onError();
            });

            this.datePicker.show()
                .then(done.fail)
                .catch(done);
        });

        it('should return a promise that will be rejected if the plugin is not available', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
            this.datePicker.show()
                .then(done.fail)
                .catch(done);
        });
    });

    describe('hide', function() {
        it('should hide date picker', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'hide');

            this.datePicker.hide();

            expect(datePickerMock.hide).toHaveBeenCalled();
        });

        it('should return a promise that will be resolved in case of success', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'hide').and.callFake(function(onSuccess) {
                onSuccess();
            });

            this.datePicker.hide()
                .then(done)
                .catch(done.fail);
        });

        it('should return a promise that will be rejected in case of error', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(datePickerMock);
            spyOn(datePickerMock, 'hide').and.callFake(function(onSuccess, onError) {
                onError();
            });

            this.datePicker.hide()
                .then(done.fail)
                .catch(done);
        });

        it('should return a promise that will be rejected if the plugin is not available', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
            this.datePicker.hide()
                .then(done.fail)
                .catch(done);
        });
    });
});
