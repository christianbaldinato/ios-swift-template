/**
 * ------------------------------------------------------------------------
 * Entry point
 * ------------------------------------------------------------------------
 */

'use strict';
var ng = require('angular');
var Component = require('./scripts/component');

module.exports = ng.module('dbp-ui-inline-status', [])
    .component('dbpUiInlineStatus', Component);

