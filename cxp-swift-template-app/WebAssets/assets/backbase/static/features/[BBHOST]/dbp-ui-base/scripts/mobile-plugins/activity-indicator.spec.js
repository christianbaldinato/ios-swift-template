'use strict';
var _ = require('lodash');
var ActivityIndicator = require('./activity-indicator');
var utils = require('dbp-base').utils;

describe('ActivityIndicator', function() {
    var activityIndicatorMock = {
        show: _.noop,
        hide: _.noop
    };
    beforeEach(function() {
        this.activityIndicator = ActivityIndicator();
    });
    afterEach(function() {
        this.activityIndicator = null;
    });
    describe('show', function() {
        it('should show activity indicator with given message', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(activityIndicatorMock);
            spyOn(activityIndicatorMock, 'show');

            this.activityIndicator.show('Test message');

            expect(activityIndicatorMock.show).toHaveBeenCalled();
            expect(activityIndicatorMock.show.calls.argsFor(0)[2]).toBe('Test message');
        });

        it('should return a promise will be resolved in case of success', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(activityIndicatorMock);
            spyOn(activityIndicatorMock, 'show').and.callFake(function(onSuccess) {
                onSuccess();
            });

            this.activityIndicator.show()
                .then(done)
                .catch(done.fail);
        });

        it('should return a promise that will be rejected in case of error', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(activityIndicatorMock);
            spyOn(activityIndicatorMock, 'show').and.callFake(function(onSuccess, onError) {
                onError();
            });

            this.activityIndicator.show()
                .then(done.fail)
                .catch(done);
        });

        it('should return a promise that will be rejected if the plugin is not available', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
            this.activityIndicator.show()
                .then(done.fail)
                .catch(done);
        });
    });

    describe('hide', function() {
        it('should hide activity indicator', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(activityIndicatorMock);
            spyOn(activityIndicatorMock, 'hide');

            this.activityIndicator.hide();

            expect(activityIndicatorMock.hide).toHaveBeenCalled();
        });

        it('should return a promise that will be resolved in case of success', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(activityIndicatorMock);
            spyOn(activityIndicatorMock, 'hide').and.callFake(function(onSuccess) {
                onSuccess();
            });

            this.activityIndicator.hide()
                .then(done)
                .catch(done.fail);
        });

        it('should return a promise that will be rejected in case of error', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(activityIndicatorMock);
            spyOn(activityIndicatorMock, 'hide').and.callFake(function(onSuccess, onError) {
                onError();
            });

            this.activityIndicator.hide()
                .then(done.fail)
                .catch(done);
        });

        it('should return a promise that will be rejected if the plugin is not available', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
            this.activityIndicator.hide()
                .then(done.fail)
                .catch(done);
        });
    });
});
