'use strict';

var _ = require('lodash');
var Camera = require('./camera');
var utils = require('dbp-base').utils;

describe('Camera', function() {
    var cameraMock = {
        getImageFromCamera: _.noop,
        getImageFromGallery: _.noop
    };

    beforeEach(function() {
        this.camera = Camera();
    });

    describe('getImageFromCamera', function() {
        it('should get image from camera with given options', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(cameraMock);
            spyOn(cameraMock, 'getImageFromCamera');

            this.camera.getImageFromCamera(500, 500);

            expect(cameraMock.getImageFromCamera).toHaveBeenCalled();
            expect(cameraMock.getImageFromCamera.calls.argsFor(0)[2]).toBe(500);
            expect(cameraMock.getImageFromCamera.calls.argsFor(0)[3]).toBe(500);
        });

        it('should return a promise will be resolved in case of success', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(cameraMock);
            spyOn(cameraMock, 'getImageFromCamera').and.callFake(function(onSuccess) {
                onSuccess({});
            });

            this.camera.getImageFromCamera()
                .then(done)
                .catch(done.fail);
        });

        it('should return a promise that will be rejected in case of error', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(cameraMock);
            spyOn(cameraMock, 'getImageFromCamera').and.callFake(function(onSuccess, onError) {
                onError();
            });

            this.camera.getImageFromCamera()
                .then(done.fail)
                .catch(done);
        });

        it('should return a promise that will be rejected if the plugin is not available', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());

            this.camera.getImageFromCamera()
                .then(done.fail)
                .catch(done);
        });
    });

    describe('getImageFromGallery', function() {
        it('should get image from gallery with given options', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(cameraMock);
            spyOn(cameraMock, 'getImageFromGallery');

            this.camera.getImageFromGallery(500, 500);

            expect(cameraMock.getImageFromGallery).toHaveBeenCalled();
            expect(cameraMock.getImageFromGallery.calls.argsFor(0)[2]).toBe(500);
            expect(cameraMock.getImageFromGallery.calls.argsFor(0)[3]).toBe(500);
        });

        it('should return a promise will be resolved in case of success', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(cameraMock);
            spyOn(cameraMock, 'getImageFromGallery').and.callFake(function(onSuccess) {
                onSuccess({});
            });

            this.camera.getImageFromGallery()
                .then(done)
                .catch(done.fail);
        });

        it('should return a promise that will be rejected in case of error', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(cameraMock);
            spyOn(cameraMock, 'getImageFromGallery').and.callFake(function(onSuccess, onError) {
                onError();
            });

            this.camera.getImageFromGallery()
                .then(done.fail)
                .catch(done);
        });

        it('should return a promise that will be rejected if the plugin is not available', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());

            this.camera.getImageFromGallery()
                .then(done.fail)
                .catch(done);
        });
    });
});
