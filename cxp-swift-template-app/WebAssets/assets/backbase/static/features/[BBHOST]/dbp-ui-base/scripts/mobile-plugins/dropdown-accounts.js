'use strict';

var utils = require('dbp-base').utils;

/**
 * Dropdown Accounts factory
 * Uses dropdown accounts mobile plugin
 * @see @link https://stash.backbase.com/projects/SDK-EXT/repos/dropdown/browse/android/src/main/java/extensions/backbase/com/dropdown/accounts
 * @returns {Object}
 * @public
 */
function DropdownAccounts() {
    var PLUGIN_NAME = 'DropdownAccounts';

    return {
        /**
         * Method to show the dropdown Accounts
         * @param {object} options
         * @property {string} options.title - Accounts dropdown title
         * @property {Array} options.items - Contains array of accounts
         * @return {object} Promise
         */
        show: function show(options) {
            return new Promise(function(resolve, reject) {
                options = utils.defaults(options, {
                    title: '',
                    items: []
                });
                utils.getMobilePlugin(PLUGIN_NAME).show(resolve, reject, options.title, JSON.stringify(options.items));
            });
        }
    };
}

module.exports = DropdownAccounts;
