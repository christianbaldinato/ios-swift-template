'use strict';

var INLINE_STATUS_CLASS_NAME = 'dbp-ui-inline-status';

/**
 * @ngInject
 */
function MainCtrl($element) {
    this.el = $element;
}

MainCtrl.prototype.$postLink = function() {
    this.el.addClass(INLINE_STATUS_CLASS_NAME);
};

module.exports = MainCtrl;
