'use strict';

var utils = require('dbp-base').utils;

/**
 * Activity Indicator factory
 * Uses ActivityIndicator mobile plugin
 * See https://stash.backbase.com/projects/SDK-EXT/repos/activity-indicator/browse
 * @returns {Object}
 * @public
 */

function ActivityIndicator() {
    var PLUGIN_NAME = 'ActivityIndicator';

    return {
        /**
         * Shows activity indicator with given message
         * @param {String} [message]
         * @returns {Promise}
         * @public
         */
        show: function show(message) {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).show(resolve, reject, message || '');
            });
        },

        /**
         * Hides activity indicator
         * @returns {Promise}
         * @public
         */
        hide: function hide() {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).hide(resolve, reject);
            });
        }
    };
}

module.exports = ActivityIndicator;
