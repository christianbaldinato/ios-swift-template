'use strict';

var utils = require('dbp-base').utils;
/**
 * Alert Dialog
 * Uses AlertDialog mobile plugin
 * @link https://stash.backbase.com/projects/SDK-EXT/repos/alert-dialog/browse
 * @return {Object}
 * @public
 */
function AlertDialog() {
    var PLUGIN_NAME = 'AlertDialog';
    /**
     * Determines the kind of button to be rendered by the system
     */
    var ButtonType = {
        POSITIVE: 'POSITIVE',
        NEGATIVE: 'NEGATIVE',
        NEUTRAL: 'NEUTRAL'
    };

    return {
        ButtonType: ButtonType,

        /**
         * Method to show the alert dialog
         * @param {object} options
         * @property {string} options.title - Alert title
         * @property {string} options.message - Alert message
         * @property {Array} options.buttons - Alert buttons
         * @return {object} Promise
         */
        show: function show(options) {
            return new Promise(function(resolve, reject) {
                options = utils.defaults(options, {
                    title: '',
                    message: '',
                    buttons: []
                });
                utils.getMobilePlugin(PLUGIN_NAME).show(resolve, reject, options.title, options.message, JSON.stringify(options.buttons));
            });
        }
    };
}

module.exports = AlertDialog;
