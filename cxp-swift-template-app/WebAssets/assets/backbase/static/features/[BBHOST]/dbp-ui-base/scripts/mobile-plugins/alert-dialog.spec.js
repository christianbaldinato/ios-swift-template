'use strict';

var _ = require('lodash');
var AlertDialog = require('./alert-dialog');
var utils = require('dbp-base').utils;

describe('AlertDialog', function() {
    var options = {
        title: 'abc',
        message: 'mock message',
        buttons: [{
            type: 'NEGATIVE',
            text: 'Cancel',
            callbackFn: 'onCancelDialog'
        }, {
            type: 'POSITIVE',
            text: 'Ok',
            callbackFn: 'onOkDialog'
        }]
    };

    var alertDialogMock = {
        show: _.noop
    };

    beforeEach(function() {
        this.alertDialog = AlertDialog();
    });

    it('should show alert dialog with given options', function() {

        spyOn(utils, 'getMobilePlugin').and.returnValue(alertDialogMock);
        spyOn(alertDialogMock, 'show');

        this.alertDialog.show(options);

        expect(alertDialogMock.show).toHaveBeenCalled();
        expect(alertDialogMock.show.calls.argsFor(0)[2]).toBe(options.title);
        expect(alertDialogMock.show.calls.argsFor(0)[3]).toBe(options.message);
        expect(alertDialogMock.show.calls.argsFor(0)[4]).toBe(JSON.stringify(options.buttons));
    });

    it('should return a promise will be resolved in case of success', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(alertDialogMock);
        spyOn(alertDialogMock, 'show').and.callFake(function(onSuccess) {
            onSuccess();
        });

        this.alertDialog.show()
            .then(done)
            .catch(done.fail);
    });

    it('should return a promise that will be rejected in case of error', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(alertDialogMock);
        spyOn(alertDialogMock, 'show').and.callFake(function(onSuccess, onError) {
            onError();
        });

        this.alertDialog.show()
            .then(done.fail)
            .catch(done);
    });

    it('should return a promise that will be rejected if the plugin is not available', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
        this.alertDialog.show()
            .then(done.fail)
            .catch(done);
    });
});
