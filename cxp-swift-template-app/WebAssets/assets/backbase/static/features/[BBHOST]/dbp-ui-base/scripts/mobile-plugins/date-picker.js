'use strict';

var utils = require('dbp-base').utils;

/**
 * The plugin that shows or hides date picker.
 * Uses https://stash.backbase.com/projects/SDK-EXT/repos/date-picker/browse under the hood.
 * @returns {Object}
 * @public
 */
function DatePicker() {
    // ISO-8601 date format represented as Unicode Technical Standard
    // See https://en.wikipedia.org/wiki/ISO_8601 and http://unicode.org/reports/tr35/tr35-25.html
    var DEFAULT_DATE_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ssZZZZZ';
    var PLUGIN_NAME = 'DatePicker';

    return {
        /**
         * Shows date picker with given date
         * @param    {Object} options
         * @property {String} [options.dateFormat]  Format of the dates. It will be applied for both input and output dates. See http://www.unicode.org/reports/tr35/tr35-19.html#Date_Format_Patterns for the patterns.
         * @property {String} [options.minDate]     Min date accepted by the component. Must comply with the format specified by dateFormat parameter.
         * @property {String} [options.maxDate]     Max date accepted by the component. The date must comply with the format specified by dateFormat parameter.
         * @returns  {Promise}
         * @public
         */
        show: function show(options) {
            options =  utils.defaults(options, {
                dateFormat: DEFAULT_DATE_FORMAT,
                minDate: '',
                maxDate: ''
            });

            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).show(resolve, reject, options.dateFormat, options.minDate, options.maxDate);
            });
        },

        /**
         * Hides date picker
         * @returns {Promise}
         * @public
         */
        hide: function hide() {
            return new Promise(function(resolve, reject) {
                utils.getMobilePlugin(PLUGIN_NAME).hide(resolve, reject);
            });
        }
    };
}

module.exports = DatePicker;
