'use strict';

var _ = require('lodash');
var Snackbar = require('./snackbar');
var utils = require('dbp-base').utils;

describe('Snackbar', function() {
    var snackbarMock = {
        success: _.noop,
        warning: _.noop,
        error: _.noop,
        info: _.noop,
        noInternet: _.noop,
        hide: _.noop
    };

    beforeEach(function() {
        this.snackbar = Snackbar();
    });

    function testSnackbarPlugin(methodName) {
        describe('`' + methodName + '` Method', function() {
            it('should show snackbar with given message', function() {
                spyOn(utils, 'getMobilePlugin').and.returnValue(snackbarMock);
                spyOn(snackbarMock, methodName);

                this.snackbar[methodName]('Test message');

                expect(snackbarMock[methodName]).toHaveBeenCalled();
                expect(snackbarMock[methodName].calls.argsFor(0)[2]).toBe('Test message');
            });

            it('should return a promise will be resolved in case of success', function(done) {
                spyOn(utils, 'getMobilePlugin').and.returnValue(snackbarMock);
                spyOn(snackbarMock, methodName).and.callFake(function(onSuccess) {
                    onSuccess();
                });

                this.snackbar[methodName]()
                    .then(done)
                    .catch(done.fail);
            });

            it('should return a promise that will be rejected in case of error', function(done) {
                spyOn(utils, 'getMobilePlugin').and.returnValue(snackbarMock);
                spyOn(snackbarMock, methodName).and.callFake(function(onSuccess, onError) {
                    onError();
                });

                this.snackbar[methodName]()
                    .then(done.fail)
                    .catch(done);
            });

            it('should return a promise that will be rejected if the plugin is not available', function(done) {
                spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
                this.snackbar[methodName]()
                    .then(done.fail)
                    .catch(done);
            });
        });
    }

    describe('`hide` Method', function() {
        it('should show snackbar with given message', function() {
            spyOn(utils, 'getMobilePlugin').and.returnValue(snackbarMock);
            spyOn(snackbarMock, 'hide');

            this.snackbar.hide();

            expect(snackbarMock['hide']).toHaveBeenCalled();
        });

        it('should return a promise will be resolved in case of success', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(snackbarMock);
            spyOn(snackbarMock, 'hide').and.callFake(function(onSuccess) {
                onSuccess();
            });

            this.snackbar.hide()
                .then(done)
                .catch(done.fail);
        });

        it('should return a promise that will be rejected in case of error', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(snackbarMock);
            spyOn(snackbarMock, 'hide').and.callFake(function(onSuccess, onError) {
                onError();
            });

            this.snackbar.hide()
                .then(done.fail)
                .catch(done);
        });

        it('should return a promise that will be rejected if the plugin is not available', function(done) {
            spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
            this.snackbar.hide()
                .then(done.fail)
                .catch(done);
        });
    });

    testSnackbarPlugin('success');
    testSnackbarPlugin('error');
    testSnackbarPlugin('info');
    testSnackbarPlugin('warning');
    testSnackbarPlugin('noInternet');
});
