'use strict';

var utils = require('dbp-base').utils;

/**
 * Camera factory
 * Uses Camera mobile plugin
 * @link https://stash.backbase.com/projects/SDK-EXT/repos/camera/browse
 * @returns {Object}
 * @public
 */

function Camera() {
    var PLUGIN_NAME = 'Camera';

    return {
        /**
         * Method to get the image from camera
         * @param {Number} maxWidth - Maximum width
         * @param {Number} maxHeight - Maximum height
         * @returns {Promise}
         * @public
         */
        getImageFromCamera: function getImageFromCamera(maxWidth, maxHeight) {
            return new Promise((resolve, reject) => {
                utils.getMobilePlugin(PLUGIN_NAME).getImageFromCamera(resolve, reject, maxWidth, maxHeight);
            });
        },

        /**
         * Method to get the image from gallery
         * @param {Number} maxWidth - Maximum width
         * @param {Number} maxHeight - Maximum height
         * @returns {Promise}
         * @public
         */
        getImageFromGallery: function getImageFromGallery(maxWidth, maxHeight) {
            return new Promise((resolve, reject) => {
                utils.getMobilePlugin(PLUGIN_NAME).getImageFromGallery(resolve, reject, maxWidth, maxHeight);
            });
        }
    };
}

module.exports = Camera;
