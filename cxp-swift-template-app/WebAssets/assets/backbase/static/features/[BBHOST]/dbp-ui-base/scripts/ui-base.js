/**
 * ------------------------------------------------------------------------
 * UI-Base Angular Module
 * ------------------------------------------------------------------------
 */

'use script';

var ng = require('angular');
// include components
var dbpUiInlineStatus = require('../components/dbp-ui-inline-status');
// include plugins
var dbpUiMobilePlugins = require('./mobile-plugins');
// include utilities
var dbpUiUtils = require('./utils');
var dbpUiDateOrdinal = require('./utils/date-ordinal');

/**
 * @ngInject
 * @name Config
 * @private
 * @kind function AngularJS configuration provider
 * @link https://docs.angularjs.org/api/ng/type/angular.Module#config
 */
function Config($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}
var dbpUiBase = ng.module('dbp-ui-base', [
        dbpUiInlineStatus.name,
        dbpUiMobilePlugins.name
    ])
    .constant('dbpUiUtils', dbpUiUtils)
    .config(Config)
    .filter('dbpUiDateOrdinal', dbpUiDateOrdinal);

module.exports = dbpUiBase;
