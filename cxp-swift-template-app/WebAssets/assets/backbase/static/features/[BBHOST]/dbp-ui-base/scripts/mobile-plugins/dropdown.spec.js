'use strict';

var _ = require('lodash');
var Dropdown = require('./dropdown');
var utils = require('dbp-base').utils;

describe('Dropdown', function() {
    var options = {
        title: 'abc',
        items: [
            ['Option A'],
            ['Option B'],
            ['Option C']
        ],
        selectedIndex: 1
    };

    var dropdownMock = {
        show: _.noop
    };

    beforeEach(function() {
        this.dropdown = Dropdown();
    });

    it('should show dropdown with given options', function() {
        spyOn(utils, 'getMobilePlugin').and.returnValue(dropdownMock);
        spyOn(dropdownMock, 'show');

        this.dropdown.show(options);

        expect(dropdownMock.show).toHaveBeenCalled();
        expect(dropdownMock.show.calls.argsFor(0)[2]).toBe(options.title);
        expect(dropdownMock.show.calls.argsFor(0)[3]).toBe(JSON.stringify(options.items));
        expect(dropdownMock.show.calls.argsFor(0)[4]).toBe(1);
    });

    it('should return a promise will be resolved in case of success', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(dropdownMock);
        spyOn(dropdownMock, 'show').and.callFake(function(onSuccess) {
            onSuccess();
        });

        this.dropdown.show()
            .then(done)
            .catch(done.fail);
    });

    it('should return a promise that will be rejected in case of error', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(dropdownMock);
        spyOn(dropdownMock, 'show').and.callFake(function(onSuccess, onError) {
            onError();
        });

        this.dropdown.show()
            .then(done.fail)
            .catch(done);
    });

    it('should return a promise that will be rejected if the plugin is not available', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
        this.dropdown.show()
            .then(done.fail)
            .catch(done);
    });
});
