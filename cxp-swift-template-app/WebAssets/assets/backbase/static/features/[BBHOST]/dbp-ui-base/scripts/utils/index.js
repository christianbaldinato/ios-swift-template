'use script';

var utils = require('dbp-base').utils;

/*
 * Safe way of calling $scope.$apply function.
 * $scope.$apply will only be called if application is not in $apply or $digest phase.
 *
 * @param {Object} scope Angular Scope object.
 * @param {String|Function} [exp] An angular expression to be executed.
 * @public
 */
exports.safeApply = function safeApply(scope, exp) {
    var phase = scope.$root && scope.$root.$$phase;
    if (phase === '$apply' || phase === '$digest') {
        scope.$eval(exp);
    } else {
        scope.$apply(exp);
    }
};

/*
 * Method to force hide the soft keyboard
 * @returns void
 * @public
 */
exports.hideKeyboard = function hideKeyboard() {
    var element = document.activeElement;
    if (utils.isElement(element)) {
        var readonly = element.hasAttribute('readonly');
        var disabled = element.hasAttribute('disabled');

        if (!readonly) {
            element.setAttribute('readonly', 'readonly'); // hide on input field
        }
        if (!disabled) {
            element.setAttribute('disabled', 'disabled'); // hide on text area field
        }

        setTimeout(function() {
            element.blur(); //close the keyboard
            // Remove readonly attribute after keyboard is hidden
            if (!readonly) {
                element.removeAttribute('readonly');
            }
            if (!disabled) {
                element.removeAttribute('disabled');
            }
        }, 100);
    }
};

/**
 * Trigger browser's reflow cycle for provided element
 * @param  {Object} element jqLite object or HTML Element
 * @public
 */
exports.forceReflow = function forceReflow(element) {
    if(!element) {
        throw new Error('Missing `element` argument. Should be jqLite object or HTMLElement.');
    }

    if(utils.isFunction(element.prop)) {
        // if element is jqLite object
        element.prop('offsetHeight');
    } else {
        // regular element
        element.offsetHeight;
    }
}
