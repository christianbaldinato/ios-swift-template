'use strict';

var ng = require('angular');
var name = 'dbpUiMobilePlugins';
// include plugins
var ActivityIndicator = require('./activity-indicator');
var AlertDialog = require('./alert-dialog');
var DatePicker = require('./date-picker');
var Dropdown = require('./dropdown');
var DropdownAccounts = require('./dropdown-accounts');
var Snackbar = require('./snackbar');
var Camera = require('./camera');

module.exports = ng.module(name, [])
    // plugins
    .factory('dbpUiActivityIndicator', ActivityIndicator)
    .factory('dbpUiAlertDialog', AlertDialog)
    .factory('dbpUiDatePicker', DatePicker)
    .factory('dbpUiDropdown', Dropdown)
    .factory('dbpUiDropdownAccounts', DropdownAccounts)
    .factory('dbpUiSnackbar', Snackbar)
    .factory('dbpUiCamera', Camera);
