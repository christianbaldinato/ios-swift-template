'use script';
/**
 * Ordinal date filter extends date filter with an extra format element 'oo'.
 * @param {Object} $filter
 * @return formatted date
 * @ngInject
 */
function dateOrdinal($filter) {
    return function(input, format) {
        var dateFilter = $filter('date')(input, format);
        var regex = /\d+(?=oo)/g;
        var matchDay = regex.exec(dateFilter);
        if (matchDay) {
            var day = matchDay[0];
            var suffixes = ['th', 'st', 'nd', 'rd'];
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            return dateFilter.replace('oo', suffix);
        } else {
            return dateFilter;
        }
    };
}

module.exports = dateOrdinal;
