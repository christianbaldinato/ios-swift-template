'use strict';

var _ = require('lodash');
var DropdownAccounts = require('./dropdown-accounts');
var utils = require('dbp-base').utils;

describe('DropdownAccounts', function() {
    var options = {
        title: 'abc',
        items: [
            ['primary account', '2334'],
            ['secondary account', '2345']
        ]
    };

    var dropdownAccountsMock = {
        show: _.noop
    };

    beforeEach(function() {
        this.dropdownAccounts = DropdownAccounts();
    });

    it('should show dropdown accounts with given options', function() {
        spyOn(utils, 'getMobilePlugin').and.returnValue(dropdownAccountsMock);
        spyOn(dropdownAccountsMock, 'show');

        this.dropdownAccounts.show(options);

        expect(dropdownAccountsMock.show).toHaveBeenCalled();
        expect(dropdownAccountsMock.show.calls.argsFor(0)[2]).toBe(options.title);
        expect(dropdownAccountsMock.show.calls.argsFor(0)[3]).toBe(JSON.stringify(options.items));
    });

    it('should return a promise will be resolved in case of success', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(dropdownAccountsMock);
        spyOn(dropdownAccountsMock, 'show').and.callFake(function(onSuccess) {
            onSuccess();
        });

        this.dropdownAccounts.show()
            .then(done)
            .catch(done.fail);
    });

    it('should return a promise that will be rejected in case of error', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(dropdownAccountsMock);
        spyOn(dropdownAccountsMock, 'show').and.callFake(function(onSuccess, onError) {
            onError();
        });

        this.dropdownAccounts.show()
            .then(done.fail)
            .catch(done);
    });

    it('should return a promise that will be rejected if the plugin is not available', function(done) {
        spyOn(utils, 'getMobilePlugin').and.returnValue(Promise.reject());
        this.dropdownAccounts.show()
            .then(done.fail)
            .catch(done);
    });
});
