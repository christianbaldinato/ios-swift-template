'use strict';

var utils = require('dbp-base').utils;

/**
 * Dropdown factory
 * Uses dropdown mobile plugin
 * @see @link https://stash.backbase.com/projects/SDK-EXT/repos/dropdown/browse
 * @returns {Object}
 * @public
 */
function Dropdown() {
    var PLUGIN_NAME = 'Dropdown';
    return {
        /**
         * Method to show the dropdown
         * @param {object} options
         * @property {string} options.title - Dropdown title
         * @property {Array} options.items - Contains array of dropdown items
         * @property {Array} options.selectedIndex - Index of the selected item. Optional.
         * @return {object} Promise
         */
        show: function show(options) {
            return new Promise(function(resolve, reject) {
                options = utils.defaults(options, {
                    selectedIndex: 0,
                    title: '',
                    items: []
                });
                utils.getMobilePlugin(PLUGIN_NAME).show(resolve, reject, options.title, JSON.stringify(options.items), options.selectedIndex);
            });
        }
    };
}

module.exports = Dropdown;
