/**
 * ------------------------------------------------------------------------
 * formatCurrency entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'formatAmount';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
    'use strict';

    var CURRENCY_SYMBOLS = {
        'USD': '$', // US Dollar
        'EUR': '€', // Euro
        'GBP': '£', // British Pound Sterling
        'JPY': '¥' // Japanese Yen
    };

    /**
     * @ngInject
     */
    function formatAmount($filter) {
        return {
            restrict: 'A',
            template: '<span class="integer">{{integer}}</span>{{separator}}' +
                      '<span class="decimals" style="font-size: 50%;vertical-align: text-top;">{{decimals}}</span>',
            link: function(scope, element, attrs) {
                element.addClass('format-amount');

                function format() {
                    var value = attrs.formatAmount;
                    var currencySymbol = CURRENCY_SYMBOLS[attrs.formatAmountCurrency];
                    var creditDebit = attrs.formatAmountCreditDebit;

                    if (creditDebit === 'DBIT') {
                        value = Math.abs(value) * -1;
                    }

                    if (typeof value ==='undefined' || value === '') {
                        scope.integer    = '';
                        scope.decimals   = '';
                        scope.separator  = '';
                        scope.value      = 0;
                    }
                    else {
                        var formatted = $filter('currency')(value, currencySymbol);
                        scope.separator  = formatted.match(/([,.])[0-9]{2}$/)[1];
                        var split = formatted.split(scope.separator);
                        scope.integer    = split[0];
                        scope.decimals   = split[1];
                        scope.value      = value;
                    }
                }

                scope.$watch(function() {
                    return [attrs.formatAmount, attrs.formatAmountCurrency]
                }, format, true);
            }
        }
    }

    return formatAmount;
}));
