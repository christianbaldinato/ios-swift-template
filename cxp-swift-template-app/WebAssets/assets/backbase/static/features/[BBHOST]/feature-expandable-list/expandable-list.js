/**
 * ------------------------------------------------------------------------
 * expandableList entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'expandableList';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
    'use strict';

    function expandableList() {
        return {
            restrict: 'A',
            link: function (scope, element) {

                var interactionMap = {
                    expanded: false
                };

                element.on('click', toggleList);

                function toggleList(e) {
                    interactionMap.expanded = !interactionMap.expanded;
                    element[0].classList.toggle('is-open');
                    scope.mainCtrl.expandableListExpanded(interactionMap.expanded);
                }

            }
        }
    }

    return expandableList;
}));
