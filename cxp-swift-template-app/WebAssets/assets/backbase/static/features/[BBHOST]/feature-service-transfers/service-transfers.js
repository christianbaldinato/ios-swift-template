/**
 * ------------------------------------------------------------------------
 * expandableList entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'serviceTransfers';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {

    'use strict';

    // todo: test failure
    /* 
      {
        "code": "400",
        "message": "'accountId' must be specified.",
        "errors" : [{
            "code": 400,
            " message" : " 'accountId' must be specified."
        }
        ]
      }
    */

    var Status = {
        AUTH_REQUIRED: 'AUTHORIZATION_REQUIRED'
    };

    function generateUUID () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x7 | 0x8)).toString(16);
        });
        return uuid;
    }

    function matchTransfer(transferA, transferB) {
        return transferA.accountId == transferB.accountId &&
            transferA.instructedAmount == transferB.instructedAmount &&
            transferA.instructedCurrency == transferB.instructedCurrency &&
            transferA.counterpartyName == transferB.counterpartyName &&
            transferA.counterpartyIban == transferB.counterpartyIban;
    }

    function unwrap(response) {
        return response.data;
    }

    function handleError(requestDetails) {
        // Return error handler.
        return function(err) {
            // Normalise error.
            throw {
                message: err.message,
                requestDetails: requestDetails,
                isUserError: err.status === 400,
                __httpError: err
            };
        }
    }

    // @ngInject
    var serviceTransfers = function($http) {
        var config = {
            createEndpoint: '/portalserver/payment-orders',
            detailsEndpoint: '/portalserver/payment-orders/{id}',
            cancelEndpoint: '/portalserver/payment-orders/{id}',
            submitEndpoint: '/portalserver/payment-orders/{id}/submit',
            unsubmittedEndpoint: '/portalserver/payment-orders/{accountIds}/unsubmitted'
        };

        function findTransfer(transfer) {
            return getUnsubmitted([transfer.accountId])
                .then(function(transfers) {
                    var found = null;
                    var i;
                    // Search in reverse order for transfer.
                    for (i = transfers.length - 1; found ===null && i >= 0; i--){
                        if (matchTransfer(transfer, transfers[i])) {
                            found = transfers[i];
                        }
                    }

                    return found;
                });
        }

        function getUnsubmitted(accountIds) {
            var ids = accountIds.join(',');
            var unsubmittedEndpoint = config.unsubmittedEndpoint.replace('{accountIds}', ids);
            return $http.get(unsubmittedEndpoint)
                .then(unwrap)
                .catch(handleError({accountIds: accountIds}));
        }


        return {
            /**
             *
             * @param options
             */
            setConfig: function setConfig(options) {
                config = angular.extend({}, config, options);
            },

            /**
             *
             * @param transfer
             * @return {*}
             */
            create: function create(transfer) {

                // Add transfer ID to endpoint.
                if (!transfer.id) {
                    // auto-generate transfer ID.
                    transfer.id = generateUUID();
                }
                var endpoint = config.createEndpoint.replace('{id}', transfer.id);

                // transfer = serializer.serializetransfer(transfer);
                return $http.put(endpoint, transfer)
                    .then(unwrap)
                    .then(function(response) {
                        var returnResponse = {
                            message: response.message || 'Transfer submitted',
                            transfer: transfer,
                            __httpResponse: response
                        };

                        // Normalise response to include created transferId (ugly but necessary).
                        if (!response.transferId) {
                            return findTransfer(transfer)
                                .then(function(newTransfer) {
                                    if (newTransfer && newTransfer.id) {
                                        returnResponse.transferId = newTransfer.id;
                                    }
                                    return returnResponse;
                                });
                        }
                        else {
                            returnResponse.transferId = response.transferId;
                            return returnResponse;
                        }
                    })
                    .catch(handleError({transfer: transfer}));
            },
            /**
             *
             * @param id
             * @return {*}
             */
            get: function get(id) {
                var endpoint = config.detailsEndpoint.replace('{id}', id);
                return $http.get(endpoint)
                    .then(unwrap)
                    .catch(handleError({id: id}));
            },

            /**
             *
             * @param id
             * @return {*}
             */
            submit: function submit(id) {
                var endpoint = config.submitEndpoint.replace('{id}', id);
                return $http.post(endpoint, {})
                        .then(unwrap)
                        .then(function(response) {
                            if (response.status === Status.AUTH_REQUIRED) {
                                // todo: handle auth requirements
                                throw response;
                            }

                            // Normalise response.
                            return {
                                message: response.message || 'Transfer submitted',
                                id: id,
                                __httpResponse: response
                            };
                        })
                        .catch(handleError({id: id}));
            },

            /**
             *
             * @param id
             * @return {*}
             */
            cancel: function cancel(id) {
                var endpoint = config.cancelEndpoint.replace('{id}', id);
                return $http.delete(endpoint)
                    .then(unwrap)
                    .then(function(response) {
                        // Normalise response.
                        return {
                            message: response.message || 'Transfer cancelled',
                            id: id,
                            __httpResponse: response
                        };
                    })
                    .catch(handleError({id: id}));
            }
        };
    };

    return serviceTransfers;
}));
