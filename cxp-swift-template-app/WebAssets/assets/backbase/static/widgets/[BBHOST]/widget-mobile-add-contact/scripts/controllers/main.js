'use strict';

function noop() {}

/**
 * @ngInject
 * @memberof module:widget-mobile-add-contact
 */
function MainCtrl(widget, model,  bus, bbI18n, Preference, Event, $scope, $filter, bbActivityIndicator, bbAlertDialog,
        AlertCallbackFn, serviceBeneficiary, SERVER_ROOT, ibanService) {

    var PREFERENCES = {
        WIDGET_MODE: 'widgetMode'
    };

    /**
     * Regular expressions
     */
    var regExp = {
        base64img: /^data.+/
    };

    /**
     * The contact information
     *
     * @var {Object} contact
     */
    var contact = model.getContactModel();

    /**
     * Valudation information
     *
     * @var {Object} validation
     */
    var validation = model.getValidationModel();

    /**
     * Error object
     *
     * @type {null}
     */
    var manageContactError = null;

    /**
     * All possible modes for the widget
     *
     * @var {Object} modes
     * @type {{ADD: string, EDIT: string, DELETE: string}}
     */
    var modes = {
        ADD: 'contact-add',
        EDIT: 'contact-edit',
        SHOW: 'contact-show'
    };

    /**
     * Suggested banks for IBAN input
     *
     * @public
     */
    var suggestedBanks;

    /**
     * The current mode of the widget
     *
     * @var {String} widgetMode
     * @type {null}
     */
    var widgetMode = null;
    var baseUrl = '';
    if (/^http.*/.test(SERVER_ROOT)) {
        baseUrl = SERVER_ROOT.replace(/^((\w+:)?\/\/[^\/]+\/?).*$/,'$1');
        if (baseUrl.charAt(baseUrl.length - 1) === '/') {
            baseUrl = baseUrl.slice(0, -1);
        }
    }

    return {
        get Labels() { return bbI18n.labels; },
        get contact() { return contact; },
        get validation() { return validation; },
        get manageContactError() { return manageContactError; },
        get widgetMode() { return widgetMode; },
        get suggestedBanks() { return suggestedBanks; },

        $onInit: $onInit,
        $onDestroy: $onDestroy,
        getInitials: getInitials,
        fireDeleteEvent: fireDeleteEvent,
        save: handleUpdateContact
    };

    /**
     * Angularjs hook function called after the component has been initialized and all its
     * bindings have been set up.
     * @public
     */
    function $onInit() {
        widgetMode = widget.model.getPreference(PREFERENCES.WIDGET_MODE);
        suggestedBanks = widget.model.getPreference(Preference.SUGGESTED_BANKS);

        switch(widgetMode) {
            case modes.ADD:
                bus.subscribe(widget.getPreference(Preference.CONTACT_SUBMIT_EVENT), handleCreateContact);
                bus.subscribe(widget.getPreference(Preference.CONTACT_NAVIGATION_EVENT), handleNavToContact);
                bus.subscribe(widget.getPreference(Preference.CONTACT_CANCEL_ADD_EVENT), handleCancelAddContact);
                break;
            case modes.EDIT:
                bus.subscribe(widget.getPreference(Preference.CONTACT_UPDATE_EVENT), handleUpdateContact);
                bus.subscribe(widget.getPreference(Preference.CONTACT_DELETE_EVENT), handleDeleteContact);
                bus.subscribe(widget.getPreference(Preference.CONTACT_CANCEL_EDIT_EVENT), handleCancelEditContact);
                bus.subscribe(widget.getPreference(Preference.CONTACT_NAVIGATION_EDIT_EVENT), handleNavToEditContact);
                break;
            case modes.SHOW:
                bus.subscribe(Event.Global.SAVED, handleNavToShowContact);
                bus.subscribe(widget.getPreference(Preference.CONTACT_NAVIGATION_SHOW_EVENT), handleNavToShowContact);
                bus.subscribe(widget.getPreference(Preference.CONTACT_EDIT_EVENT), handleEditContact);
                break;
        }
    }

    /**
     * Angularjs hook that is called when its containing scope is destroyed.
     * @public
     */
    function $onDestroy() {
        widgetMode = widget.model.getPreference(PREFERENCES.WIDGET_MODE);

        switch(widgetMode) {
            case modes.ADD:
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_SUBMIT_EVENT), handleCreateContact);
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_NAVIGATION_EVENT), handleNavToContact);
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_CANCEL_ADD_EVENT), handleCancelAddContact);
                break;
            case modes.EDIT:
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_UPDATE_EVENT), handleUpdateContact);
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_DELETE_EVENT), handleDeleteContact);
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_CANCEL_EDIT_EVENT), handleCancelEditContact);
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_NAVIGATION_EDIT_EVENT), handleNavToEditContact);
                break;
            case modes.SHOW:
                bus.unsubscribe(Event.Global.SAVED, handleNavToShowContact);
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_NAVIGATION_SHOW_EVENT), handleNavToShowContact);
                bus.unsubscribe(widget.getPreference(Preference.CONTACT_EDIT_EVENT), handleEditContact);
                break;
        }
    }

    /**
    * Get initials for given name
    * @param {String} name
    */
    function getInitials(name) {
        var names = typeof name === 'string' ? name.split(' ') : '';
        return names[0].charAt(0) + names[names.length - 1].charAt(0);
    }

    /**
     * Fires the edit contact event
     */
    function fireEditEvent() {
        var editContactEvent = widget.getPreference(Preference.CONTACT_EDIT_EVENT);
        bus.publish(editContactEvent);
    }

    /**
     * Fires the cancel add contact event
     */
    function fireCancelAddEvent() {
        var event = widget.getPreference(Preference.CONTACT_CANCEL_ADD_EVENT);
        bus.publish(event);
    }

    /**
     * Fires the cancel edit contact event
     */
    function fireCancelEditEvent() {
        var event = widget.getPreference(Preference.CONTACT_CANCEL_EDIT_EVENT);
        bus.publish(event);
    }

    /**
     * Fires the update contact event
     */
    function fireUpdateEvent() {
        var updateContactEvent = widget.getPreference(Preference.CONTACT_UPDATE_EVENT);
        bus.publish(updateContactEvent);
    }

    /**
     * Fires delete contact event
     */
    function fireDeleteEvent() {
        var deleteContactEvent = widget.getPreference(Preference.CONTACT_DELETE_EVENT);
        bus.publish(deleteContactEvent);
    }

    /**
     * Check if iban is not valid
     *
     * @param {String} value
     */
    function isValidIban(value) {
        return ibanService.validate(value);
    }

    /**
     * Show/hide error messages
     */
    function validateForm() {
        for (var k in validation) {
            if (k === 'accountNumber') {
                model.setError(k, !isValidIban(contact.accountNumber));
            } else {
                model.setError(k, $scope.contactForm[k].$invalid);
            }
        }
    }

    /**
     * Reset the validation
     */
    function resetValidation() {
        for (var k in validation) {
            model.setError(k, false);
        }
    }

    /**
     * Handle the navigation to ADD contact page
     */
    function handleNavToContact() {
        contact.id = '';
        contact.name = '';
        contact.partyId = '';
        contact.accountNumber = '';
        contact.phoneNumber = '';
        contact.email = '';
    }

    /**
     * Handle the navigation to EDIT contact page
     */
    function handleNavToEditContact(data) {

        model.clearContactData();
        resetValidation();

        if(data && data.id) {
            serviceBeneficiary.getContact(data.id).then(function(response) {
                contact.id = response.id;
                contact.name = response.name;
                contact.partyId = response.partyId;
                contact.accountNumber = $filter('iban')(response.account);
                contact.phoneNumber = response.phone;
                contact.email = response.email;
                contact.photo = response.photoUrl;
                // For base64 images don't prepend the base url
                contact.photoUrl = regExp.base64img.test(contact.photo) ? response.photoUrl : baseUrl + response.photoUrl;
            })
            .catch(handleDataError);
        }
    }

    /**
     * Handle the navigation to SHOW contact page
     */
    function handleNavToShowContact(data) {

        reset();
        model.clearContactData();
        resetValidation();

        if(data && data.id) {
            serviceBeneficiary.getContact(data.id).then(function(response) {
                contact.id = response.id;
                contact.name = response.name;
                contact.partyId = response.partyId;
                contact.accountNumber = $filter('iban')(response.account);
                contact.phoneNumber = response.phone;
                contact.email = response.email;
                contact.photo = response.photoUrl;
                // For base64 images don't prepend the base url
                contact.photoUrl = regExp.base64img.test(contact.photo) ? response.photoUrl : baseUrl + response.photoUrl;
            }).catch(handleDataError);
        }
    }

    /**
     * Make request to save the contact
     */
    function handleCreateContact() {

        var data;

        reset();
        validateForm();

        if ($scope.contactForm.$valid && isValidIban(contact.accountNumber)) {

            data = {
                id: model.uuID(),
                name: contact.name,
                account: contact.accountNumber,
                partyId: '1',
                isNew: true
            };

            bbActivityIndicator.show(noop, noop, bbI18n.labels.SAVING_CONTACT);

            serviceBeneficiary.postCreateContact(data).then(function(response) {
                bbActivityIndicator.hide(noop, noop);
                bus.publish(Event.Global.NOTIFY_SUCCESS, { message: bbI18n.labels.SAVED_CONTACT });
                contact.id = data.id;
                contact.partyId = data.partyId;

                // goto addressbook (can't go to details as it's in a different navigation scope).
                bus.publish('navigation:show-addressbook');
            }).catch(function(err) {
                err.callbackFn = AlertCallbackFn.RETRY_SUBMIT;
                handleUserError(err);
            });
        }
    }

    /**
     * Handle the contact edit
     */
    function handleEditContact() {
        // Go to edit contact page
        var editContactNavigationEvent = widget.getPreference(Preference.CONTACT_NAVIGATION_EDIT_EVENT);
        bus.publish(editContactNavigationEvent, {
            id: contact.id
        });
    }

    /**
     * Handle cancel contact add
     */
    function handleCancelAddContact() {
        // Go to contact list.
        var onCancelAddNavigationEvent = widget.getPreference(Preference.CONTACT_NAVIGATION_CANCEL_ADD_EVENT);
        bus.publish(onCancelAddNavigationEvent);
    }

    /**
     * Handle cancel contact edit
     */
    function handleCancelEditContact() {
        bus.publish('navigation:show-addressbook');
    }

    /**
     * Handle the contact edit
     */
    function handleUpdateContact() {
        var data;

        reset();
        validateForm();

        if ($scope.contactForm.$valid && isValidIban(contact.accountNumber)) {

            data = {
                id: contact.id,
                name: contact.name,
                account: contact.accountNumber,
                isNew: false,
                photoUrl: contact.photo
            };

            bbActivityIndicator.show(noop, noop, bbI18n.labels.SAVING_CONTACT);

            serviceBeneficiary.putUpdateContact(data).then(function(response) {
                bbActivityIndicator.hide(noop, noop);
                bus.publish(Event.Global.NOTIFY_SUCCESS, { message: bbI18n.labels.SAVED_CONTACT });

                // works for ios - addressbook nav will still be set to details page.
                bus.publish('navigation:show-addressbook');
                bus.publish(Event.Global.SAVED, {
                    id: contact.id
                });
            }).catch(function(err) {
                err.callbackFn = AlertCallbackFn.RETRY_UPDATE;
                handleUserError(err);
            });

        }
    }

    /**
     * Confirmation of delete contact
     *
     * @param data
     */
    function onDeleteContactSuccess (data) {
        data = data || {};

        if (data.callback === 'no') {
            return;
        }

        bbActivityIndicator.show(noop, noop, bbI18n.labels.DELETING_CONTACT);

        serviceBeneficiary.deleteContact(contact.id).then(function(response) {
            bbActivityIndicator.hide(noop, noop);
            bus.publish(Event.Global.NOTIFY_SUCCESS, { message: bbI18n.labels.DELETED_CONTACT });
            bus.publish(Event.Global.NAV_ADDRESSBOOK);
        }).catch(function(err) {
            err.callbackFn = AlertCallbackFn.RETRY_DELETE;
            handleUserError(err);
        });
    }

    /**
     * Handle contact delete
     */
    function handleDeleteContact() {

        var title = bbI18n.labels.REMOVE_CONTACT_TITLE;
        var message = bbI18n.labels.REMOVE_CONTACT_QUESTION.replace('{{NAME}}', contact.name);

        var buttons = [
            {
                type: 'POSITIVE',
                text: bbI18n.labels.REMOVE_CONTACT_YES,
                callbackFn: 'yes'
            },
            {
                type: 'NEGATIVE',
                text: bbI18n.labels.REMOVE_CONTACT_NO,
                callbackFn: 'no'
            }
        ];

        // Note: only works in mobile - needs polyfill for web.
        bbAlertDialog.show(onDeleteContactSuccess, noop, title, message, JSON.stringify(buttons));
    }

    /**
     * Handle error on retrieving data
     * @param err
     */
    function handleDataError(err) {
        bbActivityIndicator.hide(noop, noop);

        switch (err.code) {
            case serviceBeneficiary.E_OFFLINE:
                err.message = err.message || bbI18n.labels.ERROR_OFFLINE;
                gadgets.pubsub.publish(Event.Global.OFFLINE, err);
                break;
            case serviceBeneficiary.E_AUTH:
                err.message = err.message || bbI18n.labels.ERROR_AUTH;
                gadgets.pubsub.publish(Event.Global.AUTH, err);
                break;
            default:
                // Unexpected error.
                err.message = err.message || bbI18n.labels.ERROR_UNEXPECTED_CONTACT_LOAD;
                gadgets.pubsub.publish(Event.Global.ERROR_UNEXPECTED, err);
        }

        manageContactError = err;
    }


    /**
     * Handle error caused by the user
     *
     * @param err
     */
    function handleUserError(err) {
        bbActivityIndicator.hide(noop, noop);

        var buttons = [];
        var title = 'Error!';
        var message;

        switch (err.code) {
            case serviceBeneficiary.E_OFFLINE:
                buttons = [{type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok'}];
                message = bbI18n.labels.ERROR_OFFLINE;
                break;
            case serviceBeneficiary.E_USER:
                buttons = [{type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok'}];
                message = bbI18n.labels.ERROR_USER_CONTACT;
                break;
            case serviceBeneficiary.E_UNEXPECTED:
                buttons = [
                    {type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel'},
                    {type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: err.callbackFn}
                ];
                message = bbI18n.labels.ERROR_UNEXPECTED_CONTACT_SAVE;
                break;
            case serviceBeneficiary.E_AUTH:
                buttons = [
                    {type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel'},
                    {type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: err.callbackFn}
                ];
                message = bbI18n.labels.ERROR_AUTH;
                break;
        }

        // Callback for button click with retry ability.
        var alertCallback = function(data) {

            switch (data.callback) {
                case AlertCallbackFn.RETRY_SUBMIT:
                    handleCreateContact();
                    break;
                case AlertCallbackFn.RETRY_UPDATE:
                    handleUpdateContact();
                    break;
                case AlertCallbackFn.RETRY_DELETE:
                    onDeleteContactSuccess();
                    break;
                default:
                // do nothing
            }
        };

        bbAlertDialog.show(alertCallback, noop, title, message, JSON.stringify(buttons));
    }

    /**
     * Reset error and loading variables
     */
    function reset() {
        manageContactError = null;
    }
}

module.exports = MainCtrl;
