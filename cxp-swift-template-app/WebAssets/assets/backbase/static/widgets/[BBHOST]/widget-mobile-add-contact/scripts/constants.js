'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
            LOADED: 'cxp.item.loaded',
            NAV_ADDRESSBOOK: 'navigation:show-addressbook',
            SAVED: 'addressbook.contact.saved',
            NOTIFY_SUCCESS: 'notify:success',

            // Errors
            OFFLINE: 'offline:request-failed',
            AUTH: 'user-auth:request-unauthorised',
            ERROR_UNEXPECTED: 'error:unexpected'
        },
        Local: {
        }
    },
    Preference: {
        SUGGESTED_BANKS: 'suggestedBanks',
        CONTACT_SUBMIT_EVENT: 'contactSubmitEvent',
        CONTACT_EDIT_EVENT: 'contactEditEvent',
        CONTACT_CANCEL_EDIT_EVENT: 'contactCancelEditEvent',
        CONTACT_CANCEL_ADD_EVENT: 'contactCancelAddEvent',
        CONTACT_UPDATE_EVENT: 'contactUpdateEvent',
        CONTACT_DELETE_EVENT: 'contactDeleteEvent',
        CONTACT_ENDPOINT: 'contactEndpoint',
        CONTACT_NAVIGATION_EVENT: 'contactNavEvent',
        CONTACT_NAVIGATION_EDIT_EVENT: 'contactNavEditEvent',
        CONTACT_NAVIGATION_SHOW_EVENT: 'contactNavShowEvent',
        CONTACT_NAVIGATION_CANCEL_ADD_EVENT: 'contactCancelNavigationEvent'
    },
    AlertCallbackFn: {
        RETRY_SUBMIT: 'retrySubmit',
        RETRY_UPDATE: 'retryUpdate',
        RETRY_DELETE: 'retryDelete'
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
};

module.exports = WidgetConstants;
