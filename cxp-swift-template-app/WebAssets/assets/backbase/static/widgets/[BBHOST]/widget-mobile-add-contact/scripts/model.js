'use strict';

/**
 * Widget ViewModel
 * @ngInject
 * @name widgetModel
 * @private
 * @memberof module: widget-mobile-add-contact
 * @kind class Angular service provider
 */
function WidgetModel(widget) {

    var state = {
        contact: {
            id: '',
            partyId: '',
            name: '',
            accountNumber: '',
            phoneNumber: '',
            email: '',
            photo: '',
            photoUrl: ''
        },
        validation: {
            name: {
                error: false,
                errorMessage: 'This field is required'
            },
            accountNumber: {
                error: false,
                errorMessage: 'This is an invalid bank account number'
            }
            //phoneNumber: {
            //    value: '',
            //    error: false,
            //    errorMessage: ''
            //},
            //email: {
            //    value: '',
            //    error: false,
            //    errorMessage: 'This is an invalid email'
            //}
        }
    };

    return {

        uuID: generateUUID,

        // getters
        getContactModel: function() {
            return state.contact;
        },

        clearContactData: function() {
            for(var k in state.contact) {
                state.contact[k] = '';
            }
        },

        getValidationModel: function() {
            return state.validation;
        },

        setError: function(field, isError) {
            state.validation[field].error = isError;
        }
    };

    /**
     * UUID generator
     *
     * @returns {string}
     */
    function generateUUID(){
        var d = new Date().getTime();
        if(window.performance && typeof window.performance.now === 'function'){
            d += performance.now(); //use high-precision timer if available
        }
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    }
}

module.exports = WidgetModel;
