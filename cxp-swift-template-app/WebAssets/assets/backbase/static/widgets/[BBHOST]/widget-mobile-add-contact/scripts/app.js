'use strict';

// externals
var ng = require('angular');
var mobilePlugins = require('mobilePlugins');

// internals
var Config = require('./config');
var Model = require('./model');
var Bus = require('./bus');
var constants = require('./constants');

// Modules
var ibanModule = require('ibanModule');

// Services
var serviceBeneficiary = require('serviceBeneficiary');

// Controllers
var MainCtrl = require('./controllers/main');

/**
 * @ngInject
 */
function run(widget, bus, $rootScope, SERVER_ROOT, Event, Preference, serviceBeneficiary) {
    // Add a safeApply function.
    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    // Configure services.
    function getEndpoint(endpointPref) {
        return widget.getPreference(endpointPref).replace('$(servicesPath)', SERVER_ROOT);
    }

    serviceBeneficiary.setConfig({
        partyDataManagementEndpoint: getEndpoint(Preference.CONTACT_ENDPOINT)
    });

    bus.publish(Event.Global.LOADED, {
        id: widget.id
    });
}

/**
 * Widget Angular Module
 * @module widget-mobile-add-contact
 * @link https://docs.angularjs.org/api/ng/function/angular.module
 * @param {object} widget Widget instance
 * @returns {object} AngularJS module
 */
function App(widget) {
    var deps = [
        mobilePlugins.name
    ];

    return ng.module(widget.id, deps)
        .config(Config)
        .constant(constants)
        .factory('bus', Bus)
        .factory('model', Model)
        .controller('MainCtrl', MainCtrl)
        .factory('serviceBeneficiary', serviceBeneficiary)
        .service('ibanService', ibanModule.service)
        .filter('iban', ibanModule.filter)
        .directive('validIban', ibanModule.validator)
        .directive('ibanInput', ibanModule.input)
        .provider('widget', function() {
            this.$get = function() {
                return widget;
            };
        })
        .run(run);
}

module.exports = App;
