
'use strict';

/**
 * @ngInject
 */
function MainCtrl(bus, bbI18n) {

    var transaction = null;
    var iban = '';

    var api = {
        $onInit: $onInit,
        $onDestroy: $onDestroy,
        get transaction() { return transaction; },
        get iban() { return iban; },
        get Labels() { return bbI18n.labels; }
    };

    return api;

    /**
     * Angularjs hook function called after the component has been initialized and all its
     * bindings have been set up.
     * @public
     */
    function $onInit() {
        bus.subscribe('shc.open.transaction', handleNavOpenTransaction);
    }

    /**
     * Angularjs hook that is called when its containing scope is destroyed.
     * @public
     */
    function $onDestroy() {
        bus.unsubscribe('shc.open.transaction', handleNavOpenTransaction);
    }

    /**
     * Handle the navigation to the transaction details
     *
     * @param {Object} data
     */
    function handleNavOpenTransaction(data) {
        transaction = data.transaction;
        iban = data.iban;
        // TODO filter
        var date = new Date(transaction.bookingDateTime);
        var timeString = date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '' ) + date.getMinutes();

        transaction.date = transaction.dateString;
        transaction.time = timeString;
    }
}

module.exports = MainCtrl;
