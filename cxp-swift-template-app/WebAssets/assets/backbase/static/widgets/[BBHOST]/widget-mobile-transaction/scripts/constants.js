'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
        },
        Local: {
        }
    },
    Message: {
    },
    Preference: {
        TRANSACTION_CANCEL: 'transactionCancel'
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
};

module.exports = WidgetConstants;
