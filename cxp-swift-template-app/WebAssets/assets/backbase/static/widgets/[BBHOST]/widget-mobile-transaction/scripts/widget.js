window.shc = window.shc || {};
shc.mobile = shc.mobile || {};
shc.mobile.widget = shc.mobile.widget || {};
shc.mobile.widget.transaction = (function(angular, mobilePlugins) {
    'use strict';

    var TransactionWidget = function(widget) {
        var self = this;

        this.widget = widget;

        var config = require('./config');

        // Services
        var Bus = require('./bus');

        // Controllers
        var MainCtrl = require('./controllers/main');

        // Costants
        var constants = require('./constants');

        var deps = [
            mobilePlugins.name
        ];
        angular.module(widget.id, deps)
            .config(config)
            .constant(constants)
            // Services
            .factory('bus', Bus)
            // Controllers
            .controller('MainCtrl', MainCtrl)
            // Directives
            .directive('formatAmount', formatAmount)
            .directive('avatar', avatar)
            // Widget Object
            .provider('widget', function() {
                this.$get = function() {
                    return widget;
                };
            })
            .run(['$rootScope', function($rootScope){
                self.start($rootScope);
            }]);
        // Bootstrap the widget as an Angular App
        angular.bootstrap(widget.body, [widget.id]);
    };

    /**
     * @ngInject
     */
    TransactionWidget.prototype.start = function($rootScope) {
        // Add a safeApply function - handy for bus.
        $rootScope.safeApply = function(fn) {
            var phase = this.$root.$$phase;
            if(phase == '$apply' || phase == '$digest') {
                if(fn && (typeof(fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };

        gadgets.pubsub.publish('cxp.item.loaded', {
            id: this.widget.id
        });
    };

    return {
        init: function(widget) {
            var transactionApp = new TransactionWidget(widget);
            return transactionApp;
        }
    };
}(window.angular, window.mobilePlugins));
