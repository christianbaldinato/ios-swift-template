/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!***************************!*\
  !*** ./scripts/widget.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	window.shc = window.shc || {};
	shc.mobile = shc.mobile || {};
	shc.mobile.widget = shc.mobile.widget || {};
	shc.mobile.widget.transaction = (function(angular, mobilePlugins) {
	    'use strict';
	
	    var TransactionWidget = function(widget) {
	        var self = this;
	
	        this.widget = widget;
	
	        var config = __webpack_require__(/*! ./config */ 1);
	
	        // Services
	        var Bus = __webpack_require__(/*! ./bus */ 3);
	
	        // Controllers
	        var MainCtrl = __webpack_require__(/*! ./controllers/main */ 4);
	
	        // Costants
	        var constants = __webpack_require__(/*! ./constants */ 5);
	
	        var deps = [
	            mobilePlugins.name
	        ];
	        angular.module(widget.id, deps)
	            .config(config)
	            .constant(constants)
	            // Services
	            .factory('bus', Bus)
	            // Controllers
	            .controller('MainCtrl', MainCtrl)
	            // Directives
	            .directive('formatAmount', formatAmount)
	            .directive('avatar', avatar)
	            // Widget Object
	            .provider('widget', function() {
	                this.$get = function() {
	                    return widget;
	                };
	            })
	            .run(['$rootScope', function($rootScope){
	                self.start($rootScope);
	            }]);
	        // Bootstrap the widget as an Angular App
	        angular.bootstrap(widget.body, [widget.id]);
	    };
	
	    /**
	     * @ngInject
	     */
	    TransactionWidget.prototype.start = function($rootScope) {
	        // Add a safeApply function - handy for bus.
	        $rootScope.safeApply = function(fn) {
	            var phase = this.$root.$$phase;
	            if(phase == '$apply' || phase == '$digest') {
	                if(fn && (typeof(fn) === 'function')) {
	                    fn();
	                }
	            } else {
	                this.$apply(fn);
	            }
	        };
	
	        gadgets.pubsub.publish('cxp.item.loaded', {
	            id: this.widget.id
	        });
	    };
	
	    return {
	        init: function(widget) {
	            var transactionApp = new TransactionWidget(widget);
	            return transactionApp;
	        }
	    };
	}(window.angular, window.mobilePlugins));


/***/ },
/* 1 */
/*!***************************!*\
  !*** ./scripts/config.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var labels = __webpack_require__(/*! ../locale/all.json */ 2);
	
	/**
	 * @ngInject
	 */
	function WidgetConfig($provide, $compileProvider, bbI18nProvider) {
	    $compileProvider.debugInfoEnabled(false);
	    bbI18nProvider.setDefaultLabels(labels);
	};
	
	module.exports = WidgetConfig;


/***/ },
/* 2 */
/*!*************************!*\
  !*** ./locale/all.json ***!
  \*************************/
/***/ function(module, exports) {

	module.exports = {
		"en": {
			"TRANSACTION_DATE_TIME": "Transaction date & time",
			"TRANSACTION_TYPE": "Transaction type",
			"TRANSACTION_REFERENCE": "Reference",
			"TRANSACTION_IBAN": "IBAN"
		},
		"nl": {
			"TRANSACTION_DATE_TIME": "Transactie datum en tijd.",
			"TRANSACTION_TYPE": "Transaction type",
			"TRANSACTION_REFERENCE": "Betalingskenmerk",
			"TRANSACTION_IBAN": "IBAN"
		}
	};

/***/ },
/* 3 */
/*!************************!*\
  !*** ./scripts/bus.js ***!
  \************************/
/***/ function(module, exports) {

	'use strict';
	
	// @ngInject
	function WidgetBus($rootScope, widget) {
	    var pubsub = widget.features && widget.features.pubsub ||
	        window.gadgets && window.gadgets.pubsub;
	
	    return {
	        on: $rootScope.$on.bind($rootScope),
	        emit: $rootScope.$emit.bind($rootScope),
	        // todo: How to: $scope.$apply on subscribe.
	        subscribe: function(e, callback) {
	            return pubsub.subscribe(e, function() {
	                var args = Array.prototype.slice.call(arguments);
	                args.unshift(null); // THIS
	                $rootScope.safeApply(callback.bind.apply(callback, args));
	            });
	        },
	        publish: pubsub.publish.bind(pubsub)
	    }
	}
	
	module.exports = WidgetBus;


/***/ },
/* 4 */
/*!*************************************!*\
  !*** ./scripts/controllers/main.js ***!
  \*************************************/
/***/ function(module, exports) {

	
	'use strict';
	
	/**
	 * @ngInject
	 */
	function MainCtrl(bus, bbI18n) {
	
	    var transaction = null;
	    var iban = '';
	
	    var api = {
	        $onInit: $onInit,
	        $onDestroy: $onDestroy,
	        get transaction() { return transaction; },
	        get iban() { return iban; },
	        get Labels() { return bbI18n.labels; }
	    };
	
	    return api;
	
	    /**
	     * Angularjs hook function called after the component has been initialized and all its
	     * bindings have been set up.
	     * @public
	     */
	    function $onInit() {
	        bus.subscribe('shc.open.transaction', handleNavOpenTransaction);
	    }
	
	    /**
	     * Angularjs hook that is called when its containing scope is destroyed.
	     * @public
	     */
	    function $onDestroy() {
	        bus.unsubscribe('shc.open.transaction', handleNavOpenTransaction);
	    }
	
	    /**
	     * Handle the navigation to the transaction details
	     *
	     * @param {Object} data
	     */
	    function handleNavOpenTransaction(data) {
	        transaction = data.transaction;
	        iban = data.iban;
	        // TODO filter
	        var date = new Date(transaction.bookingDateTime);
	        var timeString = date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '' ) + date.getMinutes();
	
	        transaction.date = transaction.dateString;
	        transaction.time = timeString;
	    }
	}
	
	module.exports = MainCtrl;


/***/ },
/* 5 */
/*!******************************!*\
  !*** ./scripts/constants.js ***!
  \******************************/
/***/ function(module, exports) {

	'use strict';
	
	var WidgetConstants = {
	    /**
	     * Local and global events, that widget publishes (emits) or relies on
	     */
	    Event: {
	        Global: {
	        },
	        Local: {
	        }
	    },
	    Message: {
	    },
	    Preference: {
	        TRANSACTION_CANCEL: 'transactionCancel'
	    },
	    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
	};
	
	module.exports = WidgetConstants;


/***/ }
/******/ ]);
//# sourceMappingURL=main.js.map