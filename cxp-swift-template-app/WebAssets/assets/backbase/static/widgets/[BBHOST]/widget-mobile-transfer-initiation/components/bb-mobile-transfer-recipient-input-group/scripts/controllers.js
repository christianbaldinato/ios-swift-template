'use strict';

/**
 * @ngInject
 */
function MainCtrl() {

}

/**
 * Custom Callback event on some user action
 */
MainCtrl.prototype.onRecipient = function() {
    this.onRecipient();
};

/**
 * Called on each controller after all the controllers on an element have been constructed
 * and had their bindings initialized.
 * This is a good place to put initialization code for your controller.
 */
MainCtrl.prototype.$onInit = function() {

};

/**
 * Called whenever one-way bindings are updated.
 * The `changesObj` is a hash whose keys are the names of the bound properties that have changed,
 * and the values are an object of the form
 */
MainCtrl.prototype.$onChanges = function(changesObj) {

};

/**
 * Called on a controller when its containing scope is destroyed.
 * Use this hook for releasing external resources, watches and event handlers.
 */
MainCtrl.prototype.$onDestroy = function() {

};

/**
 * Called after this controller’s element and its children have been linked.
 * This hook can be considered analogous to the `ngAfterViewInit` and `ngAfterContentInit` hooks in Angular 2.
 */
MainCtrl.prototype.$postLink = function() {

};

/**
 * Set new contact to true
 */
MainCtrl.prototype.ibanKeyUp = function() {
	this.transfer.isNewContact = true;
};

exports.MainCtrl = MainCtrl;
