/**
 * ------------------------------------------------------------------------
 * dbpUiAmountInput entry point
 * ------------------------------------------------------------------------
 */
'use strict';

var angular = require('angular');

var Component = require('./scripts/component');

module.exports = angular.module('dbp-ui-amount-input', [ ])
    .component('dbpUiAmountInput', Component);
