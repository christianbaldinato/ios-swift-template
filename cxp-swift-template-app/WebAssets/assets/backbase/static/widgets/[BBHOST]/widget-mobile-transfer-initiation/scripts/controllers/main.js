'use strict';

/**
 * @ngInject
 * @memberof module:widget-mobile-transfer-initiation
 */
function MainCtrl(model) {
}

/**
 * Angularjs hook function called after the component has been
 * initialized and all its bindings have been set up.
 * @public
 */
MainCtrl.prototype.$onInit = function() {
};

module.exports = MainCtrl;
