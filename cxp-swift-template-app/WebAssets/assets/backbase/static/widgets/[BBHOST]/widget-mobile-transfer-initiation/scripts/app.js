'use strict';

// externals
var ng = require('angular');
var accountPanel = require('bbmAccountPanel');
var formatAmount = require('formatAmount');
var serviceTransfers = require('serviceTransfers');
var mobilePlugins = require('mobilePlugins');
var ibanModule = require('ibanModule');

// internals
var Model = require('./model');
var Bus = require('./bus');
var config = require('./config');
var constants = require('./constants');

// Controllers
var MainCtrl = require('./controllers/main');
var TransferCtrl = require('./controllers/transfer');

// Directives
var bbReference = require('./directives/reference');

// Components
var accountSelectButton = require('../components/bb-mobile-account-select-button/scripts/main');
var recipientInputs = require('../components/bb-mobile-transfer-recipient-input-group/scripts/main');
var DbpUiAmountInput = require('../components/dbp-ui-amount-input/index');
/**
 * @ngInject
 */
function run(widget, bus, Event, SERVER_ROOT, $rootScope, serviceTransfers, serviceAccounts) {
    // Add a safeApply function.
    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    // Configure services.
    function getEndpoint(endpointPref) {
        return widget.getPreference(endpointPref).replace('$(servicesPath)', SERVER_ROOT);
    }

    serviceTransfers.setConfig({
        'createEndpoint': getEndpoint('createEndpoint'),
        'unsubmittedEndpoint': getEndpoint('unsubmittedEndpoint')
    });

    serviceAccounts.setConfig({
        'listEndpoint': getEndpoint('accountsUrl')
    });

    // Publish ready event.
    bus.publish(Event.Global.LOADED, {
        id: widget.id
    });
}

/**
 * Widget Angular Module
 * @module widget-mobile-transfer-initiation
 * @link https://docs.angularjs.org/api/ng/function/angular.module
 * @param {object} widget Widget instance
 * @returns {object} AngularJS module
 */
function App(widget) {
    var deps = [
        accountSelectButton.name,
        recipientInputs.name,
        mobilePlugins.name,
        DbpUiAmountInput.name
    ];

    return ng.module(widget.id, deps)
        .config(config)
        .constant(constants)
        .factory('bus', Bus)
        .factory('serviceTransfers', serviceTransfers)
        .factory('serviceAccounts', serviceAccounts)
        .factory('model', Model)
        .directive('formatAmount', formatAmount)
        .directive('bbReference', bbReference)
        .component('bbMobileAccountPanel', accountPanel)
        .service('ibanService', ibanModule.service)
        .filter('iban', ibanModule.filter)
        .directive('validIban', ibanModule.validator)
        .directive('ibanInput', ibanModule.input)
        .controller('MainCtrl', MainCtrl)
        .controller('TransferCtrl', TransferCtrl)
        .provider('widget', function() {
            this.$get = function() {
                return widget;
            };
        })
        .run(run);
}

module.exports = App;
