/**
 * ------------------------------------------------------------------------
 * billPayment entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'billPayment';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
    'use strict';

    function billPayment() {
        return {
            restrict: 'EA',
            template: '<div class="bill-payment clearfix" ng-if="splitValue">' +
                      '<input type="number" pattern="[0-9]*" ng-keyup="checkMaxLength(splitValue, 0);checkNextFocus(splitValue, 0, $event)" ng-model="splitValue[0]"/>' +
                      '<input type="number" pattern="[0-9]*" ng-keyup="checkMaxLength(splitValue, 1);checkNextFocus(splitValue, 1, $event)" ng-model="splitValue[1]"/>' +
                      '<input type="number" pattern="[0-9]*" ng-keyup="checkMaxLength(splitValue, 2);checkNextFocus(splitValue, 2, $event)" ng-model="splitValue[2]"/>' +
                      '<input type="number" pattern="[0-9]*" ng-keyup="checkMaxLength(splitValue, 3)" ng-model="splitValue[3]"/>' +
                      '</div>',
            scope: {
                'value': '='
            },
            link: function(scope, element, attrs, ctrl) {

                var value = scope.value;
                var splitValue;

                // Convert to string and Remove non-digits
                value = (value === null || typeof value === 'undefined') ? '' : value;
                value = typeof value === 'string' ? value : '' + value;
                value = value.replace(/\D/g, '');

                // Split the input in groups of 4
                splitValue = value.match(/(\d|\D){1,4}/g);
                splitValue = splitValue || [];

                scope.splitValue = splitValue;

                /**
                 * Check if the field has max number of characters
                 */
                scope.checkMaxLength = function(splitValue, part) {    
                    var val = String(splitValue[part]);
                    if (val.length > 4) {
                        splitValue[part] = parseInt(val.substr(0, 4), 10);
                    }

                    scope.value = splitValue.map(function(item) {
                        return item === null ? '' : String(item);
                    }).join('');
                }

                /**
                 * Check if the field has max number of characters
                 */
                scope.checkNextFocus = function(splitValue, part, $event) {    
                    var val = String(splitValue[part]);
                    var next = $event.target.nextElementSibling;
                    if (val.length === 4) {
                        if(!!next) {
                            next.focus();
                        }
                    }
                }
            }
        };
    }

    return billPayment;
}));
