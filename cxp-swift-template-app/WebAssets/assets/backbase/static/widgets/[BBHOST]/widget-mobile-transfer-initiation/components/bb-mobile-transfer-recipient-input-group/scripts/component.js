'use strict';
/**
* ------------------------------------------------------------------------
* Angular bbMobileTransferRecipient component
* ------------------------------------------------------------------------
*/
var template = require('../templates/index.ng.html');
var Controllers = require('./controllers');

module.exports = {
    bindings: {
        transfer: '=',
        banks: '@',
        recipient: '<',
        labels: '<',
        onRecipient: '&',
    },
    controller: Controllers.MainCtrl,
    template: template
};
