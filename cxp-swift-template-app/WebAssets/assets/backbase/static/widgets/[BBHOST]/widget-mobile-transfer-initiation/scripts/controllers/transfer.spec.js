
// when init
// it loads transfer DTO for editing (may or may-not have ID)
    // note: DTO requires for 'account' object for viewing details, otherwise a request is needed
    // maybe attached as additional meta data - not meant for payload
// it binds a mutable transfer DTO to the view

// when from account requested
// it navigates to "from" selector

// when from account selected
// it sets from account on the model
    // DTO updates - including meta details
    // mutable transfer updates

// when recipient requested
// it navigates to the "recipient" selector

// when submitting
// it validates the form
// it submits the transfer DTO to the model
// it navigates on success / it resets & publishes on success
