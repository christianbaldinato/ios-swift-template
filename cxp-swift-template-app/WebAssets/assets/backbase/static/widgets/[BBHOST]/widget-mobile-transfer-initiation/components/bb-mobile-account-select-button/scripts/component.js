'use strict';
/**
* ------------------------------------------------------------------------
* Angular bbMobileAccountSelectButton component
* ------------------------------------------------------------------------
*/
var template = require('../templates/index.ng.html');
var Controllers = require('./controllers');

module.exports = {
    bindings: {
        selected: '<',
        onClick: '&',
        labels: '<'
    },
    controller: Controllers.MainCtrl,
    template: template
};
