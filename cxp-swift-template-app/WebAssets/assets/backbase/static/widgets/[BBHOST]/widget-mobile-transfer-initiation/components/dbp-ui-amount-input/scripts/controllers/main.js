// var base = require('dbp-base');
// var i18n = base.i18n;

var i18n = {
    utils: {
        getCurrencySymbol: function(value) {
            return '€';
        }
    }
};

var INTEGER_MAX_LENGTH = 6;
var DECIMAL_MAX_LENGTH = 2;
var CLASS_NAME = 'dbp-ui-amount-input';
var EMPTY_INTEGER_CLASS_NAME = 'empty-integer';
var EMPTY_DECIMAL_CLASS_NAME = 'empty-decimal';
var EMPTY_SYMBOL_CLASS_NAME = 'empty-symbol';

/**
 * Parses and returns the decimal part of the given amount as an integer.
 * @param {Number} amount
 * @return {Number}
 */
function parseDecimal(amount) {
    return parseInt((amount % 1).toFixed(2).substr(2), 10);
}

/**
 * Directive MainCtrl implementation
 * @ngInject
 */
function MainCtrl($scope, $element, $locale) {
    var ctrl = this;
    ctrl.element = $element;
    ctrl.maxIntegerLength = ctrl.maxIntegerLength || INTEGER_MAX_LENGTH;
    ctrl.maxDecimalLength = DECIMAL_MAX_LENGTH;
    ctrl.separator = $locale.NUMBER_FORMATS.DECIMAL_SEP;
    ctrl.separatorCharCode = ctrl.separator.charCodeAt(0);
    ctrl.decimalCodes = [108, 110, 188 , 190, 194, ctrl.separatorCharCode];

    var inputs = $element.find('input');
    ctrl.integerElement = inputs.eq(0);
    ctrl.decimalElement = inputs.eq(1);

    ctrl.integerElement.on('keydown', this.onIntegerKeyDown.bind(this));

    $scope.$watch(function() {
        return this.model.$modelValue;
    }.bind(this), function(currentValue, previousValue) {
        if (currentValue !== 'undefined' && currentValue !== previousValue) {
            this.onAmountChange();
        }
    }.bind(this));
};

/**
 * Called on each controller after all the controllers on an element have been constructed
 * and had their bindings initialized.
 * This is a good place to put initialization code for your controller.
 */
MainCtrl.prototype.$onInit = function() {
    this.setCurrencySymbol(this.currency);
    this.setIntegerAndDecimal();
    this.updateEmptyState();
};

/**
 * Called after this controller’s element and its children have been linked.
 * This hook can be considered analogous to the `ngAfterViewInit` and `ngAfterContentInit` hooks in Angular 2.
 */
MainCtrl.prototype.$postLink = function() {
    this.element.addClass(CLASS_NAME);
};

/**
 * Called whenever one-way bindings are updated.
 * The `changesObj` is a hash whose keys are the names of the bound properties that have changed,
 * and the values are an object of the form
 */
MainCtrl.prototype.$onChanges = function(changesObj) {
    if (changesObj.currency && changesObj.currency.currentValue !== changesObj.currency.previousValue) {
        this.setCurrencySymbol(changesObj.currency.currentValue);
    }
};

/**
 * Checks whether the component has focus.
 * Returns true, if either integer input or decimal input is focused.
 * Otherwise returns false.
 * @return {Boolean}
 * @public
 */
MainCtrl.prototype.hasFocus = function hasFocus() {
    var focusedElem = document.activeElement;
    return focusedElem && (focusedElem === this.decimalElement[0] ||
        focusedElem === this.integerElement[0]);
};

/**
 * Updates amount property based on the integer and decimal values
 */
MainCtrl.prototype.updateAmount = function() {
    var integer = parseInt(this.integer || 0, 10);

    // convert decimal to number
    var decimal = parseFloat('0.' + (this.decimal || 0), 10);

    this.model.$setViewValue(integer + decimal);
};

/**
 * Updates class names of directive element based on the integer and decimal values.
 * Adds "empty-integer" or "empty-decimal" class names if one of the values is empty.
 */
MainCtrl.prototype.updateEmptyState = function() {
    var isIntegerEmpty = this.integer === null || this.integer === undefined;
    var isDecimalEmpty = this.decimal === null || this.decimal === undefined;
    this.element.toggleClass(EMPTY_INTEGER_CLASS_NAME, isIntegerEmpty);
    this.element.toggleClass(EMPTY_DECIMAL_CLASS_NAME, isDecimalEmpty);

    this.empty = isIntegerEmpty && isDecimalEmpty;
};

/**
 * Sets initial integer and decimal values from amount
 * @param {Object} ctrl Controller instance
 * @private
 */
MainCtrl.prototype.setIntegerAndDecimal = function() {
    var value = parseFloat(this.model.$modelValue);
    if (isNaN(value)) {
        this.integer = this.decimal = null;
    } else {
        value = Math.abs(value);

        var integer = Math.floor(value);
        if (integer >= Math.pow(10, this.maxIntegerLength)) {
            integer = 0;
        }

        var decimal = parseDecimal(value);

        this.integer = integer === 0 ? null : integer;
        this.decimal = decimal === 0 ? null : decimal;
    }
};

/**
 * Handles external changes of the amount.
 * @param {Number} val  Current value
 * @param {Number} prev Previous value
 * @private
 */
MainCtrl.prototype.onAmountChange = function(val) {
    if (!this.hasFocus()) {
        this.setIntegerAndDecimal();
        this.updateEmptyState();
    }
};

/**
 * Process input in the Integer/Decimal input elements
 * @param  {Object} e Input event
 * @private
 */
MainCtrl.prototype.onInput = function(e) {
    this.updateAmount();
    this.model.$render();
    this.updateEmptyState();
};

/**
 * Handles keydown event on the Integer input field
 * @param  {Object} e Keydown event
 * @private
 */
MainCtrl.prototype.onIntegerKeyDown = function(e) {
    // Select decimals field when period key pressed
    if(this.decimalCodes.indexOf(e.which) !== -1) {
        e.preventDefault();
        this.decimalElement[0].focus();
    }
};

/**
 * Adds trailing zeros on blur to always have two digits in the input field
 * @param  {Object} e Blur event
 * @private
 */
MainCtrl.prototype.onDecimalBlur = function(e) {
    // convert decimal to number
    var decimal = parseInt(this.decimal || 0, 10);
    if ((''+this.decimal).length === 1) {
        this.decimal *= 10;
    }
};

/**
 * Sets the correct symbol based on the currency code
 * USD => $
 * @param  {String} value currency code value e.g. USD
 * @private
 */
MainCtrl.prototype.setCurrencySymbol = function(value) {
    this.currencySymbol = i18n.utils.getCurrencySymbol(value);
    this.element.toggleClass(EMPTY_SYMBOL_CLASS_NAME, !this.currencySymbol);
};

module.exports = MainCtrl;
