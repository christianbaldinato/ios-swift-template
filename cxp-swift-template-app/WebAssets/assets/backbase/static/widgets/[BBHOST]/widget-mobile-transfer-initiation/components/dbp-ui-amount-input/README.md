# DBP UI Amount Input
Allows user to input currency amount.

## Information
|  Name |  dbp-ui-amount-input |
|--|--|
|  Version |  0.6.0 |
|  Bundle |  DBP |
|  Owner |  Borys Ponomarenko [borysp@backbase.com](mailto:borysp@backbase.com) |
|  Icon |  ![icon](icon.png) |
|  Status |  [![Build Status](http://10.10.11.60:8080/job/dbp-ui-amount-input/badge/icon)](http://10.10.11.60:8080/job/dbp-ui-amount-input/)) |

## Usage
Install library

```bash
npm install dbp-ui-amount-input --registry=http://launchpad.backbase.com:8765/
```

- browser

```html
<script src="[features-path]/dbp-ui-amount-input/dist/dbp-ui-amount-input.js"></script>
<script>
    angular.module('app', ['dbp-ui-amount-input']);
</script>
<body>    
    <input dbp-ui-amount-input value="0" placeholder="Amount" />
</body>
```

- CommonJS

```javascript
var DbpUiAmountInput = require('dbp-ui-amount-input');

angular.module('app', [DbpUiAmountInput.name]);
```

- AMD

```javascript
define(['dbp-ui-amount-input'], function(DbpUiAmountInput) {

});
```

- ES2015

```javascript
import DbpUiAmountInput from 'dbp-ui-amount-input';
```
## Dependencies

- angular ~1.5
- dbp-base >=0.14.0 < 1.0.0
- dbp-ui-base >=0.11.0 <1.0.0

## Options
## Events
## Requirements
## References
## Development
```bash
cd dbp-ui-amount-input
npm link && npm start
```
### Development Dependencies

- angular-mocks ~1.5
- dbp-template-standalone ^1.3.0
- dbp-tool-changelog ^1.0.0

