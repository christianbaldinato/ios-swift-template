'use strict';
var ng = require('angular');
require('angular-mocks');

var bbMobileTransferRecipient = require('../../scripts/main');

var ngModule = window.module;
var ngInject = window.inject;

/*----------------------------------------------------------------*/
/* bb-mobile-transfer-recipient-input-group module testing
/*----------------------------------------------------------------*/
describe('component: bb-mobile-transfer-recipient-input-group', function() {

    beforeEach(ngModule(bbMobileTransferRecipient.name));

    beforeEach(inject(function($rootScope, _$componentController_,  $compile) {
        this.scope = $rootScope.$new();
        this.element = $compile('<bb-mobile-transfer-recipient-input-group value="4"></bb-mobile-transfer-recipient-input-group>')(this.scope);
        this.$componentController = _$componentController_;
        this.options = {
            escape: true
        };
        this.value = 4;
        this.scope.$digest();
    }));

    it('should export an object', function() {
        expect(bbMobileTransferRecipient).toBeObject();
    });

    it('should render the default template', function() {
        this.value = 4;
        var button = this.element.find('button');
        expect(button.text()).toEqual('4');
    });

    it('should set the default values and options', function() {
        // It's necessary to always pass the scope in the locals,
        // so that the controller instance can be bound to it
        this.controller = this.$componentController('bbMobileTransferRecipient', { $scope: this.scope });
        expect(this.controller.options).toBeUndefined();
        expect(this.controller.value).toBeUndefined();
    });

    it('should assign the options bindings to the options object', function() {
        // Here we are passing actual bindings to the controller
        var bindings = {
            options: this.options,
            value: this.value
        };
        this.controller = this.$componentController('bbMobileTransferRecipient', { $scope: this.scope }, bindings);
        expect(this.controller.options).toEqual({
            escape: true
        });
        expect(this.controller.value).toEqual(4);
    });

    it('should call the onAction callback', function() {
        var bindings = {
            options: this.options,
            value: this.value,
            onAction: jasmine.createSpy('actionSpy')
        };
        this.controller = this.$componentController('bbMobileTransferRecipient', { $scope: this.scope }, bindings);
        this.controller.onAction({ value: 10 });
        expect(this.controller.onAction).toHaveBeenCalledWith({ value: 10 });
    });

});
