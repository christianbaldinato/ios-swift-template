### `v0.6.0 - 26/04/2016`
* feat: add maxInputLength attribute

### `v0.5.1 - 14/04/2016`
* chore(DM-370): use standalone template

### `v0.5.0 - 05/04/2016`
* chore(DM-306): align with the services blueprint and file structure

### `v0.4.2 - 31/03/2016`
* fix: Update input values on external model change

### `v0.4.1 - 30/03/2016`
* fix: Stupid typo

### `v0.4.0 - 24/03/2016`
* feat: Focus decimals field on period key press

### `v0.3.0 - 24/03/2016`
* feat: Hide currency symbol when not set
* feat: New 'empty' controller property
* refactor: Removed unneded wrapper element

### `v0.2.0 - 24/03/2016`
* feat: New amount input implementation

### `v0.1.1 - 22/02/2016`
* fix: Removed dist folder from .npmignore file

### `v0.1.0 - 18/02/2016`
* Initial release
