'use strict';

/**
 * ------------------------------------------------------------------------
 * Angular dbpUiAmountInput component
 * ------------------------------------------------------------------------
 */
var template = require('../templates/index.ng.html');
var MainCtrl = require('./controllers/main');

module.exports = {
    require: {
        'model': '^ngModel'
    },
    bindings: {
        currency: '<',
        empty: '=?',
        maxIntegerLength: '<?'
    },
    controller: MainCtrl,
    template: template
};
