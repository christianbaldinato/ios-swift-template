/**
 * ------------------------------------------------------------------------
 * billPayment entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'bbReference';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {
    'use strict';

        function bbReference() {
            return {
                require: 'ngModel',
                restrict: 'A',
                scope: {
                    'value': '='
                },
                replace: true,
                link: function(scope, element, attrs, ngModel) {

                    scope.$watch(function() {
                        return ngModel.$viewValue;
                    }, function(val, old) {
                        ngModel.$setViewValue(format(ngModel.$viewValue));
                        ngModel.$render();
                    });

                    /**
                     * Validates the reference
                     *
                     * @param modelValue
                     * @param viewValue
                     * @return {boolean}
                     */
                    ngModel.$validators.validReference = function(modelValue, viewValue) {
                        if (ngModel.$isEmpty(modelValue)) {
                            // consider empty models to be valid
                            return true;
                        }

                        return isValid(viewValue);
                    };

                    /**
                     * Format the reference
                     *
                     * @param value
                     * @return {string|*}
                     */
                    function format(value) {
                        value = typeof value === 'string' ? value.replace(/[\D]/g, '') : '';
                        return value && value.match(/(\d|\D){1,4}/g).join(' ');
                    }

                    /**
                     * Set the validity of the reference
                     * @param value
                     * @return {boolean}
                     */
                    function isValid(value) {

                        value = typeof value === 'string' ?  value.replace(/[\D]/g, '') : '';
                        var codeLengthDigit = null;
                        var codeLength = null;
                        var weights = [2, 4, 8, 5, 10, 9, 7, 3, 6, 1, 2, 4, 8, 5];
                        var checkSum = 0;
                        var valid = value.length >= 7 && value.length <= 16;
                        var counter, digit, remaining, checkDigit, digitsToCheck;

                        // No check digit
                        if (value.length === 7) {
                            return true;
                        }

                        checkDigit = parseInt(value.charAt(0), 10);

                        // Check code length
                        if (value.length >= 9 && value.length < 16) {
                            codeLengthDigit = parseInt(value.charAt(1), 10);
                            switch (codeLengthDigit) {
                                case 0:
                                    codeLength = 10;
                                    break;
                                case 1:
                                    codeLength = 11;
                                    break;
                                case 2:
                                    codeLength = 12;
                                    break;
                                case 7:
                                    codeLength = 7;
                                    break;
                                case 8:
                                    codeLength = 8;
                                    break;
                                case 9:
                                    codeLength = 9;
                                    break;
                            }

                            // The code length digit is not in the allowed ones
                            if (codeLength === null) {
                                return false;
                            }

                            // Code length is not matching
                            valid = valid && value.substr(2).length === codeLength;
                        }

                        // Get the digits from the second position and map them to numeric array
                        digitsToCheck = value.substr(1).split('').map(function(item) { return parseInt(item, 10); });

                        // Check the sum of the digits
                        counter = 0;
                        while(digitsToCheck.length) {
                            digit = digitsToCheck.pop();
                            checkSum += digit * weights[counter];
                            counter++;
                        }

                        remaining = checkSum % 11;

                        // 11 - remaining should equal to the check digit
                        valid = valid && ( (11 - remaining) === checkDigit );

                        return valid;
                    }

                }
            };
        }


    return bbReference;
}));
