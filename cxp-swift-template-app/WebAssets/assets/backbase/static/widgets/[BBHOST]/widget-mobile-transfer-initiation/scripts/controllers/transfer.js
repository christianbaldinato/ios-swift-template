'use strict';

function noop() {}

/**
 * @ngInject
 * @memberof module:widget-mobile-transfer-initiation
 */
function TransferCtrl(widget, model, bus, Event, ibanService, serviceTransfers, serviceAccounts,
        bbAlertDialog, bbActivityIndicator, $filter, Preference) {

    /**
     * @public
     * Suggested banks for IBAN directive
     */
    var suggestedBanks = '';

    /**
     * @public
     * Store the state for transfer
     */
    var transfer = model.getTransfer();

    /**
     * @private
     * Get list of all accounts
     *
     * @type {Array}
     */
    var accounts = [];

    /**
     * @public
     */
    var editTransfer = {
        isNewContact: false,
        addContact: false,
        counterpartyIban: '',
        scheduledTransfer: {
            frequency: null,
            every: 0,
            intervals: null,
            startDate: null,
            endDate: null
        }
    };

    /**
     *
     * @type {object}
     */
    var api = {
        // methods
        $onInit: $onInit,
        $onDestroy: $onDestroy,
        gotoSelectAccount: gotoSelectAccount,
        gotoSelectRecipient: gotoSelectRecipient,
        gotoSelectScheduleDate: gotoSelectScheduleDate,
        onSubmit: onSubmit,
        isValidIban: isValidIban,

        // properties
        get transfer() { return transfer; },
        get suggestedBanks() { return suggestedBanks; },
        get Label() { return model.Label; },

        // 2-way bind.
        editTransfer: editTransfer
    };

    return api;

    /**
     * Angularjs hook function called after the component has been
     * initialized and all its bindings have been set up. On initialisation the event
     * subscriptions are made.
     *
     * @public
     */
    function $onInit() {
        bus.subscribe(Event.Global.FROM_ACCOUNT_SELECT, onSelectAccount);
        bus.subscribe(Event.Global.RECIPIENT_SELECT, onSelectRecipient);
        bus.subscribe(Event.Global.TRANSFER_CANCEL, onTransferCancel);
        bus.subscribe(Event.Global.TRANSFER_COMPLETE, onTransferComplete);
        bus.subscribe(Event.Global.SCHEDULE_SAVE, onSaveSchedule);

        bus.subscribe('backbase.bawag.sepaRead', function(data) {
            editTransfer.instructedAmount = data.amount;
            editTransfer.instructedCurrency = data.currency;
            editTransfer.counterpartyIban = data.iban;
            editTransfer.description = data.paymentDescription;
            editTransfer.remittanceInformation = data.referenceNumber;
            editTransfer.counterpartyName = data.customerData;
        });

        suggestedBanks = widget.getPreference(Preference.SUGGESTED_BANKS);

        model.setTimezoneOffset(parseInt(widget.getPreference(Preference.TIMEZONE_OFFSET), 10));
        model.setInitialSchedule();

        loadAccounts().then(function() {
            initializeFromAccount();
        });
    }

    /**
     * Unsubscribe events on destroy
     */
    function $onDestroy() {
        bus.unsubscribe(Event.Global.FROM_ACCOUNT_SELECT, onSelectAccount);
        bus.unsubscribe(Event.Global.RECIPIENT_SELECT, onSelectRecipient);
        bus.unsubscribe(Event.Global.TRANSFER_CANCEL, onTransferCancel);
        bus.unsubscribe(Event.Global.TRANSFER_COMPLETE, onTransferComplete);
        bus.unsubscribe(Event.Global.SCHEDULE_SAVE, onSaveSchedule);
    }


    /**
     * Load user's accounts
     * @private
     */
    function loadAccounts() {
        return serviceAccounts.getAll().then(function(res) {
            accounts = res;
            return accounts;
        });
    }

    /**
     * Preselect the first account from the list of accounts to simplify the transfer process
     * @private
     */
    function initializeFromAccount() {
        if (accounts.length) {
            onSelectAccount({
                account: {
                    accountId: accounts[0].id,
                    accountDetails: accounts[0]
                }
            });
        }
    }

    /**
     * Triggers the navigation flow for selecting an account.
     *
     * @public
     */
    function gotoSelectAccount() {
        bus.publish(Event.Global.NAV_SELECT_FROM_ACCOUNT);
    }

    /**
     * Triggers the navigation flow for selecting a recipient.
     *
     * @public
     */
    function gotoSelectRecipient() {
        var accountIdentification = accountIban(model.getTransfer().fromAccount);

        bus.publish(Event.Global.NAV_SELECT_RECIPIENT, {
            accountIdentification: accountIdentification
        });
    }

    /**
     * Triggers the navigation flow for selecting a schedule date.
     *
     * @public
     */
    function gotoSelectScheduleDate() {
        bus.publish(Event.Global.NAV_SELECT_DATE, transfer.schedule);
    }

    /**
     * Callback for form submit.
     *
     * @param {bool} isValid
     * @public
     * @return {Promise}
     */
    function onSubmit(isValid, form) {
        // todo: move iban validator to the form.
        if (!isValidIban(editTransfer.counterpartyIban)) {
            return showUserError(model.Label.TRANSFER_INVALID_IBAN);
        }

        // todo: move to validator on input
        if (!isValidAmount(editTransfer.instructedAmount)) {
            return showUserError(model.Label.TRANSFER_INVALID_AMOUNT);
        }

        if (!isValid) {
            return handleFormError(form);
        }

        // Submit the transfer from the form.
        doSubmit();
    }

    /**
     *
     * @return {Function|any|*}
     */
    function doSubmit() {
        bbActivityIndicator.show(noop, noop, model.Label.SUBMITTING_TRANSFER);

        // TODO: take it dynamically
        editTransfer.instructedCurrency = editTransfer.instructedCurrency || 'EUR';

        // Overwrite the schedule object
        editTransfer.scheduledTransfer = angular.extend({}, editTransfer.scheduledTransfer, {
            frequency: transfer.schedule.frequency,
            every: transfer.schedule.every,
            intervals: transfer.schedule.intervals,
            startDate: transfer.schedule.startDate,
            endDate: transfer.schedule.endDate
        });

        return model.submitTransfer(editTransfer)
            .then(handleSuccess)
            .catch(function(err) {
                handleError(err, doSubmit);
            });
    }

    /**
     *
     * @param form
     */
    function handleFormError(form) {
        var errorMap = {
            accountId: model.Label.TRANSFER_INVALID_ACCOUNT,
            // todo: beneficiary as normal form fields
            // counterpartyName: model.Label.TRANSFER_INVALID_NAME,
            // counterpartyIban: model.Label.TRANSFER_INVALID_IBAN,
            instructedAmount: model.Label.TRANSFER_INVALID_AMOUNT,
            description: model.Label.TRANSFER_INVALID_DESCRIPTION,
            remittanceInformation: model.Label.TRANSFER_INVALID_REFERENCE
        };

        // Find first error.
        var errorField = null;
        for (var field in errorMap) {
            if (form[field] && form[field].$invalid) {
                errorField = field;
                break;
            }
        }

        // Show error.
        if (errorField) {
            showUserError(errorMap[errorField]);
        }
        else {
            // Form is invalid, but no error fields - this is unexpected.
            showUnexpectedError(model.Label.ERROR_UNEXPECTED_TRANSFER_SUBMIT);
        }
    }

    /**
     * Show alert dialog with user error
     *
     * @param message
     */
    function showUserError(message) {
        var buttons = [{ type: 'POSITIVE', text: model.Label.BUTTON_OK, callbackFn: 'ok' }];
        var title = model.Label.ERROR_ALERT;

        bbAlertDialog.show(noop, noop, title, message, JSON.stringify(buttons));
    }

    /**
     *
     */
    function showUnexpectedError() {
        // Call for button click with retry ability.
        var alertCallback = function(data) {
            if (data.callback === 'retry') {
                retry.call();
            } // else ignore (cancel / ok).
        };
    }

    /**
     * Check if iban is not valid
     *
     * @param {String} value
     */
    function isValidIban(value) {
        return ibanService.validate(value);
    }

    /**
     * Check if amount is valid
     *
     * @param {Number} value
     */
    function isValidAmount(value) {
        return !isNaN(value) && value > 0;
    }

    /**
     * Subscriber for when account is selected from outside the widget.
     */
    function onSelectAccount(data) {
        editTransfer.accountId = data.account.accountId;

        // Set account details on the model.
        model.setFromAccountDetails(data.account.accountDetails);

        // Clear the recipient details
        if (ibansEqual(accountIban(data.account.accountDetails), editTransfer.counterpartyIban)) {
            model.clearRecipient();
            editTransfer.counterpartyName = '';
            editTransfer.counterpartyIban = '';
        }
    }

    /**
     * Subscriber for when recipient is selected from outside the widget.
     */
    function onSelectRecipient(data) {
        // Update form model.
        editTransfer.counterpartyName = data.name;
        editTransfer.counterpartyIban = $filter('iban')(data.accountIdentification);
        editTransfer.isNewContact = false;
        editTransfer.addContact = false;
    }


    /**
     * Subscriber for when transaction is scheduled from outside the widget.
     */
    function onSaveSchedule(data) {

        transfer.schedule.date = data.date;
        transfer.schedule.dateText = data.dateText;
        transfer.schedule.frequencyText = data.frequencyText;
        transfer.schedule.frequency = data.frequency;
        transfer.schedule.intervals = data.intervals;
        transfer.schedule.every = data.every;

        var date = new Date(data.date);

        // TODO: change when there is interval
        transfer.schedule.startDate = model.getTimezoneTimestamp(date);
        transfer.schedule.endDate = model.getTimezoneTimestamp(date);
    }

    /**
     * Executed when transfer completes
     */
    function onTransferComplete() {
        clearObject(editTransfer);
        model.clear();
        initializeFromAccount();
    }

    /**
     * Executed when transfer is canceled
     */
    function onTransferCancel() {
        clearObject(editTransfer);
        model.clear();
        initializeFromAccount();
    }

    /**
    * Clear obect properties
    */
    function clearObject(obj) {
        for (var o in obj) {
            if (typeof obj[o] === 'object') {
                clearObject(obj[o]);
            } else {
                delete editTransfer[o];
            }
        }
    }

    function handleSuccess(response) {
        bbActivityIndicator.hide(noop, noop);
        bus.publish(Event.Global.NAV_REVIEW_TRANSFER, {
            response: response,
            transferData: transfer,
            transfer: editTransfer
        });
        bus.publish('bawag.data.review-transfer', {
            response: response,
            transferData: transfer,
            transfer: editTransfer
        });
    }

    function handleError(error, retry) {
        bbActivityIndicator.hide();
        // Callback for button click with retry ability.
        var alertCallback = function(data) {
            if (data.callback === 'retry') {
                retry.call();
            } // else ignore (cancel / ok).
        };

        var buttons = alertButtons(error.code);
        var message = errorMessage(error.code);
        var title = model.Label.ERROR_ALERT;

        bbAlertDialog.show(alertCallback, noop, title, message, JSON.stringify(buttons));
    }

    function alertButtons(errorCode) {
        var buttons;
        switch (errorCode) {
            case serviceTransfers.E_AUTH:
            case serviceTransfers.E_USER:
                buttons = [{ type: 'POSITIVE', text: model.Label.BUTTON_OK, callbackFn: 'ok' }];
                break;
            case serviceTransfers.E_OFFLINE:
            default: // unexpected
                buttons = [{ type: 'NEUTRAL', text: model.Label.BUTTON_CANCEL, callbackFn: 'cancel' },
                    { type: 'POSITIVE', text: model.Label.BUTTON_RETRY, callbackFn: 'retry' }];
                // todo
                break;
        }
        return buttons;
    }

    function errorMessage(errorCode) {
        var errorMessages = {};
        errorMessages[serviceTransfers.E_AUTH] = model.Label.ERROR_AUTH_TRANSFER;
        errorMessages[serviceTransfers.E_USER] = model.Label.ERROR_USER_TRANSFER; // todo: more user errors
        errorMessages[serviceTransfers.E_OFFLINE] = model.Label.ERROR_OFFLINE;
        return errorMessages[errorCode] || model.Label.ERROR_UNEXPECTED_TRANSFER_SUBMIT;
    }

    function accountIban(account) {
        return (account.accountIdentification && account.accountIdentification[0]) ?
                account.accountIdentification[0].id : null
    }

    function ibansEqual(iban1, iban2) {
        if (!iban1 || !iban2) {
            return false;
        }

        function normaliseIban(iban) {
            return iban.toLowerCase().replace(/[^A-z0-9]/g, '');
        }

        return normaliseIban(iban1) === normaliseIban(iban2);
    }

}

module.exports = TransferCtrl;
