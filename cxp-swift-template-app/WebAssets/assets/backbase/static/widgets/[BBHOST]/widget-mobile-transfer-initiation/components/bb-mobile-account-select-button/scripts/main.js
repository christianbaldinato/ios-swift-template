/**
 * ------------------------------------------------------------------------
 * Entry point
 * ------------------------------------------------------------------------
 */

'use strict';
var ng = require('angular');
var Component = require('./component');

module.exports = ng.module('bb-mobile-account-select-button', [])
    .component('bbMobileAccountSelectButton', Component)

