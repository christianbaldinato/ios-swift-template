'use strict';

/**
 * Widget ViewModel
 * @ngInject
 * @name widgetModel
 * @private
 * @memberof module: widget-mobile-transfer-initiation
 * @kind class Angular service provider
 */
function WidgetModel(widget, serviceTransfers, bbI18n, $filter) {
    /**
     * Format for scheduling the transaction
     *
     * @type {string}
     */
    var dateFormat = 'yyyy-MM-dd';

    /**
     *
     * @type {object}
     */
    var state = {
        timezoneOffset: 0,
        fromAccount: {},
        isSubmitting: false,
        recipient: {},
        schedule: {
            "frequency": null,
            "frequencyText": '',
            "every": 0,
            "intervals": null,
            "startDate": null,
            "endDate": null,
            "date": null,
            "dateText": ''
        },
        details: {}
    };

    /**
     * Resolve translations
     */
    bbI18n.labelsLoaded.then(function(i18n) {
        state.schedule.dateText = i18n.TODAY;
        state.schedule.frequencyText = i18n.ONLY_ONCE;
    });

    return {
        get Label() { return bbI18n.labels; },

        /**
         * Returns transfer object
         * @public
         * @return {Object} Transfer object
         */
        getTransfer: function getTransfer() {
            return state;
        },

        /**
         * Loads the current transfer object
         * @public
         * @return {Object} Transfer
         */
        loadTransferDetails: function loadTransfer() {
            // todo: Domain model for transfer DTO.
            return Promise.resolve({});
        },

        /**
         * Set the select FROM account details.
         * @public
         * @param {Object} fromAccountDetails Account details (alias, balance, etc).
         * @return {ViewModel} Chainable instance.
         */
        setFromAccountDetails: function setFromAccountDetails(fromAccountDetails) {
            state.fromAccount = fromAccountDetails;
        },

        /**
         * Clear the recipient object
         *
         * @public
         */
        clearRecipient: function clearRecipient() {
            state.recipient = {};
        },

        /**
         * Clear the state
         *
         * @public
         */
        clear: function() {
            state.fromAccount = {};
            state.isSubmitting = false;
            state.recipient = {};
            state.details = {};
            state.schedule = {
                frequency: null,
                frequencyText: bbI18n && bbI18n.labels.ONLY_ONCE || '',
                every: 0,
                intervals: null,
                startDate: null,
                endDate: null,
                dateText: bbI18n && bbI18n.labels.TODAY || ''
            };

            setInitialSchedule();
        },

        /**
         * Submit the transfer
         *
         * @param {Object} transfer Transfer Object
         * @return {Promise} Response
         */
        submitTransfer: function submitTransfer(transfer) {
            // Save transfer to state.
            state.details = transfer;
            state.isSubmitting = true;

            // Todo: currency
            // Submit to service.
            return serviceTransfers.create(transfer)
                .then(function(response) {
                    state.isSubmitting = false;
                    return response;
                })
                .catch(function(err) {
                    state.isSubmitting = false;
                    throw err;
                });
        },

        getTimezoneTimestamp: getTimezoneTimestamp,

        /**
         * Get timezone offset from state
         */
        getTimezoneOffset: getTimezoneOffset,

        /**
         * Save timezone offset in state
         */
        setTimezoneOffset: setTimezoneOffset,

        /**
         * Set initial values of the schedule object*
         */
        setInitialSchedule: setInitialSchedule
    };

    /**
     * Add timezone offset to date
     *
     * @param date {Date}
     */
    function getTimezoneTimestamp(date) {
        var time;
        var offset;

        if (Object.prototype.toString.call(date) !== '[object Date]') {
            console.log('Not a date object', date);
            time = (new Date()).getTime();
        }

        time = date.getTime();
        offset = getTimezoneOffset() * 60000; // convert from seconds to milliseconds

        return time + offset;
    }


    /**
     * Get timezone offset from state
     */
    function getTimezoneOffset() {
        return state.timezoneOffset;
    }

    /**
     * Save timezone offset in state
     */
    function setTimezoneOffset(timezoneOffset) {
        state.timezoneOffset = timezoneOffset;
    }

    /**
     * Set initial values of the schedule object
     */
    function setInitialSchedule() {
        var date = new Date();
        var timestamp = getTimezoneTimestamp(new Date());
        state.schedule.startDate = timestamp;
        state.schedule.endDate = timestamp;
        state.schedule.date = $filter('date')(new Date(), dateFormat);
    }
}

module.exports = WidgetModel;
