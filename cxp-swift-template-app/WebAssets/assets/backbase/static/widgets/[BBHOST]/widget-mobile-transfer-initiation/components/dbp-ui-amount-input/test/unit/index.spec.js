'use strict';

// Dependencies
var ng = require('angular');
require('angular-mocks');

// Component
var dbpUiAmountInput = require('../../index');

var ngModule = window.module;
var ngInject = window.inject;

var CLASS_NAME = 'dbp-ui-amount-input';
var EMPTY_INTEGER_CLASS_NAME = 'empty-integer';
var EMPTY_DECIMAL_CLASS_NAME = 'empty-decimal';
var EMPTY_SYMBOL_CLASS_NAME = 'empty-symbol';

function createComponent(amount, currency) {
    var template = '<dbp-ui-amount-input amount="amount" currency="currency"></dbp-ui-amount-input>';

    return ngInject(function($rootScope, $parse, $compile) {
        this.scope = $rootScope.$new();
        this.scope.amount = amount;
        this.scope.currency = currency;
        this.$element = $compile(template)(this.scope);
        this.scope.$digest();
    });
}

describe('dbp-ui-amount-input', function() {
    beforeEach(ngModule(dbpUiAmountInput.name));

    describe('General check', function() {
        beforeEach(createComponent());

        it('should have class name', function() {
            expect(this.$element.hasClass(CLASS_NAME)).toEqual(true);
        });

        it('should have set of children', function() {
            var children = this.$element.children();
            expect(children.length).toEqual(4);
            expect(children.eq(0).hasClass(CLASS_NAME + '__currency-symbol'));
            expect(children.eq(1).hasClass(CLASS_NAME + '__integer'));
            expect(children.eq(2).hasClass(CLASS_NAME + '__separator'));
            expect(children.eq(3).hasClass(CLASS_NAME + '__decimal'));
        });
    });

    describe('Empty amount', function() {
        beforeEach(createComponent());

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.integer = wrapper.eq(1);
            this.separator = wrapper.eq(2);
            this.decimal = wrapper.eq(3);
        });

        it('should have initial values', function() {
            expect(this.integer.val()).toEqual('');
            expect(this.separator.text()).toEqual('.');
            expect(this.decimal.val()).toEqual('');
        });

        it('should have empty class names', function() {
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(true);
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(true);
        });
    });

    describe('Float number amount', function() {
        beforeEach(createComponent(123.3));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.integer = wrapper.eq(1);
            this.separator = wrapper.eq(2);
            this.decimal = wrapper.eq(3);
        });

        it('should have initial values', function() {
            expect(this.integer.val()).toEqual('123');
            expect(this.separator.text()).toEqual('.');
            expect(this.decimal.val()).toEqual('30');
        });

        it('should have empty class names', function() {
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
        });
    });

    describe('Integer number amount', function() {
        beforeEach(createComponent(234));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.integer = wrapper.eq(1);
            this.separator = wrapper.eq(2);
            this.decimal = wrapper.eq(3);
        });

        it('should have initial values', function() {
            expect(this.integer.val()).toEqual('234');
            expect(this.separator.text()).toEqual('.');
            expect(this.decimal.val()).toEqual('');
        });

        it('should have empty class names', function() {
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(true);
        });
    });

    describe('Decimal number amount', function() {
        beforeEach(createComponent(.76));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.integer = wrapper.eq(1);
            this.separator = wrapper.eq(2);
            this.decimal = wrapper.eq(3);
        });

        it('should have initial values', function() {
            expect(this.integer.val()).toEqual('');
            expect(this.separator.text()).toEqual('.');
            expect(this.decimal.val()).toEqual('76');
        });

        it('should have empty class names', function() {
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(true);
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
        });
    });

    describe('Too big float number amount', function() {
        beforeEach(createComponent(12345678.7654321));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.integer = wrapper.eq(1);
            this.separator = wrapper.eq(2);
            this.decimal = wrapper.eq(3);
        });

        it('should have initial values', function() {
            expect(this.integer.val()).toEqual('');
            expect(this.separator.text()).toEqual('.');
            expect(this.decimal.val()).toEqual('77');
        });

        it('should have empty class names', function() {
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(true);
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
        });
    });

    describe('Integer input changes', function() {
        beforeEach(createComponent(123.45));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.integer = wrapper.eq(1);
        });

        it('should reflect input changes', function() {

            expect(this.integer.val()).toEqual('123');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.integer.val(445);
            this.integer.triggerHandler('input');
            expect(this.integer.val()).toEqual('445');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(445.45);
        });

        it('should react to empty value', function() {
            expect(this.integer.val()).toEqual('123');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.integer.val('');
            this.integer.triggerHandler('input');

            expect(this.integer.val()).toEqual('');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(true);
            expect(this.scope.amount).toEqual(0.45);
        });

        it('should react to too big value', function() {
            expect(this.integer.val()).toEqual('123');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.integer.val(45454545);
            this.integer.triggerHandler('input');

            expect(this.integer.val()).toEqual('123');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);
        });

        it('should react to invalid value', function() {
            expect(this.integer.val()).toEqual('123');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.integer.val('adf');
            this.integer.triggerHandler('input');

            expect(this.integer.val()).toEqual('');
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(true);
            expect(this.scope.amount).toEqual(0.45);
        });
    });

    describe('Decimal input changes', function() {
        beforeEach(createComponent(123.45));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.decimal = wrapper.eq(3);
            document.body.appendChild(this.$element[0]);
        });

        afterEach(function() {
            document.body.removeChild(this.$element[0]);
        });

        it('should reflect input changes', function() {
            expect(this.decimal.val()).toEqual('45');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.decimal.val(98);
            this.decimal.triggerHandler('input');

            expect(this.decimal.val()).toEqual('98');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.98);
        });

        it('should react to empty value', function() {
            expect(this.decimal.val()).toEqual('45');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.decimal.val('');
            this.decimal.triggerHandler('input');

            expect(this.decimal.val()).toEqual('');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(true);
            expect(this.scope.amount).toEqual(123);
        });

        it('should react to one digit value', function() {
            expect(this.decimal.val()).toEqual('45');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.decimal[0].focus();
            this.decimal.val('1');
            this.decimal.triggerHandler('input');

            expect(this.decimal.val()).toEqual('1');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.1);

            this.decimal.triggerHandler('blur');

            expect(this.decimal.val()).toEqual('10');
            expect(this.scope.amount).toEqual(123.1);
        });

        it('should react to too big value', function() {
            expect(this.decimal.val()).toEqual('45');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.decimal.val(989898);
            this.decimal.triggerHandler('input');

            expect(this.decimal.val()).toEqual('45');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);
        });

        it('should react to invalid value', function() {
            expect(this.decimal.val()).toEqual('45');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.scope.amount).toEqual(123.45);

            this.decimal.val('adf');
            this.decimal.triggerHandler('input');

            expect(this.decimal.val()).toEqual('');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(true);
            expect(this.scope.amount).toEqual(123);
        });
    });

    describe('Currency symbol changes', function() {
        beforeEach(createComponent(123.56, 'USD'));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.symbol = wrapper.eq(0);
        });

        it('should have initial values', function() {
            expect(this.symbol.text()).toEqual('$');
            expect(this.$element.hasClass(EMPTY_SYMBOL_CLASS_NAME)).toEqual(false);
        });

        it('should react to currency changes', function() {
            this.scope.currency = null;
            this.scope.$digest();

            expect(this.symbol.text()).toEqual('');
            expect(this.$element.hasClass(EMPTY_SYMBOL_CLASS_NAME)).toEqual(true);

            this.scope.currency = 'GBP';
            this.scope.$digest();

            expect(this.symbol.text()).toEqual('£');
            expect(this.$element.hasClass(EMPTY_SYMBOL_CLASS_NAME)).toEqual(false);
        });
    });

    describe('Amount changes', function() {
        beforeEach(createComponent(0, 'USD'));

        beforeEach(function() {
            var wrapper = this.$element.children();
            this.integer = wrapper.eq(1);
            this.decimal = wrapper.eq(3);
        });

        it('should react to amount changes', function() {
            this.scope.amount = 448.51;
            this.scope.$digest();
            expect(this.integer.val()).toEqual('448');
            expect(this.decimal.val()).toEqual('51');
            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(false);
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(false);

            this.scope.amount = 0;
            this.scope.$digest();

            expect(this.integer.val()).toEqual('');
            expect(this.decimal.val()).toEqual('');

            expect(this.$element.hasClass(EMPTY_DECIMAL_CLASS_NAME)).toEqual(true);
            expect(this.$element.hasClass(EMPTY_INTEGER_CLASS_NAME)).toEqual(true);
        });
    });
});
