'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
            LOADED: 'cxp.item.loaded',
            NAV_SELECT_FROM_ACCOUNT: 'navigation:transfers.transfer.navigation-select-from-account',
            NAV_SELECT_RECIPIENT: 'navigation:transfers.transfer.navigation-beneficiary-select',
            NAV_SELECT_DATE: 'navigation:transfers.transfer.navigation-date-select',
            NAV_REVIEW_TRANSFER: 'navigation:transfers.transfer.review',
            FROM_ACCOUNT_SELECT: 'transfers.transfer.from-account-select',
            RECIPIENT_SELECT: 'transfers.transfer.beneficiary-select',
            SCHEDULE_SAVE: 'transfers.transfer.schedule-save',
            TRANSFER_CANCEL: 'transfers.transfer.cancel',
            TRANSFER_COMPLETE: 'transfers.transfer.complete',
        },
        Local: {
            ERROR_SUBMIT_UNEXPECTED: 'error.submit.unexpected'
        }
    },
    Preference: {
        SUGGESTED_BANKS: 'suggestedBanks',
        TIMEZONE_OFFSET: 'timezoneOffset'
    },
    Message: {
    },
    Error: {
        UNEXPECTED: 1
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
};

module.exports = WidgetConstants;
