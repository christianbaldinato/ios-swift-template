
window.bb = window.bb || {};
window.bb.mobile = window.bb.mobile || {};
window.bb.mobile.widget = window.bb.mobile.widget || {};
window.bb.mobile.widget.faq = (function($) {
    'use strict';

    var CONTENT_SERVICES_PROTOCOL = 'cs';

    var StructuredContent = {

        /**
         * prefModified callback
         * @param  {Object} widget Backbase widget object
         */
        prefModified: function(widget) {
            widget.refreshIncludes();

            var $widget = $(widget.htmlNode);
            $('.bd-structured-content-widget', $widget).attr('data-type', widget.getPreference('contentType') || 'Content');
        },
        /**
         * Initialize structured content widget
         * @param  {Object} widget Backbase widget object
         */
        start: function(widget) {
            var contentRef = widget.getPreference('contentRef'),
                dragItem,
                $widget = $(widget.body),
                isMasterpage = window.be && be.utils ? be.utils.module('top.bd.PageMgmtTree.selectedLink')['isMasterPage'] : false,
                isManageable = isMasterpage || (
                    widget.model.manageable === 'true' ||
                    widget.model.manageable === '' ||
                    widget.model.manageable === undefined
                );

            // This function will be executed when the widget is fully loaded, including its content
            var widgetContentIsLoaded = function() {

                // Inform the widget's host that the widget is fully loaded
                gadgets.pubsub.publish('cxp.item.loaded', {
                    id: widget.id
                });

                // accordian
                $(widget.body).find('.question').bind('touchstart mouseup', function(el) {
                    $(this).siblings('.answer-container').slideToggle();
                });
            };

            // accordian
            if ($(widget.body).find('.question').length) {
                widgetContentIsLoaded();
            }
            else {

                /**
                 * The code below observes retrieval of content, including images, and sends the 'cxp.item.loaded' event when all content is fully loaded.
                 * NOTE: Not tested on Android or with multiple images in the same structured content widget
                 * @author Niels Mouthaan
                 */

                // Create observer that will observe mutations to the content of the widget
                MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
                var observer = new MutationObserver(function(mutations, observer) {

                    // Function that checks if all images are loaded
                    var numberOfImages = 0;
                    var checkIfAllImagesAreLoaded = function() {
                        if (numberOfImages < 1) {
                            widgetContentIsLoaded();
                        }
                    };

                    // Loop through all mutations
                    for (var mutationIndex = 0; mutationIndex < mutations.length; mutationIndex++) {
                        var mutation = mutations[mutationIndex];

                        // Loop through all images in each mutation
                        var images = $(mutation.target).find('img');
                        numberOfImages = images.length;
                        images.load(function() {

                            // Image is loaded, check again if all images are loaded
                            numberOfImages--;
                            checkIfAllImagesAreLoaded();
                        });
                    }

                    // Initially check if everything is loaded
                    checkIfAllImagesAreLoaded();

                });

                // Register the observer on the placeholder of the content
                var placeholders = $widget.find('.bp-g-include');
                placeholders.each(function(placeholderIndex) {
                    observer.observe(placeholders[placeholderIndex], {
                        childList: true,
                    });
                });
            }

            /**
             * Logic applicable to CXP Manager functionality is provided below
             */

            if (!isManageable) return;
            if (!contentRef) {
                $widget.addClass('bd-empty-structured-content');
            }

            $('.bd-structured-content-widget', $widget).attr('data-type', widget.getPreference('contentType') || 'Content');

            widget.addEventListener('preferences-form', function(event) {
                var aPrefs = b$.portal.portalModel.filterPreferences(widget.model.preferences.array);
                for (var i = aPrefs.length - 1; i >= 0; i--) {
                    if (aPrefs[i].label == 'Template') {
                        var sTemplateVariants = widget.getPreference('templateList');
                        var arr = sTemplateVariants.split(',');
                        for (var j = 0, k = 0; j < arr.length; j++, k++) {
                            aPrefs[i].inputType.options[k] = {
                                label: arr[j],
                                value: arr[j + 1]
                            };
                            j++;
                        }
                    }
                }
            });

            widget.addEventListener('bdDrop.enter', function(e) {
                var item = e.detail.info.helper.bdDragData.fileDataArray[0]; // e.detail.info.helper is a HTMLElement!@$#%

                if (item.metaData && item.metaData['cmis:objectTypeId'] && item.metaData['cmis:objectTypeId'].property === 'bb:structuredcontent') {
                    dragItem = item;
                    $widget.addClass('bd-over-structured-content');
                } else {
                    dragItem = undefined;
                }
            });

            widget.addEventListener('bdDrop.leave', function(e) {
                $widget.removeClass('bd-over-structured-content');
            });

            widget.addEventListener('bdDrop.drop', function(e) {
                if (dragItem) {
                    if (!widget.model.preferences.getByName('contentRef')) {
                        var contentRefPref = b$.portal.portalModel.createPreference('contentRef', 'contentRef', undefined, '');
                        widget.model.preferences.add(contentRefPref);
                    }
                    widget.setPreference('contentRef', [CONTENT_SERVICES_PROTOCOL, dragItem.repository === 'contentRepository' ? dragItem.repository : '@portalRepository', dragItem.contentUId].join(':'));
                    widget.model.save(function() {
                        widget.refreshHTML();
                        $widget.removeClass('bd-empty-structured-content');
                    });
                    $widget.removeClass('bd-over-structured-content');
                }
                dragItem = undefined;
            });

            if (window.bd && window.bd.bindDropEvents) {
                window.bd.bindDropEvents(widget.htmlNode);
            }
        }
    };

    return StructuredContent;
})(jQuery);
