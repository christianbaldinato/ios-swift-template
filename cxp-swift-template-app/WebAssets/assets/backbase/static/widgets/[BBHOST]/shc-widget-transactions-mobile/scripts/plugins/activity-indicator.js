(function() {
    'use strict';

    /**
     * @ngInject
     */
    function ActivityIndicator() {
        this.$get = function(widget) {
            if (widget.features && widget.features.ActivityIndicator) {
                return widget.features.ActivityIndicator;
            } else {
                return null;
            }
        };
    }

    module.exports = ActivityIndicator;
})();
