'use strict';

/**
 * @ngInject
 */
function MainCtrl($scope, $rootScope, $timeout, $filter, TransactionsModel, Event, serviceAccounts,
    bbI18n) {
    /**
     * @public
     */
    var initLoading = true;
    var isLoading = true;
    var account = null;
    var transactions = [];
    var groupedTransactions = []; // grouped by date
    var transactionsError = null;
    var searchText = '';
    var hasMore = null;
    var lastDate = null;

    var fromHasFocus = false;
    var untilHasFocus = false;
    var dateNow = new Date();
    var datePrev = dateNow.setMonth(dateNow.getMonth() - 3);
    var fromDate = new Date(datePrev);
    var untilDate = new Date();

    /**
     * @private
     */
    var model = TransactionsModel;

    // api
    var api = {
        // getters
        get initLoading() {
            return initLoading;
        },
        get isLoading() {
            return isLoading;
        },
        get account() {
            return account;
        },
        get transactions() {
            return transactions;
        },
        get groupedTransactions() {
            return groupedTransactions;
        },
        get transactionsError() {
            return transactionsError;
        },
        get hasTransactions() {
            return hasTransactions();
        },
        get hasMore() {
            return hasMore;
        },
        get lastDate() {
            return lastDate;
        },
        get Labels() {
            return bbI18n.labels;
        },
        get fromHasFocus() {
            return fromHasFocus;
        },
        get untilHasFocus() {
            return untilHasFocus;
        },

        // methods
        $onInit: $onInit,
        $onDestroy: $onDestroy,
        showTransactionDetails: showTransactionDetails,
        load: load,
        loadMore: loadMore,

        // 2-way bind
        fromHasFocus: fromHasFocus,
        untilHasFocus: untilHasFocus,
        fromDate: fromDate,
        untilDate: untilDate
            // searchText: searchText
    };

    return api;

    /**
     * @public
     */
    function $onInit() {
        initEvents();

        $scope.$watch(function() {
            return api.fromHasFocus;
        }, dateFocus);
        $scope.$watch(function() {
            return api.untilHasFocus;
        }, dateFocus);
    }

    function initEvents() {
        gadgets.pubsub.subscribe('shc.open.transactions', onOpen);
    }

    function dateFocus() {
        if (api.fromHasFocus === false && api.untilHasFocus === false) {
            // alert(api.fromDate.getTime() + ' | ' + api.untilDate.getTime());
            load('&df=' + (api.fromDate.getTime() - api.fromDate.getTimezoneOffset() * 60000) + '&dt=' + (api.untilDate.getTime() - api.untilDate.getTimezoneOffset() * 60000));
        }
    }

    function onOpen(data) {

        data.timeout = data.timeout || 0;
        data.resetAccount = !!data.resetAccount;

        if (!account || (account.id !== data.account.id) || !!data.forceReset) {
            reset();

            // When is new transaction it needs time to get from PENDING to EXECUTED

            initLoading = true;
            transactions = [];
            $timeout(function() {
                // Reset account so the account balance is refreshed
                if (data.resetAccount) {
                    serviceAccounts.get(data.account.id)
                        .then(function(acc) {
                            account = acc;
                            load();
                        })
                        .catch(function(err) {
                            account = false;
                            throw err;
                        });
                } else {
                    account = data.account;
                    load();
                }
            }, data.timeout);

        }
    }

    /**
     * @public
     */
    function $onDestroy() {
        gadgets.pubsub.unsubscribe('shc.open.transactions', onOpen);
    }

    /**
     * @public
     */
    function showTransactionDetails(transaction) {
        gadgets.pubsub.publish('shc.open.transaction', {
            transaction: transaction,
            iban: $filter('iban')(account.accountIdentification[0].id)
        });
    }

    /**
     * @public
     */
    function load(filter) {
        initLoading = true;
        transactions = [];
        return loadMore(filter);
    }

    /**
     * @public
     */
    function loadMore(filter) {
        isLoading = true;
        model.getListMore(account.id, transactions.length, filter)
            .then(enhanceTransactions)
            .then(moreTransactionsLoaded) // returns all transactions
            .then(groupTransactions) // groups all
            .then(groupsLoaded)
            .catch(handleError);
    }

    /**
     *
     * @param moreTransactions
     * @return {Array}
     */
    function moreTransactionsLoaded(moreTransactions) {
        hasMore = (moreTransactions.length >= TransactionsModel.PAGE_SIZE);
        transactions = transactions.concat(moreTransactions);
        return transactions;
    }

    /**
     *
     * @param grouped
     */
    function groupsLoaded(grouped) {
        groupedTransactions = grouped;

        var lastGroup = groupedTransactions[groupedTransactions.length - 1];
        lastDate = lastGroup && lastGroup.title;

        initLoading = false;
        isLoading = false;
    }

    /**
     * @public
     */
    function hasTransactions() {
        return transactions.length > 0;
    }

    function enhanceTransactions(trans) {
        return trans.map(enhanceTransaction);
    }

    function enhanceTransaction(transaction) {
        // todo: use locale date formatting.
        var date = new Date(transaction.bookingDateTime);
        var day = bbI18n.labels['DAY_' + (date.getDay() + 1)];
        var month = bbI18n.labels['MONTH_' + (date.getMonth() + 1)];
        transaction.dateString = day + ', ' +
            month + ' ' +
            date.getDate();
        return transaction;
    }

    function handleError(err) {
        initLoading = false;
        isLoading = false;
        switch (err.code) {
            case TransactionsModel.E_OFFLINE:
                err.message = err.message || bbI18n.labels.ERROR_OFFLINE;
                gadgets.pubsub.publish(Event.Global.OFFLINE, err);
                break;
            case TransactionsModel.E_AUTH:
                err.message = err.message || bbI18n.labels.ERROR_AUTH;
                gadgets.pubsub.publish(Event.Global.AUTH, err);
                break;
            default:
                // Unexpected error.
                err.message = err.message || bbI18n.labels.ERROR_UNEXPECTED_TRANSACTIONS;
                gadgets.pubsub.publish(Event.Global.ERROR_UNEXPECTED, err);
        }

        transactionsError = err;
    }

    function reset() {
        $rootScope.safeApply(function() {
            initLoading = true;
            isLoading = true;
            transactionsError = null;
            account = null;
            transactions = [];
            searchText = '';
            hasMore = null;
            lastDate = null;
        });
    }

    function groupTransactions(trans) {
        var grouped = [];
        var groupByKey = 'dateString';
        var groupHash = {}; // dateString: group index

        trans.forEach(function(transaction) {
            var key = transaction[groupByKey];

            // Create group if doesn't exist already.
            if (typeof groupHash[key] === 'undefined') {
                groupHash[key] = grouped.length;
                grouped.push({
                    title: key,
                    transactions: []
                });
            }

            // Get group index for key & push transaction to group.
            var groupIndex = groupHash[key];
            grouped[groupIndex].transactions.push(transaction);
        });

        return grouped;
    }
}

module.exports = MainCtrl;
