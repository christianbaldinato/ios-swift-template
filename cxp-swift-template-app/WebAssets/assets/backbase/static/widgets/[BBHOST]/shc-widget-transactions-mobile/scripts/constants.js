'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
            OFFLINE: 'offline:request-failed',
            AUTH: 'user-auth:request-unauthorised',
            ERROR_UNEXPECTED: 'error:unexpected'
        },
        Local: {
        }
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
};

module.exports = WidgetConstants;
