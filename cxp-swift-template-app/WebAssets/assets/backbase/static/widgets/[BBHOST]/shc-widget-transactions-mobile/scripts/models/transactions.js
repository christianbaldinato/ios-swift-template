'use strict';

var E_OFFLINE = 1;
var E_UNEXPECTED = 2;
var E_AUTH = 3;
var PAGE_SIZE = 20;
var SORT = '-bookingDateTime';

/**
 * @ngInject
 */
function TransactionsModel($http, $q, widget) {
    var config = {
        pageSize: PAGE_SIZE,
        sort: SORT
    };
    var transactionsUrlPref = widget.model.getPreference('transactionsUrl');
    var endpoint = transactionsUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);

    var api = {
        E_OFFLINE: E_OFFLINE,
        E_AUTH: E_AUTH,
        E_UNEXPECTED: E_UNEXPECTED,
        PAGE_SIZE: PAGE_SIZE,
        SORT: SORT,
        getList: getList,
        getListMore: getListMore
    };

    return api;

    function getList(accountId) {
        return getListMore(accountId);
    };

    function getListMore(accountId, from, filter) {
        from = from || 0;
        var url = endpoint.replace('$(accountId)', accountId);
        url = (url+ '?f=' + from +
                '&l=' + config.pageSize +
                '&sort=' + config.sort);

        if (filter && filter.length > 0) {
            url += filter;
        }

        return $http.get(url)
            .then(parseResponse)
            .catch(onError);
    }

    function parseResponse(response) {
        return response.data;
    }

    /*
     * Normalise errors
     */
    function onError(httpErr) {
        var err = {
            __httpResponse: httpErr
        };

        if (httpErr.status <= 1) {
            err.code = E_OFFLINE;
        }
        else if (httpErr.status === 401) {
            err.code = E_AUTH;
        }
        else {
            err.code = E_UNEXPECTED;
        }

        throw err;
    }
}

module.exports = TransactionsModel;
