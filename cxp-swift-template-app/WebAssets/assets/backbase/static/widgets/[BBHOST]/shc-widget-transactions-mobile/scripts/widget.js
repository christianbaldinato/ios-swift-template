window.shc = window.shc || {};
shc.mobile = shc.mobile || {};
shc.mobile.widget = shc.mobile.widget || {};
shc.mobile.widget.transactions = (function(angular, mobilePlugins) {
    'use strict';

    var TransactionsWidget = function(widget) {
        var self = this;

        this.widget = widget;

        var config = require('./config');

        // Controllers
        var MainCtrl = require('./controllers/main');

        // Models
        var TransactionsModel = require('./models/transactions');

        // Constants
        var constants = require('./constants');

        // Native Plugins
        var AlertDialogProvider = require('./plugins/alert-dialog');
        var ActivityIndicatorProvider = require('./plugins/activity-indicator');

        var deps = [
            mobilePlugins.name
        ];
        angular.module(widget.id, deps)
            .config(config)
            .constant(constants)
            // Filters
            .filter('iban', ibanModule.filter)
            // Models
            .service('TransactionsModel', TransactionsModel)
            .service('ibanService', ibanModule.service)
            .factory('serviceAccounts', serviceAccounts)
            // Controllers
            .controller('MainCtrl', MainCtrl)
            // Directives
            .directive('formatAmount', formatAmount)
            .directive('focusElement', focusElement)
            .directive('searchBox', searchBox)
            .directive('validIban', ibanModule.validator)
            .directive('avatar', avatar)
            // Native Plugins
            .provider('AlertDialog', AlertDialogProvider)
            .provider('ActivityIndicator', ActivityIndicatorProvider)
            // Widget Object
            .provider('widget', function() {
                this.$get = function() {
                    return widget;
                };
            })
            .run(function($rootScope, widget, serviceAccounts, SERVER_ROOT){
                // Add a safeApply function.
                $rootScope.safeApply = function(fn) {
                    var phase = this.$root.$$phase;
                    if(phase == '$apply' || phase == '$digest') {
                        if(fn && (typeof(fn) === 'function')) {
                            fn();
                        }
                    } else {
                        this.$apply(fn);
                    }
                };

                serviceAccounts.setConfig({
                    'listEndpoint': getEndpoint('accountsListEndpoint')
                });

                self.start();

                // Configure services.
                function getEndpoint(endpointPref) {
                    return widget.getPreference(endpointPref).replace('$(servicesPath)', SERVER_ROOT);
                }
            });
        // Bootstrap the widget as an Angular App
        angular.bootstrap(widget.body, [widget.id]);
    };

    TransactionsWidget.prototype.start = function() {
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: this.widget.id
        });
    };

    return {
        init: function(widget) {
            var transactionsApp = new TransactionsWidget(widget);
            return transactionsApp;
        }
    };
}(window.angular, window.mobilePlugins));
