/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!***************************!*\
  !*** ./scripts/widget.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	window.shc = window.shc || {};
	shc.mobile = shc.mobile || {};
	shc.mobile.widget = shc.mobile.widget || {};
	shc.mobile.widget.transactions = (function(angular, mobilePlugins) {
	    'use strict';
	
	    var TransactionsWidget = function(widget) {
	        var self = this;
	
	        this.widget = widget;
	
	        var config = __webpack_require__(/*! ./config */ 1);
	
	        // Controllers
	        var MainCtrl = __webpack_require__(/*! ./controllers/main */ 3);
	
	        // Models
	        var TransactionsModel = __webpack_require__(/*! ./models/transactions */ 4);
	
	        // Constants
	        var constants = __webpack_require__(/*! ./constants */ 5);
	
	        // Native Plugins
	        var AlertDialogProvider = __webpack_require__(/*! ./plugins/alert-dialog */ 6);
	        var ActivityIndicatorProvider = __webpack_require__(/*! ./plugins/activity-indicator */ 7);
	
	        var deps = [
	            mobilePlugins.name
	        ];
	        angular.module(widget.id, deps)
	            .config(config)
	            .constant(constants)
	            // Filters
	            .filter('iban', ibanModule.filter)
	            // Models
	            .service('TransactionsModel', TransactionsModel)
	            .service('ibanService', ibanModule.service)
	            .factory('serviceAccounts', serviceAccounts)
	            // Controllers
	            .controller('MainCtrl', MainCtrl)
	            // Directives
	            .directive('formatAmount', formatAmount)
	            .directive('focusElement', focusElement)
	            .directive('searchBox', searchBox)
	            .directive('validIban', ibanModule.validator)
	            .directive('avatar', avatar)
	            // Native Plugins
	            .provider('AlertDialog', AlertDialogProvider)
	            .provider('ActivityIndicator', ActivityIndicatorProvider)
	            // Widget Object
	            .provider('widget', function() {
	                this.$get = function() {
	                    return widget;
	                };
	            })
	            .run(function($rootScope, widget, serviceAccounts, SERVER_ROOT){
	                // Add a safeApply function.
	                $rootScope.safeApply = function(fn) {
	                    var phase = this.$root.$$phase;
	                    if(phase == '$apply' || phase == '$digest') {
	                        if(fn && (typeof(fn) === 'function')) {
	                            fn();
	                        }
	                    } else {
	                        this.$apply(fn);
	                    }
	                };
	
	                serviceAccounts.setConfig({
	                    'listEndpoint': getEndpoint('accountsListEndpoint')
	                });
	
	                self.start();
	
	                // Configure services.
	                function getEndpoint(endpointPref) {
	                    return widget.getPreference(endpointPref).replace('$(servicesPath)', SERVER_ROOT);
	                }
	            });
	        // Bootstrap the widget as an Angular App
	        angular.bootstrap(widget.body, [widget.id]);
	    };
	
	    TransactionsWidget.prototype.start = function() {
	        gadgets.pubsub.publish('cxp.item.loaded', {
	            id: this.widget.id
	        });
	    };
	
	    return {
	        init: function(widget) {
	            var transactionsApp = new TransactionsWidget(widget);
	            return transactionsApp;
	        }
	    };
	}(window.angular, window.mobilePlugins));


/***/ },
/* 1 */
/*!***************************!*\
  !*** ./scripts/config.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var labels = __webpack_require__(/*! ../locale/all.json */ 2);
	
	/**
	 * @ngInject
	 */
	function WidgetConfig($provide, $compileProvider, bbI18nProvider) {
	    $compileProvider.debugInfoEnabled(false);
	    bbI18nProvider.setDefaultLabels(labels);
	};
	
	module.exports = WidgetConfig;


/***/ },
/* 2 */
/*!*************************!*\
  !*** ./locale/all.json ***!
  \*************************/
/***/ function(module, exports) {

	module.exports = {
		"en": {
			"MONTH_1": "January",
			"MONTH_2": "February",
			"MONTH_3": "March",
			"MONTH_4": "April",
			"MONTH_5": "May",
			"MONTH_6": "June",
			"MONTH_7": "July",
			"MONTH_8": "August",
			"MONTH_9": "September",
			"MONTH_10": "October",
			"MONTH_11": "November",
			"MONTH_12": "December",
			"DAY_1": "Sunday",
			"DAY_2": "Monday",
			"DAY_3": "Tuesday",
			"DAY_4": "Wednesday",
			"DAY_5": "Thursday",
			"DAY_6": "Friday",
			"DAY_7": "Saturday",
			"SEARCH_PLACEHOLDER": "Search",
			"TRY_AGAIN": "Try Again",
			"ERROR_OFFLINE": "Unable to establish connection. Check your internet connection.",
			"ERROR_AUTH": "It appears as if you've been logged out.",
			"SHOWING_TRANSACTIONS_UNTIL": "Showing transactions until",
			"NO_TRANSACTIONS": "You have no transactions for this account",
			"NO_TRANSACTIONS_SEARCH": "There are no transactions containing your search query",
			"LOAD_MORE_TRANSACTIONS": "Load more transactions",
			"ERROR_UNEXPECTED_TRANSACTIONS": "Unexpected error loading your transactions."
		},
		"nl": {
			"MONTH_1": "Januari",
			"MONTH_2": "Februari",
			"MONTH_3": "Maart",
			"MONTH_4": "April",
			"MONTH_5": "Mei",
			"MONTH_6": "Juni",
			"MONTH_7": "Juli",
			"MONTH_8": "Augustus",
			"MONTH_9": "September",
			"MONTH_10": "Oktober",
			"MONTH_11": "November",
			"MONTH_12": "December",
			"DAY_1": "Zondag",
			"DAY_2": "Maandag",
			"DAY_3": "Dinsdag",
			"DAY_4": "Woensdag",
			"DAY_5": "Donderdag",
			"DAY_6": "Vrijdag",
			"DAY_7": "Zaterdag",
			"SEARCH_PLACEHOLDER": "Zoeken",
			"TRY_AGAIN": "Probeer het nog eens",
			"ERROR_OFFLINE": "Kan geen verbinding maken. Controleer uw internetverbinding.",
			"ERROR_AUTH": "Het lijkt alsof u bent uitgelogd.",
			"SHOWING_TRANSACTIONS_UNTIL": "Resultaat transacties tot",
			"NO_TRANSACTIONS": "Je hebt geen transacties.",
			"NO_TRANSACTIONS_SEARCH": "Geen transacties gevonden",
			"LOAD_MORE_TRANSACTIONS": "Laad meer transacties",
			"ERROR_UNEXPECTED_TRANSACTIONS": "Onverwachte fout bij het laden van uw transacties"
		},
		"de": {
			"MONTH_1": "Januar",
			"MONTH_2": "Februar",
			"MONTH_3": "März",
			"MONTH_4": "April",
			"MONTH_5": "Mai",
			"MONTH_6": "Juni",
			"MONTH_7": "Juli",
			"MONTH_8": "August",
			"MONTH_9": "September",
			"MONTH_10": "Oktober",
			"MONTH_11": "November",
			"MONTH_12": "Dezember",
			"DAY_1": "Sonntag",
			"DAY_2": "Montag",
			"DAY_3": "Dienstag",
			"DAY_4": "Mittwoch",
			"DAY_5": "Donnerstag",
			"DAY_6": "Freitag",
			"DAY_7": "Samstag",
			"SEARCH_PLACEHOLDER": "Suche",
			"TRY_AGAIN": "Versuchen Sie es erneut",
			"ERROR_OFFLINE": "Es konnte keine Verbindung aufgebaut werden. Bitte überprüfen Sie Ihre Internetverbindung.",
			"ERROR_AUTH": "Es scheint, als ob Sie bereits ausgeloggt sind.",
			"SHOWING_TRANSACTIONS_UNTIL": "Ergebnis Transaktionen bis",
			"NO_TRANSACTIONS": "Sie haben keine Transaktionen.",
			"NO_TRANSACTIONS_SEARCH": "Keine Transaktionen gefunden",
			"LOAD_MORE_TRANSACTIONS": "Laden Sie mehr Transaktionen",
			"ERROR_UNEXPECTED_TRANSACTIONS": "Unerwarteter Fehler während des Ladevorgangs der Transaktionen"
		}
	};

/***/ },
/* 3 */
/*!*************************************!*\
  !*** ./scripts/controllers/main.js ***!
  \*************************************/
/***/ function(module, exports) {

	'use strict';
	
	/**
	 * @ngInject
	 */
	function MainCtrl($scope, $rootScope, $timeout, $filter, TransactionsModel, Event, serviceAccounts,
	    bbI18n) {
	    /**
	     * @public
	     */
	    var initLoading = true;
	    var isLoading = true;
	    var account = null;
	    var transactions = [];
	    var groupedTransactions = []; // grouped by date
	    var transactionsError = null;
	    var searchText = '';
	    var hasMore = null;
	    var lastDate = null;
	
	    var fromHasFocus = false;
	    var untilHasFocus = false;
	    var dateNow = new Date();
	    var datePrev = dateNow.setMonth(dateNow.getMonth() - 3);
	    var fromDate = new Date(datePrev);
	    var untilDate = new Date();
	
	    /**
	     * @private
	     */
	    var model = TransactionsModel;
	
	    // api
	    var api = {
	        // getters
	        get initLoading() {
	            return initLoading;
	        },
	        get isLoading() {
	            return isLoading;
	        },
	        get account() {
	            return account;
	        },
	        get transactions() {
	            return transactions;
	        },
	        get groupedTransactions() {
	            return groupedTransactions;
	        },
	        get transactionsError() {
	            return transactionsError;
	        },
	        get hasTransactions() {
	            return hasTransactions();
	        },
	        get hasMore() {
	            return hasMore;
	        },
	        get lastDate() {
	            return lastDate;
	        },
	        get Labels() {
	            return bbI18n.labels;
	        },
	        get fromHasFocus() {
	            return fromHasFocus;
	        },
	        get untilHasFocus() {
	            return untilHasFocus;
	        },
	
	        // methods
	        $onInit: $onInit,
	        $onDestroy: $onDestroy,
	        showTransactionDetails: showTransactionDetails,
	        load: load,
	        loadMore: loadMore,
	
	        // 2-way bind
	        fromHasFocus: fromHasFocus,
	        untilHasFocus: untilHasFocus,
	        fromDate: fromDate,
	        untilDate: untilDate
	            // searchText: searchText
	    };
	
	    return api;
	
	    /**
	     * @public
	     */
	    function $onInit() {
	        initEvents();
	
	        $scope.$watch(function() {
	            return api.fromHasFocus;
	        }, dateFocus);
	        $scope.$watch(function() {
	            return api.untilHasFocus;
	        }, dateFocus);
	    }
	
	    function initEvents() {
	        gadgets.pubsub.subscribe('shc.open.transactions', onOpen);
	    }
	
	    function dateFocus() {
	        if (api.fromHasFocus === false && api.untilHasFocus === false) {
	            // alert(api.fromDate.getTime() + ' | ' + api.untilDate.getTime());
	            load('&df=' + (api.fromDate.getTime() - api.fromDate.getTimezoneOffset() * 60000) + '&dt=' + (api.untilDate.getTime() - api.untilDate.getTimezoneOffset() * 60000));
	        }
	    }
	
	    function onOpen(data) {
	
	        data.timeout = data.timeout || 0;
	        data.resetAccount = !!data.resetAccount;
	
	        if (!account || (account.id !== data.account.id) || !!data.forceReset) {
	            reset();
	
	            // When is new transaction it needs time to get from PENDING to EXECUTED
	
	            initLoading = true;
	            transactions = [];
	            $timeout(function() {
	                // Reset account so the account balance is refreshed
	                if (data.resetAccount) {
	                    serviceAccounts.get(data.account.id)
	                        .then(function(acc) {
	                            account = acc;
	                            load();
	                        })
	                        .catch(function(err) {
	                            account = false;
	                            throw err;
	                        });
	                } else {
	                    account = data.account;
	                    load();
	                }
	            }, data.timeout);
	
	        }
	    }
	
	    /**
	     * @public
	     */
	    function $onDestroy() {
	        gadgets.pubsub.unsubscribe('shc.open.transactions', onOpen);
	    }
	
	    /**
	     * @public
	     */
	    function showTransactionDetails(transaction) {
	        gadgets.pubsub.publish('shc.open.transaction', {
	            transaction: transaction,
	            iban: $filter('iban')(account.accountIdentification[0].id)
	        });
	    }
	
	    /**
	     * @public
	     */
	    function load(filter) {
	        initLoading = true;
	        transactions = [];
	        return loadMore(filter);
	    }
	
	    /**
	     * @public
	     */
	    function loadMore(filter) {
	        isLoading = true;
	        model.getListMore(account.id, transactions.length, filter)
	            .then(enhanceTransactions)
	            .then(moreTransactionsLoaded) // returns all transactions
	            .then(groupTransactions) // groups all
	            .then(groupsLoaded)
	            .catch(handleError);
	    }
	
	    /**
	     *
	     * @param moreTransactions
	     * @return {Array}
	     */
	    function moreTransactionsLoaded(moreTransactions) {
	        hasMore = (moreTransactions.length >= TransactionsModel.PAGE_SIZE);
	        transactions = transactions.concat(moreTransactions);
	        return transactions;
	    }
	
	    /**
	     *
	     * @param grouped
	     */
	    function groupsLoaded(grouped) {
	        groupedTransactions = grouped;
	
	        var lastGroup = groupedTransactions[groupedTransactions.length - 1];
	        lastDate = lastGroup && lastGroup.title;
	
	        initLoading = false;
	        isLoading = false;
	    }
	
	    /**
	     * @public
	     */
	    function hasTransactions() {
	        return transactions.length > 0;
	    }
	
	    function enhanceTransactions(trans) {
	        return trans.map(enhanceTransaction);
	    }
	
	    function enhanceTransaction(transaction) {
	        // todo: use locale date formatting.
	        var date = new Date(transaction.bookingDateTime);
	        var day = bbI18n.labels['DAY_' + (date.getDay() + 1)];
	        var month = bbI18n.labels['MONTH_' + (date.getMonth() + 1)];
	        transaction.dateString = day + ', ' +
	            month + ' ' +
	            date.getDate();
	        return transaction;
	    }
	
	    function handleError(err) {
	        initLoading = false;
	        isLoading = false;
	        switch (err.code) {
	            case TransactionsModel.E_OFFLINE:
	                err.message = err.message || bbI18n.labels.ERROR_OFFLINE;
	                gadgets.pubsub.publish(Event.Global.OFFLINE, err);
	                break;
	            case TransactionsModel.E_AUTH:
	                err.message = err.message || bbI18n.labels.ERROR_AUTH;
	                gadgets.pubsub.publish(Event.Global.AUTH, err);
	                break;
	            default:
	                // Unexpected error.
	                err.message = err.message || bbI18n.labels.ERROR_UNEXPECTED_TRANSACTIONS;
	                gadgets.pubsub.publish(Event.Global.ERROR_UNEXPECTED, err);
	        }
	
	        transactionsError = err;
	    }
	
	    function reset() {
	        $rootScope.safeApply(function() {
	            initLoading = true;
	            isLoading = true;
	            transactionsError = null;
	            account = null;
	            transactions = [];
	            searchText = '';
	            hasMore = null;
	            lastDate = null;
	        });
	    }
	
	    function groupTransactions(trans) {
	        var grouped = [];
	        var groupByKey = 'dateString';
	        var groupHash = {}; // dateString: group index
	
	        trans.forEach(function(transaction) {
	            var key = transaction[groupByKey];
	
	            // Create group if doesn't exist already.
	            if (typeof groupHash[key] === 'undefined') {
	                groupHash[key] = grouped.length;
	                grouped.push({
	                    title: key,
	                    transactions: []
	                });
	            }
	
	            // Get group index for key & push transaction to group.
	            var groupIndex = groupHash[key];
	            grouped[groupIndex].transactions.push(transaction);
	        });
	
	        return grouped;
	    }
	}
	
	module.exports = MainCtrl;


/***/ },
/* 4 */
/*!****************************************!*\
  !*** ./scripts/models/transactions.js ***!
  \****************************************/
/***/ function(module, exports) {

	'use strict';
	
	var E_OFFLINE = 1;
	var E_UNEXPECTED = 2;
	var E_AUTH = 3;
	var PAGE_SIZE = 20;
	var SORT = '-bookingDateTime';
	
	/**
	 * @ngInject
	 */
	function TransactionsModel($http, $q, widget) {
	    var config = {
	        pageSize: PAGE_SIZE,
	        sort: SORT
	    };
	    var transactionsUrlPref = widget.model.getPreference('transactionsUrl');
	    var endpoint = transactionsUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);
	
	    var api = {
	        E_OFFLINE: E_OFFLINE,
	        E_AUTH: E_AUTH,
	        E_UNEXPECTED: E_UNEXPECTED,
	        PAGE_SIZE: PAGE_SIZE,
	        SORT: SORT,
	        getList: getList,
	        getListMore: getListMore
	    };
	
	    return api;
	
	    function getList(accountId) {
	        return getListMore(accountId);
	    };
	
	    function getListMore(accountId, from, filter) {
	        from = from || 0;
	        var url = endpoint.replace('$(accountId)', accountId);
	        url = (url+ '?f=' + from +
	                '&l=' + config.pageSize +
	                '&sort=' + config.sort);
	
	        if (filter && filter.length > 0) {
	            url += filter;
	        }
	
	        return $http.get(url)
	            .then(parseResponse)
	            .catch(onError);
	    }
	
	    function parseResponse(response) {
	        return response.data;
	    }
	
	    /*
	     * Normalise errors
	     */
	    function onError(httpErr) {
	        var err = {
	            __httpResponse: httpErr
	        };
	
	        if (httpErr.status <= 1) {
	            err.code = E_OFFLINE;
	        }
	        else if (httpErr.status === 401) {
	            err.code = E_AUTH;
	        }
	        else {
	            err.code = E_UNEXPECTED;
	        }
	
	        throw err;
	    }
	}
	
	module.exports = TransactionsModel;


/***/ },
/* 5 */
/*!******************************!*\
  !*** ./scripts/constants.js ***!
  \******************************/
/***/ function(module, exports) {

	'use strict';
	
	var WidgetConstants = {
	    /**
	     * Local and global events, that widget publishes (emits) or relies on
	     */
	    Event: {
	        Global: {
	            OFFLINE: 'offline:request-failed',
	            AUTH: 'user-auth:request-unauthorised',
	            ERROR_UNEXPECTED: 'error:unexpected'
	        },
	        Local: {
	        }
	    },
	    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
	};
	
	module.exports = WidgetConstants;


/***/ },
/* 6 */
/*!*****************************************!*\
  !*** ./scripts/plugins/alert-dialog.js ***!
  \*****************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function AlertDialog() {
	        this.$get = function(widget) {
	            if (widget.features && widget.features.AlertDialog) {
	                return widget.features.AlertDialog;
	            } else {
	                return null;
	            }
	        };
	    }
	
	    module.exports = AlertDialog;
	})();


/***/ },
/* 7 */
/*!***********************************************!*\
  !*** ./scripts/plugins/activity-indicator.js ***!
  \***********************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function ActivityIndicator() {
	        this.$get = function(widget) {
	            if (widget.features && widget.features.ActivityIndicator) {
	                return widget.features.ActivityIndicator;
	            } else {
	                return null;
	            }
	        };
	    }
	
	    module.exports = ActivityIndicator;
	})();


/***/ }
/******/ ]);
//# sourceMappingURL=main.js.map