(function() {
    'use strict';

    /**
     * @ngInject
     */
    function SecureStorage() {
        this.$get = function(widget) {
            if (widget.features && widget.features.SecureStoragePlugin) {
                return widget.features.SecureStoragePlugin;
            } else {
                return null;
            }
        };
    }

    module.exports = SecureStorage;
})();
