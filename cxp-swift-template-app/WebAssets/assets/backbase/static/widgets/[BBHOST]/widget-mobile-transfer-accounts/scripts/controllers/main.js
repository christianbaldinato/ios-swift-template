(function() {
    'use strict';

    /**
     * @ngInject
     */
    function MainCtrl(AccountsModel, bbI18n) {

        /**
         * List of accounts
         *
         * @type {Array}
         */
        var accounts = [];

        /**
         * Public API
         *
         * @type {object}
         */
        var api = {
            get accounts() { return accounts; },
            get Labels() { return bbI18n.labels; },

            $onInit: $onInit,
            $onDestroy: $onDestroy,
            publishAccount: publishAccount
        };

        return api;

        /**
         * Initialize widget
         */
        function $onInit() {
            // TODO use the object from transfer initiation
            AccountsModel.getList().then(function(response) {
                accounts = response.data;
            });
        }

        /**
         * Destroy widget
         */
        function $onDestroy() {}


        /**
         *
         * @param account
         */
        function publishAccount(account) {
            gadgets.pubsub.publish('transfers.transfer.from-account-select', {
                account:{
                    accountId: account.id,
                    accountDetails: account
                }
            });
            gadgets.pubsub.publish('navigation:go-back-to-transfer');
        }
    }

    module.exports = MainCtrl;
})();
