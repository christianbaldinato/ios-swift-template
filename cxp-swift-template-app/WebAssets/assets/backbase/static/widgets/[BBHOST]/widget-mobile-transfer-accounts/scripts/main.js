/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!***************************!*\
  !*** ./scripts/widget.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(/*! style!css!less!../styles/main.less */ 1);
	
	window.shc = window.shc || {};
	shc.mobile = shc.mobile || {};
	shc.mobile.widget = shc.mobile.widget || {};
	shc.mobile.widget.transferAccounts = (function(angular, mobilePlugins) {
	    'use strict';
	
	    var TransferAccountsWidget = function(widget) {
	        var self = this;
	
	        this.widget = widget;
	
	        var config = __webpack_require__(/*! ./config */ 5);
	
	        // Controllers
	        var MainCtrl = __webpack_require__(/*! ./controllers/main */ 7);
	
	        // Models
	        var AccountsModel = __webpack_require__(/*! ./models/accounts */ 8);
	
	        // Native Plugins
	        var AlertDialogProvider = __webpack_require__(/*! ./plugins/alert-dialog */ 9);
	        var ActivityIndicatorProvider = __webpack_require__(/*! ./plugins/activity-indicator */ 10);
	
	        var deps = [ mobilePlugins.name ];
	        angular.module(widget.id, deps)
	            .config(config)
	            // Filters
	            .filter('iban', ibanModule.filter)
	            // Models
	            .service('AccountsModel', AccountsModel)
	            // Controllers
	            .controller('MainCtrl', MainCtrl)
	            // Directives
	            .directive('formatAmount', formatAmount)
	            // Native Plugins
	            .provider('AlertDialog', AlertDialogProvider)
	            .provider('ActivityIndicator', ActivityIndicatorProvider)
	            // Widget Object
	            .provider('widget', function() {
	                this.$get = function() {
	                    return widget;
	                };
	            })
	            .run(function(){
	                self.start();
	            });
	        // Bootstrap the widget as an Angular App
	        angular.bootstrap(widget.body, [widget.id]);
	    };
	
	    TransferAccountsWidget.prototype.start = function() {
	        gadgets.pubsub.publish('cxp.item.loaded', {
	            id: this.widget.id
	        });
	    };
	
	    return {
	        init: function(widget) {
	            var transferAccountsApp = new TransferAccountsWidget(widget);
	            return transferAccountsApp;
	        }
	    };
	}(window.angular, window.mobilePlugins));


/***/ },
/* 1 */
/*!**************************************************************************!*\
  !*** ./~/style-loader!./~/css-loader!./~/less-loader!./styles/main.less ***!
  \**************************************************************************/
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(/*! !./../~/css-loader!./../~/less-loader!./main.less */ 2);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(/*! ./../~/style-loader/addStyles.js */ 4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../node_modules/css-loader/index.js!./../node_modules/less-loader/index.js!./main.less", function() {
				var newContent = require("!!./../node_modules/css-loader/index.js!./../node_modules/less-loader/index.js!./main.less");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 2 */
/*!*********************************************************!*\
  !*** ./~/css-loader!./~/less-loader!./styles/main.less ***!
  \*********************************************************/
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(/*! ./../~/css-loader/lib/css-base.js */ 3)();
	// imports
	
	
	// module
	exports.push([module.id, ".widget-mobile-transfer-accounts .panel-title {\n  background-color: #ece8e4;\n  margin: 0;\n  padding: 10px 15px;\n  font-size: 12px;\n}\n.widget-mobile-transfer-accounts .accounts-list {\n  margin-top: 0px;\n}\n.widget-mobile-transfer-accounts .accounts-list > li.panel.panel-default {\n  margin-bottom: 0;\n  border-bottom: 1px solid #ece8e4;\n}\n.widget-mobile-transfer-accounts .accounts-list .account .account-title {\n  font-size: 18px;\n}\n.widget-mobile-transfer-accounts .contact-block .account-title {\n  font-size: 18px;\n  margin: 0 0 5px;\n}\n.widget-mobile-transfer-accounts .account-list-item-balance {\n  padding-right: 0;\n}\n", ""]);
	
	// exports


/***/ },
/* 3 */
/*!**************************************!*\
  !*** ./~/css-loader/lib/css-base.js ***!
  \**************************************/
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 4 */
/*!*************************************!*\
  !*** ./~/style-loader/addStyles.js ***!
  \*************************************/
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(true) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 5 */
/*!***************************!*\
  !*** ./scripts/config.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var labels = __webpack_require__(/*! ../locale/all.json */ 6);
	
	/**
	 * @ngInject
	 */
	function WidgetConfig($provide, $compileProvider, bbI18nProvider) {
	    $compileProvider.debugInfoEnabled(false);
	    bbI18nProvider.setDefaultLabels(labels);
	};
	
	module.exports = WidgetConfig;


/***/ },
/* 6 */
/*!*************************!*\
  !*** ./locale/all.json ***!
  \*************************/
/***/ function(module, exports) {

	module.exports = {
		"en": {
			"ACCOUNTS": "Accounts"
		},
		"nl": {
			"ACCOUNTS": "Rekeningen"
		}
	};

/***/ },
/* 7 */
/*!*************************************!*\
  !*** ./scripts/controllers/main.js ***!
  \*************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function MainCtrl(AccountsModel, bbI18n) {
	
	        /**
	         * List of accounts
	         *
	         * @type {Array}
	         */
	        var accounts = [];
	
	        /**
	         * Public API
	         *
	         * @type {object}
	         */
	        var api = {
	            get accounts() { return accounts; },
	            get Labels() { return bbI18n.labels; },
	
	            $onInit: $onInit,
	            $onDestroy: $onDestroy,
	            publishAccount: publishAccount
	        };
	
	        return api;
	
	        /**
	         * Initialize widget
	         */
	        function $onInit() {
	            // TODO use the object from transfer initiation
	            AccountsModel.getList().then(function(response) {
	                accounts = response.data;
	            });
	        }
	
	        /**
	         * Destroy widget
	         */
	        function $onDestroy() {}
	
	
	        /**
	         *
	         * @param account
	         */
	        function publishAccount(account) {
	            gadgets.pubsub.publish('transfers.transfer.from-account-select', {
	                account:{
	                    accountId: account.id,
	                    accountDetails: account
	                }
	            });
	            gadgets.pubsub.publish('navigation:go-back-to-transfer');
	        }
	    }
	
	    module.exports = MainCtrl;
	})();


/***/ },
/* 8 */
/*!************************************!*\
  !*** ./scripts/models/accounts.js ***!
  \************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function AccountsModel($http, widget) {
	        var self = this;
	
	        var accountsUrlPref = widget.model.getPreference('accountsUrl');
	        this.url = accountsUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);
	
	        this.getList = function() {
	            return $http({
	                method: 'GET',
	                url: this.url
	            });
	        };
	    }
	
	    module.exports = AccountsModel;
	})();


/***/ },
/* 9 */
/*!*****************************************!*\
  !*** ./scripts/plugins/alert-dialog.js ***!
  \*****************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function AlertDialog() {
	        this.$get = function(widget) {
	            if (widget.features && widget.features.AlertDialog) {
	                return widget.features.AlertDialog;
	            } else {
	                return null;
	            }
	        };
	    }
	
	    module.exports = AlertDialog;
	})();


/***/ },
/* 10 */
/*!***********************************************!*\
  !*** ./scripts/plugins/activity-indicator.js ***!
  \***********************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function ActivityIndicator() {
	        this.$get = function(widget) {
	            if (widget.features && widget.features.ActivityIndicator) {
	                return widget.features.ActivityIndicator;
	            } else {
	                return null;
	            }
	        };
	    }
	
	    module.exports = ActivityIndicator;
	})();


/***/ }
/******/ ]);
//# sourceMappingURL=main.js.map