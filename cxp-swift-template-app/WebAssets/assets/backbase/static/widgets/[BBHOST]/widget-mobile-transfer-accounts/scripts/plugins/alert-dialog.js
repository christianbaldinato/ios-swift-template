(function() {
    'use strict';

    /**
     * @ngInject
     */
    function AlertDialog() {
        this.$get = function(widget) {
            if (widget.features && widget.features.AlertDialog) {
                return widget.features.AlertDialog;
            } else {
                return null;
            }
        };
    }

    module.exports = AlertDialog;
})();
