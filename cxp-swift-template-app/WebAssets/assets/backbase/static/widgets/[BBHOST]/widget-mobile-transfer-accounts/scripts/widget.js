require('!style!css!less!../styles/main.less');

window.shc = window.shc || {};
shc.mobile = shc.mobile || {};
shc.mobile.widget = shc.mobile.widget || {};
shc.mobile.widget.transferAccounts = (function(angular, mobilePlugins) {
    'use strict';

    var TransferAccountsWidget = function(widget) {
        var self = this;

        this.widget = widget;

        var config = require('./config');

        // Controllers
        var MainCtrl = require('./controllers/main');

        // Models
        var AccountsModel = require('./models/accounts');

        // Native Plugins
        var AlertDialogProvider = require('./plugins/alert-dialog');
        var ActivityIndicatorProvider = require('./plugins/activity-indicator');

        var deps = [ mobilePlugins.name ];
        angular.module(widget.id, deps)
            .config(config)
            // Filters
            .filter('iban', ibanModule.filter)
            // Models
            .service('AccountsModel', AccountsModel)
            // Controllers
            .controller('MainCtrl', MainCtrl)
            // Directives
            .directive('formatAmount', formatAmount)
            // Native Plugins
            .provider('AlertDialog', AlertDialogProvider)
            .provider('ActivityIndicator', ActivityIndicatorProvider)
            // Widget Object
            .provider('widget', function() {
                this.$get = function() {
                    return widget;
                };
            })
            .run(function(){
                self.start();
            });
        // Bootstrap the widget as an Angular App
        angular.bootstrap(widget.body, [widget.id]);
    };

    TransferAccountsWidget.prototype.start = function() {
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: this.widget.id
        });
    };

    return {
        init: function(widget) {
            var transferAccountsApp = new TransferAccountsWidget(widget);
            return transferAccountsApp;
        }
    };
}(window.angular, window.mobilePlugins));
