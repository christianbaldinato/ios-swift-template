'use strict';

var labels = require('../locale/all.json');

/**
 * @ngInject
 */
function WidgetConfig($provide, $compileProvider, bbI18nProvider) {
    $compileProvider.debugInfoEnabled(false);
    bbI18nProvider.setDefaultLabels(labels);
};

module.exports = WidgetConfig;
