(function() {
    'use strict';

    /**
     * @ngInject
     */
    function AccountsModel($http, widget) {
        var self = this;

        var accountsUrlPref = widget.model.getPreference('accountsUrl');
        this.url = accountsUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);

        this.getList = function() {
            return $http({
                method: 'GET',
                url: this.url
            });
        };
    }

    module.exports = AccountsModel;
})();
