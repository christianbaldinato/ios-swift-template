'use strict';

function noop() { }

/**
 * @ngInject
 */
function MainCtrl(widget, bbAuth, model, loginSettings, $q, $scope, Event, bbI18n,
        // plugins
        bbActivityIndicator, bbAlertDialog) {

    var username = '';
    var password = '';

    // api
    return {
        get isLoading() { return model.isLoading },
        get Label() { return bbI18n.labels; },
        get pinEnabled() { return model.pinEnabled },
        $onInit: $onInit
    };

    /**
     * @public
     */
    function $onInit() {
        initEvents();
        initSettings();

        gadgets.pubsub.publish('cxp.item.loaded', {
            id: widget.id
        });
    }

    function initEvents() {
        $scope.$on(Event.Local.ERROR_LOGIN, function(e, err) {
            handleError(err);
        });
    }

    function initSettings() {
        model.initSettings()
            .then(function() {
                if (model.touchIdEnabled) {
                    touchIdLogin();
                }
            });
    }

    /*
     * Check touch ID, then log in
     */
    function touchIdLogin() {
        return model.touchIdAuth()
            .catch(function() { return false; }) // ignore touch errors
            .then(function(result) {
                if (!result) {
                    return;
                }

                bbActivityIndicator.show(function(data){}, function(error){}, bbI18n.labels.LOGIN_WAIT);
                return model.touchIdLogin()
                    .then(handleLoggedIn);
            })
            .catch(function(error) {
                return handleError(error, touchIdLogin); // pass this function as retry method
            });
    }

    function handleLoggedIn() {
        // Notify user-auth.
        gadgets.pubsub.publish('user-auth');
    }

    function handleError(error, retry) {
        if (!error.code) {
            return; // error with touch ID, not with the service.
        }

        // Call for button click with retry ability.
        var alertCallback = function(data) {
            if (data.callback === 'retry') {
                retry.call();
            } // else ignore (cancel / ok).
        };

        var buttons = alertButtons(error.code);
        var message = errorMessage(error.code);
        var title = bbI18n.labels.ERROR_ALERT;

        bbActivityIndicator.hide();
        bbAlertDialog.show(alertCallback, noop, title, message, JSON.stringify(buttons));
    }

    function alertButtons(errorCode) {
        var buttons;
        switch (errorCode) {
            case bbAuth.E_AUTH:
                buttons = [{ type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok' }];
                break;
            case bbAuth.E_OFFLINE:
            default: // unexpected
                buttons = [{ type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel' },
                    { type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: 'retry' }];
                // todo
                break;
        }
        return buttons;
    }

    function errorMessage(errorCode) {
        var errorMessages = {};
        errorMessages[bbAuth.E_AUTH] = bbI18n.labels.ERROR_TOUCH_AUTH;
        errorMessages[bbAuth.E_OFFLINE] = bbI18n.labels.ERROR_OFFLINE;
        return errorMessages[errorCode] || bbI18n.labels.ERROR_UNEXPECTED_LOGIN;
    }
}

module.exports = MainCtrl;
