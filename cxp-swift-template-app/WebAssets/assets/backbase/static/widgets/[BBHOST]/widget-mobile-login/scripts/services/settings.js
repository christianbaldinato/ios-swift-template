(function() {
    'use strict';

    function noop() {}

    var TOUCHID_ENABLED = 'com.backbase.login.touchid.enabled';
    var PIN_ENABLED = 'com.backbase.login.pin.enabled';
    var USERNAME = 'com.backbase.login.username';
    var PASSWORD = 'com.backbase.login.password';

    /**
     * @ngInject
     */
    function settings($q, bbSecureStorage) {
        return {
            getTouchIdEnabled: makeBooleanGetter(TOUCHID_ENABLED),
            getPinEnabled: makeBooleanGetter(PIN_ENABLED),
            getUsername: makeStringGetter(USERNAME),
            getPassword: makeStringGetter(PASSWORD),
            saveCredentials: saveCredentials
        };

        function saveCredentials(username, password) {
            set(USERNAME, username);
            set(PASSWORD, password);
        }

        /*
         * Create a function which gets a boolean from secure storage.
         */
        function makeBooleanGetter(key) {
            return function getter() {
                return get(key).then(isTrue);
            }
        }

        /*
         * Create a function which gets a stringfrom secure storage.
         */
        function makeStringGetter(key) {
            return function getter() {
                return get(key);
            }
        }

        function get(key) {
            if (!bbSecureStorage || bbSecureStorage.unavailable) {
                return $q.resolve(null);
            }

            return $q(function(resolve, reject) {
                var onSuccess = function(data) {
                    resolve(data.result);
                };
                var onFailure = function(err) {
                    resolve(null);
                };
                bbSecureStorage.stringForKey(onSuccess, onFailure, key);
            });
        }

        function set(key, value) {
            if (!bbSecureStorage || bbSecureStorage.unavailable) {
                return;
            }
            bbSecureStorage.setStringForKey(noop, noop, value, key);
        }

        function isTrue(string) {
            return string === 'true';
        }
    }

    module.exports = settings;
})();
