window.shc = window.shc || {};
shc.mobile = shc.mobile || {};
shc.mobile.widget = shc.mobile.widget || {};
shc.mobile.widget.login = (function(angular, mobilePlugins) {
    'use strict';

    var LoginWidget = function(widget) {
        var self = this;

        this.widget = widget;

        var config = require('./config');
        var constants = require('./constants');

        // Controllers
        var MainCtrl = require('./controllers/main');
        var LoginFormController = require('./controllers/login-form');
        // var LoginDigitsController = require('./controllers/login-digits');

        // Services
        var BbAuth = require('./services/auth');
        var LoginSettings = require('./services/settings');

        // Model
        var Model = require('./model');

        var deps = [ mobilePlugins.name ];
        angular.module(widget.id, deps)
            .config(config)
            .constant(constants)
            // Services
            .factory('bbAuth', BbAuth)
            .factory('loginSettings', LoginSettings)
            // Model
            .factory('model', Model)
            // Controllers
            .controller('MainCtrl', MainCtrl)
            .controller('LoginFormController', LoginFormController)
            // .controller('LoginDigitsController', LoginDigitsController)
            // Widget Object
            .provider('widget', function() {
                this.$get = function() {
                    return widget;
                };
            })
            .run(function(){
                self.start();
            });
        // Bootstrap the widget as an Angular App
        angular.bootstrap(widget.body, [widget.id]);
    };

    LoginWidget.prototype.start = function() {
    };

    return {
        init: function(widget) {
            var loginApp = new LoginWidget(widget);
            return loginApp;
        }
    };
}(window.angular, window.mobilePlugins));
