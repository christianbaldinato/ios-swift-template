'use strict';

function noop() { }

/**
 * @ngInject
 */
function LoginFormController(model, $scope, Event, loginSettings, bbAuth, bbI18n,
        // plugins
        bbActivityIndicator, bbAlertDialog) {

    /**
     * @public
     */
    var credentials = {
        username: '',
        password: ''
    };

    return {
        credentials: credentials, // for 2 way bind
        touchLoginError: function() { return model.touchLoginError; },
        submit: submit
    };

    /**
     * @public
     */
    function submit() {
        bbActivityIndicator.show(function(data){}, function(error){}, bbI18n.labels.LOGIN_WAIT);

        return model.login(credentials.username, credentials.password)
            .then(handleLoggedIn)
            .catch(function(error) {
                return handleError(error, submit); // pass this function as retry method
            });
    };

    function handleLoggedIn() {
        // Save successful credentials in storage.
        loginSettings.saveCredentials(credentials.username, credentials.password);

        // Notify user-auth.
        gadgets.pubsub.publish('user-auth');
    }

    function handleError(error, retry) {
        // Call for button click with retry ability.
        var alertCallback = function(data) {
            if (data.callback === 'retry') {
                retry.call();
            } // else ignore (cancel / ok).
        };

        var buttons = alertButtons(error.code);
        var message = errorMessage(error.code);
        var title = bbI18n.labels.ERROR_ALERT;

        bbActivityIndicator.hide(function(data){}, function(error){});
        bbAlertDialog.show(alertCallback, noop, title, message, JSON.stringify(buttons));
    }

    function alertButtons(errorCode) {
        var buttons;
        switch (errorCode) {
            case bbAuth.E_AUTH:
                buttons = [{ type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok' }];
                break;
            case bbAuth.E_OFFLINE:
            default: // unexpected
                buttons = [{ type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel' },
                    { type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: 'retry' }];
                // todo
                break;
        }
        return buttons;
    }

    function errorMessage(errorCode) {
        var errorMessages = {};
        errorMessages[bbAuth.E_AUTH] = bbI18n.labels.ERROR_AUTH_LOGIN;
        errorMessages[bbAuth.E_OFFLINE] = bbI18n.labels.ERROR_OFFLINE;
        return errorMessages[errorCode] || bbI18n.labels.ERROR_UNEXPECTED_LOGIN;
    }
}

module.exports = LoginFormController;
