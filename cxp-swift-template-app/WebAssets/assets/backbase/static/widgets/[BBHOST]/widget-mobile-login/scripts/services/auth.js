'use strict';

var E_OFFLINE = 1;
var E_UNEXPECTED = 2;
var E_AUTH = 3;
var TIMEOUT = 10000;

/**
 * @ngInject
 */
function bbAuth($http, widget) {
    // todo use setConfig, move preference to widget bootstrap.
    var authUrlPref = widget.model.getPreference('authUrl');
    var authUrl = authUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot) + '?rd=' + new Date().getTime();
    var sessionUrlPref = widget.model.getPreference('sessionUrl');
    var sessionUrl = sessionUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);

    // api
    return {
        login: login,
        E_OFFLINE: E_OFFLINE,
        E_AUTH: E_AUTH,
        E_UNEXPECTED: E_UNEXPECTED
    };

    /**
     * @public
     */
    function login(username, password, portalName) {
        return createSession(username, password)
            .then(function(session) {
                return loginSession(username, session.id, portalName)
            })
            .catch(onError);
    };

    function createSession(username, password) {
        return $http({
            method: 'POST',
            url: sessionUrl,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Req-X-Auth-Token': 'JWT_COOKIE'
            },
            transformRequest: function(obj) {
                var str = [];
                for (var p in obj) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
                return str.join("&");
            },
            data: {
                username: username,
                password: password
            },
            timeout: TIMEOUT
        })
        .then(function(response) {
            return response.data.session;
        });
    }

    function loginSession(username, sessionId, portalName) {
        return $http({
            method: 'POST',
            url: authUrl,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Req-X-Auth-Token': 'JWT_COOKIE'
            },
            transformRequest: function(obj) {
                var str = [];
                for (var p in obj) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
                return str.join("&");
            },
            data: {
                'j_username': username,
                'j_password': sessionId,
                'portal_name': portalName
            },
            timeout: TIMEOUT

        });
    }

    /*
     * Normalise errors
     */
    function onError(httpErr) {
        var err = {
            __httpResponse: httpErr
        };

        if (httpErr.status <= 1) {
            err.code = E_OFFLINE;
        }
        else if (httpErr.status === 401) {
            err.code = E_AUTH;
        }
        else {
            err.code = E_UNEXPECTED;
        }

        // Normalise message (note: don't use httpErr.message - not suitable for users).
        if (httpErr.data && httpErr.data.message) {
            err.message = httpErr.data.message;
        }

        throw err;
    }
}

module.exports = bbAuth;
