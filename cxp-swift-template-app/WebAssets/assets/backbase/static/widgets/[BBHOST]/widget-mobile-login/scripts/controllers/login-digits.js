(function() {
    'use strict';

    /**
     * @ngInject
     */
    function LoginDigitsController(SessionModel, AuthModel, bbAlertDialog, bbActivityIndicator, bbSecureStorage) {
        var self = this;

        this.digits = [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }];
        this.focusedElement = null;

        this.username = 'sara';
        this.password = 'password';
        this.validDigits = '';

        bbSecureStorage.stringForKey(function(data) {
            self.username = data.result;
        }, function(error) {
            console.log('usernameError', error);
        }, 'backbase.shc.secureStorage.username');
        bbSecureStorage.stringForKey(function(data) {
            self.password = data.result;
        }, function(error) {
            console.log('passwordError', error);
        }, 'backbase.shc.secureStorage.password');

        bbSecureStorage.stringForKey(function(data) {
            self.validDigits = data.result;
        }, function(error) {}, 'backbase.shc.secureStorage.pin');

        this.activityIndicator = bbActivityIndicator;

        this.setFocusedElement = function(event) {
            if (this.focusedElement !== event.target) {
                this.focusedElement = event.target;
            }
        };
        this.unsetFocusedElement = function(event) {
            if (this.focusedElement === event.target) {
                this.focusedElement = null;
            }
        };

        this.focusChange = function(digit) {
            var index = parseInt(angular.element(this.focusedElement).attr('data-index'), 10);
            if (digit.value.length === 1) {
                if (index < this.digits.length - 1) {
                    angular.element(this.focusedElement).parent().next().find('input')[0].focus();
                }
                if (index === this.digits.length - 1) {
                    this.validateDigits();
                }
            }
        };

        this.validateDigits = function() {
            var toValidate = '';
            for (var i = 0; i < this.digits.length; i++) {
                toValidate += this.digits[i].value;
            }

            if (toValidate === this.validDigits) {
                this.submit();
            } else {
                var alertCallback = function() {
                    for (i = 0; i < this.digits.length; i++) {
                        this.digits[i].value = '';
                    }
                    angular.element(this.focusedElement)[0].blur();
                };
                var alert = bbAlertDialog.show(function() {}, function() {}, 'Error!', 'The 5 digits entered were incorrect! Please try again.', JSON.stringify([{
                    type: 'POSITIVE',
                    text: 'OK',
                    callbackFn: 'alertCallback'
                }]));
            }
        };

        this.submit = function() {
            this.activityIndicator.show(function(data){}, function(error){}, 'Please wait...');
            SessionModel.create(this.username, this.password).then(function(response) {
                self.authenticate(response.data.session.id);
            }, function(error) {
                var alertCallback = angular.noop;
                self.activityIndicator.hide(function(data){}, function(error){});
                var alert = bbAlertDialog.show(function() {}, function() {}, 'Error!', error.data.errors[0].message, JSON.stringify([{
                    type: 'POSITIVE',
                    text: 'OK',
                    callbackFn: 'alertCallback'
                }]));
            });
        };

        this.authenticate = function(sessionId) {
            AuthModel.login(this.username, sessionId, b$.portal.portalName).then(function(response) {
                gadgets.pubsub.publish('user-auth');
            }, function(error) {
                var alertCallback = angular.noop;
                this.activityIndicator.hide(function(data){}, function(error){});
                var alert = bbAlertDialog.show(function() {}, function() {}, 'Error!', error, JSON.stringify([{
                    type: 'POSITIVE',
                    text: 'OK',
                    callbackFn: 'alertCallback'
                }]));
            });
        };
    }

    module.exports = LoginDigitsController;
})();
