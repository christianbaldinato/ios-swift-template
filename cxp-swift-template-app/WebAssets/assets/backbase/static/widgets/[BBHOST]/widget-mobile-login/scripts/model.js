'use strict';

function noop() { }

/**
 * @ngInject
 */
function Model(loginSettings, bbAuth, $q,
        // plugins
        bbTouchId) {

    /**
      * @public
      */
    var isLoading = true;
    var pinEnabled = false;
    var touchIdEnabled = false;
    var touchLoginError = null;
    var loginError = null;

    /**
      * @private
      */
    var touchUsername = '';
    var touchPassword = '';

    return {
        // getters
        get isLoading() { return isLoading; },
        get pinEnabled() { return pinEnabled },
        get touchIdEnabled() { return touchIdEnabled },
        get touchLoginError() { return touchLoginError },
        get loginError() { return loginError },

        // actions
        initSettings: initSettings,
        touchIdAuth: touchIdAuth,
        touchIdLogin: touchIdLogin,
        login: login
    };

    function initSettings() {
        // Read basic loginSettings from secure storage.
        var touchIdEnabledPromise = loginSettings.getTouchIdEnabled();
        var pinEnabledPromise = loginSettings.getPinEnabled();

        return $q.all([ touchIdEnabledPromise, pinEnabledPromise ]).then(function(results) {
            touchIdEnabled = results[0];
            pinEnabled = results[1];

            if (touchIdEnabled) {
                return initTouchId();
            }
        })
        .then(function() {
            isLoading = false;
        })
        .catch(function(err) {
            // Ignore failure to get touch ID & pin settings.
            isLoading = false;
        });
    }

    function initTouchId() {
        // Check if username & password available.
        // Note: this is a bit backwards. We should not be *able* to access the username
        // and password on the device until *after* touch ID has been successful.
        // See https://backbase.atlassian.net/browse/BMDA-174
        var usernamePromise = loginSettings.getUsername();
        var passwordPromise = loginSettings.getPassword();

        return $q.all([usernamePromise, passwordPromise]).then(function(results) {
            touchUsername = results[0];
            touchPassword = results[1];

        })
        .catch(function(err) {
            // Ignore errors with touch ID.
        });
    }

    // Resolve true or false.
    function touchIdAuth() {
        return $q(function(resolve, reject) {
            if (touchUsername && touchPassword) {
                bbTouchId.authenticate(function() {
                    resolve(true);
                }, function() {
                    resolve(false);
                });
            }
            else {
                resolve(false);
            }
        });
    }

    function touchIdLogin() {
        return login(touchUsername, touchPassword)
            .catch(function(error) {
                touchLoginError = error;
                throw error;
            });
    }

    function login(username, password) {
        return bbAuth.login(username, password, 'ctbc-android') // todo: remove magic string
            .catch(function(error) {
                loginError = error;
                throw error;
            });
    }
}

module.exports = Model;
