/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!***************************!*\
  !*** ./scripts/widget.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	window.shc = window.shc || {};
	shc.mobile = shc.mobile || {};
	shc.mobile.widget = shc.mobile.widget || {};
	shc.mobile.widget.login = (function(angular, mobilePlugins) {
	    'use strict';
	
	    var LoginWidget = function(widget) {
	        var self = this;
	
	        this.widget = widget;
	
	        var config = __webpack_require__(/*! ./config */ 1);
	        var constants = __webpack_require__(/*! ./constants */ 3);
	
	        // Controllers
	        var MainCtrl = __webpack_require__(/*! ./controllers/main */ 4);
	        var LoginFormController = __webpack_require__(/*! ./controllers/login-form */ 5);
	        // var LoginDigitsController = require('./controllers/login-digits');
	
	        // Services
	        var BbAuth = __webpack_require__(/*! ./services/auth */ 6);
	        var LoginSettings = __webpack_require__(/*! ./services/settings */ 7);
	
	        // Model
	        var Model = __webpack_require__(/*! ./model */ 8);
	
	        var deps = [ mobilePlugins.name ];
	        angular.module(widget.id, deps)
	            .config(config)
	            .constant(constants)
	            // Services
	            .factory('bbAuth', BbAuth)
	            .factory('loginSettings', LoginSettings)
	            // Model
	            .factory('model', Model)
	            // Controllers
	            .controller('MainCtrl', MainCtrl)
	            .controller('LoginFormController', LoginFormController)
	            // .controller('LoginDigitsController', LoginDigitsController)
	            // Widget Object
	            .provider('widget', function() {
	                this.$get = function() {
	                    return widget;
	                };
	            })
	            .run(function(){
	                self.start();
	            });
	        // Bootstrap the widget as an Angular App
	        angular.bootstrap(widget.body, [widget.id]);
	    };
	
	    LoginWidget.prototype.start = function() {
	    };
	
	    return {
	        init: function(widget) {
	            var loginApp = new LoginWidget(widget);
	            return loginApp;
	        }
	    };
	}(window.angular, window.mobilePlugins));


/***/ },
/* 1 */
/*!***************************!*\
  !*** ./scripts/config.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var labels = __webpack_require__(/*! ../locale/all.json */ 2);
	
	/**
	 * @ngInject
	 */
	function WidgetConfig($provide, $compileProvider, bbI18nProvider) {
	    $compileProvider.debugInfoEnabled(false);
	    bbI18nProvider.setDefaultLabels(labels);
	};
	
	module.exports = WidgetConfig;


/***/ },
/* 2 */
/*!*************************!*\
  !*** ./locale/all.json ***!
  \*************************/
/***/ function(module, exports) {

	module.exports = {
		"en": {
			"BUTTON_OK": "OK",
			"BUTTON_CANCEL": "Cancel",
			"BUTTON_RETRY": "Retry",
			"USERNAME": "Username",
			"PASSWORD": "Password",
			"ERROR_ALERT": "Error!",
			"ERROR_OFFLINE": "Unable to establish connection. Check your internet connection.",
			"LOGIN_PROMO_HEADER": "Do you travel a lot? ",
			"LOGIN_PROMO_ACTION": "Apply for the Backbase credit card.",
			"LOGIN": "Log in",
			"LOGIN_WAIT": "Please wait...",
			"ERROR_AUTH_LOGIN": "Authentication has failed.",
			"ERROR_UNEXPECTED_LOGIN": "Unexpected error logging in.",
			"ERROR_TOUCH_AUTH": "Authentication has failed. You will need to login with your username and password to re-enable touch ID"
		},
		"nl": {
			"BUTTON_OK": "OK",
			"BUTTON_CANCEL": "Annuleer",
			"BUTTON_RETRY": "Opnieuw proberen",
			"USERNAME": "Gebruikersnaam",
			"PASSWORD": "Wachtwoord",
			"ERROR_ALERT": "Fout!",
			"ERROR_OFFLINE": "Kan geen verbinding maken. Controleer uw internetverbinding.",
			"LOGIN_PROMO_HEADER": "Reis je veel?",
			"LOGIN_PROMO_ACTION": "Meld je aan voor de Backbase credit card.",
			"LOGIN": "Inloggen",
			"LOGIN_WAIT": "Even geduld aub",
			"ERROR_AUTH_LOGIN": "Verificatie is mislukt",
			"ERROR_UNEXPECTED_LOGIN": "Onverwachte fout in te loggen.",
			"ERROR_TOUCH_AUTH": "Verificatie is mislukt. U dient in te loggen met uw gebruikersnaam en wachtwoord opnieuw in te schakelen aanraking ID"
		}
	};

/***/ },
/* 3 */
/*!******************************!*\
  !*** ./scripts/constants.js ***!
  \******************************/
/***/ function(module, exports) {

	'use strict';
	
	var WidgetConstants = {
	    /**
	     * Local and global events, that widget publishes (emits) or relies on
	     */
	    Event: {
	        Global: {
	        },
	        Local: {
	            'ERROR_LOGIN': 'error:login'
	        }
	    },
	    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
	};
	
	module.exports = WidgetConstants;
	


/***/ },
/* 4 */
/*!*************************************!*\
  !*** ./scripts/controllers/main.js ***!
  \*************************************/
/***/ function(module, exports) {

	'use strict';
	
	function noop() { }
	
	/**
	 * @ngInject
	 */
	function MainCtrl(widget, bbAuth, model, loginSettings, $q, $scope, Event, bbI18n,
	        // plugins
	        bbActivityIndicator, bbAlertDialog) {
	
	    var username = '';
	    var password = '';
	
	    // api
	    return {
	        get isLoading() { return model.isLoading },
	        get Label() { return bbI18n.labels; },
	        get pinEnabled() { return model.pinEnabled },
	        $onInit: $onInit
	    };
	
	    /**
	     * @public
	     */
	    function $onInit() {
	        initEvents();
	        initSettings();
	
	        gadgets.pubsub.publish('cxp.item.loaded', {
	            id: widget.id
	        });
	    }
	
	    function initEvents() {
	        $scope.$on(Event.Local.ERROR_LOGIN, function(e, err) {
	            handleError(err);
	        });
	    }
	
	    function initSettings() {
	        model.initSettings()
	            .then(function() {
	                if (model.touchIdEnabled) {
	                    touchIdLogin();
	                }
	            });
	    }
	
	    /*
	     * Check touch ID, then log in
	     */
	    function touchIdLogin() {
	        return model.touchIdAuth()
	            .catch(function() { return false; }) // ignore touch errors
	            .then(function(result) {
	                if (!result) {
	                    return;
	                }
	
	                bbActivityIndicator.show(function(data){}, function(error){}, bbI18n.labels.LOGIN_WAIT);
	                return model.touchIdLogin()
	                    .then(handleLoggedIn);
	            })
	            .catch(function(error) {
	                return handleError(error, touchIdLogin); // pass this function as retry method
	            });
	    }
	
	    function handleLoggedIn() {
	        // Notify user-auth.
	        gadgets.pubsub.publish('user-auth');
	    }
	
	    function handleError(error, retry) {
	        if (!error.code) {
	            return; // error with touch ID, not with the service.
	        }
	
	        // Call for button click with retry ability.
	        var alertCallback = function(data) {
	            if (data.callback === 'retry') {
	                retry.call();
	            } // else ignore (cancel / ok).
	        };
	
	        var buttons = alertButtons(error.code);
	        var message = errorMessage(error.code);
	        var title = bbI18n.labels.ERROR_ALERT;
	
	        bbActivityIndicator.hide();
	        bbAlertDialog.show(alertCallback, noop, title, message, JSON.stringify(buttons));
	    }
	
	    function alertButtons(errorCode) {
	        var buttons;
	        switch (errorCode) {
	            case bbAuth.E_AUTH:
	                buttons = [{ type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok' }];
	                break;
	            case bbAuth.E_OFFLINE:
	            default: // unexpected
	                buttons = [{ type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel' },
	                    { type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: 'retry' }];
	                // todo
	                break;
	        }
	        return buttons;
	    }
	
	    function errorMessage(errorCode) {
	        var errorMessages = {};
	        errorMessages[bbAuth.E_AUTH] = bbI18n.labels.ERROR_TOUCH_AUTH;
	        errorMessages[bbAuth.E_OFFLINE] = bbI18n.labels.ERROR_OFFLINE;
	        return errorMessages[errorCode] || bbI18n.labels.ERROR_UNEXPECTED_LOGIN;
	    }
	}
	
	module.exports = MainCtrl;


/***/ },
/* 5 */
/*!*******************************************!*\
  !*** ./scripts/controllers/login-form.js ***!
  \*******************************************/
/***/ function(module, exports) {

	'use strict';
	
	function noop() { }
	
	/**
	 * @ngInject
	 */
	function LoginFormController(model, $scope, Event, loginSettings, bbAuth, bbI18n,
	        // plugins
	        bbActivityIndicator, bbAlertDialog) {
	
	    /**
	     * @public
	     */
	    var credentials = {
	        username: '',
	        password: ''
	    };
	
	    return {
	        credentials: credentials, // for 2 way bind
	        touchLoginError: function() { return model.touchLoginError; },
	        submit: submit
	    };
	
	    /**
	     * @public
	     */
	    function submit() {
	        bbActivityIndicator.show(function(data){}, function(error){}, bbI18n.labels.LOGIN_WAIT);
	
	        return model.login(credentials.username, credentials.password)
	            .then(handleLoggedIn)
	            .catch(function(error) {
	                return handleError(error, submit); // pass this function as retry method
	            });
	    };
	
	    function handleLoggedIn() {
	        // Save successful credentials in storage.
	        loginSettings.saveCredentials(credentials.username, credentials.password);
	
	        // Notify user-auth.
	        gadgets.pubsub.publish('user-auth');
	    }
	
	    function handleError(error, retry) {
	        // Call for button click with retry ability.
	        var alertCallback = function(data) {
	            if (data.callback === 'retry') {
	                retry.call();
	            } // else ignore (cancel / ok).
	        };
	
	        var buttons = alertButtons(error.code);
	        var message = errorMessage(error.code);
	        var title = bbI18n.labels.ERROR_ALERT;
	
	        bbActivityIndicator.hide(function(data){}, function(error){});
	        bbAlertDialog.show(alertCallback, noop, title, message, JSON.stringify(buttons));
	    }
	
	    function alertButtons(errorCode) {
	        var buttons;
	        switch (errorCode) {
	            case bbAuth.E_AUTH:
	                buttons = [{ type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok' }];
	                break;
	            case bbAuth.E_OFFLINE:
	            default: // unexpected
	                buttons = [{ type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel' },
	                    { type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: 'retry' }];
	                // todo
	                break;
	        }
	        return buttons;
	    }
	
	    function errorMessage(errorCode) {
	        var errorMessages = {};
	        errorMessages[bbAuth.E_AUTH] = bbI18n.labels.ERROR_AUTH_LOGIN;
	        errorMessages[bbAuth.E_OFFLINE] = bbI18n.labels.ERROR_OFFLINE;
	        return errorMessages[errorCode] || bbI18n.labels.ERROR_UNEXPECTED_LOGIN;
	    }
	}
	
	module.exports = LoginFormController;


/***/ },
/* 6 */
/*!**********************************!*\
  !*** ./scripts/services/auth.js ***!
  \**********************************/
/***/ function(module, exports) {

	'use strict';
	
	var E_OFFLINE = 1;
	var E_UNEXPECTED = 2;
	var E_AUTH = 3;
	var TIMEOUT = 10000;
	
	/**
	 * @ngInject
	 */
	function bbAuth($http, widget) {
	    // todo use setConfig, move preference to widget bootstrap.
	    var authUrlPref = widget.model.getPreference('authUrl');
	    var authUrl = authUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot) + '?rd=' + new Date().getTime();
	    var sessionUrlPref = widget.model.getPreference('sessionUrl');
	    var sessionUrl = sessionUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);
	
	    // api
	    return {
	        login: login,
	        E_OFFLINE: E_OFFLINE,
	        E_AUTH: E_AUTH,
	        E_UNEXPECTED: E_UNEXPECTED
	    };
	
	    /**
	     * @public
	     */
	    function login(username, password, portalName) {
	        return createSession(username, password)
	            .then(function(session) {
	                return loginSession(username, session.id, portalName)
	            })
	            .catch(onError);
	    };
	
	    function createSession(username, password) {
	        return $http({
	            method: 'POST',
	            url: sessionUrl,
	            headers: {
	                'Content-Type': 'application/x-www-form-urlencoded',
	                'Accept': 'application/json',
	                'Req-X-Auth-Token': 'JWT_COOKIE'
	            },
	            transformRequest: function(obj) {
	                var str = [];
	                for (var p in obj) {
	                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	                }
	                return str.join("&");
	            },
	            data: {
	                username: username,
	                password: password
	            },
	            timeout: TIMEOUT
	        })
	        .then(function(response) {
	            return response.data.session;
	        });
	    }
	
	    function loginSession(username, sessionId, portalName) {
	        return $http({
	            method: 'POST',
	            url: authUrl,
	            headers: {
	                'Content-Type': 'application/x-www-form-urlencoded',
	                'Accept': 'application/json',
	                'Req-X-Auth-Token': 'JWT_COOKIE'
	            },
	            transformRequest: function(obj) {
	                var str = [];
	                for (var p in obj) {
	                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	                }
	                return str.join("&");
	            },
	            data: {
	                'j_username': username,
	                'j_password': sessionId,
	                'portal_name': portalName
	            },
	            timeout: TIMEOUT
	
	        });
	    }
	
	    /*
	     * Normalise errors
	     */
	    function onError(httpErr) {
	        var err = {
	            __httpResponse: httpErr
	        };
	
	        if (httpErr.status <= 1) {
	            err.code = E_OFFLINE;
	        }
	        else if (httpErr.status === 401) {
	            err.code = E_AUTH;
	        }
	        else {
	            err.code = E_UNEXPECTED;
	        }
	
	        // Normalise message (note: don't use httpErr.message - not suitable for users).
	        if (httpErr.data && httpErr.data.message) {
	            err.message = httpErr.data.message;
	        }
	
	        throw err;
	    }
	}
	
	module.exports = bbAuth;


/***/ },
/* 7 */
/*!**************************************!*\
  !*** ./scripts/services/settings.js ***!
  \**************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    function noop() {}
	
	    var TOUCHID_ENABLED = 'com.backbase.login.touchid.enabled';
	    var PIN_ENABLED = 'com.backbase.login.pin.enabled';
	    var USERNAME = 'com.backbase.login.username';
	    var PASSWORD = 'com.backbase.login.password';
	
	    /**
	     * @ngInject
	     */
	    function settings($q, bbSecureStorage) {
	        return {
	            getTouchIdEnabled: makeBooleanGetter(TOUCHID_ENABLED),
	            getPinEnabled: makeBooleanGetter(PIN_ENABLED),
	            getUsername: makeStringGetter(USERNAME),
	            getPassword: makeStringGetter(PASSWORD),
	            saveCredentials: saveCredentials
	        };
	
	        function saveCredentials(username, password) {
	            set(USERNAME, username);
	            set(PASSWORD, password);
	        }
	
	        /*
	         * Create a function which gets a boolean from secure storage.
	         */
	        function makeBooleanGetter(key) {
	            return function getter() {
	                return get(key).then(isTrue);
	            }
	        }
	
	        /*
	         * Create a function which gets a stringfrom secure storage.
	         */
	        function makeStringGetter(key) {
	            return function getter() {
	                return get(key);
	            }
	        }
	
	        function get(key) {
	            if (!bbSecureStorage || bbSecureStorage.unavailable) {
	                return $q.resolve(null);
	            }
	
	            return $q(function(resolve, reject) {
	                var onSuccess = function(data) {
	                    resolve(data.result);
	                };
	                var onFailure = function(err) {
	                    resolve(null);
	                };
	                bbSecureStorage.stringForKey(onSuccess, onFailure, key);
	            });
	        }
	
	        function set(key, value) {
	            if (!bbSecureStorage || bbSecureStorage.unavailable) {
	                return;
	            }
	            bbSecureStorage.setStringForKey(noop, noop, value, key);
	        }
	
	        function isTrue(string) {
	            return string === 'true';
	        }
	    }
	
	    module.exports = settings;
	})();


/***/ },
/* 8 */
/*!**************************!*\
  !*** ./scripts/model.js ***!
  \**************************/
/***/ function(module, exports) {

	'use strict';
	
	function noop() { }
	
	/**
	 * @ngInject
	 */
	function Model(loginSettings, bbAuth, $q,
	        // plugins
	        bbTouchId) {
	
	    /**
	      * @public
	      */
	    var isLoading = true;
	    var pinEnabled = false;
	    var touchIdEnabled = false;
	    var touchLoginError = null;
	    var loginError = null;
	
	    /**
	      * @private
	      */
	    var touchUsername = '';
	    var touchPassword = '';
	
	    return {
	        // getters
	        get isLoading() { return isLoading; },
	        get pinEnabled() { return pinEnabled },
	        get touchIdEnabled() { return touchIdEnabled },
	        get touchLoginError() { return touchLoginError },
	        get loginError() { return loginError },
	
	        // actions
	        initSettings: initSettings,
	        touchIdAuth: touchIdAuth,
	        touchIdLogin: touchIdLogin,
	        login: login
	    };
	
	    function initSettings() {
	        // Read basic loginSettings from secure storage.
	        var touchIdEnabledPromise = loginSettings.getTouchIdEnabled();
	        var pinEnabledPromise = loginSettings.getPinEnabled();
	
	        return $q.all([ touchIdEnabledPromise, pinEnabledPromise ]).then(function(results) {
	            touchIdEnabled = results[0];
	            pinEnabled = results[1];
	
	            if (touchIdEnabled) {
	                return initTouchId();
	            }
	        })
	        .then(function() {
	            isLoading = false;
	        })
	        .catch(function(err) {
	            // Ignore failure to get touch ID & pin settings.
	            isLoading = false;
	        });
	    }
	
	    function initTouchId() {
	        // Check if username & password available.
	        // Note: this is a bit backwards. We should not be *able* to access the username
	        // and password on the device until *after* touch ID has been successful.
	        // See https://backbase.atlassian.net/browse/BMDA-174
	        var usernamePromise = loginSettings.getUsername();
	        var passwordPromise = loginSettings.getPassword();
	
	        return $q.all([usernamePromise, passwordPromise]).then(function(results) {
	            touchUsername = results[0];
	            touchPassword = results[1];
	
	        })
	        .catch(function(err) {
	            // Ignore errors with touch ID.
	        });
	    }
	
	    // Resolve true or false.
	    function touchIdAuth() {
	        return $q(function(resolve, reject) {
	            if (touchUsername && touchPassword) {
	                bbTouchId.authenticate(function() {
	                    resolve(true);
	                }, function() {
	                    resolve(false);
	                });
	            }
	            else {
	                resolve(false);
	            }
	        });
	    }
	
	    function touchIdLogin() {
	        return login(touchUsername, touchPassword)
	            .catch(function(error) {
	                touchLoginError = error;
	                throw error;
	            });
	    }
	
	    function login(username, password) {
	        return bbAuth.login(username, password, 'ctbc-android') // todo: remove magic string
	            .catch(function(error) {
	                loginError = error;
	                throw error;
	            });
	    }
	}
	
	module.exports = Model;


/***/ }
/******/ ]);
//# sourceMappingURL=main.js.map