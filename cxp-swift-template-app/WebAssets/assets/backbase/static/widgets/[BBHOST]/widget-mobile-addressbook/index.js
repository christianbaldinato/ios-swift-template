'use strict';

/**
 * ------------------------------------------------------------------------
 * widget-mobile-addressbook entry file
 * ------------------------------------------------------------------------
 */
var ng = require('angular');

// AngularJS module application
var App = require('./scripts/app');

/**
 * @param  {object} widget  Widget instance
 * @return {object}         Widget application
 */
module.exports = function(widget) {
    var appInstance = App(widget);
    return {
        start: function() {
            ng.bootstrap(widget.body, [appInstance.name]);
            return this;
        }
    };
};
