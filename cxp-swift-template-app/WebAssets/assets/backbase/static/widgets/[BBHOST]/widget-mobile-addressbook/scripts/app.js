'use strict';

// externals
var ng = require('angular');
var mobilePlugins = require('mobilePlugins');

// internals
var Config = require('./config');
var Model = require('./model');
var Bus = require('./bus');
var constants = require('./constants');
var ActivityIndicatorProvider = require('./plugins/activity-indicator');

// Modules
var ibanModule = require('ibanModule');

// Services
var serviceBeneficiary = require('serviceBeneficiary');

// Controllers
var MainCtrl = require('./controllers/main');
var ErrorCtrl = require('./controllers/error');

// Directives
var avatar = require('avatar');

/**
 * @ngInject
 */
function run(widget, bus, $rootScope, SERVER_ROOT, Event, Preference, serviceBeneficiary) {
    // Add a safeApply function.
    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    // Configure services.
    function getEndpoint(endpointPref) {
        return widget.getPreference(endpointPref).replace('$(servicesPath)', SERVER_ROOT);
    }

    serviceBeneficiary.setConfig({
        counterparties: getEndpoint(Preference.CONTACT_LIST),
        partyDataManagementEndpoint: getEndpoint(Preference.CONTACT_ENDPOINT),
        partyDataManagementDetailsEndpoint: getEndpoint(Preference.CONTACT_DETAILS_ENDPOINT)
    });

    bus.publish(Event.Global.LOADED, {
        id: widget.id
    });
}

/**
 * Widget Angular Module
 * @module widget-mobile-addressbook
 * @link https://docs.angularjs.org/api/ng/function/angular.module
 * @param {object} widget Widget instance
 * @returns {object} AngularJS module
 */
function App(widget) {
    var deps = [
        mobilePlugins.name
    ];

    return ng.module(widget.id, deps)
        .config(Config)
        .constant(constants)
        .factory('bus', Bus)
        .factory('model', Model)
        .controller('MainCtrl', MainCtrl)
        .controller('ErrorCtrl', ErrorCtrl)
        .factory('ActivityIndicator', ActivityIndicatorProvider)
        .service('ibanService', ibanModule.service)
        .factory('serviceBeneficiary', serviceBeneficiary)
        .filter('iban', ibanModule.filter)
        .directive('validIban', ibanModule.validator)
        .directive('contactItem', contactItem)
        .directive('searchBox', searchBox)
        .directive('avatar', avatar)
        .provider('widget', function() {
            this.$get = function() {
                return widget;
            };
        })
        .filter('keylength', function(){
            return function(input){
                if(!angular.isObject(input)){
                    throw Error("Usage of non-objects with keylength filter!!")
                }
                return Object.keys(input).length;
            }
        })
        .run(run);
}

module.exports = App;
