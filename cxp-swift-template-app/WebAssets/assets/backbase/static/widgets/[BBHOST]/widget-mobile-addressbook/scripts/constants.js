'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
            LOADED: 'cxp.item.loaded',
            NAV_ADDRESSBOOK: 'navigation:show-addressbook',
            REFRESH_ADDRESSBOOK: 'refresh-addressbook'
        },
        Local: {
            SWIPEABLE_CELL_ENABLE: 'swipeable-cell:disable-gesture-recognizers',
            SWIPEABLE_CELL_DISABLE: 'swipeable-cell:enable-gesture-recognizers'
        }
    },
    Preference: {
        CONTACT_SHOW: 'contactShowEvent',
        CONTACT_SHOW_VIEW: 'contactShowViewEvent',
        CONTACT_LIST: 'counterparties',
        CONTACT_ENDPOINT: 'contactEndpoint',
        CONTACT_DETAILS_ENDPOINT: 'contactDetailsEndpoint'
    },
    Message: {
        LOADING: 'Loading contacts',
        DELETED: 'Contact deleted'
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
};

module.exports = WidgetConstants;
