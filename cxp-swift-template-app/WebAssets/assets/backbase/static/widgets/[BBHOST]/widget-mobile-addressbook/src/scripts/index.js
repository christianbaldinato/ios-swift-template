/**
 * ------------------------------------------------------------------------
 * widgetMobileAddressbook entry point
 * ------------------------------------------------------------------------
 */
(function (window, factory) {
    'use strict';
    var name = 'widgetMobileAddressbook';
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return factory(name);
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(name);
    } else {
        window[name] = factory(name);
    }
}(this, function (name) {

    'use strict';

    var bus = window.gadgets.pubsub, ng = window.angular;

    /**
     * Main Controller
     * @param {object} widget cxp widget instance
     */
    function MainCtrl(widget, addressBookService) {

        // console.log(ngTouch);

        var vm = this;
        vm.details = {};

        init();

        vm.swipeableCellOpen = function () {
            bus.publish('swipeable-cell:disable-gesture-recognizers');
        };

        vm.swipeableCellClosed = function () {
            bus.publish('swipeable-cell:enable-gesture-recognizers');
        };

        vm.expandableListExpanded = function (expanded) {
            return expanded;
        };

        vm.getContact = function (id) {
            if (!vm.details[id] && vm.expandableListExpanded) {
                addressBookService.getContact(id).then(function (beneficiary) {
                    vm.details[id] = beneficiary;
                });
            }
        };

        vm.showDetails = function (id) {
            bus.publish('navigation:addressbook-contact', { id: id });
        }

        vm.modifyContact = function (beneficiary) {
            if(beneficiary && beneficiary.hasOwnProperty('id')) {
                bus.publish(widget.getPreference('navigation:add-contact'), {id: beneficiary.id});
            } else {
                bus.publish(widget.getPreference('navigation:add-contact'));
            }
        };
        
        vm.askRemoveContact = function (contact) {
            if (confirm('Are you sure you want to delete contact ' + contact.name + ' from your address book?')) {
                contact.removed = true;
            } else {
                contact.cancelledRemove = true;
            }
        }

        vm.removeContact = function (contact) {
            addressBookService.removeContact(contact.id)
                 .then(function (res) {
                var index = vm.contacts.indexOf(contact);
                if (index !== -1) {
                    vm.contacts.splice(index, 1);
                    vm.groupedContacts = addressBookService.groupContactsByInitial(vm.contacts);
                }
                })
                .catch(function(err) {
                    // todo: error handling
                    alert('something went wrong');
                });
        };

        bus.publish(widget.getPreference('preload:modifyContact'));

        bus.subscribe('navigation:show-addressbook', function () {
            init();
        });

        bus.publish('cxp.item.loaded', {
            id: widget.model.name
        });

        function init() {
            addressBookService.getContacts().then(function (contacts) {
                vm.contacts = contacts;
                vm.groupedContacts = addressBookService.groupContactsByInitial(vm.contacts);
            });
            vm.isAnyDragged = {};
        }
    }

    function addressBookService($http) {
        // todo: get from widget preferences.
        var serverPortalUrl = b$.portal.portalModel.serverURL,
            contactList = '/services/rest/v1/counterparties',
            contactDetail = '/services/rest/v1/party-data-management/counter-party/{id}/details',
            removeEndpoint= '/services/rest/v1/party-data-management/counter-party/{id}';

        return {
            getContacts: getContacts,
            getContact: getContact,
            removeContact: removeContact,
            groupContactsByInitial: groupContactsByInitial
        };

        // TODO: extract this service as a shared feature
        function getContact(id) {
            var url = serverPortalUrl + contactDetail;
            url = url.replace('{id}', id);
            return $http.get(url)
                .then(function (res) {
                    return deserializeBeneficiary(res.data);
                }, onError);

            function onError(error) {
                return error;
            }
        }

        function getContacts() {
            return $http.get(serverPortalUrl + contactList)
                .then(success)
                .then(sortContacts)
                .catch(error);

            function success(response) {
                return response.data.map(deserializeBeneficiary);
            }

            function sortContacts(contacts) {
                // note: sorts in placd
                contacts.sort(function(a, b) {
                    if(a.name < b.name) return -1;
                    if(a.name > b.name) return 1;
                    return 0;
                });
                return contacts;
            }

            function error(error) {
                if (error) {
                    console.error(error);
                }
            }
        }


        /**
         * Group contacts by first letter
         */
        function groupContactsByInitial(contacts) {
            var groups = { };
            contacts.forEach(function(contact) {
                var letter = contact.name[0].toUpperCase();
                if (!groups[letter]) {
                    groups[letter] = [];
                }
                groups[letter].push(contact);
            });

            return groups;
        }

        function removeContact(id) {
            var data = {};
            var url = serverPortalUrl + removeEndpoint;
            url = url.replace('{id}', id);
            return $http.delete(url, data).then(success, error);

            function success(response) {
                return response;
            }

            function error(error) {
                return error;
            }
        }

/*
{
  "id": "7beaad98-139f-47f0-9bce-937056cc131c",
  "account": "NL51INGB2147735757",
  "active": true,
  "name": "Jerry Carrol",
  "partyId": "1",
  "photoUrl": "/portalserver/static/mock-data/contacts/Personas-JerryCarrol.jpg",
  "email": "jerry.carrol@backbase.com",
  "phone": "+1-253-515-0164",
  "address": null,
  "city": null,
  "state": null,
  "dateOfBirth": "1983-02-08"
}
*/

        function deserializeBeneficiary(beneficiaryData) {
            return beneficiaryData;
        }
    }

    function iban() {
        return function (input) {
          return input.match(/(\d|\D){1,4}/g).join(' ');
        };
    }

    /**
     * Error Controller
     * Binds the widget errors to the view
     * @param {object} widget cxp widget instance
     */
    function ErrorCtrl(widget) {
        //empty for now
    }

    /**
     * Create Angular Module
     * @param  {object} widget widget instance
     * @param  {array} deps   angular modules dependencies
     * @return {object}        angular module
     */
    function createModule(widget, deps) {
        return ng.module(name, deps || [])
            .value('widget', widget)
            .controller('MainCtrl', ['widget', 'addressBookService', '$scope', MainCtrl])
            .controller('ErrorCtrl', ['widget', ErrorCtrl])
            .directive('contactItem', contactItem)
            .directive('searchBox', searchBox)
            .filter('iban', iban)
            .factory('addressBookService', ['$http', addressBookService]);
    }

    /**
     * Main Widget function
     * @param  {object} widget instance
     * @return {object}        Application object
     * @public
     */
    function App(widget, deps) {
        var obj = Object.create(App.prototype);
        var args = Array.prototype.slice.call(arguments);
        obj.widget = widget;
        obj.module = createModule.apply(obj, args);
        return obj;
    }

    /**
     * Widget proto
     * @type {object}
     */
    App.prototype = {
        bootstrap: function () {
            ng.bootstrap(this.widget.body, [this.module.name]);
            return this;
        }, destroy: function () {
            this.module = null;
        }
    };

    return App;

}));
