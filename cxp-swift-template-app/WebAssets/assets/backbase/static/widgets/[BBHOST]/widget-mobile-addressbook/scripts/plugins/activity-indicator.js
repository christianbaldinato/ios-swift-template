'use strict';

/**
 * @ngInject
 */
function ActivityIndicator(widget) {
    if (widget.features && widget.features.ActivityIndicator) {
        return widget.features.ActivityIndicator;
    } else {
        return {
            show: function() {},
            hide: function() {}
        };
    }
}

module.exports = ActivityIndicator;
