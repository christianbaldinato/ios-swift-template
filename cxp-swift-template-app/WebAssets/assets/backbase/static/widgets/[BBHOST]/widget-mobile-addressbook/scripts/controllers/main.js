'use strict';

function noop() {}

/**
 * @ngInject
 * @memberof module:widget-mobile-addressbook
 */
function MainCtrl(widget, bus, Preference, Event, serviceBeneficiary,
        Message) {
    var vm = this;
    vm.details = {};

    /**
     * Angularjs hook function called after the component has been initialized and all its
     * bindings have been set up.
     * @public
     */
    this.$onInit = function() {
        bus.publish(widget.getPreference('preload:modifyContact'));
        bus.subscribe(Event.Global.NAV_ADDRESSBOOK, onNav);
        bus.subscribe(Event.Global.REFRESH_ADDRESSBOOK, init);
        init();
    };

    /**
     * Angularjs hook that is called when its containing scope is destroyed.
     * @public
     */
    this.$onDestroy = function() {
        bus.unsubscribe(Event.Global.NAV_ADDRESSBOOK, onNav);
        bus.unsubscribe(Event.Global.REFRESH_ADDRESSBOOK, init);
    };

    /**
     * Show contact
     *
     * @param id
     */
    vm.showDetails = function (id) {
        bus.publish(widget.getPreference(Preference.CONTACT_SHOW_VIEW), { id: id });
    };

    /**
     * Initialize the Address book
     */
    function init() {
        serviceBeneficiary.getContacts().then(function(response) {
            return response.data;
        }).then(function (contacts) {
            contacts.sort(function(a, b) {
                if(a.name < b.name) return -1;
                if(a.name > b.name) return 1;
                return 0;
            });
            return contacts;
        }).then(function (contacts) {
            vm.contacts = contacts;
            vm.groupedContacts = serviceBeneficiary.groupContactsByInitial(vm.contacts);
        });
    }

    function onNav() {
        init();
    }
}

module.exports = MainCtrl;
