'use strict';

var locales = require('../locale/all.json');

/**
 * @ngInject
 * @name widgetConfig
 * @private
 * @memberof module:widget-mobile-transfer-review
 * @kind function AngularJS configuration provider
 * @link https://docs.angularjs.org/api/ng/type/angular.Module#config
 */
function WidgetConfig($compileProvider, widgetProvider, $provide) {
    // get the widget instance to configure the application
    // based on the widget instance object
    var widget = widgetProvider.$get();

    $compileProvider.debugInfoEnabled(false);
    // Simple i18n factory based on widget.locale property
    /* istanbul ignore next */
    $provide.factory('i18n', function() {
        return locales[widget.locale] || locales['en'] || {};
    });
}

module.exports = WidgetConfig;
