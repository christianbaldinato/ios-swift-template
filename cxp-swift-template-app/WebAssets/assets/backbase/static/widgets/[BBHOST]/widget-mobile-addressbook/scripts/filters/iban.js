'use strict';

/**
 * @ngInject
 * @memberof module:widget-mobile-addressbook
 */
function iban() {
    return function (input) {
        return input.match(/(\d|\D){1,4}/g).join(' ');
    };
}

module.exports = iban;
