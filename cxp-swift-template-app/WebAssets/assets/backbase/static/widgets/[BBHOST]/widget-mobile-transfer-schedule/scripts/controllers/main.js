(function(){

    'use strict';

    /**
     * @ngInject
     * @memberof module:widget-mobile-transfer-schedule
     */
    function MainCtrl(widget, model, bbDatePicker, bbFrequencyPicker, Event, bus, $filter, bbI18n, $q) {

        /**
         * Store the schedule object
         *
         * @type {{}}
         */
        var schedule = {};

        var dateFormat = 'yyyy-MM-dd';
        var dateFormatText = 'd MMM yyyy';
        var minDate = getMinSheduleDate();
        var maxDate = getMaxSheduleDate();
        var frequencies = model.getFrequencies();
        var intervals = model.getIntervals();
        /**
         *
         * @type {object}
         */
        var api = {
            $onInit: $onInit,
            $onDestroy: $onDestroy,
            scheduleTransaction: scheduleTransaction,
            scheduleFrequency: scheduleFrequency,
            saveSchedule: saveSchedule,

            get Label() { return bbI18n.labels; },
            get schedule() { return schedule; }
        };

        return api;

        /**
         * On widget init
         */
        function $onInit() {
            bus.subscribe(Event.Global.NAV_SELECT_DATE, handleNavigation);

            bbI18n.labels
        }

        /**
         * On widget destroy
         */
        function $onDestroy() {
            bus.unsubscribe(Event.Global.NAV_SELECT_DATE, handleNavigation);
        }

        /**
         * Handle the navigation to the schedule transaction widget
         */
        function handleNavigation(data) {
            schedule = data;
        }

        /**
         * On selecting schedule date
         *
         * @public
         */
        function scheduleTransaction() {

            datePickerShow(dateFormat, minDate, maxDate, schedule.date)
                .then(function(obj) {
                    if (typeof obj === 'object' && !!obj.date ) {
                        // Check if chosen date is today
                        if (isSameDate(new Date(obj.date), new Date())) {
                            schedule.dateText = bbI18n.labels.TODAY;
                        } else {
                            schedule.dateText = $filter('date')(obj.date, dateFormatText);
                        }

                        schedule.date = obj.date;
                    }
                });
        }

        function datePickerShow(dateFormat, minDate, maxDate, selectedDate) {
            return $q(function(resolve, reject) {
                bbDatePicker.show(resolve, reject, dateFormat, minDate, maxDate, selectedDate);
            });
        }

        /**
         * On frequency select
         *
         * @public
         */
        function scheduleFrequency() {
            var translations = [];
            for(var key in frequencies) {
                if(frequencies.hasOwnProperty(key) ) {
                    translations.push(bbI18n.labels[frequencies[key]]);
                }
            }

            return frequencyPluginShow(intervals, translations, schedule.intervals || 0, schedule.frequency || 0)
                .then(function(obj) {
                    if (obj.interval === null && obj.frequency === null) {
                        schedule.frequencyText = bbI18n.labels.ONLY_ONCE;
                        schedule.intervals = null;
                        schedule.frequency = null;
                    } else {
                        schedule.frequency = obj.frequency;
                        schedule.frequencyText = bbI18n.labels[frequencies[obj.frequency]];

                        if(obj.frequency > 0) {
                            schedule.frequencyText = bbI18n.labels.EVERY + ' ' + intervals[obj.interval] + ' ' + schedule.frequencyText;
                        }

                        if (obj.interval) {
                            schedule.intervals = parseInt(obj.interval, 10);
                            schedule.every = 1; // TODO: day of week, month, ...
                        } else {
                            schedule.intervals = null;
                            schedule.every = 0;
                        }
                    }
                });
        }

        function frequencyPluginShow(intervals, translations, selectedInterval, selectedFrequency) {
            return $q(function(resolve, reject) {
                bbFrequencyPicker.show(resolve, reject, intervals, translations, selectedInterval, selectedFrequency)
            });
        }

        /**
         * On saving schedule
         *
         * @public
         */
         function saveSchedule () {
            bus.publish(Event.Global.SCHEDULE_SAVE, schedule);
            bus.publish('navigation:go-back-to-transfer');
         }


        /**
         * @private
         * @return {string}
         */
        function getMinSheduleDate() {
            var date = new Date();
            return formatDate(date);
        }
        /**
         * @private
         * @return {string}
         */
        function getMaxSheduleDate() {
            var date = new Date();
            date.setFullYear(date.getFullYear() + 10); // add 10 years
            return formatDate(date);
        }


        /**
         * Format date
         *
         * @param date {Date}
         * @return {string}
         */
        function formatDate(date) {
            if (Object.prototype.toString.call(date) !== '[object Date]') {
                console.log('Not valid date', date);
                return formatDate(new Date());
            }

            return $filter('date')(date, dateFormat);
        }

        /**
         * Check if passed date is today's date
         *
         * @param d1
         * @param d2
         * @return {boolean}
         */
        function isSameDate(d1, d2) {
            return d1.getFullYear() === d2.getFullYear() &&
                   d1.getMonth() === d2.getMonth() &&
                   d1.getDate() === d2.getDate();
        }

    }


    module.exports = MainCtrl;
})();
