'use strict';

// @ngInject
function WidgetBus($rootScope, widget) {
    var pubsub = widget.features && widget.features.pubsub ||
        window.gadgets && window.gadgets.pubsub;

    return {
            on: $rootScope.$on.bind($rootScope),
            emit: $rootScope.$emit.bind($rootScope),
            // todo: How to: $scope.$apply on subscribe.
            subscribe: function(e, callback) {
                return pubsub.subscribe(e, function() {
                    var args = Array.prototype.slice.call(arguments);
                    args.unshift(null); // THIS
                    $rootScope.safeApply(callback.bind.apply(callback, args));
                });
            },
            publish: pubsub.publish.bind(pubsub)
    }
}

module.exports = WidgetBus;
