'use strict';

/**
 * Widget ViewModel
 * @ngInject
 * @name widgetModel
 * @private
 * @memberof module: widget-mobile-transfer-schedule
 * @kind class Angular service provider
 */
function WidgetModel() {

    return {
        getFrequencies: getFrequencies,
        getIntervals: getIntervals
    };

    /**
     * Get list of frequencies
     * @return {Array}
     */
    function getFrequencies() {
        return ['ONLY_ONCE','WEEKS','MONTHS','END_OF_MONTH'];
    }

    /**
     * Get list of intervals
     * @return {Array}
     */
    function getIntervals() {
        return ['1','2','3','4','5','6','7','8','9','10','11','12'];
    }
}

module.exports = WidgetModel;
