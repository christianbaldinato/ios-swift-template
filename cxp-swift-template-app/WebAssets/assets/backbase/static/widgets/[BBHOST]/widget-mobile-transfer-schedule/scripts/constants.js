'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
            LOADED: 'cxp.item.loaded',
            NAV_SELECT_DATE: 'navigation:transfers.transfer.navigation-date-select',
            SCHEDULE_SAVE: 'transfers.transfer.schedule-save'
        },
        Local: {
        }
    },
    Preference: {
    },
    Error: {
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
};

module.exports = WidgetConstants;
