'use strict';

// externals
var ng = require('angular');
var mobilePlugins = require('mobilePlugins');

// internals
var Model = require('./model');
var Bus = require('./bus');
var config = require('./config');
var constants = require('./constants');

// Controllers
var MainCtrl = require('./controllers/main');

/**
 * @ngInject
 */
function run(widget, bus, Event, SERVER_ROOT, $rootScope) {
    // Add a safeApply function.
    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    // Configure services.
    function getEndpoint(endpointPref) {
        return widget.getPreference(endpointPref).replace('$(servicesPath)', SERVER_ROOT);
    }

    // Publish ready event.
    bus.publish(Event.Global.LOADED, {
        id: widget.id
    });
}

/**
 * Widget Angular Module
 * @module widget-mobile-transfer-schedule
 * @link https://docs.angularjs.org/api/ng/function/angular.module
 * @param {object} widget Widget instance
 * @returns {object} AngularJS module
 */
function App(widget) {
    var deps = [
        mobilePlugins.name
    ];

    return ng.module(widget.id, deps)
        .config(config)
        .constant(constants)
        .factory('bus', Bus)
        .factory('model', Model)
        .controller('MainCtrl', MainCtrl)
        .provider('widget', function() {
            this.$get = function() {
                return widget;
            };
        })
        .run(run);
}

module.exports = App;
