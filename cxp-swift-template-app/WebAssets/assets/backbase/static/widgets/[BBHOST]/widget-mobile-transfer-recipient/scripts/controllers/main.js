(function() {
    'use strict';

    function noop() {}

    /**
     * Group contacts by first letter
     */
    function groupBeneficiariesByInitial(contacts) {
        var groups = { };
        contacts.forEach(function(contact) {
            var letter = contact.name[0].toUpperCase();
            if (!groups[letter]) {
                groups[letter] = [];
            }
            groups[letter].push(contact);
        });

        return groups;
    }

    /**
     * @ngInject
     */
    function MainCtrl(AccountsModel, BeneficiariesModel, Event, $scope, SERVER_ROOT, bbI18n) {
        var accounts = [];
        var beneficiaries = [];
        var groupedBeneficiaries;
        var fromAccountIdentifier = null;
        var search = { text: '' };
        var baseUrl = '';

        if (/^http.*/.test(SERVER_ROOT)) {
            baseUrl = SERVER_ROOT.replace(/^((\w+:)?\/\/[^\/]+\/?).*$/,'$1');
            if (baseUrl.charAt(baseUrl.length - 1) === '/') {
                baseUrl = baseUrl.slice(0, -1);
            } 
        }   

        return {
            get accounts() { return accounts; },
            get beneficiaries() { return beneficiaries; },
            get fromAccountIdentifier() { return fromAccountIdentifier; },
            get Labels() { return bbI18n.labels; },
            get groupedBeneficiaries() { return groupedBeneficiaries; },
            search: search,

            $onInit: $onInit,
            $onDestroy: $onDestroy,
            publishAccount: publishAccount,
            publishBeneficiary: publishBeneficiary
        };

        function $onInit() {
            gadgets.pubsub.subscribe(Event.Global.NAV_SELECT_RECIPIENT, onSelectRecipient);

            var accountsLoading = true;
            AccountsModel.getList().then(function(response) {
                accountsLoading = false;
                accounts = response.data;
            });

            groupedBeneficiaries = { };

            var beneficiariesLoading = true;
            BeneficiariesModel.getList().then(function(response) {
                beneficiariesLoading = false;
                beneficiaries = response.data;
                groupedBeneficiaries = groupBeneficiariesByInitial(beneficiaries);
            });
        };

        function $onDestroy() {
            gadgets.pubsub.unsubscribe(Event.Global.NAV_SELECT_RECIPIENT, onSelectRecipient);
        };

        function publishAccount(account) {
            gadgets.pubsub.publish('transfers.transfer.beneficiary-select', {
                name: account.alias,
                accountIdentification: account.accountIdentification[0].id
            });
            gadgets.pubsub.publish('navigation:go-back-to-transfer');
        };

        function publishBeneficiary(beneficiary) {
            gadgets.pubsub.publish('transfers.transfer.beneficiary-select', {
                name: beneficiary.name,
                accountIdentification: beneficiary.account
            });
            gadgets.pubsub.publish('navigation:go-back-to-transfer');
        };

        /**
         * Get initials for the contact list
         *
         * @param name
         * @return {string}
         */
        function getInitials(name) {
            var names = name.split(' ');
            return names[0].charAt(0) + names[names.length - 1].charAt(0);
        };

        // TODO extract function from publishAccount and publishBeneficiary
        function sendData(data, type){

            var name,
                accountIdentification = '';

            if( type == 'account' ){
                name = data.account.alias;
                accountIdentification = data.account.accountIdentification[0].id;
            } else {
                name: data.beneficiary.account;
                accountIdentification: data.beneficiary.name;
            }

            gadgets.pubsub.publish('transfers.transfer.beneficiary-select', {
                name: name,
                accountIdentification: accountIdentification
            });

            gadgets.pubsub.publish('navigation:go-back-to-transfer');
        }

        /**
         * Function that is called when other widget pusblishes event
         * that navigates to this widget
         * It is used to filter the accounts that are not selected in from account field
         */
        function onSelectRecipient(data) {
            $scope.$evalAsync(function() {
                search.text = '';
                fromAccountIdentifier = data.accountIdentification;
            });
        }
    }

    module.exports = MainCtrl;
})();
