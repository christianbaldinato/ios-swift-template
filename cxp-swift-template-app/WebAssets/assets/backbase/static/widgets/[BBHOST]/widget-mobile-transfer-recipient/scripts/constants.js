'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
            NAV_SELECT_RECIPIENT: 'navigation:transfers.transfer.navigation-beneficiary-select'
        },
        Local: {}
    },
    Error: {
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
};

module.exports = WidgetConstants;
