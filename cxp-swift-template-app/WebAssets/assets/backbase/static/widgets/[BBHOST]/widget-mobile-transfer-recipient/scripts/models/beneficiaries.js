(function() {
    'use strict';

    /**
     * @ngInject
     */
    function BeneficiariesModel($http, $q, widget) {
        var self = this;

        var beneficariesUrlPref = widget.model.getPreference('beneficiariesUrl');
        this.beneficiariesUrl = beneficariesUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);

        this.getList = function() {

            var deferred = $q.defer();

            $http.get(this.beneficiariesUrl).then(function(response) {

                var contacts = response.data;

                contacts.sort(function(a, b) {
                    if(a.name < b.name) return -1;
                    if(a.name > b.name) return 1;
                    return 0;
                });

                response.data = contacts;

                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            }, function(update) {
                deferred.update(update);
            });

            return deferred.promise;
        };

    }

    module.exports = BeneficiariesModel;
})();
