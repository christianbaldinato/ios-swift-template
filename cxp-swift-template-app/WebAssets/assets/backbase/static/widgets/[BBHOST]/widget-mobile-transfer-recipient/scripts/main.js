/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!***************************!*\
  !*** ./scripts/widget.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	window.shc = window.shc || {};
	shc.mobile = shc.mobile || {};
	shc.mobile.widget = shc.mobile.widget || {};
	shc.mobile.widget.transferRecipient = (function(angular, mobilePlugins) {
	    'use strict';
	
	    var TransferRecipientWidget = function(widget) {
	        var self = this;
	
	        this.widget = widget;
	
	        var config = __webpack_require__(/*! ./config */ 1);
	
	        var constants = __webpack_require__(/*! ./constants */ 3);
	
	        // Controllers
	        var MainCtrl = __webpack_require__(/*! ./controllers/main */ 4);
	
	        // Models
	        var AccountsModel = __webpack_require__(/*! ./models/accounts */ 5);
	        var BeneficiariesModel = __webpack_require__(/*! ./models/beneficiaries */ 6);
	
	        // Native Plugins
	        var AlertDialogProvider = __webpack_require__(/*! ./plugins/alert-dialog */ 7);
	        var ActivityIndicatorProvider = __webpack_require__(/*! ./plugins/activity-indicator */ 8);
	
	        var deps = [
	            mobilePlugins.name
	        ];
	        angular.module(widget.id, deps)
	            .config(config)
	            .constant(constants)
	            // Filters
	            .filter('iban', ibanModule.filter)
	            // Models
	            .service('AccountsModel', AccountsModel)
	            .service('BeneficiariesModel', BeneficiariesModel)
	            // Controllers
	            .controller('MainCtrl', MainCtrl)
	            // Directives
	            .directive('formatAmount', formatAmount)
	            .directive('searchBox', searchBox)
	            .directive('avatar', avatar)
	            // Native Plugins
	            .provider('AlertDialog', AlertDialogProvider)
	            .provider('ActivityIndicator', ActivityIndicatorProvider)
	            // Widget Object
	            .provider('widget', function() {
	                this.$get = function() {
	                    return widget;
	                };
	            })
	            .run(function(){
	                self.start();
	            });
	        // Bootstrap the widget as an Angular App
	        angular.bootstrap(widget.body, [widget.id]);
	    };
	
	    TransferRecipientWidget.prototype.start = function() {
	        gadgets.pubsub.publish('cxp.item.loaded', {
	            id: this.widget.id
	        });
	    };
	
	    return {
	        init: function(widget) {
	            var transferRecipientApp = new TransferRecipientWidget(widget);
	            return transferRecipientApp;
	        }
	    };
	}(window.angular, window.mobilePlugins));


/***/ },
/* 1 */
/*!***************************!*\
  !*** ./scripts/config.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var labels = __webpack_require__(/*! ../locale/all.json */ 2);
	
	/**
	 * @ngInject
	 */
	function WidgetConfig($provide, $compileProvider, bbI18nProvider) {
	    $compileProvider.debugInfoEnabled(false);
	    bbI18nProvider.setDefaultLabels(labels);
	};
	
	module.exports = WidgetConfig;


/***/ },
/* 2 */
/*!*************************!*\
  !*** ./locale/all.json ***!
  \*************************/
/***/ function(module, exports) {

	module.exports = {
		"en": {
			"ACCOUNTS": "Accounts",
			"SEARCH_PLACEHOLDER": "Search"
		},
		"nl": {
			"ACCOUNTS": "Rekeningen",
			"SEARCH_PLACEHOLDER": "Zoeken"
		}
	};

/***/ },
/* 3 */
/*!******************************!*\
  !*** ./scripts/constants.js ***!
  \******************************/
/***/ function(module, exports) {

	'use strict';
	
	var WidgetConstants = {
	    /**
	     * Local and global events, that widget publishes (emits) or relies on
	     */
	    Event: {
	        Global: {
	            NAV_SELECT_RECIPIENT: 'navigation:transfers.transfer.navigation-beneficiary-select'
	        },
	        Local: {}
	    },
	    Error: {
	    },
	    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
	};
	
	module.exports = WidgetConstants;


/***/ },
/* 4 */
/*!*************************************!*\
  !*** ./scripts/controllers/main.js ***!
  \*************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    function noop() {}
	
	    /**
	     * Group contacts by first letter
	     */
	    function groupBeneficiariesByInitial(contacts) {
	        var groups = { };
	        contacts.forEach(function(contact) {
	            var letter = contact.name[0].toUpperCase();
	            if (!groups[letter]) {
	                groups[letter] = [];
	            }
	            groups[letter].push(contact);
	        });
	
	        return groups;
	    }
	
	    /**
	     * @ngInject
	     */
	    function MainCtrl(AccountsModel, BeneficiariesModel, Event, $scope, SERVER_ROOT, bbI18n) {
	        var accounts = [];
	        var beneficiaries = [];
	        var groupedBeneficiaries;
	        var fromAccountIdentifier = null;
	        var search = { text: '' };
	        var baseUrl = '';
	
	        if (/^http.*/.test(SERVER_ROOT)) {
	            baseUrl = SERVER_ROOT.replace(/^((\w+:)?\/\/[^\/]+\/?).*$/,'$1');
	            if (baseUrl.charAt(baseUrl.length - 1) === '/') {
	                baseUrl = baseUrl.slice(0, -1);
	            } 
	        }   
	
	        return {
	            get accounts() { return accounts; },
	            get beneficiaries() { return beneficiaries; },
	            get fromAccountIdentifier() { return fromAccountIdentifier; },
	            get Labels() { return bbI18n.labels; },
	            get groupedBeneficiaries() { return groupedBeneficiaries; },
	            search: search,
	
	            $onInit: $onInit,
	            $onDestroy: $onDestroy,
	            publishAccount: publishAccount,
	            publishBeneficiary: publishBeneficiary
	        };
	
	        function $onInit() {
	            gadgets.pubsub.subscribe(Event.Global.NAV_SELECT_RECIPIENT, onSelectRecipient);
	
	            var accountsLoading = true;
	            AccountsModel.getList().then(function(response) {
	                accountsLoading = false;
	                accounts = response.data;
	            });
	
	            groupedBeneficiaries = { };
	
	            var beneficiariesLoading = true;
	            BeneficiariesModel.getList().then(function(response) {
	                beneficiariesLoading = false;
	                beneficiaries = response.data;
	                groupedBeneficiaries = groupBeneficiariesByInitial(beneficiaries);
	            });
	        };
	
	        function $onDestroy() {
	            gadgets.pubsub.unsubscribe(Event.Global.NAV_SELECT_RECIPIENT, onSelectRecipient);
	        };
	
	        function publishAccount(account) {
	            gadgets.pubsub.publish('transfers.transfer.beneficiary-select', {
	                name: account.alias,
	                accountIdentification: account.accountIdentification[0].id
	            });
	            gadgets.pubsub.publish('navigation:go-back-to-transfer');
	        };
	
	        function publishBeneficiary(beneficiary) {
	            gadgets.pubsub.publish('transfers.transfer.beneficiary-select', {
	                name: beneficiary.name,
	                accountIdentification: beneficiary.account
	            });
	            gadgets.pubsub.publish('navigation:go-back-to-transfer');
	        };
	
	        /**
	         * Get initials for the contact list
	         *
	         * @param name
	         * @return {string}
	         */
	        function getInitials(name) {
	            var names = name.split(' ');
	            return names[0].charAt(0) + names[names.length - 1].charAt(0);
	        };
	
	        // TODO extract function from publishAccount and publishBeneficiary
	        function sendData(data, type){
	
	            var name,
	                accountIdentification = '';
	
	            if( type == 'account' ){
	                name = data.account.alias;
	                accountIdentification = data.account.accountIdentification[0].id;
	            } else {
	                name: data.beneficiary.account;
	                accountIdentification: data.beneficiary.name;
	            }
	
	            gadgets.pubsub.publish('transfers.transfer.beneficiary-select', {
	                name: name,
	                accountIdentification: accountIdentification
	            });
	
	            gadgets.pubsub.publish('navigation:go-back-to-transfer');
	        }
	
	        /**
	         * Function that is called when other widget pusblishes event
	         * that navigates to this widget
	         * It is used to filter the accounts that are not selected in from account field
	         */
	        function onSelectRecipient(data) {
	            $scope.$evalAsync(function() {
	                search.text = '';
	                fromAccountIdentifier = data.accountIdentification;
	            });
	        }
	    }
	
	    module.exports = MainCtrl;
	})();


/***/ },
/* 5 */
/*!************************************!*\
  !*** ./scripts/models/accounts.js ***!
  \************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function AccountsModel($http, widget) {
	        var self = this;
	
	        var accountsUrlPref = widget.model.getPreference('accountsUrl');
	        this.url = accountsUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);
	
	        this.getList = function() {
	            return $http({
	                method: 'GET',
	                url: this.url
	            });
	        };
	    }
	
	    module.exports = AccountsModel;
	})();


/***/ },
/* 6 */
/*!*****************************************!*\
  !*** ./scripts/models/beneficiaries.js ***!
  \*****************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function BeneficiariesModel($http, $q, widget) {
	        var self = this;
	
	        var beneficariesUrlPref = widget.model.getPreference('beneficiariesUrl');
	        this.beneficiariesUrl = beneficariesUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);
	
	        this.getList = function() {
	
	            var deferred = $q.defer();
	
	            $http.get(this.beneficiariesUrl).then(function(response) {
	
	                var contacts = response.data;
	
	                contacts.sort(function(a, b) {
	                    if(a.name < b.name) return -1;
	                    if(a.name > b.name) return 1;
	                    return 0;
	                });
	
	                response.data = contacts;
	
	                deferred.resolve(response);
	            }, function (error) {
	                deferred.reject(error);
	            }, function(update) {
	                deferred.update(update);
	            });
	
	            return deferred.promise;
	        };
	
	    }
	
	    module.exports = BeneficiariesModel;
	})();


/***/ },
/* 7 */
/*!*****************************************!*\
  !*** ./scripts/plugins/alert-dialog.js ***!
  \*****************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function AlertDialog() {
	        this.$get = function(widget) {
	            if (widget.features && widget.features.AlertDialog) {
	                return widget.features.AlertDialog;
	            } else {
	                return null;
	            }
	        };
	    }
	
	    module.exports = AlertDialog;
	})();


/***/ },
/* 8 */
/*!***********************************************!*\
  !*** ./scripts/plugins/activity-indicator.js ***!
  \***********************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    /**
	     * @ngInject
	     */
	    function ActivityIndicator() {
	        this.$get = function(widget) {
	            if (widget.features && widget.features.ActivityIndicator) {
	                return widget.features.ActivityIndicator;
	            } else {
	                return null;
	            }
	        };
	    }
	
	    module.exports = ActivityIndicator;
	})();


/***/ }
/******/ ]);
//# sourceMappingURL=main.js.map