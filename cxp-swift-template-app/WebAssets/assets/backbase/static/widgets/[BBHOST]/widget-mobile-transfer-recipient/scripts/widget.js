window.shc = window.shc || {};
shc.mobile = shc.mobile || {};
shc.mobile.widget = shc.mobile.widget || {};
shc.mobile.widget.transferRecipient = (function(angular, mobilePlugins) {
    'use strict';

    var TransferRecipientWidget = function(widget) {
        var self = this;

        this.widget = widget;

        var config = require('./config');

        var constants = require('./constants');

        // Controllers
        var MainCtrl = require('./controllers/main');

        // Models
        var AccountsModel = require('./models/accounts');
        var BeneficiariesModel = require('./models/beneficiaries');

        // Native Plugins
        var AlertDialogProvider = require('./plugins/alert-dialog');
        var ActivityIndicatorProvider = require('./plugins/activity-indicator');

        var deps = [
            mobilePlugins.name
        ];
        angular.module(widget.id, deps)
            .config(config)
            .constant(constants)
            // Filters
            .filter('iban', ibanModule.filter)
            // Models
            .service('AccountsModel', AccountsModel)
            .service('BeneficiariesModel', BeneficiariesModel)
            // Controllers
            .controller('MainCtrl', MainCtrl)
            // Directives
            .directive('formatAmount', formatAmount)
            .directive('searchBox', searchBox)
            .directive('avatar', avatar)
            // Native Plugins
            .provider('AlertDialog', AlertDialogProvider)
            .provider('ActivityIndicator', ActivityIndicatorProvider)
            // Widget Object
            .provider('widget', function() {
                this.$get = function() {
                    return widget;
                };
            })
            .run(function(){
                self.start();
            });
        // Bootstrap the widget as an Angular App
        angular.bootstrap(widget.body, [widget.id]);
    };

    TransferRecipientWidget.prototype.start = function() {
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: this.widget.id
        });
    };

    return {
        init: function(widget) {
            var transferRecipientApp = new TransferRecipientWidget(widget);
            return transferRecipientApp;
        }
    };
}(window.angular, window.mobilePlugins));
