(function() {
    'use strict';

    function noop() {}

    /**
     * @ngInject
     */
    function MainCtrl($scope, AccountsModel, Event, bbI18n) {
        var initLoading = true;
        var accounts;
        var accountsError = null;
        var accountsLoading = true;

        var currencySymbols = {
            'USD': '$', // US Dollar
            'EUR': '€', // Euro
            'GBP': '£', // British Pound Sterling
            'JPY': '¥' // Japanese Yen
        };

        return {
            $onInit: $onInit,
            $onDestroy: $onDestroy,
            get initLoading() { return initLoading; },
            get accounts() { return accounts; },
            get accountsLoading() { return accountsLoading; },
            get accountsError() { return accountsError; },
            get hasAccounts() { return hasAccounts() },
            get Labels() { return bbI18n.labels; },
            currencySymbols: currencySymbols,
            showTransactions: showTransactions,
            makePayment: makePayment,
            loadAccounts: loadAccounts
        };

        /**
         * @public
         */
        function $onInit() {
            accounts = [];
            gadgets.pubsub.publish('homeLoaded', { });
            gadgets.pubsub.subscribe('accounts:reload', loadAccounts);

            gadgets.pubsub.subscribe('bawag.payment.successful', function() {
                window.setTimeout(function() {
                    cxp.mobile.reload();
                }, 1000);
            });

            loadAccounts();
        }

        /**
         * @public
         */
        function $onDestroy() {
            gadgets.pubsub.unsubscribe('accounts:reload', loadAccounts);
        }

        /**
         * @public
         */
        function loadAccounts() {
            accountsLoading = true;
            AccountsModel.getList()
                .then(function(results) {
                    initLoading = false;
                    accounts = results;
                    accountsLoading = false;
                })
            .catch(handleError);
        }

        /**
         * @public
         */
        function hasAccounts() {
            return accounts.length > 0;
        }

        /**
         * @public
         */
        function showTransactions(account) {
            gadgets.pubsub.publish('shc.open.transactions', {
                account: account
            });
        }

        /**
         * @public
         */
        function makePayment(account) {
            gadgets.pubsub.publish('transfers.transfer.from-account-select', {
                account:{
                    accountId: account.id,
                    accountDetails: account
                }
            });
            gadgets.pubsub.publish('navigation:go-back-to-transfer', {
                account: account
            });
        }

        /**
         * Handle data error
         *
         * @param err
         */
        function handleError(err) {
            initLoading = false;
            accountsLoading = false;
            switch (err.code) {
                case AccountsModel.E_OFFLINE:
                    err.message = err.message || bbI18n.labels.ERROR_OFFLINE;
                    gadgets.pubsub.publish(Event.Global.OFFLINE, err);
                    break;
                case AccountsModel.E_AUTH:
                    err.message = err.message || bbI18n.labels.ERROR_AUTH;
                    gadgets.pubsub.publish(Event.Global.AUTH, err);
                    break;
                default:
                    // Unexpected error.
                    err.message = err.message || bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;
                    gadgets.pubsub.publish(Event.Global.ERROR_UNEXPECTED, err);
            }

            accountsError = err;
        }

        /**
         * Handle user error
         *
         * @param error
         * @return {*}
         */
        function errorMessage(error) {
            if (error.message) {
                return error.message;
            }

            if (!error.code) {
                return bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;
            }

            var messages = [];
            messages[AccountsModel.E_OFFLINE] = bbI18n.labels.ERROR_OFFLINE;
            messages[AccountsModel.E_AUTH] = bbI18n.labels.ERROR_AUTH;
            messages[AccountsModel.E_UNEXPECTED] = bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;

            return (messages[error.code]) ? messages[error.code] : bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;
        }
    }

    module.exports = MainCtrl;
})();
