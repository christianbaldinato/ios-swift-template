(function() {
    'use strict';

    var E_OFFLINE = 1;
    var E_UNEXPECTED = 2;
    var E_AUTH = 3;

    /**
     * @ngInject
     */
    function AccountsModel($http, widget) {
        var accountsUrlPref = widget.model.getPreference('accountsUrl');
        var url = accountsUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);

        return {
            getList: getList,
            E_OFFLINE: E_OFFLINE,
            E_AUTH: E_AUTH,
            E_UNEXPECTED: E_UNEXPECTED
        };

        function getList() {
            return $http.get(url)
                .then(function(response) {
                    return response.data;
                })
                .catch(onError);
        }

        /*
         * Normalise errors
         */
        function onError(httpErr) {
            var err = {
                __httpResponse: httpErr
            };

            if (httpErr.status <= 1) {
                err.code = E_OFFLINE;
            }
            else if (httpErr.status === 401) {
                err.code = E_AUTH;
            }
            else {
                err.code = E_UNEXPECTED;
            }

            throw err;
        }
    }

    module.exports = AccountsModel;
})();
