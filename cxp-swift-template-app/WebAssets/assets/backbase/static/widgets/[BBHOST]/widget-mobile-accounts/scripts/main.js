/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!***************************!*\
  !*** ./scripts/widget.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	window.shc = window.shc || {};
	shc.mobile = shc.mobile || {};
	shc.mobile.widget = shc.mobile.widget || {};
	shc.mobile.widget.accounts = (function(angular, ibanModule, mobilePlugins) {
	    'use strict';
	
	    var AccountsWidget = function(widget) {
	        var self = this;
	
	        this.widget = widget;
	
	        var config = __webpack_require__(/*! ./config */ 1);
	        var constants = __webpack_require__(/*! ./constants */ 3);
	
	        // Controllers
	        var MainCtrl = __webpack_require__(/*! ./controllers/main */ 4);
	
	        // Models
	        var AccountsModel = __webpack_require__(/*! ./models/accounts */ 5);
	
	        var deps = [
	            mobilePlugins.name
	        ];
	        angular.module(widget.id, deps)
	            .config(config)
	            .constant(constants)
	            // Models
	            .service('AccountsModel', AccountsModel)
	            // Controllers
	            .controller('MainCtrl', MainCtrl)
	            .service('ibanService', ibanModule.service)
	            .filter('iban', ibanModule.filter)
	            // Directives
	            .directive('validIban', ibanModule.validator)
	            .directive('formatAmount', formatAmount)
	            // Widget Object
	            .provider('widget', function() {
	                this.$get = function() {
	                    return widget;
	                };
	            })
	            .run(function(){
	                self.start();
	            });
	        // Bootstrap the widget as an Angular App
	        angular.bootstrap(widget.body, [widget.id]);
	    };
	
	    AccountsWidget.prototype.start = function() {
	        gadgets.pubsub.publish('cxp.item.loaded', {
	            id: this.widget.id
	        });
	    };
	
	    return {
	        init: function(widget) {
	            var accountsApp = new AccountsWidget(widget);
	            return accountsApp;
	        }
	    };
	}(window.angular, window.ibanModule, window.mobilePlugins));


/***/ },
/* 1 */
/*!***************************!*\
  !*** ./scripts/config.js ***!
  \***************************/
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var labels = __webpack_require__(/*! ../locale/all.json */ 2);
	
	/**
	 * @ngInject
	 */
	function WidgetConfig($provide, $compileProvider, bbI18nProvider) {
	    $compileProvider.debugInfoEnabled(false);
	    bbI18nProvider.setDefaultLabels(labels);
	};
	
	module.exports = WidgetConfig;


/***/ },
/* 2 */
/*!*************************!*\
  !*** ./locale/all.json ***!
  \*************************/
/***/ function(module, exports) {

	module.exports = {
		"en": {
			"TRY_AGAIN": "Try Again",
			"ERROR_OFFLINE": "Unable to establish connection. Check your internet connection.",
			"ERROR_AUTH": "It appears as if you've been logged out.",
			"SEE_TRANSACTION": "See transactions",
			"MAKE_PAYMENT": "Make a payment",
			"NO_ACCOUNTS": "You have no accounts",
			"ERROR_UNEXPECTED_ACCOUNTS": "Unexpected error loading your accounts"
		},
		"nl": {
			"TRY_AGAIN": "Probeer het nog eens",
			"ERROR_OFFLINE": "Kan geen verbinding maken. Controleer uw internetverbinding.",
			"ERROR_AUTH": "Het lijkt alsof u bent uitgelogd.",
			"SEE_TRANSACTION": "Bekijk transacties",
			"MAKE_PAYMENT": "Maak een betaling",
			"NO_ACCOUNTS": "Je hebt geen accounts",
			"ERROR_UNEXPECTED_ACCOUNTS": "Onverwachte fout bij het laden van uw accounts"
		},
		"de": {
			"TRY_AGAIN": "Nochmal versuchen",
			"ERROR_OFFLINE": "Es konnte keine Verbindung aufgebaut werden. Bitte überprüfen Sie Ihre Internetverbindung.",
			"ERROR_AUTH": "Es scheint, als ob Sie bereits ausgeloggt sind.",
			"SEE_TRANSACTION": "Transaktionen",
			"MAKE_PAYMENT": "Überweisung",
			"NO_ACCOUNTS": "Sie haben keine Konten",
			"ERROR_UNEXPECTED_ACCOUNTS": "Unerwarteter Fehler"
		}
	};

/***/ },
/* 3 */
/*!******************************!*\
  !*** ./scripts/constants.js ***!
  \******************************/
/***/ function(module, exports) {

	'use strict';
	
	var WidgetConstants = {
	    /**
	     * Local and global events, that widget publishes (emits) or relies on
	     */
	    Event: {
	        Global: {
	            OFFLINE: 'offline:request-failed',
	            AUTH: 'user-auth:request-unauthorised',
	            ERROR_UNEXPECTED: 'error:unexpected'
	        },
	        Local: {
	        }
	    },
	    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver'
	};
	
	module.exports = WidgetConstants;


/***/ },
/* 4 */
/*!*************************************!*\
  !*** ./scripts/controllers/main.js ***!
  \*************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    function noop() {}
	
	    /**
	     * @ngInject
	     */
	    function MainCtrl($scope, AccountsModel, Event, bbI18n) {
	        var initLoading = true;
	        var accounts;
	        var accountsError = null;
	        var accountsLoading = true;
	
	        var currencySymbols = {
	            'USD': '$', // US Dollar
	            'EUR': '€', // Euro
	            'GBP': '£', // British Pound Sterling
	            'JPY': '¥' // Japanese Yen
	        };
	
	        return {
	            $onInit: $onInit,
	            $onDestroy: $onDestroy,
	            get initLoading() { return initLoading; },
	            get accounts() { return accounts; },
	            get accountsLoading() { return accountsLoading; },
	            get accountsError() { return accountsError; },
	            get hasAccounts() { return hasAccounts() },
	            get Labels() { return bbI18n.labels; },
	            currencySymbols: currencySymbols,
	            showTransactions: showTransactions,
	            makePayment: makePayment,
	            loadAccounts: loadAccounts
	        };
	
	        /**
	         * @public
	         */
	        function $onInit() {
	            accounts = [];
	            gadgets.pubsub.publish('homeLoaded', { });
	            gadgets.pubsub.subscribe('accounts:reload', loadAccounts);
	
	            gadgets.pubsub.subscribe('bawag.payment.successful', function() {
	                window.setTimeout(function() {
	                    cxp.mobile.reload();
	                }, 1000);
	            });
	
	            loadAccounts();
	        }
	
	        /**
	         * @public
	         */
	        function $onDestroy() {
	            gadgets.pubsub.unsubscribe('accounts:reload', loadAccounts);
	        }
	
	        /**
	         * @public
	         */
	        function loadAccounts() {
	            accountsLoading = true;
	            AccountsModel.getList()
	                .then(function(results) {
	                    initLoading = false;
	                    accounts = results;
	                    accountsLoading = false;
	                })
	            .catch(handleError);
	        }
	
	        /**
	         * @public
	         */
	        function hasAccounts() {
	            return accounts.length > 0;
	        }
	
	        /**
	         * @public
	         */
	        function showTransactions(account) {
	            gadgets.pubsub.publish('shc.open.transactions', {
	                account: account
	            });
	        }
	
	        /**
	         * @public
	         */
	        function makePayment(account) {
	            gadgets.pubsub.publish('transfers.transfer.from-account-select', {
	                account:{
	                    accountId: account.id,
	                    accountDetails: account
	                }
	            });
	            gadgets.pubsub.publish('navigation:go-back-to-transfer', {
	                account: account
	            });
	        }
	
	        /**
	         * Handle data error
	         *
	         * @param err
	         */
	        function handleError(err) {
	            initLoading = false;
	            accountsLoading = false;
	            switch (err.code) {
	                case AccountsModel.E_OFFLINE:
	                    err.message = err.message || bbI18n.labels.ERROR_OFFLINE;
	                    gadgets.pubsub.publish(Event.Global.OFFLINE, err);
	                    break;
	                case AccountsModel.E_AUTH:
	                    err.message = err.message || bbI18n.labels.ERROR_AUTH;
	                    gadgets.pubsub.publish(Event.Global.AUTH, err);
	                    break;
	                default:
	                    // Unexpected error.
	                    err.message = err.message || bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;
	                    gadgets.pubsub.publish(Event.Global.ERROR_UNEXPECTED, err);
	            }
	
	            accountsError = err;
	        }
	
	        /**
	         * Handle user error
	         *
	         * @param error
	         * @return {*}
	         */
	        function errorMessage(error) {
	            if (error.message) {
	                return error.message;
	            }
	
	            if (!error.code) {
	                return bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;
	            }
	
	            var messages = [];
	            messages[AccountsModel.E_OFFLINE] = bbI18n.labels.ERROR_OFFLINE;
	            messages[AccountsModel.E_AUTH] = bbI18n.labels.ERROR_AUTH;
	            messages[AccountsModel.E_UNEXPECTED] = bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;
	
	            return (messages[error.code]) ? messages[error.code] : bbI18n.labels.ERROR_UNEXPECTED_ACCOUNTS;
	        }
	    }
	
	    module.exports = MainCtrl;
	})();


/***/ },
/* 5 */
/*!************************************!*\
  !*** ./scripts/models/accounts.js ***!
  \************************************/
/***/ function(module, exports) {

	(function() {
	    'use strict';
	
	    var E_OFFLINE = 1;
	    var E_UNEXPECTED = 2;
	    var E_AUTH = 3;
	
	    /**
	     * @ngInject
	     */
	    function AccountsModel($http, widget) {
	        var accountsUrlPref = widget.model.getPreference('accountsUrl');
	        var url = accountsUrlPref.replace('$(servicesPath)', b$.portal.config.serverRoot);
	
	        return {
	            getList: getList,
	            E_OFFLINE: E_OFFLINE,
	            E_AUTH: E_AUTH,
	            E_UNEXPECTED: E_UNEXPECTED
	        };
	
	        function getList() {
	            return $http.get(url)
	                .then(function(response) {
	                    return response.data;
	                })
	                .catch(onError);
	        }
	
	        /*
	         * Normalise errors
	         */
	        function onError(httpErr) {
	            var err = {
	                __httpResponse: httpErr
	            };
	
	            if (httpErr.status <= 1) {
	                err.code = E_OFFLINE;
	            }
	            else if (httpErr.status === 401) {
	                err.code = E_AUTH;
	            }
	            else {
	                err.code = E_UNEXPECTED;
	            }
	
	            throw err;
	        }
	    }
	
	    module.exports = AccountsModel;
	})();


/***/ }
/******/ ]);
//# sourceMappingURL=main.js.map