window.shc = window.shc || {};
shc.mobile = shc.mobile || {};
shc.mobile.widget = shc.mobile.widget || {};
shc.mobile.widget.accounts = (function(angular, ibanModule, mobilePlugins) {
    'use strict';

    var AccountsWidget = function(widget) {
        var self = this;

        this.widget = widget;

        var config = require('./config');
        var constants = require('./constants');

        // Controllers
        var MainCtrl = require('./controllers/main');

        // Models
        var AccountsModel = require('./models/accounts');

        var deps = [
            mobilePlugins.name
        ];
        angular.module(widget.id, deps)
            .config(config)
            .constant(constants)
            // Models
            .service('AccountsModel', AccountsModel)
            // Controllers
            .controller('MainCtrl', MainCtrl)
            .service('ibanService', ibanModule.service)
            .filter('iban', ibanModule.filter)
            // Directives
            .directive('validIban', ibanModule.validator)
            .directive('formatAmount', formatAmount)
            // Widget Object
            .provider('widget', function() {
                this.$get = function() {
                    return widget;
                };
            })
            .run(function(){
                self.start();
            });
        // Bootstrap the widget as an Angular App
        angular.bootstrap(widget.body, [widget.id]);
    };

    AccountsWidget.prototype.start = function() {
        gadgets.pubsub.publish('cxp.item.loaded', {
            id: this.widget.id
        });
    };

    return {
        init: function(widget) {
            var accountsApp = new AccountsWidget(widget);
            return accountsApp;
        }
    };
}(window.angular, window.ibanModule, window.mobilePlugins));
