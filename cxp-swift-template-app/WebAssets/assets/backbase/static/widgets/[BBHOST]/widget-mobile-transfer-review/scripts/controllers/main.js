'use strict';

function noop() {}

/**
 * @ngInject
 * @memberof module:widget-mobile-transfer-review
 */
function MainCtrl(widget, $rootScope, model, bus, Event, Preference,
                  bbI18n, ErrorCodes, serviceBeneficiary, AlertCallbackFn,
                  // plugins
                  bbActivityIndicator, bbAlertDialog, bbSnackbar) {

    var recipient = {};

    /**
     * Loading message
     *
     * @type {boolean}
     */
    var isLoading = true;
    var showAuth = false;
    var authCode = '';

    /**
     * Transfer state
     */
    var transfer = model.getTransfer();


    /**
     * Transfer state
     */
    var transferData = {};

    /**
     * Account state
     */
    var account = model.getAccount();

    /**
     * Transfer review error.
     */
    var transferReviewError = null;

    return {
        get isLoading() { return isLoading; },
        get transferReviewError() { return transferReviewError; },
        get transfer() { return transfer; },
        get transferData() { return transferData; },
        get account() { return account; },
        get Labels() { return bbI18n.labels; },
        get showAuth() { return showAuth; },

        $onInit: $onInit,
        $onDestroy: $onDestroy,
        submitTransfer: submitTransfer,
        cancelTransfer: cancelTransfer,
        authCode: authCode
    };

    /**
     * Angularjs hook function called after the component has been initialized and all its
     * bindings have been set up. This will trigger the transaction and account details to load.
     * @public
     */
    function $onInit() {
        // Load transfer from nav event.
        bus.subscribe('bawag.data.review-transfer', handleNavEvent);
        bus.subscribe(Event.Local.VIEW_DISAPPEAR, handleViewDisappear);
    };

    /**
     *
     */
    function $onDestroy() {
        bus.unsubscribe('bawag.data.review-transfer', handleNavEvent);
        bus.unsubscribe(Event.Local.VIEW_DISAPPEAR, handleViewDisappear);
    };

    /**
     *
     * @return {*}
     */
    function submitTransfer() {

        if (!transfer.details || !transfer.details.id) {
            // todo: emit error; probably should go back to transfer initiation
            return;
        }

        bbActivityIndicator.show(noop, noop, bbI18n.labels.EXECUTING_TRANSFER);
        var auth = null;
        if (showAuth === true && this.authCode.length > 0) {
            auth = this.authCode;
        }
        return model.submitTransfer(transfer.details.id, auth)
            .then(function(response) {
                bbActivityIndicator.hide(noop, noop);

                if (response.__httpResponse && response.__httpResponse.status === 'AUTHORIZATION_NEEDED') {
                    // bus.publish('shc.open.approve');
                    showAuth = true;
                    return;
                }
                showAuth = false;

                bus.publish('bawag.payment.successful', {});
                bbSnackbar.success(noop, noop, bbI18n.labels.TRANSFER_SUCCESS);

                bus.publish(Event.Global.NOTIFY_SUCCESS,
                    { message: bbI18n.labels.TRANSFER_SUCCESS });

                // Add new Contact
                if (recipient.isNewContact && recipient.addContact) {
                    var data = {
                        id: serviceBeneficiary.generateUUID(),
                        name: recipient.counterpartyName,
                        account: recipient.counterpartyIban,
                        partyId: '1',
                        isNew: true
                    };

                    // Add new contact from recipient data
                    serviceBeneficiary.postCreateContact(data).then(function(response) {
                        bus.publish(Event.Global.REFRESH_ADDRESSBOOK);
                    })['catch'](function(e) {
                        // Don't show error messages, the user did't trigered this action
                        console.log('error', e);
                    });
                }

                bus.publish(Event.Global.EXECUTE, { transfer: transfer.details });
                bus.publish(Event.Global.TRANSFER_COMPLETE, { transfer: transfer.details });
                bus.publish(Event.Global.NAV_HOME);

                reset();
            })
            .catch(function(err) {
                err.callbackFn = AlertCallbackFn.RETRY_SUBMIT;
                handleUserError(err);
            });
    };

    /**
     *
     */
    function cancelTransfer() {
        var title = bbI18n.labels.TRANSFER_CANCEL_ALERT;
        var message = bbI18n.labels.TRANSFER_CANCEL_CONFIRM;;

        var buttons = [
            {
                type: 'POSITIVE',
                text: bbI18n.labels.TRANSFER_BUTTON_CANCEL_YES,
                callbackFn: 'yes'
            },
            {
                type: 'NEGATIVE',
                text: bbI18n.labels.TRANSFER_BUTTON_CANCEL_NO,
                callbackFn: 'no'
            }
        ];

        function onSuccess (data) {
            if (data.callback === 'yes') {
                doCancelTransfer();
            }
        }

        // Note: only works in mobile - needs polyfill for web.
        bbAlertDialog.show(onSuccess, noop, title, message, JSON.stringify(buttons));
    };

    /**
     * Navigation to Transfer Review
     *
     * @param data
     */
    function handleNavEvent(data) {

        var transferId = data.response.__httpResponse.id;//data && data.response && data.response.transferId;

        transferData = data && data.transferData || {};

        if (transferId) {
            loadTransferAndAccount(transferId);

            // If recipient should be added as new contact
            recipient.isNew = data.transfer && data.transfer.isNewContact;
            recipient = angular.copy(data.transfer);
        }
    }

    /**
     * Fired when the 'X' button is clicked
     * Delete the transfer if the closed page is same as the one that the widget is in
     *
     * @param {Object} data
     */
    function handleViewDisappear(data) {
        if (!!data && !!data.pageId && !!transfer && !!transfer.details && !!transfer.details.id &&
            data.pageId === widget.getPreference(Preference.PAGE_ID)) {

            model.cancelTransfer(transfer.details.id)
                .then(function() {
                    reset();
                })
                .catch(function(err) {
                    reset();
                    // todo: error handling. Do we have to show error message, because techically the user didn't
                    // ask for deleting the transaction. This is just workaround...
                    //
                    console.log(err);
                });
        }
    }


    /**
     *
     * @param transferId
     * @return {*}
     */
    function loadTransferAndAccount(transferId) {

        isLoading = true;

        return model.loadTransfer(transferId)
            .then(function() {
                // Transfer details should be loaded now.
                if (transfer.details && transfer.details.accountId) {
                    return loadAccount(transfer.details.accountId)
                        .then(function() {
                            isLoading = false;
                        }).catch(function(err) {
                            isLoading = false;
                            handleDataError(err);
                        });
                }
            })
            .catch(function(err) {
                isLoading = false;
                handleDataError(err);
            });
    }

    /**
     * Load accounts and filter the current one
     *
     * @param accountId
     * @return {*}
     */
    function loadAccount(accountId) {
        return model.loadAccount(accountId);
    }

    /**
     * Cancel the transaction
     */
    function doCancelTransfer() {
        bbActivityIndicator.show(noop, noop, bbI18n.labels.CANCELLING_TRANSFER);
        model.cancelTransfer(transfer.details.id)
            .then(function() {
                reset();
                bbActivityIndicator.hide(noop, noop);
                bus.publish(Event.Global.CANCEL);
                bus.publish(Event.Global.NAV_HOME);
            })
            .catch(function(err) {
                err.callbackFn = AlertCallbackFn.RETRY_CANCEL;
                handleUserError(err);
            });
    }

    /**
     * Reset the model
     */
    function reset() {
        model.reset();
    }

    /**
     * Handle error on retrieving data
     * @param err
     */
    function handleDataError(err) {
        bbActivityIndicator.hide(noop, noop);

        isLoading = false;
        switch (err.code) {
            case ErrorCodes.E_OFFLINE:
                err.message = err.message || bbI18n.labels.ERROR_OFFLINE;
                gadgets.pubsub.publish(Event.Global.E_OFFLINE, err);
                break;
            case ErrorCodes.E_AUTH:
                err.message = err.message || bbI18n.labels.ERROR_AUTH;
                gadgets.pubsub.publish(Event.Global.E_AUTH, err);
                break;
            default:
                // Unexpected error.
                err.message = err.message || bbI18n.labels.ERROR_UNEXPECTED_TRANSFER_EXECUTE;
                gadgets.pubsub.publish(Event.Global.E_UNEXPECTED, err);
        }

        transferReviewError = err;
    }

    /**
     * Handle error caused by the user
     *
     * @param err
     */
    function handleUserError(err) {
        bbActivityIndicator.hide(noop, noop);
        isLoading = false;
        var buttons = [];
        var title = bbI18n.labels.ERROR_ALERT;
        var message;

        switch (err.code) {
            case ErrorCodes.E_OFFLINE:
                buttons = [{type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok'}];
                message = bbI18n.labels.ERROR_OFFLINE;
                break;
            case ErrorCodes.E_USER:
                buttons = [{type: 'POSITIVE', text: bbI18n.labels.BUTTON_OK, callbackFn: 'ok'}];
                message = bbI18n.labels.ERROR_USER_TRANSFER_EXECUTE;
                break;
            case ErrorCodes.E_UNEXPECTED:
                buttons = [{type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel'},
                           {type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: err.callbackFn}];
                message = bbI18n.labels.ERROR_UNEXPECTED_TRANSFER_EXECUTE;
                break;
            case ErrorCodes.E_AUTH:
                buttons = [{type: 'NEUTRAL', text: bbI18n.labels.BUTTON_CANCEL, callbackFn: 'cancel'},
                           {type: 'POSITIVE', text: bbI18n.labels.BUTTON_RETRY, callbackFn: err.callbackFn}];
                message = bbI18n.labels.ERROR_AUTH;
                break;
        }

        // Callback for button click with retry ability.
        var alertCallback = function(data) {

            switch (data.callback) {
                case AlertCallbackFn.RETRY_CANCEL:
                    doCancelTransfer();
                    break;
                case AlertCallbackFn.RETRY_SUBMIT:
                    submitTransfer();
                    break;
                default:
                    // do nothing
            }
        };

        bbAlertDialog.show(alertCallback, noop, title, message, JSON.stringify(buttons));
    }
}

module.exports = MainCtrl;
