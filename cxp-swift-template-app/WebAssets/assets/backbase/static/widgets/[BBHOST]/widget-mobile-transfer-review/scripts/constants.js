'use strict';

var WidgetConstants = {
    /**
     * Local and global events, that widget publishes (emits) or relies on
     */
    Event: {
        Global: {
            LOADED: 'cxp.item.loaded',
            CANCEL: 'transfers.transfer.cancel',
            EXECUTE: 'transfers.transfer.execute',
            TRANSFER_COMPLETE: 'transfers.transfer.complete',
            NAV_REVIEW_TRANSFER: 'navigation:transfers.transfer.review',
            NAV_HOME: 'navigation:home',
            REFRESH_ADDRESSBOOK: 'refresh-addressbook',
            OPEN_TRANSACTIONS: 'shc.open.transactions',
            // Errors
            E_OFFLINE: 'offline:request-failed',
            E_AUTH: 'user-auth:request-unauthorised',
            E_UNEXPECTED: 'error:unexpected',
            // Notify
            NOTIFY_SUCCESS: 'notify:success'
        },
        Local: {
            VIEW_DISAPPEAR: 'backbase.lpmobile.viewWillDisappear'
        }
    },
    ErrorCodes: {
        E_OFFLINE: 1,
        E_UNEXPECTED: 2,
        E_AUTH: 3,
        E_USER: 4
    },
    AlertCallbackFn: {
        RETRY_CANCEL: 'retryCancel',
        RETRY_SUBMIT: 'retrySubmit'
    },
    Preference: {
        CONTACT_ENDPOINT: 'contactEndpoint',
        PAGE_ID: 'pageId'
    },
    SERVER_ROOT: window.b$ && window.b$.portal && window.b$.portal.config && window.b$.portal.config.serverRoot || '/portalserver',
};

module.exports = WidgetConstants;
