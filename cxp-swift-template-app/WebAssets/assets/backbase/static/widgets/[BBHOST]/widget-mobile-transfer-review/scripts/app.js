'use strict';

// externals
var ng = require('angular');
var serviceTransfers = require('serviceTransfers');
var serviceAccounts = require('serviceAccounts');
var accountPanel = require('bbmAccountPanel');
var formatAmount = require('formatAmount');
var mobilePlugins = require('mobilePlugins');
var ibanModule = require('ibanModule');

// internals
var Config = require('./config');
var Model = require('./model');
var Bus = require('./bus');
var constants = require('./constants');

// Services
var serviceBeneficiary = require('serviceBeneficiary');

// Controllers
var MainCtrl = require('./controllers/main');

/**
 * @ngInject
 */
function run(widget, bus, $rootScope, serviceTransfers, serviceAccounts,
             SERVER_ROOT, Event, Preference, serviceBeneficiary) {
    // Add a safeApply function.
    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    // Configure services.
    function getEndpoint(endpointPref) {
        return widget.getPreference(endpointPref).replace('$(servicesPath)', SERVER_ROOT);
    }

    serviceTransfers.setConfig({
        'detailsEndpoint': getEndpoint('detailsEndpoint'),
        'cancelEndpoint': getEndpoint('cancelEndpoint'),
        'submitEndpoint': getEndpoint('submitEndpoint')
    });

    serviceAccounts.setConfig({
        'listEndpoint': getEndpoint('accountsListEndpoint')
    });

    serviceBeneficiary.setConfig({
        partyDataManagementEndpoint: getEndpoint(Preference.CONTACT_ENDPOINT)
    });

    bus.publish(Event.Global.LOADED, {
        id: widget.id
    });
}

/**
 * Widget Angular Module
 * @module widget-mobile-transfer-review
 * @link https://docs.angularjs.org/api/ng/function/angular.module
 * @param {object} widget Widget instance
 * @returns {object} AngularJS module
 */
function App(widget) {
    var deps = [
        mobilePlugins.name
    ];

    return ng.module(widget.id, deps)
        .config(Config)
        .constant(constants)
        .factory('bus', Bus)
        .factory('serviceTransfers', serviceTransfers)
        .factory('serviceAccounts', serviceAccounts)
        .factory('serviceBeneficiary', serviceBeneficiary)
        .service('ibanService', ibanModule.service)
        .filter('iban', ibanModule.filter)
        .directive('validIban', ibanModule.validator)
        .factory('model', Model)
        .controller('MainCtrl', MainCtrl)
        .provider('widget', function() {
            this.$get = function() {
                return widget;
            };
        })
        .component('bbMobileAccountPanel', accountPanel)
        .directive('formatAmount', formatAmount)
        .run(run);
}

module.exports = App;
