
'use strict';

/**
 * Widget ViewModel
 * @ngInject
 * @name widgetModel
 * @private
 * @memberof module: widget-mobile-transfer-review
 * @kind class Angular service provider
 */
function WidgetModel(widget, serviceTransfers, serviceAccounts) {
    // state
    var state = {
        transfer: {
            isLoading: false,
            isSubmitting: false,
            details: null
        },
        account: {
            isLoading: false,
            details: null
        }
    };

    /**
     * Public API.
     *
     * @type {Object}
     */
    var api = {
        // getters
        loadTransfer: loadTransfer,
        loadAccount: loadAccount,
        getTransfer: getTransfer,
        getAccount: getAccount,

        // methods
        submitTransfer: submitTransfer,
        cancelTransfer: cancelTransfer,
        reset: reset
    };

    /**
     * Returns transfer object
     *
     * @public
     * @return {Object} Transfer object
     */
    function getTransfer() {
        return state.transfer; // will be observed, and updated on loadTransfer
    }

    /**
     * Returns account object
     *
     * @public
     * @return {Object} Account object
     */
    function getAccount() {
        return state.account; // will be observed
    }

    /**
     * Load the transfer to review
     *
     * @private
     * @return {Object} Transfer object
     */
    function loadTransfer(transferId) {
        state.transfer.isLoading = true;
        return serviceTransfers.get(transferId)
            .then(function(transfer) {
                state.transfer.details = transfer;
                state.transfer.isLoading = false;
                return transfer;
            })
            .catch(function(err) {
                state.transfer.isLoading = false;
                throw err;
            })
    }

    /**
     * Load the account details for the transfer.
     * @private
     * @return {Object} Account object
     */
    function loadAccount(accountId) {
        state.account.isLoading = true;
        return serviceAccounts.get(accountId)
            .then(function(account) {
                state.account.details = account;
                state.account.isLoading = false;
                return account;
            })
            .catch(function(err) {
                state.account.isLoading = false;
                throw err;
            });
    }

    /**
     * Submit the transfer to the backend.
     * @public
     * @return {Promise} response
     */
    function submitTransfer(transferId, authPassword) {
        state.transfer.isSubmitting = true;
        return serviceTransfers.submit(transferId, authPassword)
            .then(function(response) {
                state.transfer.isSubmitting = false;
                return response;
            })
            .catch(function(err) {
                state.transfer.isSubmitting = false;
                throw err;
            });
    }

    /**
     * Cancel the transfer (delete).
     * @public
     * return {Promise} response
     */
    function cancelTransfer(transferId) {
        state.transfer.isCancelling = true;
        return serviceTransfers.cancel(transferId)
            .then(function(response) {
                state.transfer.isCancelling = false;
                return response;
            })
            .catch(function(err) {
                state.transfer.isCancelling = false;
                throw err;
            });
    }

    /**
     * Reset the state.
     * @public
     */
    function reset() {
        state.transfer.details = null;
        state.transfer.isSubmitting = false;
        state.transfer.isLoading = false;

        state.account.details = null;
        state.account.isLoading = false;
    }

    return api;
}

module.exports = WidgetModel;
