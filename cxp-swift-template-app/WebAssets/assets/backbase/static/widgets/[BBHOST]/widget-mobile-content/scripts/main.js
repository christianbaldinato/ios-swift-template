window.shc = window.shc || {};
shc.mobile = shc.mobile || {};
shc.mobile.widget = shc.mobile.widget || {};
shc.mobile.widget.content = {};
(function(angular) {
    'use strict';

    var bus = gadgets.pubsub;
    var ice = window.ice;
    var DraggableICEBehavior = window.DraggableICEBehavior;

    var setBackgroundImage = function(widget) {

        var bgImage = $(widget.body).find('.bg-image');

        // If there is no image, return
        if (bgImage.length === 0) {
            return;
        }

        var bgImageTarget = getBackgroundPosition(widget, widget.getPreference('bgImageContainer')),
            bgSize = widget.getPreference('bgImageSize'),
            bgImageScroll = widget.getPreference('bgImageScroll');

        if (bgImage && bgImageTarget) {
            bgImageTarget.css({
                'background-image': 'url(' + bgImage.attr('src') + ')',
                'background-size': bgSize,
                'background-position': 'center top',
                'background-repeat': 'no-repeat',
                'background-attachment': bgImageScroll
            });
        }
    };

    var getBackgroundPosition = function(widget, bgImagePosition) {
        switch (bgImagePosition) {
            case 'widget':
                var target = $(widget.body).find('.bp-widget');
                if (!target) {
                    target = $(widget.body).closest('.bp-widget');
                }
                return target;
            case 'fullscreen':
                return $("body");
        }
    };

    /**
     * @param {jQuery} $htmlNode root node to start searching from
     * @returns {String} background image src that is suitable for a LP background
     */
    function getLPBackground($htmlNode) {
        return $htmlNode.find('.lp-background-image:first img').prop('src');
    }

    /**
     * @param widget
     */
    var ContentAccordion = function(widget) {
        this.widget = widget;
        this.$widget = $(widget.body);
    };

    ContentAccordion.prototype.init = function() {

        ice.init(this.widget);

        var upClass = 'lp-icon-caret-up';
        var downClass = 'lp-icon-caret-down';
        var $collapsible = $('.lp-faq-content', this.$widget);
        var $arrow = $('.lp-accordion-toggle-class');

        $('.lp-faq-title', this.$widget).click(function() {
            var $target = $(this);
            var isCollapsed = $target.hasClass('collapsed');

            if (isCollapsed) {
                $arrow.removeClass(downClass).addClass(upClass);
                $collapsible.slideDown(300);
            } else {
                $arrow.removeClass(upClass).addClass(downClass);
                $collapsible.slideUp(300);
            }

            $target.toggleClass('collapsed', !isCollapsed);
        });
    };

    /**
     * @param widget
     * @constructor
     */
    var AdvancedContentTemplate = function(widget) {
        this.widget = widget;
        this.$widget = $(widget.body);
    };

    AdvancedContentTemplate.prototype.init = function() {

        var self = this;

        ice.init(self.widget).then(function() {

            self.DraggableICEBehavior = new DraggableICEBehavior(self.widget, {});
            self.DraggableICEBehavior.init(bd.designMode);

            self.setHeight();

            if (self.$widget.is(':visible')) {
                self.setBackground();

                // create an observer instance
                self.$widget[0].addEventListener("DOMNodeInserted", function(e) {
                    self.setBackground();
                }, false);

            }

            self.widget.model.addEventListener('PrefModified', function(evt) {
                if (evt.attrName === 'widgetContentsUpdated') {
                    self.setHeight();
                    self.setBackground();
                }
            });

            function launchpadShowHandler() {
                self.setBackground();
                if ($(self.widget.htmlNode).attr('data-event') === 'launchpad:show') {
                    // clear "deferred" event bus
                    $(self.widget.htmlNode).removeAttr('data-event');
                }
            }

            $(self.widget.htmlNode).on('launchpad:show', function() {
                launchpadShowHandler();
            });

            // expect "deferred" event in case
            // if 'launchpad:show' was fired before 'launchpad:show' listener was added
            if ($(self.widget.htmlNode).attr('data-event') === 'launchpad:show') {
                launchpadShowHandler();
            }

            // LPES-3167
            if (bd.designMode) {
                var node = self.widget;
                var panel;
                while (node.parentNode) {
                    node = node.parentNode;
                    if (node.nodeName === 'TCont') {
                        var tCont = node;
                        break;
                    }
                    panel = node;
                }

                if (tCont) {
                    $(tCont.htmlNode).find('.bp-tContFn-tab').on('click', function(e) {
                        var panelIndex = tCont.childNodes.indexOf(panel);
                        var index = $(e.currentTarget).index();
                        // tCont childNodes are shifted and the fallback node is the first actually:
                        // [1] -> 1
                        // [2] -> 2
                        // [*] -> 0 (its node is inserted first)
                        // so we shift the index as well:
                        index = (index + 1) % tCont.childNodes.length;

                        if (panelIndex === index && getLPBackground($(panel.htmlNode)) === getLPBackground(self.$widget)) {
                            self.setBackground();
                        }
                    });
                }
            }
        });
    };

    AdvancedContentTemplate.prototype.setHeight = function() {
        var height = this.widget.getPreference('height');
        if (height) {
            this.$widget.height(height);
        }
    };

    AdvancedContentTemplate.prototype.setBackground = function() {
        ice.fixEditing(this.widget);
        setBackgroundImage(this.widget);
    };


    /**
     * Factory for two types of content: content template and accordion.
     * @param widget
     */
    var ContentFactory = function(widget) {
        this.widget = widget;
    };

    ContentFactory.prototype.init = function() {

        var isAccordion = this.widget.model.getPreference('templateUrl').indexOf('accordion') > -1,
            widgetWrapper;

        widgetWrapper = isAccordion ? new ContentAccordion(this.widget) : new AdvancedContentTemplate(this.widget);
        widgetWrapper.init();

        return widgetWrapper;
    };

    shc.mobile.widget.content.init = function(widget) {
        bus.publish('cxp.item.loaded', {id: widget.id});
        var widgetWrapper = new ContentFactory(widget);
        widgetWrapper.init();
    };
})(window.angular);