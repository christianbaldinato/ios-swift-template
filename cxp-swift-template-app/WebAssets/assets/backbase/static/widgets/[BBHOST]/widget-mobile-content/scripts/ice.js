(function () {

    var fixEditing = function (widget) {
        var selector = '[content][editable=false]:not(img)',
            restart = function(){
            var name, instance,
                cke = window.CKEDITOR;

            if(cke && cke.instances){
                for(name in cke.instances) {
                    instance = cke.instances[name];
                    if(this === instance.element.$) {
                        instance.destroy();
                        $(this).attr('contenteditable', true);
                        cke.inline(this);
                        return;
                    }
                }
            }
        };

        $(widget.body)
            .off('.lp-ice')
            .on('focus.lp-ice', selector, function(){
                $(widget.body).find(selector).each(restart);
            })
            .find(selector).each(restart);
    };

    var init = function(widget) {

        var dfd = new $.Deferred();

        // Extend widget in design mode
        if (window.be && window.be.ice && window.bd && window.bd.designMode === 'true') {

            // Clone and extend default ice config
            widget.iceConfig = $.extend(true, {}, window.be.ice.config);
            widget.iceConfig.events.push('lp-drag');

            // var isMasterpage = window.be.utils.module('top.window.bd.PageMgmtTree.selectedLink.isMasterPage'),
            var isMasterpage = top && top.window.bd && top.window.bd.PageMgmtTree && top.window.bd.PageMgmtTree.selectedLink && top.window.bd.PageMgmtTree.selectedLink.isMasterPage,
                isManageable = isMasterpage || (
                    widget.model.manageable === 'true' ||
                    widget.model.manageable === '' ||
                    widget.model.manageable === undefined
                );

            if (isManageable && window.be.ice.controller) {
                var templateUrl = String(widget.getPreference('templateUrl')),
                    enableEditing = function() {
                        return window.be.ice.controller.edit(widget, templateUrl)
                            .then(function(dom) {
                                $(widget.body).find('.bp-g-include').html(dom);
                                fixEditing(widget);
                                return dom;
                            });
                    };

                return enableEditing();
            }
        }
        else {
            // Hide broken images on live
            $('img[src=""], img:not([src])', widget.body).addClass('window.be-ice-hide-image');
            dfd.resolve();
        }
        return dfd.promise();
    };


    window.ice = {
        init: init,
        fixEditing: fixEditing
    };
})();

